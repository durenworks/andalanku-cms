<?php

	use SendGrid\Response;

	include "check-admin-session.php";

	$status 		= sanitize_sql_string($_REQUEST["src_status"]);
	$category 		= sanitize_sql_string($_REQUEST["src_category"]);
	$branch 		= sanitize_sql_string($_REQUEST["src_branch"]);
	$searchDate		= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$page 			= sanitize_int($_REQUEST["page"]);

	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";
	} else {
		
		//dicari 30 hari ke belakang
		$startDate 	= date('Y-m-d', strtotime('-30 days'))." 00:00:00";
		$endDate 	= date("Y-m-d")." 23:59:59";
	}

	$query 	= "select COUNT(a.id_complain) as num
				from complains a 
				left join customers b on a.user_id=b.id_customer
				left join agreement_list c on c.customer_id = b.id_customer
				where (message like '%$keyword%' or ticket_number like '%$keyword%' or c.agreement_number like '%$keyword%' or b.customer_name like '%$keyword%') 
				and submitted_date>='$startDate' and submitted_date<='$endDate' ";
	if($status <> '') $query = $query." and a.status='$status' ";
	if($category <> '') $query = $query." and category='$category' ";
	if($branch <> '') $query = $query." and branch='$branch' ";
					  
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;

	$query 	= "select a.*, b.customer_name, b.andalan_customer_id 
			   from complains a 
			   left join customers b on a.user_id=b.id_customer 
			   left join agreement_list c on c.customer_id = b.id_customer
			   where (message like '%$keyword%' or ticket_number like '%$keyword%' or c.agreement_number like '%$keyword%' or b.customer_name like '%$keyword%') 
			   and submitted_date>='$startDate' and submitted_date<='$endDate' ";
	if($status <> '') $query = $query." and a.status='$status' ";
	if($category <> '') $query = $query." and category='$category' ";
	if($branch <> '') $query = $query." and branch='$branch' ";
	$query = $query." order by submitted_date ASC LIMIT $start,$limit";
	$result = mysqli_query($mysql_connection, $query);  

	echo "<table class='table table-hover'>
			  <tr>
					<th width='5%'>No</th>
					<th width='10%'>Nomor Tiket</th>
					<th width='10%'>Tanggal Komplain</th>
					<th width='10%'>Kategori</th>
					<th width='10%'>Nama Konsumen</th>
					<th width='10%'>Nomor Kontrak</th>
					<th width='10%'>Cabang</th>
					<th>Pesan</th>
					<th width='10%'>Status</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		$message = $data['message'];
		$message = substr($message, 0, 200);
		if(strlen($data['message']) > 200) $message = $message." ...";
		
		$status = $data['status'];
		
		if($status == 'SUBMITTED') $status = "<b><font color='red'>".$status."</font></b>";
		else if($status == 'ON PROCESS') $status = "<b><font color='blue'>".$status."</font></b>";
		else if($status == 'SOLVED') $status = "<b><font color='green'>".$status."</font></b>";
		
		//cek apakah ada dokumen yang diupload
		$queryDocs	= 'select * from complain_media where id_complain='.$data[id_complain];
		$resultDocs	= mysqli_query($mysql_connection, $queryDocs);
		if(mysqli_num_rows($resultDocs) > 0) {
			$downloadLink = '<br><a href="download_document?type=complain&id='.$data[id_complain].'&ticket='.$data[ticket_number].'" target="_blank"><i class="fa fa-paperclip"></i> Media</a>';
		}
		else {
			$downloadLink = '';
		}

		//----- Ambil Nomor Kontrak ------//
		$agreement_number = '';
		$branch_stack = '';
		$query_agrrement = "select agreement_number, branch from agreement_list where customer_id = '$data[user_id]'";
		$result_agreement = mysqli_query($mysql_connection, $query_agrrement);
		
		//$agreement_number = mysqli_fetch_assoc($result_agreement);
		while ($data_agreement = mysqli_fetch_assoc($result_agreement)) {
			
			if ($data_agreement[branch] == '') {
				
				$api_url	= $afis_api_url.'/Agreement/ActiveAgrementNumber/'.$data[andalan_customer_id];
				$ch 		= curl_init();

				curl_setopt($ch, CURLOPT_URL, $api_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

				$result_afis = curl_exec($ch);
				$result_afis = json_decode($result_afis,true);

				$responseArray = $result_afis['Response']['Data'];

				foreach($responseArray as $key=>$value) {
					
					$agreementNo = trim($value['agreementNo']);

					if ($data_agreement[agreement_number] == $agreementNo) {
						$branch = trim($value['branchfullname']);
						
						$query = "update agreement_list set branch='$branch ' where agreement_number='$data_agreement[agreement_number]'";
						mysqli_query($mysql_connection, $query);
					}
				}
			}
			else {
				$branch = $data_agreement[branch];
			}

			if($agreement_number != '') {
				$agreement_number = $agreement_number . ', ' . $data_agreement[agreement_number];
				$branch_stack = $branch_stack . ', ' . $branch;
			}
			else {
				$agreement_number = $data_agreement[agreement_number];
				$branch_stack = $branch;
			}
		}
		
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data[ticket_number] . $downloadLink . '</td>
				  <td>' . date("d-m-Y H:i:s", strtotime($data[submitted_date])) . '</td>
				  <td>' . $data[category] . '</td>
				  <td>' . $data[customer_name] . '</td>
				  <td>' . $agreement_number . '</td>
				  <td>' . $branch_stack . '</td>
				  <td>' . $message . '</td>
				  <td>' . $status . '<br>
					<a href="#modalMedia" onclick="viewMedia('.$data[id_complain].')" class="btn btn-sm btn-primary"><i class="fa fa-image"></i> View Media</a>					
					&nbsp;&nbsp;
					<a href="#modal" onclick="viewDetail('.$data[id_complain].')" class="btn btn-sm btn-warning"><i class="fa fa-file-text-o"></i> View Detail</a>
				  </td>
				</tr>';
		$i++;
	}

	echo "</table>";

	include "inc-paging.php";
?>
