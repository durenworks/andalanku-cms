<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>

<!-- daterange picker -->
<link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<link href="plugins/datepicker/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
<link href="plugins/timepicker/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Pengajuan Via Website</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-globe"></i> Pengajuan Via Website</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-6">
						<div class="input-group">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input class="form-control pull-right active" name="searchDate" id="searchDate" type="text" placeholder="Tanggal">
						</div>
					</div>
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
						<button type="button" class="btn btn-flat btn-success" onClick="saveToExcel();"><i class="fa fa-file-excel-o"></i> Export To Excel</button>
					</div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->
	
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Detail Subscriber</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-6">
				<div class="form-group">
					<label for="name">Nama</label>
					<input type="text" name="name" readonly="readonly" class="form-control" id="name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" name="email" readonly="readonly" class="form-control" id="email">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="phone">Telepon</label>
					<input type="text" name="phone" readonly="readonly" class="form-control" id="phone">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="created_at">Tanggal Pengajuan</label>
					<input type="text" name="created_at" readonly="readonly" class="form-control" id="created_at">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="simulation_type">Produk</label>
					<input type="text" name="simulation_type" readonly="readonly" class="form-control" id="simulation_type">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="plafond">Plafond</label>
					<input type="text" name="plafond" readonly="readonly" class="form-control" id="plafond">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="otr">Harga OTR</label>
					<input type="text" name="otr" readonly="readonly" class="form-control" id="otr">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="dp_percent"><span id="spanDpPercent">Jumlah DP (%)</span></label>
					<input type="text" name="dp_percent" readonly="readonly" class="form-control" id="dp_percent">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="dp_amount"><span id="spanDpAmount">Jumlah DP (Rp)</span></label>
					<input type="text" name="dp_amount" readonly="readonly" class="form-control" id="dp_amount">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="tenor">Tenor</label>
					<input type="text" name="tenor" readonly="readonly" class="form-control" id="tenor">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="insurance">Asuransi</label>
					<input type="text" name="insurance" readonly="readonly" class="form-control" id="insurance">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="region">Wilayah</label>
					<input type="text" name="region" readonly="readonly" class="form-control" id="region">
				</div>
			</div>
			
			<br>
			<div class="col-md-12">
				<div class="form-group">
					<button data-remodal-action="cancel" class="remodal-cancel">Tutup</button>
				</div>
			</div>
			
			</p>
		</div>	
	</div>



	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>
	
	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>
	
	<!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
	<script src="plugins/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.js" type="text/javascript"></script>

	<script>

		$body = $("body");
		
		$('#searchDate').daterangepicker({"autoApply" : true});

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'searchDate': $('input[name=searchDate]').val(),
								'keyword'	: $('input[name=keyword]').val(),
								'page'		: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_subscriber_web.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );
		
		function viewDetail(subscriberID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_subscriber_detail.php',
                data: 'subscriberID=' + subscriberID,
                dataType: 'json'
            }).done(function(data) { console.log(data);

				$("#name").val(data.name);
				$("#email").val(data.email);
				$("#phone").val(data.phone);
				$("#created_at").val(data.created_at);
				$("#simulation_type").val(data.product);
				$("#plafond").val(data.plafond);
				$("#otr").val(data.otr);
				$("#dp_percent").val(data.dp_percent);
				$("#dp_amount").val(data.dp_amount);
				$("#tenor").val(data.tenor);
				$("#insurance").val(data.insurance);
				$("#region").val(data.region);
				
            	$body.removeClass("loadingClass");
            });
        }
		
		function saveToExcel() {
			
			var keyword = $('input[name=keyword]').val();
			window.open("excel_subscriber.php?keyword="+keyword+"&type=w");
		}
		
		function deleteSubscriber(subscriberID,subscriberName){

			if (confirm('Apakah Anda yakin akan menghapus subscriber ' + subscriberName +' ?')) {

				$body.addClass("loadingClass");

				var formData = { 'subscriberID'	: subscriberID };

				$.ajax({
					type: "POST",
					url: 'ajax_delete_subscriber.php',
					data: formData
				}).done(function(data) {
					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data Sudah Dihapus"));
						searchData(1);
						setTimeout(clearnotif, 5000);
					}
				});
			}
		}
		
	</script>

  </body>
</html>
