<?php
	include "check-admin-session.php";

	$keyword			= sanitize_sql_string($_REQUEST["keyword"]);
	$page 				= sanitize_int($_REQUEST["page"]);
	$branch 			= sanitize_sql_string($_REQUEST["src_branch"]);
	
	$query 			= "SELECT distinct count(distinct t1.sender_id) as num
						FROM   inbox t1 
						LEFT JOIN customers c on c.id_customer = t1.sender_id
						INNER JOIN 
						(
							SELECT Max(date) date, sender_id
							FROM   inbox 
							GROUP BY sender_id 
						) AS t2 
							ON t1.sender_id = t2.sender_id 
							AND t1.date = t2.date 
							LEFT JOIN agreement_list t3 on t1.sender_id = t3.customer_id
						WHERE t1.sender_id <> -2
						AND t1.sender_id <> -3
						AND c.is_active = 1
						AND t1.content like '%$keyword%'";

	if($branch <> '') $query = $query." and t3.branch='$branch' ";
	
	$result 		= mysqli_query($mysql_connection, $query);

	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query 			= "SELECT distinct t1.sender_id, t1.date,
						DATE_FORMAT(t1.date, '%a, %e %b') dateonly, 
						DATE_FORMAT(t1.date,'%k:%i') timeonly, 
						t1.content, t1.status,  c.customer_name, c.andalan_customer_id
						FROM   inbox t1 
						LEFT JOIN customers c on c.id_customer = t1.sender_id
						INNER JOIN 
						(
							SELECT Max(date) date, sender_id
							FROM   inbox 
							GROUP BY sender_id 
						) AS t2 
							ON t1.sender_id = t2.sender_id 
							AND t1.date = t2.date 
							LEFT JOIN agreement_list t3 on t1.sender_id = t3.customer_id
						WHERE t1.sender_id <> -2
						AND t1.sender_id <> -3
						AND c.is_active = 1
						AND t1.content like '%$keyword%'";
	if($branch <> '') $query = $query." and t3.branch='$branch' ";
	
	$query = $query . "ORDER BY t1.date DESC";
	$query = $query." LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		$content = json_decode($data['content']);
		$message = $content->message;
		if (strlen($message) > 200 ) {
			$message = substr(trim($message), 0, strpos(trim($message), ' ', 200));
		}
		else {
			$message = trim($message);	
		}
		
		$sender = $data['customer_name'];
		if ($data['andalan_customer_id'] <> '') {
			$sender = $sender . ' - ' . $data['andalan_customer_id'];
		}

		$secondDate = date('D, j M');
		
		if ($data['dateonly'] != $secondDate) {
			$time = $data['dateonly'];
		}
		else {
			$time = $data['timeonly'];
		}

		
		if ($data['status'] == 0) {
			$message = '<b>'. $message .'</b>';
			//$status = '<i class="fa fa-envelope"></i>';
			$status = '<img src="img/inbox_close_2.png" alt="Unread" height="20">';
		}
		else {
			//$status = '<i class="fa fa-envelope-open"></img>';
			$status = '<img src="img/inbox_open_2.png" alt="Read" height="20">';
		}

		//----- Ambil Nomor Kontrak ------//
		$agreement_number = '';
		$branch_stack = '';
		$query_agrrement = "select agreement_number, branch from agreement_list where customer_id = '$data[sender_id]'";
		$result_agreement = mysqli_query($mysql_connection, $query_agrrement);
		
		//$agreement_number = mysqli_fetch_assoc($result_agreement);
		while ($data_agreement = mysqli_fetch_assoc($result_agreement)) {
			if ($data_agreement[branch] == '') {
				
				$api_url  	   = $afis_api_url.'/Agreement/ActiveAgrementNumber/'.$data['andalan_customer_id'];
				$ch = curl_init();

				curl_setopt($ch,CURLOPT_URL, $api_url);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

				$result_afis = curl_exec($ch);
				$result_afis = json_decode($result_afis,true);

				$responseArray = $result_afis['Response']['Data'];

				foreach($responseArray as $key=>$value) {
					
					$agreementNo = trim($value['agreementNo']);

					if ($data_agreement[agreement_number] == $agreementNo) {
						$branch = trim($value['branchfullname']);
						
						$queryBranchUpdate = "update agreement_list set branch='$branch ' where agreement_number='$data_agreement[agreement_number]'";
						mysqli_query($mysql_connection, $queryBranchUpdate);
					}
				}
			}
			else {
				$branch = $data_agreement['branch'];
			}

			if($agreement_number != '') {
				$agreement_number = $agreement_number . ', ' . $data_agreement['agreement_number'];
				$branch_stack = $branch_stack . ', ' . $branch;
			}
			else {
				$agreement_number = $data_agreement['agreement_number'];
				$branch_stack = $branch;
			}
		}

		if ($branch_stack != '') {
			$sender = $sender . ' (' . $branch_stack . ')';
		}
		echo '<tr>';
				  
		echo '		  
				  <td width="3%">'. $status .'</td>
				  <td width = "40%">
				  <a href="direct_message_detail?id='. @$data['sender_id'] .'" title="' .  @$sender . '">' .  @$sender . '</a>
				  </td>
				  <td width="44%"></td>
				  <td width="3%"></td>
				  <td width="10%">' . @$time . '</td>
			  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>