<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";
	include "common_vars.php";

	$fullname = $_SESSION['fullname'];
?>

<!-- daterange picker -->
<link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<link href="plugins/datepicker/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
<link href="plugins/timepicker/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Komplain</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-exclamation-triangle"></i> Komplain</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row form-group">
					<div class="col-md-3">
						<select name="src_status" class="form-control" id="src_status">
							<option value=''>Semua Status</option>
							<option value='SUBMITTED'>SUBMITTED</option>
							<option value='ON PROCESS'>ON PROCESS</option>
							<option value='SOLVED'>SOLVED</option>
						</select>
					</div>
					<div class="col-md-3">
						<select name="src_category" class="form-control" id="src_category">
							<option value=''>Semua Kategori</option>
							<?php 
								foreach($const_complain_category as $key=>$value) {
									
									echo '<option value="'.$value.'">'.$value.'</option>';
								}
							?>
						</select>
					</div>
					<div class="col-md-6">
						<div class="input-group">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input class="form-control pull-right active" name="searchDate" id="searchDate" type="text" placeholder="Tanggal">
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-3">
					<select name="src_branch" class="form-control" id="src_branch">
							<option value=''>Semua Cabang</option>
							<?php 
								$query = "select * from branches";
								$result = mysqli_query($mysql_connection, $query);
								while ($data = mysqli_fetch_array($result)) {									
									echo '<option value="'.$data['name'].'">'.$data['name'].'</option>';
								}
							?>
						</select>
					</div>
					<div class="col-md-6">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Cari</button>
						<button type="button" class="btn btn-flat btn-success" onClick="saveToExcel();"><i class="fa fa-file-excel-o"></i> Export To Excel</button>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->

	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Detail Komplain</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-4">
				<div class="form-group">
					<label for="ticket_number">Nomor Tiket</label>
					<input type="hidden" id="id_complain" name="id_complain" value="0">
					<input type="text" name="ticket_number" readonly="readonly" class="form-control" id="ticket_number">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="submitted_date">Tanggal Komplain</label>
					<input type="text" name="submitted_date" readonly="readonly" class="form-control" id="submitted_date">
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label for="category">Kategori</label>
					<input type="text" name="category" readonly="readonly" class="form-control" id="category">
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label for="customer_name">Nama</label>
					<input type="text" name="customer_name" readonly="readonly" class="form-control" id="customer_name">
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<label for="phone">Telepon</label>
					<input type="text" name="phone" readonly="readonly" class="form-control" id="phone">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="status">Status</label>
					<input type="text" name="status" readonly="readonly" class="form-control" id="status">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="message">Pesan</label>
					<textarea name="message" readonly="readonly" class="form-control" id="message"></textarea>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="on_process_date">Tanggal Proses</label>
					<input type="text" name="on_process_date" readonly="readonly" class="form-control" id="on_process_date">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="processed_by_name">Diproses Oleh</label>
					<input type="text" name="processed_by_name" readonly="readonly" class="form-control" id="processed_by_name">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="solved_date">Tanggal Selesai Proses</label>
					<input type="text" name="solved_date" readonly="readonly" class="form-control" id="solved_date">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="solved_by_name">Selesai Proses Oleh</label>
					<input type="text" name="solved_by_name" readonly="readonly" class="form-control" id="solved_by_name">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="solution">Solusi</label>
					<textarea name="solution" readonly="readonly" placeholder="Solusi" class="form-control" id="solution"></textarea>
				</div>
			</div>
			
			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Tutup</button>
		<button data-remodal-action="confirm" class="remodal-confirm">Selesai Proses</button>
	</div>

	<div class="remodal" data-remodal-id="modalMedia" role="dialog" aria-labelledby="modalMediaTitle" aria-describedby="modalMediaDesc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modalMediaTitle">Media Komplain</h4>
			<p id="modalMediaDesc">
				<span id="spanMedia"></span>			
			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Tutup</button>
	</div>

	
    <?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>
	
	<!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
	<script src="plugins/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.js" type="text/javascript"></script>

	<script>

		$body = $("body");
		
		$('#searchDate').daterangepicker({"autoApply" : true});
		
		var currentSearchPage = 1;

		function searchData(searchPage) {

			$body.addClass("loadingClass");
			
			if(searchPage=='0') searchPage = currentSearchPage;

            var formData = {
								'src_status'	: $('select[name=src_status]').val(),
								'src_category'	: $('select[name=src_category]').val(),
								'searchDate'	: $('input[name=searchDate]').val(),
								'src_branch'	: $('select[name=src_branch]').val(),
								'keyword'		: $('input[name=keyword]').val(),
								'page'			: searchPage
						   };

			
            $.ajax({
                type: "POST",
                url: 'ajax_search_complain.php',
                data: formData
            }).done(function(data) { 
                $('#dataSpan').html(data);
				currentSearchPage = searchPage;
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );
		
		function viewDetail(complainID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_complain_detail.php',
                data: 'complainID=' + complainID,
                dataType: 'json'
            }).done(function(data) { 

				$("#id_complain").val(data.id_complain);
				$("#ticket_number").val(data.ticket_number);
				$("#submitted_date").val(data.submitted_date);
				$("#category").val(data.category);
				$("#customer_name").val(data.customer_name);
				$("#phone").val(data.phone);
				$("#status").val(data.status);
				$("#message").val(data.message);
				$("#on_process_date").val(data.on_process_date);
				$("#processed_by_name").val(data.processed_by_name);
				$("#solved_date").val(data.solved_date);
				$("#solved_by_name").val(data.solved_by_name);
				$("#solution").val(data.solution);
				
				if(data.status == 'SOLVED') $('textarea[name="solution"]').attr('readonly', true);
				else $('textarea[name="solution"]').attr('readonly', false);
				
            	$body.removeClass("loadingClass");
            });
        }
		
		function viewMedia(complainID) {
			
			$body.addClass("loadingClass");

            var formData = { 'complainID' : complainID };

            $.ajax({
                type: "POST",
                url: 'ajax_search_complain_media.php',
                data: formData
            }).done(function(data) {
                $('#spanMedia').html(data);
                $body.removeClass("loadingClass");
            });
		}
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
		
		$(document).on('confirmation', '.remodal', function() {
			
			if($('input[name=status]').val() == 'SOLVED') {
				
				alert('Proses gagal. Status komplain sudah solved');
				return false;
			}
			

            if(confirm("Update status komplain menjadi solved?")) {
				
				var formData = {
					'id_complain'		: $('#id_complain').val(),
					'solution'			: $('textarea[name=solution]').val()
				};
				
				$body.addClass("loadingClass");
				
				$.ajax({
					type: "POST",
					url: 'ajax_edit_complain.php',
					data: formData
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data komplain telah disimpan"));
						modalAdd.close();
						searchData(0);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					} else if (data == 'already_solved') {
						$('#addNotification').html(showNotification("error", "Proses gagal. Status komplain sudah solved"));
					} 
				});
			}
        });
		
		function resetdata() {
			
			$("#id_complain").val('0');
			$("#ticket_number").val('');
			$("#submitted_date").val('');
			$("#category").val('');
			$("#customer_name").val('');
			$("#phone").val('');
			$("#status").val('');
			$("#message").val('');
			$("#on_process_date").val('');
			$("#processed_by_name").val('');
			$("#solved_date").val('');
			$("#solved_by_name").val('');
			$("#message").val('');
			$("#addNotification").html('');
			$('textarea[name="solution"]').attr('readonly', false);
		}

		function saveToExcel() {
			
			var src_status 	= $('select[name=src_status]').val();
			var src_category = $('select[name=src_category]').val();
			var searchDate	= $('input[name=searchDate]').val();
			var keyword		= $('input[name=keyword]').val();
		
			//alert($('select[name=src_status]').val());
			window.open("excel_complain.php?src_status="+$('select[name=src_status]').val()+"&searchCategory="+$('select[name=src_category]').val()+"&searchDate="+$('input[name=searchDate]').val()+"&keyword="+$('input[name=keyword]').val());
		}
	</script>

  </body>
</html>
