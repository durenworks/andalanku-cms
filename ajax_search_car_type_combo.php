<?php
	include "check-admin-session.php";

	$src_car_brand	= sanitize_sql_string($_REQUEST["src_car_brand"]);
	$withAll		= sanitize_sql_string($_REQUEST["withAll"]);

	$query = "select * 
			  from car_types 
			  where id_car_brand='$src_car_brand'
			  order by name ASC "; 
	$result = mysqli_query($mysql_connection, $query);
	
	if($withAll == 'Y') $selectName = 'src_car_type';
	else $selectName = 'id_car_type';

	echo '<select name="'.$selectName.'" class="form-control" id="'.$selectName.'">'; 
	
	if($withAll == 'Y') echo '<option value="">Semua Tipe Mobil</option>';

	while ($data = mysqli_fetch_array($result)) {
		echo '<option value="'.$data['id_car_type'].'">'.$data['name'].'</option>';
	}

	echo "</select>";
?>