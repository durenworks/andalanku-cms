<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";
	include "afis_call.php";

	$fullname = $_SESSION['fullname'];
	
	//cari pengajuan kredit yang belum sukses di AFIS, lalu kirim ulang ke AFIS
	$queryAfis	= " select id, json_data from loan_applications where afis_success='0' and json_data!='' ";
	$resultAfis	= mysqli_query($mysql_connection, $queryAfis);
	
	$afis_api_url = $afis_api_url.'/andalanku/PengajuanKredit';
	
	while($dataAfis = mysqli_fetch_array($resultAfis)) {
		
		$loanID		= $dataAfis['id'];
		$json_data	= $dataAfis['json_data'];
		
		$afis_response	= json_decode(afis_post_json($afis_api_url, $json_data));
		
		if ($afis_response->Meta->StatusCode == 200) {
			
			$queryUpdateApplication = "UPDATE loan_applications SET afis_success=1 WHERE id = '$loanID'";
			mysqli_query($mysql_connection, $queryUpdateApplication);
		}
	}
?>

<!-- daterange picker -->
<link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<link href="plugins/datepicker/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
<link href="plugins/timepicker/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Konsumen</li>
			  <li>Pengajuan Kredit</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-shopping-bag"></i> Pengajuan Kredit</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row form-group">
					<div class="col-md-6">
						<div class="input-group">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input class="form-control pull-right active" name="searchDate" id="searchDate" type="text" placeholder="Tanggal">
						</div>
					</div>
					<div class="col-md-3">
						<select name="src_product" class="form-control" id="src_product">
							<option value=''>Semua Produk</option>
							<option value='NEW CAR'>Mobil Baru</option>
							<option value='USED CAR'>Mobil Bekas</option>
							<option value='DANA ANDALANKU'>Dana Andalanku</option>
						</select>
					</div>
					<div class="col-md-3">
						<select name="src_status" class="form-control" id="src_status">
							<option value=''>Semua Status</option>
							<option value='Input Aplikasi'>Input Aplikasi</option>
							<option value='Analisa Kredit'>Analisa Kredit</option>
							<option value='Approval Kredit'>Approval Kredit</option>
							<option value='Approved'>Approved</option>
							<option value='Reject'>Reject</option>
							<option value='Canceled'>Canceled</option>
						</select>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-6">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword (Nama / Nomor Telepon / Nomor Tiket)" />
					</div>
					<div class="col-md-3">
						<select name="src_pengajuan" class="form-control" id="src_pengajuan">
							<option value=''>Pengajuan Oleh</option>
							<option value='Agent'>Agent</option>
							<option value='Pribadi'>Pribadi</option>
						</select>
					</div>
					<div class="col-md-3">
						<select name="src_customer_type" class="form-control" id="src_customer_type">
							<option value=''>Jenis Konsumen</option>
							<option value='EMPLOYEE'>Karyawan</option>
							<option value='NON EMPLOYEE'>Non Karyawan</option>
						</select>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<div class="row form-group">
					<div class="col-md-3">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Cari</button>
						<button type="button" class="btn btn-flat btn-success" onClick="saveToExcel();"><i class="fa fa-file-excel-o"></i> Export To Excel</button>
					</div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->
	
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Detail Pengajuan Kredit</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-6">
				<div class="form-group">
					<label for="ticket_number">Nomor Tiket</label>
					<input type="hidden" id="id_loan_application" name="id_loan_application" value="0">
					<input type="text" name="ticket_number" readonly="readonly" class="form-control" id="ticket_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="status">Status Pengajuan</label>
					<input type="text" name="status" readonly="readonly" class="form-control" id="status">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="date">Tanggal Pengajuan</label>
					<input type="text" name="date" readonly="readonly" class="form-control" id="date">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="customer_name">Nama</label>
					<input type="text" name="customer_name" readonly="readonly" class="form-control" id="customer_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="referal_name">Nama Referal</label>
					<input type="text" name="referal_name" readonly="readonly" class="form-control" id="referal_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="phone_number">Telepon</label>
					<input type="text" name="phone_number" readonly="readonly" class="form-control" id="phone_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="product">Produk</label>
					<input type="text" name="product" readonly="readonly" class="form-control" id="product">
				</div>
			</div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="asset">Mobil</label>
                    <input type="text" name="asset" readonly="readonly" class="form-control" id="asset">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="assetYear">Tahun</label>
                    <input type="text" name="assetYear" readonly="readonly" class="form-control" id="assetYear">
                </div>
            </div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="otr">Harga OTR</label>
					<input type="text" name="otr" readonly="readonly" class="form-control" id="otr">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="dp_percent"><span id="spanDpPercent">Jumlah DP (%)</span></label>
					<input type="text" name="dp_percent" readonly="readonly" class="form-control" id="dp_percent">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="dp_amount"><span id="spanDpAmount">Jumlah DP (Rp)</span></label>
					<input type="text" name="dp_amount" readonly="readonly" class="form-control" id="dp_amount">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="tenor">Tenor</label>
					<input type="text" name="tenor" readonly="readonly" class="form-control" id="tenor">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="insurance">Asuransi</label>
					<input type="text" name="insurance" readonly="readonly" class="form-control" id="insurance">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="region">Wilayah</label>
					<input type="text" name="region" readonly="readonly" class="form-control" id="region">
				</div>
			</div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="branch">Cabang</label>
                    <input type="text" name="branch" readonly="readonly" class="form-control" id="branch">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="surveyDate">Waktu Survey</label>
                    <input type="text" name="surveyDate" readonly="readonly" class="form-control" id="surveyDate">
                </div>
            </div>
			
			<div class="col-md-12">
				<div class="form-group">
					<span id="spanBtnProcess"></span>
				</div>
			</div>
			
			<br>
			<div class="col-md-12">
				<div class="form-group">
					<button data-remodal-action="cancel" class="remodal-cancel">Tutup</button>
				</div>
			</div>
			
			</p>
		</div>	
	</div>


    <?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>

	<!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
	<script src="plugins/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.js" type="text/javascript"></script>
	
	<script>

		$body = $("body");
		
		$('#searchDate').daterangepicker({"autoApply" : true});

		function searchData(searchPage) {

			$body.addClass("loadingClass");

            var formData = {
								'src_product'		: $('select[name=src_product]').val(),
								'src_status'		: $('select[name=src_status]').val(),
								'searchDate'		: $('input[name=searchDate]').val(),
								'keyword'			: $('input[name=keyword]').val(),
								'src_pengajuan'		: $('select[name=src_pengajuan]').val(),
								'src_customer_type'	: $('select[name=src_customer_type]').val(),
								'page'				: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_loan_application.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }
		
		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );
		
		function viewDetail(loanApplicationID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_loan_application_detail.php',
                data: 'loanApplicationID=' + loanApplicationID,
                dataType: 'json'
            }).done(function(data) { console.log(data);

				$("#id_loan_application").val(data.id);
				$("#ticket_number").val(data.ticket_number);
				$("#status").val(data.status);
				$("#date").val(data.date);
				$("#customer_name").val(data.customer_name);
				$("#referal_name").val(data.referal_name);
				$("#phone_number").val(data.phone_number);
				$("#product").val(data.product);
				$("#otr").val(data.otr);
				$("#plafond").val(data.plafond);
				$("#tenor").val(data.tenor);
				$("#insurance").val(data.insurance);
				$("#region").val(data.region);
                $("#branch").val(data.branch_name);
                $("#surveyDate").val(data.survey_date);
                $("#asset").val(data.collateral);
                $("#assetYear").val(data.collateral_year);
				
				if(data.product == 'DANA ANDALANKU') {
					
					$("#spanDpPercent").html('Angsuran');
					$("#spanDpAmount").html('Dana Diterima');
					
					$("#dp_percent").val(data.installment);
					$("#dp_amount").val(data.received_funds);
				}
				else {
					
					$("#spanDpPercent").html('Jumlah DP (%)');
					$("#spanDpAmount").html('Jumlah DP (Rp)');
					
					$("#dp_percent").val(data.dp_percent);
					$("#dp_amount").val(data.dp_amount);
				}
				
            	$body.removeClass("loadingClass");
            });
        }
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
		
		function deleteLoanApplication(loanApplicationID, ticketNumber){

        	if (confirm('Anda yakin akan menghapus pengajuan kredit ' + ticketNumber + '?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_delete_loan_application.php',
                    data: 'loanApplicationID=' + loanApplicationID  
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Data pengajuan kredit telah dihapus"));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	} 
                });
            }
        }
		
		function saveToExcel() {
			
			var src_product 	= $('select[name=src_product]').val();
			var src_status 		= $('select[name=src_status]').val();
			var searchDate 		= $('input[name=searchDate]').val();
			var keyword 		= $('input[name=keyword]').val();
			var src_pengajuan 	= $('select[name=src_pengajuan]').val();
			window.open("excel_loan_application.php?src_product="+src_product+"&src_status="+src_status+"&searchDate="+searchDate+"&keyword="+keyword+"&src_pengajuan="+src_pengajuan);
		}
	</script>

  </body>
</html>
