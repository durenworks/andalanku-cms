<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";
?>

<link href="plugins/summernote/summernote.css" rel="stylesheet">

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Display</li>
              <li><a href="news">Berita</a></li>
              <li>Tambah Berita</a></li>
            </ol>
          </section>
		  <br>
          <!-- Main content -->
          <section class="content">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Tambah Berita</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>


				<div class="row form-group">
					<div class="col-md-12">
						<label>Judul Berita</label>
						<input type="text" name="title" id="title" class="form-control" placeholder="Judul Berita"/>
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-12">
						<label>Isi Berita</label>
						<textarea name="content" id="content" rows="8" class="form-control" placeholder="Isi Berita"></textarea>
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-12">
						<label>Gambar Berita</label>
						<table width="100%" class='table'>
							<tr>
								<td width="150px" align="center">
									<img id="imageView1" src="img/none.png" style="width: 90px; " alt="Gambar Berita"/>
								</td>
								<td>
									<input type='file' name="imageInput1" id="imageInput1" accept='image/*'/><br>
									<label style="font-size:12px; color:red;">Ukuran gambar yang direkomendasikan : 1080 x 607 px</label>
								</td>
							</tr>
						</table>
				   </div>
				</div>

				<div class="row form-group">
					<div class="col-md-12 text-center">
						<button type="button" style="width:100px" class="btn btn-primary btn-large" onclick="save();">Simpan</button>
						<button type="button" style="width:100px" class="btn btn-warning btn-large" onclick="cancel();">Batal</button>
					</div>
				</div>

				<br><br><br>

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script src="plugins/summernote/summernote.js"></script>
	
    <script src="js/inc-function.js"></script>

    <script type="text/javascript">

    	$body = $("body");
		
		$(document).ready(function() {
		  $('#content').summernote({
			height: 300, 
			placeholder: 'Isi Berita',
			toolbar: [
					// [groupName, [list of button]]
					['style', ['bold', 'italic', 'underline', 'clear']],
					['font', ['strikethrough', 'superscript', 'subscript']],
					['fontsize', ['fontsize']],
					['color', ['color']],
					['para', ['ul', 'ol', 'paragraph']],
					['height', ['height']],
					['insert', ['picture', 'video']],
					['table', ['table']],
  					['view', ['fullscreen', 'codeview', 'help']],
				]
		  });
		});

    	function save() {

    		$body.addClass("loadingClass");

    		var imageInput1 = $('#imageInput1').prop('files')[0];

    		var form_data = new FormData();
          	form_data.append('title', $('input[name=title]').val());
			form_data.append('content', $('textarea[name=content]').val());
			form_data.append('imageInput1', imageInput1);
			form_data.append('_ref', '<?php echo md5('news#'.date('d-m-Y')); ?>');

			$.ajax({
				type: "POST",
                url: 'ajax_add_news.php',
                processData: false,
                contentType: false,
                data: form_data
			}).done(function(data) {

				data = $.trim(data); console.log(data);
				$body.removeClass("loadingClass");

				if (data == 'success') {
					$("#mainNotification").html(showNotification("success", "Data Sudah Disimpan"));
					resetdata();
					setTimeout(clearnotif, 5000);
					window.location = 'news';

				} else if (data == 'empty') {
					$('#mainNotification').html(showNotification("error", "Input Tidak Lengkap"));
				} else if (data == 'incorrect_file_type') {
					$('#mainNotification').html(showNotification("error", "File gambar artikel harus berupa file jpg atau png atau gif"));
				}

				$('html, body').animate({scrollTop: '0px'}, 0);
			});

    	}

    	function cancel() {
    		window.location = 'news';
    	}

    	function resetdata() {

    		$('input[name=title]').val("");
			$('#content').summernote('reset');
			$('#imageInput1').val(""); $("#imageView1").attr('src', '');
    	}

    	function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

    	function readURL(input, view) {

		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $(view).attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imageInput1").change(function(){
		    readURL(this, "#imageView1");
		});


    </script>

  </body>
</html>
