<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Display</li>
			  <li>Berita</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-newspaper-o"></i> Berita</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Cari</button>
						<a href="add_news">
							<button type="button" class="btn btn-primary  btn-flat " ><i class="fa fa-plus"></i> Tambah Berita</button>
						</a>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->

    <?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

    <script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

            var formData = {
								'keyword'	: $('input[name=keyword]').val(),
								'page'		: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_news.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		function deleteNews(newsID,newsImg,title){

			if (confirm('Apakah Anda yakin akan menghapus berita ' + title +' ?')) {

				$body.addClass("loadingClass");

				var formData = {
						'newsID'	: newsID,
						'newsImg'	: newsImg
					 };

				$.ajax({
					type: "POST",
					url: 'ajax_delete_news.php',
					data: formData
				}).done(function(data) {
					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data Sudah Dihapus"));
						searchData(1);
						setTimeout(clearnotif, 5000);
					}
				});
			}
		}

		searchData(1);
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
	</script>

  </body>
</html>
