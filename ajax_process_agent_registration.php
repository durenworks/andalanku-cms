<?php
	include "check-admin-session.php";
	include "afis_call.php";
	include "agent_pks_pdf.php";

	$userID				= $_SESSION['userID'];
	$id_register_agent	= sanitize_int($_REQUEST["id_register_agent"]);
	$status				= sanitize_sql_string($_REQUEST["status"]);

	if ($id_register_agent <> '0' && $status <> '') {
	
		$now = date("Y-m-d H:i:s");	
		
		$queryRequest 	= "select * from register_agent_history where id_register_agent='$id_register_agent'";
		$resultRequest	= mysqli_query($mysql_connection, $queryRequest);
		$dataRequest 	= mysqli_fetch_array($resultRequest);
		$id_customer	= $dataRequest['id_customer'];
		
		//ambil data customer
		$queryCustomer  = "select * from customers where id_customer='".$id_customer."' ";
		$resultCustomer = mysqli_query($mysql_connection, $queryCustomer);
		$dataCustomer 	= mysqli_fetch_array($resultCustomer);
		
		//ambil data alamat
		$queryAddress  = "select a.address, a.rt, a.rw, a.zip_code,  
						  e.name as village_name, d.name as district_name, 
						  c.name as regency_name, b.name as province_name  						  
						  from register_agent_history a  
						  left join provinces b on a.province_id=b.id 
						  left join regencies c on a.regency_id=c.id
						  left join districts d on a.district_id=d.id
						  left join villages e on a.village_id=e.id
						  where a.id_register_agent='".$id_register_agent."' ";
		$resultAddress = mysqli_query($mysql_connection, $queryAddress);
		$dataAddress = mysqli_fetch_array($resultAddress);		

		if($status == 'APPROVED') {			
			
			//==================================== AFIS CALL ====================================
			
			/*
				referalCategory :
				1. Jika nik_upline kosong = 01
				2. Jika nik_upline terisi = 04
			*/
			
			if($dataRequest['nik_upline'] == '') $referalCategoryID = '01';
			else {
				
				$referalCategoryID = '04';
				
				//ambil nama upline
				//ambil data customer
				$queryUpline  = "select customer_name from customers where nip='".$dataRequest['nik_upline']."' ";
				$resultUpline = mysqli_query($mysql_connection, $queryUpline);
				$dataUpline   = mysqli_fetch_array($resultUpline);
			}
			
			//cari bank id 
			$queryBank  = "select bank_id from bank_master where short_name='".$dataRequest['bank_name']."' ";
			$resultBank = mysqli_query($mysql_connection, $queryBank);
			$dataBank   = mysqli_fetch_array($resultBank);	
			$bankID		= $dataBank['bank_id'];
			
			//cari bank branch id 
			$queryBank  = "select bank_branch_id,bank_code from bank_branch where bank_branch_name='".$dataRequest['bank_branch_name']."' ";
			$resultBank = mysqli_query($mysql_connection, $queryBank);
			$dataBank   = mysqli_fetch_array($resultBank);	
			$bankBranchID = $dataBank['bank_branch_id'];
			$bankCode 	  = $dataBank['bank_code'];
			
			$afis_api_url	= $afis_api_url.'/Andalanku/RegisterAgent';
			
			$body_param	= array();
			$body_param["ReferalCategoryID"]		 = $referalCategoryID;
			$body_param["ReferalName"]				 = $dataCustomer['customer_name'];
			$body_param["BankID"]					 = $bankID;
			$body_param["BankBranchID"]				 = $bankBranchID;
			$body_param["AccountNo"]				 = $dataRequest['bank_account_number'];
			$body_param["AccountName"]				 = $dataRequest['bank_account_holder'];
			$body_param["BankCode"]					 = $bankCode;
			$body_param["ReferalAddress"]			 = $dataAddress['address'];
			$body_param["RT"]						 = $dataAddress['rt'];
			$body_param["RW"]						 = $dataAddress['rw'];
			$body_param["Kelurahan"]				 = $dataAddress['village_name'];
			$body_param["Kecamatan"]				 = $dataAddress['district_name'];
			$body_param["City"]						 = $dataAddress['regency_name'];
			$body_param["KodePos"]					 = $dataAddress['zip_code'];
			$body_param["NPWP"]						 = $dataRequest['npwp'];
			$body_param["JenisPajak"]				 = 'pph21';
			$body_param["IsPotongPajak"]			 = 1;
			$body_param["IsActive"]					 = 1;
			$body_param["AreaPhone1"] 				 = substr($dataRequest['phone_number'],0,4);
			$body_param["Phone1"] 					 = substr($dataRequest['phone_number'],4);
			$body_param["AreaPhone2"] 				 = $dataRequest['area_code'];
			$body_param["Phone2"] 					 = $dataRequest['home_phone_number'];
			$body_param["AreaFax"] 					 = "";
			$body_param["Fax"] 						 = "";
			$body_param["BankBranch"] 				 = $dataRequest['bank_branch_name'];
			$body_param["EmployeeID"] 				 = $dataRequest['nik_upline'];
			$body_param["NoKTP"] 					 = $dataRequest['id_card_number'];
			$body_param["BranchId"] 				 = $dataRequest['branch_id'];
			$body_param["ImageKtp"] 				 = "0x"; //base64_encode(file_get_contents($customer_document_image_folder.'/'.$dataRequest['id_card_image'])); 
			$body_param["ImageBukuRekeningTabungan"] = "0x"; //base64_encode(file_get_contents($customer_document_image_folder.'/'.$dataRequest['bank_account_image']));
			
			$jsonData = json_encode($body_param);
			$queryUpdate = "UPDATE register_agent_history SET json_data = '$jsonData' WHERE id_register_agent = '$id_register_agent'";
			mysqli_query($mysql_connection, $queryUpdate);
			
			$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_param));
			$api_response['afis_response'] 	= $afis_response;

			$now = date("Y-m-d H:i:s");			
	
			if ($afis_response->StatusCode == 500) {
				$queryUpdate = "UPDATE register_agent_history SET afis_status = 'failed'  
								WHERE id_register_agent = '$id_register_agent'";
				mysqli_query($mysql_connection, $queryUpdate);	
				
				/* SEND INBOX */
				$title = "Pendaftaran Agent";
				$content = '{"message":"Mohon maaf, pengajuan anda belum dapat disetujui. Silahkan mencoba mengajukan kembali atau hubungi customer service kami."}';
				
				$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
								values('$id_customer', '$now', 'agent registration', '$title', '0', '$content')";
				mysqli_query($mysql_connection, $queryInsert);

				echo 'Proses Gagal. Tidak dapat terhubung dengan AFIS';
				exit;
			} 
			else {
				
				$afisData = $afis_response->Response->Data[0];
				
				if($afisData->AgentCode<>'' && $afisData->AgentCode<>'-') {
					
					$agent_code = $afisData->AgentCode;
					
					$queryUpdate = "UPDATE register_agent_history SET 
									status = 'APPROVED',
									afis_status = 'success'  
									WHERE id_register_agent = '$id_register_agent'";
					mysqli_query($mysql_connection, $queryUpdate);
					
					$query = "update customers set customer_type='AGENT', 
							  agent_code='$agent_code' where id_customer='$id_customer' ";
					mysqli_query($mysql_connection, $query);

					$pks_filename = create_pks($id_register_agent);
					
					/* SEND INBOX */
					$title   = "Pendaftaran Agent";
					$inboxMsg = "Pendaftaran Agent anda berhasil, Selamat bergabung sebagai Mitra Bisnis Andalanku!\r\n\r\n
								 Nama Agent : ".$dataCustomer['customer_name']."\r\n
								 Nomor Agent : ".$agent_code."\r\n
								 Nama Upliner : ".$dataUpline['customer_name']."\r\n"; 
					$content = '{"message": "'.$inboxMsg.'"}';
					
					$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
									values('$id_customer', '$now', 'agent registration', '$title', '0', '$content')";
					mysqli_query($mysql_connection, $queryInsert);
					
					//ambil id yang terakhir
					$query = "select id from inbox where customer_id='$id_customer' 
								and (sender_id is null or sender_id='') 
								and date = '$now'";
					$result= mysqli_query($mysql_connection, $query);
					$data  = mysqli_fetch_array($result);
					$id_inbox = $data['id'];
					
					$queryInsert = "insert into inbox_media (inbox_id, file_name, file_type)
									values('$id_inbox','$pks_filename', 'pdf')";
					mysqli_query($mysql_connection, $queryInsert);
				
					echo 'success';
					exit;
				}
				else {
					
					$queryUpdate = "UPDATE register_agent_history SET 
									status = 'FAILED',
									afis_status = 'success'  
									WHERE id_register_agent = '$id_register_agent'";
					mysqli_query($mysql_connection, $queryUpdate);

					/* SEND INBOX */
					$title = "Pendaftaran Agent";
					$content = '{"message":"Pendaftaran agent anda tidak dapat diproses. Status : '.$afisData->errMessage.'"}';
					
					$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
									values('$id_customer', '$now', 'agent registration', '$title', '0', '$content')";
					mysqli_query($mysql_connection, $queryInsert);
					
					echo 'Proses approval gagal : '.$afisData->errMessage;
					exit;
				}
			}
			//==================================== AFIS CALL ====================================
		}
		else {
			
			$now = date("Y-m-d H:i:s");
			
			$queryUpdate = "UPDATE register_agent_history SET status = 'REJECTED', updated_at = '$now'   
							WHERE id_register_agent = '$id_register_agent'";
			mysqli_query($mysql_connection, $queryUpdate);	
			
			$queryUpdate = "update customers set agent_code='' where id_customer='$id_customer' ";
			mysqli_query($mysql_connection, $queryUpdate);
		
			/* SEND INBOX */		
			$title = "Pendaftaran Agent";
			$content = '{"message":"Maaf pendaftaran agent anda ditolak, lengkapi dengan data yang valid dan submit ulang pengajuan anda."}';			
			
			$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
							values('$id_customer', '$now', 'agent registration', '$title', '0', '$content')";
			mysqli_query($mysql_connection, $queryInsert);

		}

		echo 'success';
		exit;
		
	} else {
		echo "empty";
		exit;
	}
?>
