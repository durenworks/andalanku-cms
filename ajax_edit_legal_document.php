<?php

	$_ref = $_REQUEST['_ref'];
	if($_ref == md5('tos#'.date('d-m-Y'))) {
		$currentURL = 'tos';
	}
	else if($_ref == md5('privacypolicy#'.date('d-m-Y'))) {
		$currentURL = 'privacypolicy';
	}

	include "check-admin-session.php";

	$userID				= $_SESSION['userID'];
	$loginType			= $_SESSION['loginType'];
	$id_legal_document	= sanitize_int($_REQUEST["id_legal_document"]);
	$legal_type			= sanitize_sql_string($_REQUEST["legal_type"]);
	$content 			= sanitize_sql_string($_REQUEST["content"]);

	if ($id_legal_document <> '0' && $legal_type <> '' && $content <> '') {
	
		$now = date("Y-m-d H:i:s");
		
		if($loginType == 'SUPERADMIN') $status = 'APPROVED';
		else $status = 'PENDING';
		
		$query 	= "select * 
				   from legal_documents
				   where id_legal_document='$id_legal_document' ";
		$result = mysqli_query($mysql_connection, $query);
		$data   = mysqli_fetch_array($result);

		if($data['is_published'] == 0) {
			
			$query = "	UPDATE legal_documents set content='$content', 
						edit_by='$userID', edit_date='$now', 
						status='$status' 
						where id_legal_document='$id_legal_document' ";
			mysqli_query($mysql_connection, $query);
		}
		else if($data['is_published'] == 1) {
			
			$query = "	INSERT INTO legal_documents(type, date, input_by, content, status)
						VALUES ('$legal_type', '$now', '$userID', '$content', '$status')";
			mysqli_query($mysql_connection, $query);
		}

		echo 'success';
		exit;
		
	} else {
		echo "empty";
		exit;
	}
?>
