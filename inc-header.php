<?php ini_set("display_errors","0"); error_reporting(0);?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Andalanku :: Backend</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />

    <link href="css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

	  <link rel="stylesheet" href="plugins/remodal/remodal.css">
	  <link rel="stylesheet" href="plugins/remodal/remodal-default-theme.css">

    <link rel="shortcut icon" href="img/favicon.ico">

  </head>
