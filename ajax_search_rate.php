<?php
	include "check-admin-session.php";

	$query 	= "select * from rates order by id_rate ASC ";
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
					<th width='5%'>No</th>
					<th>Tenor</th>
					<th>Rate</th>
					<th>Admin</th>
				</tr>	";

	$i = 1;

	while ($data = mysqli_fetch_array($result)) {
	
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data['tenor'] . '</td>
				  <td>' . number_format($data['rate'],2,',','.') . '%</td>
				  <td>' . number_format($data['admin'],0,',','.') . '</td>
				</tr>';
		$i++;
	}

	echo "</table>";
?>
