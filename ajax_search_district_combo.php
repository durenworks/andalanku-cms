<?php
	include "check-admin-session.php";

	$regency_id = sanitize_sql_string($_REQUEST["regency_id"]);
	$addressType = sanitize_sql_string($_REQUEST["addressType"]);

	$query = "select * 
			  from districts 
			  where regency_id='$regency_id' 
			  order by name ASC"; 
	$result = mysqli_query($mysql_connection, $query);
	
	$addrType = '';
	if($addressType == 'domicile') $addrType = '_domicile';
	else if($addressType == 'office') $addrType = '_office';

	echo "<select name='district_id".$addrType."' id='district_id".$addrType."' class='form-control' onchange='getVillage(\"false\", \"".$addressType."\");'>";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
	}

	echo "</select>";
?>