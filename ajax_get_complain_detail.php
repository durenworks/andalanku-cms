<?php
	include "check-admin-session.php";
//    include "inc-db.php";
//    include "sanitize.inc.php";
//    $userID			= '1';
	$userID		= $_SESSION['userID'];
	$complainID = sanitize_int($_REQUEST["complainID"]);
	
	$query 	= "select * from complains where id_complain='$complainID'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
	$id_customer = $data['user_id'];

	if($data['status'] == 'SUBMITTED' && $data['on_process_date'] == '') {
		
		$now = date("Y-m-d H:i:s");
		
		$query 	= "update complains set status='ON PROCESS', on_process_date='$now', processed_by='$userID' 
				   where id_complain='$complainID'";
		mysqli_query($mysql_connection, $query);

        $queryMedia 	= "select b.url 
						   from complain_media a 
						   left join media b on a.id_media=b.id_media 
						   where id_complain='$complainID'";
        $resultMedia	= mysqli_query($mysql_connection, $queryMedia);
        $arrayMediaURL 	= array();
        $i = 1;
        while($dataMedia = mysqli_fetch_array($resultMedia) ) {

            $arrayMediaURL[$i] = $backend_url.'/'.$media_image_folder.'/'.$dataMedia['url'];
            $i++;
        }

        //==================== INBOX ====================
        $content					 = array();
        $content['ticket_number'] 	 = $data['ticket_number'];
        $content['category'] 	 	 = $data['category'];
        $content['input_date'] 		 = $data['submitted_date'];
        $content['status'] 		 	 = 'ON PROCESS';
        $content['complain_message'] = $data['message'];
        $content['processed_status'] = 'ON PROCESS';
        $content['processed_date']   = $now;
        $content['solved_status'] 	 = '';
        $content['solved_date']   	 = '';
        $content['complain_media']   = $arrayMediaURL;
        $content = json_encode($content);

		$ticket_number = $data['ticket_number'];
        $query = "select id from inbox where 
                    content like '%$ticket_number%'
                    and customer_id='$id_customer'";
        $result = mysqli_query($mysql_connection, $query);
        $result = mysqli_fetch_array($result);
        $inboxId = $result['id'];

        //updare ke tabel inbox
        $queryUpdate = "update inbox set content='$content', date='$now' where id='$inboxId'";
        mysqli_query($mysql_connection, $queryUpdate);
	}
	
	$query 	= "select a.*, b.customer_name, c.fullname as processed_by_name, d.fullname as solved_by_name 
			   from complains a 
			   left join customers b on a.user_id=b.id_customer 
			   left join users c on a.processed_by=c.user_id  
			   left join users d on a.solved_by=d.user_id  
			   where id_complain='$complainID'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
	
	$data['submitted_date'] = date("d-m-Y H:i:s", strtotime($data['submitted_date']));
	if($data['on_process_date'] <> '') $data['on_process_date'] = date("d-m-Y H:i:s", strtotime($data['on_process_date']));
	if($data['solved_date'] <> '') $data['solved_date'] = date("d-m-Y H:i:s", strtotime($data['solved_date']));

	$data['message']  = str_replace("<br />", "\r\n", $data['message']);
	$data['solution'] = str_replace("<br />", "\r\n", $data['solution']);
	
	echo json_encode($data);
?>
