<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Car Prices</li>
			  <li>Model Mobil</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-car"></i> Model Mobil</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<select name="src_car_brand" class="form-control" id="src_car_brand" onchange="getCarTypes('Y', 'spanSrcCarType');">
							<option value="">Semua Merk Mobil</option>
							<?php 
								$query = "select * from car_brands order by name asc";
								$result= mysqli_query($mysql_connection, $query);
								while($data = mysqli_fetch_array($result)) {
									echo '<option value="'.$data['id_car_brand'].'">'.$data['name'].'</option>';
								}
							?>
						</select>
					</div>
					<div class="col-md-3">
						<span id="spanSrcCarType">
						<select name="src_car_type" class="form-control" id="src_car_type">
							<option value="">Semua Tipe Mobil</option>
						</select>
						</span>
					</div>
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
						<a href="#modal">
							<button type="button" class="btn btn-primary  btn-flat " ><i class="fa fa-plus"></i> Tambah Model Mobil</button>
						</a>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->


	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Tambah Model Mobil</h4>
			<p id="modal1Desc"><span id="addNotification"></span>
			
			<input type="hidden" id="id_car_trim" name="id_car_trim" value="0">
					
			<div class="col-md-12">
				<div class="form-group">
					<label for="id_car_brand">Merk Mobil</label>
					<select name="id_car_brand" class="form-control" id="id_car_brand" onchange="getCarTypes('N', 'spanCarType');">
						<?php 
							$query = "select * from car_brands order by name asc";
							$result= mysqli_query($mysql_connection, $query);
							while($data = mysqli_fetch_array($result)) {
								echo '<option value="'.$data['id_car_brand'].'">'.$data['name'].'</option>';
							}
						?>
					</select>
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="id_car_type">Tipe Mobil</label>
					<span id="spanCarType"></span>
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="asset_code">Asset Code</label>
					<input type="text" name="asset_code" placeholder="Asset Code" class="form-control" id="asset_code">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="name">Nama</label>
					<input type="text" name="name" placeholder="Nama" class="form-control" id="name">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="price">Harga</label>
					<input type="text" name="price" placeholder="Harga" class="form-control" id="price">
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="is_active">Status</label>
					<select name="is_active" class="form-control" id="is_active">
						<option value="1">Aktif</option>
						<option value="0">Tidak Aktif</option>
					</select>
				</div>
			</div>

			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
		<button data-remodal-action="confirm" class="remodal-confirm">OK</button>
	</div>

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'src_car_brand'	: $('select[name=src_car_brand]').val(),
								'src_car_type'	: $('select[name=src_car_type]').val(),
								'keyword'		: $('input[name=keyword]').val(),
								'page'			: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_car_trim.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);
		
		function getCarTypes(withAll, spanName, selectedID=0) {
			
			$body.addClass("loadingClass");
			
			var src_car_brand = '';
			if(withAll == 'Y') src_car_brand = $('select[name=src_car_brand]').val();
			else src_car_brand = $('select[name=id_car_brand]').val();

			var formData = {'src_car_brand' : src_car_brand, 'withAll' : withAll };

            $.ajax({
                type: "POST",
                url: 'ajax_search_car_type_combo.php',
                data: formData
            }).done(function(data) {
                $('#'+spanName).html(data);
				if(selectedID != 0) $("#id_car_type").val(selectedID);
                $body.removeClass("loadingClass");
            });
		}
		
		getCarTypes('N', 'spanCarType');

		var modalAdd = $('[data-remodal-id=modal]').remodal( );

        $(document).on('confirmation', '.remodal', function() {
			
            var form_data = new FormData();
			form_data.append('id_car_trim', $('input[name=id_car_trim]').val());
			form_data.append('id_car_brand', $('select[name=id_car_brand]').val());
			form_data.append('id_car_type', $('select[name=id_car_type]').val());
			form_data.append('asset_code', $('input[name=asset_code]').val());
          	form_data.append('name', $('input[name=name]').val());
			form_data.append('price', $('input[name=price]').val());
			form_data.append('is_active', $('select[name=is_active]').val());            

			$body.addClass("loadingClass");

			if ($("#id_car_trim").val() == '0') {
				$.ajax({
					type: "POST",
					url: 'ajax_add_car_trim.php',
					processData: false,
					contentType: false,
					data: form_data
				}).done(function(data) { 

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data model mobil telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					} else if (data == 'name_exist') {
						$('#addNotification').html(showNotification("error", "Nama model mobil sudah digunakan"));
					}
				});
			} else {
				$.ajax({
					type: "POST",
					url: 'ajax_edit_car_trim.php',
					processData: false,
					contentType: false,
					data: form_data
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data model mobil telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					} else if (data == 'name_exist') {
						$('#addNotification').html(showNotification("error", "Nama model mobil sudah digunakan"));
					}

				});
			}
			
        });

		function resetdata() {
            $("#id_car_trim").val(0);
			$("#id_car_brand").val($("#id_car_brand option:first").val());
			getCarTypes('N', 'spanCarType');
            $("#asset_code").val("");
			$("#name").val("");
			$("#price").val("");
			$("#is_active").val($("#is_active option:first").val());
			$("#modal1Title").html('Tambah Merk Mobil');
            $("#addNotification").html("");
        }

        function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

		function getedit(carTrimID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_car_trim_detail.php',
                data: 'carTrimID=' + carTrimID,
                dataType: 'json'
            }).done(function(data) { 
			
				$("#id_car_trim").val(data.id_car_trim);
				$("#id_car_brand").val(data.id_car_brand);
				getCarTypes('N', 'spanCarType', data.id_car_type);
				$("#asset_code").val(data.asset_code);
				$("#name").val(data.name);
				$("#price").val(data.price);
				$("#is_active").val(data.is_active);
				
				$body.removeClass("loadingClass");
            });
        }

        function deleteCarTrim(carTrimID, carTrimName){

        	if (confirm('Anda yakin akan menghapus model ' + carTrimName + '?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_delete_car_trim.php',
                    data: 'carTrimID=' + carTrimID  
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Data model mobil telah dihapus"));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	} else if (data == 'in_use') {
						$('#mainNotification').html(showNotification("error", "Model mobil "+carTrimName+" tidak dapat dihapus"));
					}
                });
            }
        }

        $(document).on('closing', '.remodal', function(e) {
            resetdata();
        });

		onlynumber('price');
	</script>

  </body>
</html>
