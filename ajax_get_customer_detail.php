<?php
	include "check-admin-session.php";

	$customerID = sanitize_int($_REQUEST["customerID"]);
	
	$query 	= "select a.*, b.name as occupation_name 
			   from customers a 
			   left join occupations b on a.occupation = b.id 
			   where id_customer='$customerID'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
	
	$queryAddress  = "select b.*,
					  f.name as village_name, e.name as district_name, 
					  d.name as regency_name, c.name as province_name, 
					  b.zip_code 
					  from address_customers a 
					  left join address b on a.address_id=b.id 
					  left join provinces c on b.province_id=c.id 
					  left join regencies d on b.regency_id=d.id
					  left join districts e on b.district_id=e.id
					  left join villages f on b.village_id=f.id
					  where a.is_active='1' 
					  and a.user_id='$customerID'
					  and a.address_type='LEGAL' ";
	$resultAddress1 = mysqli_query($mysql_connection, $queryAddress);
	$data['legal_address'] = '';
	
	if(mysqli_num_rows($resultAddress1) > 0) {
		
		$dataAddress = mysqli_fetch_array($resultAddress1);
		
		if($dataAddress['address'] <> '') {
			$legal_address = $dataAddress['address']."\r\n".
							   'RT '.$dataAddress['rt'].' RW '.$dataAddress['rw']."\r\n".
							   $dataAddress['village_name'].', '.$dataAddress['district_name']."\r\n".
							   $dataAddress['regency_name'].', '.$dataAddress['province_name']."\r\n".
							   $dataAddress['zip_code'];
						   
			$data['legal_address'] 	= $legal_address;
			$data['address'] 		= $dataAddress['address'];
			$data['rt'] 			= $dataAddress['rt'];
			$data['rw'] 			= $dataAddress['rw'];
			$data['zip_code']		= $dataAddress['zip_code'];
			$data['village_id'] 	= $dataAddress['village_id'];
			$data['district_id'] 	= $dataAddress['district_id'];
			$data['district_id'] 	= $dataAddress['district_id'];
			$data['regency_id'] 	= $dataAddress['regency_id'];
			$data['province_id'] 	= $dataAddress['province_id'];
		}
	}
	
	$queryAddress  = "select b.*,
					  f.name as village_name, e.name as district_name, 
					  d.name as regency_name, c.name as province_name, 
					  b.zip_code 
					  from address_customers a 
					  left join address b on a.address_id=b.id 
					  left join provinces c on b.province_id=c.id 
					  left join regencies d on b.regency_id=d.id
					  left join districts e on b.district_id=e.id
					  left join villages f on b.village_id=f.id
					  where a.is_active='1' 
					  and a.user_id='$customerID'
					  and a.address_type='DOMICILE' ";
	$resultAddress2 = mysqli_query($mysql_connection, $queryAddress);
	$data['domicile_address'] = '';
	
	if(mysqli_num_rows($resultAddress2) > 0) {
		
		$dataAddress = mysqli_fetch_array($resultAddress2);
		
		if($dataAddress['address'] <> '') {
			$domicile_address = $dataAddress['address']."\r\n".
							   'RT '.$dataAddress['rt'].' RW '.$dataAddress['rw']."\r\n".
							   $dataAddress['village_name'].', '.$dataAddress['district_name']."\r\n".
							   $dataAddress['regency_name'].', '.$dataAddress['province_name']."\r\n".
							   $dataAddress['zip_code'];
						   
			$data['domicile_address'] 		= $domicile_address;
			$data['address_domicile'] 		= $dataAddress['address'];
			$data['rt_domicile'] 			= $dataAddress['rt'];
			$data['rw_domicile'] 			= $dataAddress['rw'];
			$data['zip_code_domicile']		= $dataAddress['zip_code'];
			$data['village_id_domicile'] 	= $dataAddress['village_id'];
			$data['district_id_domicile'] 	= $dataAddress['district_id'];
			$data['district_id_domicile'] 	= $dataAddress['district_id'];
			$data['regency_id_domicile'] 	= $dataAddress['regency_id'];
			$data['province_id_domicile'] 	= $dataAddress['province_id'];
		}
	}
	
	$queryAddress  = "select b.*,
					  f.name as village_name, e.name as district_name, 
					  d.name as regency_name, c.name as province_name, 
					  b.zip_code 
					  from address_customers a 
					  left join address b on a.address_id=b.id 
					  left join provinces c on b.province_id=c.id 
					  left join regencies d on b.regency_id=d.id
					  left join districts e on b.district_id=e.id
					  left join villages f on b.village_id=f.id
					  where a.is_active='1' 
					  and a.user_id='$customerID'
					  and a.address_type='OFFICE' ";
	$resultAddress3 = mysqli_query($mysql_connection, $queryAddress);
	$data['office_address'] = '';
	
	if(mysqli_num_rows($resultAddress3) > 0) {
		
		$dataAddress = mysqli_fetch_array($resultAddress3);
		
		if($dataAddress['address'] <> '') {
			$office_address = $dataAddress['address']."\r\n".
							   'RT '.$dataAddress['rt'].' RW '.$dataAddress['rw']."\r\n".
							   $dataAddress['village_name'].', '.$dataAddress['district_name']."\r\n".
							   $dataAddress['regency_name'].', '.$dataAddress['province_name']."\r\n".
							   $dataAddress['zip_code'];
						   
			$data['office_address'] 		= $office_address;
			$data['address_office'] 		= $dataAddress['address'];
			$data['rt_office'] 				= $dataAddress['rt'];
			$data['rw_office'] 				= $dataAddress['rw'];
			$data['zip_code_office']		= $dataAddress['zip_code'];
			$data['village_id_office'] 		= $dataAddress['village_id'];
			$data['district_id_office'] 	= $dataAddress['district_id'];
			$data['district_id_office'] 	= $dataAddress['district_id'];
			$data['regency_id_office'] 		= $dataAddress['regency_id'];
			$data['province_id_office'] 	= $dataAddress['province_id'];
		}
	}
	
	if($data['date_of_birth'] <> '') $data['date_of_birth'] = date('d-m-Y', strtotime($data['date_of_birth']));
	if($data['registration_date'] <> '') $data['registration_date'] = date('d-m-Y H:i:s', strtotime($data['registration_date']));
	$data['monthly_income'] = number_format($data['monthly_income'],0,',','.');
	$data['additional_income'] = number_format($data['additional_income'],0,',','.');
	
	
	$customer_documents = "<table class='table table-hover'>
							  <tr>
								  <th width='10%'>No</th>
								  <th width='30%'>Tipe Dokumen</th>
								  <th width='20%'>Status</th>
								  <th width='40%'>Gambar</th>
								</tr>";
	
	$queryDocs = "select * from customer_documents where customer_id='$customerID' and is_active='1' ";
	$resultDocs= mysqli_query($mysql_connection, $queryDocs);
	
	$i = 1;

	while ($dataDocs = mysqli_fetch_array($resultDocs)) {
		
		if($dataDocs['is_active'] == '1') $status = 'Aktif';
		else $status = 'Tidak Aktif';
		
		$customer_documents .= '<tr>
									  <td align="left">'.$i.'</th>
									  <td align="left">'.$dataDocs['type'].'</td>
									  <td align="left">'.$status.'</td>
									  <td>
										<a href="user_files/customer_document_image/'.$dataDocs['image'].'" target="_blank">
											<img src="user_files/customer_document_image/'.$dataDocs['image'].'" style="width:100px" alt=" "/>
										</a>
									  </td>
								  </tr>';
		$i++;
	}

	$customer_documents .= "</table>";
	
	$data['customer_documents'] = $customer_documents;
	
	echo json_encode($data);
	exit;
?>
