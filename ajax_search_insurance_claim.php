<?php
	include "check-admin-session.php";

	$status 		= sanitize_sql_string($_REQUEST["src_status"]);
	$searchDate		= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$page 			= sanitize_int($_REQUEST["page"]);
	
	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";
	} else {
		$startDate	= date("Y-m")."-01 00:00:00";
		$endDate	= date("Y-m")."-31 23:59:59";
	}

	$query 	= "select COUNT(a.id) as num
				from insurance_claims a 
				left join customers b on a.customer_id=b.id_customer 
				where (b.customer_name like '%$keyword%' or b.phone_number like '%$keyword%' or a.ticket_number like '%$keyword%') 
				and claim_date>='$startDate' and claim_date<='$endDate' ";
	if($status <> '') $query = $query." and a.status='$status' ";
					  

	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;

	$query 	= "select a.*, b.customer_name, b.phone_number   
			   from insurance_claims a 
			   left join customers b on a.customer_id=b.id_customer 
			   where (b.customer_name like '%$keyword%' or b.phone_number like '%$keyword%' or a.ticket_number like '%$keyword%') 
			   and claim_date>='$startDate' and claim_date<='$endDate'  ";
	if($status <> '') $query = $query." and a.status='$status' ";
	$query = $query." order by claim_date ASC LIMIT $start,$limit";
	
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
					<th width='5%'>No</th>
					<th width='8%'>Nomor Tiket</th>
					<th width='10%'>Tanggal Klaim</th>
					<th width='12%'>Nama Konsumen</th>
					<th width='10%'>Nomor Kontrak</th>
					<th width='10%'>Jenis Kejadian</th>
					<th width='10%'>Tanggal Kejadian</th>
					<th width='10%'>Lokasi</th>
					<th>Catatan</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		$event_case = $data['event_case'];
		
		if($event_case == 'ST') $event_case = 'STOLEN';
		else if($event_case == 'TLO') $event_case = 'TOTAL LOST';
		else if($event_case == 'A') $event_case = 'ACCIDENT';
		
		$status = $data['status'];
		
		if($status == 'SUBMITTED') $status = "<b><font color='red'>".$status."</font></b>";
		else if($status == 'ON PROCESS') $status = "<b><font color='blue'>".$status."</font></b>";
		else if($status == 'ON HANDLE') $status = "<b><font color='green'>".$status."</font></b>";

		$claim_type = '';
		if ($data['claim_type'] == 'TLO') {
			$claim_type = 'Total Loss Only';
		}
		elseif ($data['claim_type'] == 'ARK') {
			$claim_type = 'All Risk';
		}
		
		//cek apakah ada dokumen yang diupload
		$queryDocs	= 'select * from insurance_claim_media where id_claim='.$data[id];
		$resultDocs	= mysqli_query($mysql_connection, $queryDocs);
		if(mysqli_num_rows($resultDocs) > 0) {
			$downloadLink = '<br><a href="download_document?type=ins_claim&id='.$data[id].'&ticket='.$data[ticket_number].'" target="_blank"><i class="fa fa-paperclip"></i> Media</a>';
		}
		else {
			$downloadLink = '';
		}
		
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data['ticket_number'] . $downloadLink . '</td>
				  <td>' . date("d-m-Y H:i:s", strtotime($data['claim_date'])) . '</td>
				  <td>' . $data['customer_name'] . '<br>'. $data['phone_number'] .'</td>
				  <td>' . $data['agreement_number'] . '<br>' . $claim_type . '</td>
				  <td>' . $event_case . '</td>
				  <td>' . date("d-m-Y", strtotime($data['event_date'])) . '</td>
				  <td>' . $data['location'] . '</td>
				  <td>' . $data['notes'] . '<br>
					<a href="#modal" onclick="viewDetail('.$data[id].')" class="btn btn-sm btn-warning"><i class="fa fa-file-text-o"></i> View Detail</a>
				  </td>
				  <td>' . $status . '<br>
					<a href="#modalMedia" onclick="viewMedia('.$data[id].')" class="btn btn-sm btn-primary"><i class="fa fa-image"></i> View Media</a>
				  </td>
			  </tr>';
		$i++;
	}

	echo "</table>";

	include "inc-paging.php";
?>
