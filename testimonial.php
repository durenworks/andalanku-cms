<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Display</li>
			  <li>Testimonial</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-commenting-o"></i> Testimonial</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
						<a href="#modal">
							<button type="button" class="btn btn-primary  btn-flat " ><i class="fa fa-plus"></i> Tambah Testimonial</button>
						</a>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->


	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Tambah Testimonial</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-12">
				<div class="form-group">
					<label for="name">Nama</label>
					<input type="hidden" id="id_testimonial" name="id_testimonial" value="0">
					<input type="hidden" id="oldImg" name="oldImg" value="">
					<input type="text" name="name" placeholder="Nama" class="form-control" id="name">
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="profession">Profesi</label>
					<input type="text" name="profession" placeholder="Profesi" class="form-control" id="profession">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="testimonial">Testimonial</label>
					<input type="text" name="testimonial" placeholder="Testimonial" class="form-control" id="testimonial">
				</div>
			</div>

			<div class="col-md-12">
				<label>Gambar</label>
				<table width="100%" class='table'>
					<tr>
						<td width="150px" align="center">
							<img id="imageView1" src="img/none.png" style="width: 90px;" alt="Gambar"/>
						</td>
						<td>
							<input type='file' name="imageInput1" id="imageInput1" accept='image/*'/>
						</td>
					</tr>
				</table>
			</div>

			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
		<button data-remodal-action="confirm" class="remodal-confirm">OK</button>
	</div>

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'keyword'	: $('input[name=keyword]').val(),
								'page'		: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_testimonial.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);

		var modalAdd = $('[data-remodal-id=modal]').remodal( );

        $(document).on('confirmation', '.remodal', function() {
			
			var imageInput1 = $('#imageInput1').prop('files')[0];
			
            var form_data = new FormData();
			form_data.append('id_testimonial', $('input[name=id_testimonial]').val());
          	form_data.append('oldImg', $('input[name=oldImg]').val());
          	form_data.append('name', $('input[name=name]').val());
			form_data.append('profession', $('input[name=profession]').val());
			form_data.append('testimonial', $('input[name=testimonial]').val());
			form_data.append('imageInput1', imageInput1);
            

			$body.addClass("loadingClass");

			if ($("#id_testimonial").val() == '0') {
				$.ajax({
					type: "POST",
					url: 'ajax_add_testimonial.php',
					processData: false,
					contentType: false,
					data: form_data
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data testimonial telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					}
				});
			} else {
				$.ajax({
					type: "POST",
					url: 'ajax_edit_testimonial.php',
					processData: false,
					contentType: false,
					data: form_data
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data testimonial telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					} 

				});
			}
			
        });

		function resetdata() {
            $("#id_testimonial").val(0);
            $("#name").val("");
			$("#profession").val("");
			$("#testimonial").val("");
			$('#imageInput1').val(""); 
			$("#imageView1").attr('src', '');
            $("#modal1Title").html('Tambah Testimonial');
            $("#addNotification").html("");
        }

        function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

		function getedit(testimonialID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_detail.php',
                data: 'type=tst&id=' + testimonialID,
                dataType: 'json'
            }).done(function(data) { 
			
				$("#id_testimonial").val(data.id_testimonial);
				$("#oldImg").val(data.image);
				$("#name").val(data.name);
				$("#profession").val(data.profession);
				$("#testimonial").val(data.testimonial);
				$("#imageView1").attr('src', 'user_files/testimonial_image/'+data.image);
				$("#modal1Title").html('Edit Testimonial');
				
				$body.removeClass("loadingClass");
            });
        }

        function deleteTestimonial(testimonialID, testimonialImg, testimonialName){

        	if (confirm('Anda yakin akan menghapus testimonial ' + testimonialName + '?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_delete_testimonial.php',
                    data: 'testimonialID=' + testimonialID + '&testimonialImg=' + testimonialImg
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Data testimonial telah dihapus"));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	} else if (data == 'in_use') {
						$('#mainNotification').html(showNotification("error", "testimonial "+testimonialName+" tidak dapat dihapus"));
					}
                });
            }
        }

        $(document).on('closing', '.remodal', function(e) {
            resetdata();
        });

		function readURL(input, view) {

		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $(view).attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imageInput1").change(function(){
		    readURL(this, "#imageView1");
		});
	</script>

  </body>
</html>
