<?php
	include "check-admin-session.php";

	$carBrandID		= sanitize_int($_REQUEST["carBrandID"]);
	$carBrandImg	= sanitize_sql_string($_REQUEST["carBrandImg"]);

	if ($carBrandID <> 0) {

		if($carBrandImg != "") @unlink("user_files/car_brand_image/".$carBrandImg);
		
		//delete car trims, media, dan car media
		$query = "select id_car_type FROM car_types WHERE id_car_brand='$carBrandID'";
		$result= mysqli_query($mysql_connection, $query);
		while($data = mysqli_fetch_array($result)) {
			
			$carTypeID = $data['id_car_type'];
			
			$queryDelete = "DELETE FROM car_trims WHERE id_car_type='$carTypeID'";
			mysqli_query($mysql_connection, $queryDelete);
			
			$queryMedia = "select a.id_media, b.url 
						   FROM car_type_media a 
						   Left JOIN media b on a.id_media=b.id_media 
						   WHERE a.id_car_type='$carTypeID'";
			$resultMedia= mysqli_query($mysql_connection, $queryMedia);
			$idMediaList= '';
			while($dataMedia = mysqli_fetch_array($resultMedia)) {
				
				$idMedia		= $dataMedia['id_media'];
				$idMediaList	= $idMediaList.$idMedia.',';
				$url			= $dataMedia['url'];
				
				@unlink("user_files/media_image/".$url);
			}
			
			if($idMediaList<>'') $idMediaList = substr($idMediaList,0,strlen($idMediaList)-1);
			
			$queryDelete = "DELETE FROM media WHERE id_media in ( ".$idMediaList." )";
			mysqli_query($mysql_connection, $queryDelete);
			
			$queryDelete = "DELETE FROM car_type_media WHERE id_car_type='$carTypeID'";
			mysqli_query($mysql_connection, $queryDelete);
		}		

		$query = "DELETE FROM car_types WHERE id_car_brand='$carBrandID'";
		mysqli_query($mysql_connection, $query);
		
		$query = "DELETE FROM car_brands WHERE id_car_brand='$carBrandID'";
		mysqli_query($mysql_connection, $query);

		echo 'success';

	} else {
		echo "empty";
	}
?>
