<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Konsumen</li>
			  <li>Data Konsumen</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-id-badge"></i> Data Konsumen</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-3">
						<select name="src_type" class="form-control" id="src_type">
							<option value=''>Tipe Konsumen</option>
							<option value='CUSTOMER'>Customer</option>
							<option value='AGENT'>Agent</option>
							<option value='EMPLOYEE'>Karyawan</option>
						</select>
					</div>
					<div class="col-md-3">
						<select name="src_status" class="form-control" id="src_status">
							<option value=''>Status Konsumen</option>
							<option value='new'>New User</option>
							<option value='existing'>Exisiting Customer</option>
						</select>
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
						<button type="button" class="btn btn-flat btn-success" onClick="saveToExcel();"><i class="fa fa-file-excel-o"></i> Export To Excel</button>
					</div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->
	
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Edit Konsumen</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-6">
				<div class="form-group">
					<label for="customer_name">Nama</label>
					<input type="hidden" id="customer_id" name="customer_id" value="0">
					<input type="text" name="customer_name" placeholder="Nama Lengkap" class="form-control" id="customer_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="customer_type">Tipe Konsumen</label>
					<select name="customer_type" class="form-control" id="customer_type">
						<option value='CUSTOMER'>Customer</option>
						<option value='AGENT'>Agent</option>
						<option value='EMPLOYEE'>Karyawan</option>
					</select>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="id_number">Nomor KTP</label>
					<input type="text" name="id_number" class="form-control" id="id_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="npwp">NPWP</label>
					<input type="text" name="npwp" class="form-control" id="npwp">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" name="email" class="form-control" id="email">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="phone_number">Nomor HP</label>
					<input type="text" name="phone_number" class="form-control" id="phone_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="home_phone_number">Nomor Telp Rumah</label>
					<input type="text" name="home_phone_number" class="form-control" id="home_phone_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="referral_code">Kode Referal</label>
					<input type="text" name="referral_code" class="form-control" id="referral_code">
				</div>
			</div>			
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="place_of_birth">Tempat Lahir</label>
					<input type="text" name="place_of_birth" class="form-control" id="place_of_birth">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="date_of_birth">Tanggal Lahir (dd-mm-yyyy)</label>
					<input type="text" name="date_of_birth" class="form-control" id="date_of_birth">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="gender">Jenis Kelamin</label>
					<select name="gender" class="form-control" id="gender">
						<option value='L'>Laki-laki</option>
						<option value='P'>Perempuan</option>
					</select>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="mother_name">Nama Ibu Kandung</label>
					<input type="text" name="mother_name" class="form-control" id="mother_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="occupation">Pekerjaan</label>
					<select name="occupation" class="form-control" id="occupation">
						<?php 
							$query = "select * from occupations order by name ASC";
							$result= mysqli_query($mysql_connection, $query);
							while($data = mysqli_fetch_array($result)) {
								echo "<option value='".$data['id']."'>".$data['name']."</option>";
							}
						?>
					</select>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="monthly_income">Penghasilan Per Bulan</label>
					<input type="text" name="monthly_income" class="form-control" id="monthly_income">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="additional_income">Penghasilan Tambahan</label>
					<input type="text" name="additional_income" class="form-control" id="additional_income">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="side_job">Pekerjaan Sampingan</label>
					<input type="text" name="side_job" class="form-control" id="side_job">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="nip">NIP</label>
					<input type="text" name="nip" class="form-control" id="nip">
				</div>
			</div>

			<!-- =============================== ALAMAT LEGAL =============================== -->
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="address">Alamat Legal</label>
					<input type="text" name="address" placeholder="Alamat" class="form-control" id="address">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="rt">RT</label>
					<input type="text" name="rt" placeholder="RT" class="form-control" id="rt">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="rw">RW</label>
					<input type="text" name="rw" placeholder="RW" class="form-control" id="rw">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="zip_code">Kode Pos</label>
					<input type="text" name="zip_code" placeholder="Kode Pos" class="form-control" id="zip_code">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="province_id">Propinsi</label>
					<select name="province_id" id="province_id" class="form-control" onchange="getCity('false', 'legal');">
						<?php 
							$query = "select * from provinces order by name ASC"; 
							$result = mysqli_query($mysql_connection, $query);
							while ($data = mysqli_fetch_array($result)) {
								echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
							}
						?>
					</select>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="regency_id">Kota / Kabupaten</label>
					<span id="spanCityList">
						<select name="regency_id" id="regency_id" class="form-control">
						</select>
					</span>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="district_id">Kecamatan</label>
					<span id="spanDistrictList">
						<select name="district_id" id="district_id" class="form-control">
						</select>
					</span>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="village_id">Kelurahan</label>
					<span id="spanVillageList">
						<select name="village_id" id="village_id" class="form-control">
						</select>
					</span>
				</div>
			</div>
			
			<div class="col-md-12"><hr style="border:1px solid #ccc;"></div>
			
			<!-- =============================== ALAMAT DOMISILI =============================== -->
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="address_domicile">Alamat Domisili</label>
					<input type="text" name="address_domicile" placeholder="Alamat" class="form-control" id="address_domicile">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="rt_domicile">RT</label>
					<input type="text" name="rt_domicile" placeholder="RT" class="form-control" id="rt_domicile">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="rw_domicile">RW</label>
					<input type="text" name="rw_domicile" placeholder="RW" class="form-control" id="rw_domicile">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="zip_code_domicile">Kode Pos</label>
					<input type="text" name="zip_code_domicile" placeholder="Kode Pos" class="form-control" id="zip_code_domicile">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="province_id_domicile">Propinsi</label>
					<select name="province_id_domicile" id="province_id_domicile" class="form-control" onchange="getCity('false', 'domicile');">
						<?php 
							$query = "select * from provinces order by name ASC"; 
							$result = mysqli_query($mysql_connection, $query);
							while ($data = mysqli_fetch_array($result)) {
								echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
							}
						?>
					</select>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="regency_id_domicile">Kota / Kabupaten</label>
					<span id="spanCityList_domicile">
						<select name="regency_id_domicile" id="regency_id_domicile" class="form-control">
						</select>
					</span>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="district_id_domicile">Kecamatan</label>
					<span id="spanDistrictList_domicile">
						<select name="district_id_domicile" id="district_id_domicile" class="form-control">
						</select>
					</span>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="village_id_domicile">Kelurahan</label>
					<span id="spanVillageList_domicile">
						<select name="village_id_domicile" id="village_id_domicile" class="form-control">
						</select>
					</span>
				</div>
			</div>
			
			<div class="col-md-12"><hr style="border:1px solid #ccc;"></div>
			
			<!-- =============================== ALAMAT OFFICE =============================== -->
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="address_office">Alamat Kantor</label>
					<input type="text" name="address_office" placeholder="Alamat" class="form-control" id="address_office">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="rt_office">RT</label>
					<input type="text" name="rt_office" placeholder="RT" class="form-control" id="rt_office">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="rw_office">RW</label>
					<input type="text" name="rw_office" placeholder="RW" class="form-control" id="rw_office">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="zip_code_office">Kode Pos</label>
					<input type="text" name="zip_code_office" placeholder="Kode Pos" class="form-control" id="zip_code_office">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="province_id_office">Propinsi</label>
					<select name="province_id_office" id="province_id_office" class="form-control" onchange="getCity('false', 'office');">
						<?php 
							$query = "select * from provinces order by name ASC"; 
							$result = mysqli_query($mysql_connection, $query);
							while ($data = mysqli_fetch_array($result)) {
								echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
							}
						?>
					</select>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="regency_id_office">Kota / Kabupaten</label>
					<span id="spanCityList_office">
						<select name="regency_id_office" id="regency_id_office" class="form-control">
						</select>
					</span>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="district_id_office">Kecamatan</label>
					<span id="spanDistrictList_office">
						<select name="district_id_office" id="district_id_office" class="form-control">
						</select>
					</span>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="village_id_office">Kelurahan</label>
					<span id="spanVillageList_office">
						<select name="village_id_office" id="village_id_office" class="form-control">
						</select>
					</span>
				</div>
			</div>
			
			<div class="col-md-12"><hr style="border:1px solid #ccc;"></div>

			</p>
			<br>
			<div class="col-md-12">
				<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
				<button data-remodal-action="confirm" class="remodal-confirm" name="btnSaveEdit" id="btnSaveEdit">OK</button>
			</div>
		</div>
		
		
	</div>

	<div class="remodal" data-remodal-id="modalDetail" role="dialog" aria-labelledby="modalDetailTitle" aria-describedby="modalDetailDesc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modalDetailTitle">Detail Konsumen</h4>
			<p id="modalDetailDesc">
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="detail_bank_name">Nama Bank</label>
					<input type="text" name="detail_bank_name" readonly="readonly" class="form-control" id="detail_bank_name">
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="detail_bank_account_number">Nomor Rekening</label>
					<input type="text" name="detail_bank_account_number" readonly="readonly" class="form-control" id="detail_bank_account_number">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="detail_bank_account_holder">Nama Pemilik Rekening</label>
					<input type="text" name="detail_bank_account_holder" readonly="readonly" class="form-control" id="detail_bank_account_holder">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group text-left">
					<label for="detail_bank_account_image">Foto Buku Rekening</label>
					<span id="detail_bank_account_image"></span>
				</div>
			</div>

			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>         
	</div>

	<div class="remodal" data-remodal-id="modalDetailAll" role="dialog" aria-labelledby="modalDetailAllTitle" aria-describedby="modalDetailAllDesc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modalDetailAllTitle">Detail Konsumen</h4>
			<p id="modalDetailAllDesc">
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="detail_customer_name">Nama Konsumen</label>
					<input type="text" name="detail_customer_name" readonly="readonly" class="form-control" id="detail_customer_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_customer_type">Tipe Konsumen</label>
					<input type="text" name="detail_customer_type" readonly="readonly" class="form-control" id="detail_customer_type">
				</div>
			</div>
			
			<div id="divAgentCode">
				<div class="col-md-6">
					<div class="form-group">
						<label for="detail_agent_code">Kode Agen</label>
						<input type="text" name="detail_agent_code" readonly="readonly" class="form-control" id="detail_agent_code">
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_id_number">Nomor KTP</label>
					<input type="text" name="detail_id_number" readonly="readonly" class="form-control" id="detail_id_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_npwp">NPWP</label>
					<input type="text" name="detail_npwp" readonly="readonly" class="form-control" id="detail_npwp">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_email">Email</label>
					<input type="text" name="detail_email" readonly="readonly" class="form-control" id="detail_email">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_phone_number">Nomor HP</label>
					<input type="text" name="detail_phone_number" readonly="readonly" class="form-control" id="detail_phone_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_home_phone_number">Nomor Telp Rumah</label>
					<input type="text" name="detail_home_phone_number" readonly="readonly" class="form-control" id="detail_home_phone_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_referral_code">Kode Referal</label>
					<input type="text" name="detail_referral_code" readonly="readonly" class="form-control" id="detail_referral_code">
				</div>
			</div>			
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_place_of_birth">Tempat, Tanggal Lahir</label>
					<input type="text" name="detail_place_of_birth" readonly="readonly" class="form-control" id="detail_place_of_birth">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_gender">Jenis Kelamin</label>
					<input type="text" name="detail_gender" readonly="readonly" class="form-control" id="detail_gender">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_mother_name">Nama Ibu Kandung</label>
					<input type="text" name="detail_mother_name" readonly="readonly" class="form-control" id="detail_mother_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					
					<div id="divPekerjaan">
					<label for="detail_occupation_name">Pekerjaan</label>
					<input type="text" name="detail_occupation_name" readonly="readonly" class="form-control" id="detail_occupation_name">
					</div>
					
					<div id="divNIP">
					<label for="detail_nip">NIP</label>
					<input type="text" name="detail_nip" readonly="readonly" class="form-control" id="detail_nip">
					</div>
					
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_monthly_income">Penghasilan Per Bulan</label>
					<input type="text" name="detail_monthly_income" readonly="readonly" class="form-control" id="detail_monthly_income">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_additional_income">Penghasilan Tambahan</label>
					<input type="text" name="detail_additional_income" readonly="readonly" class="form-control" id="detail_additional_income">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_side_job">Pekerjaan Sampingan</label>
					<input type="text" name="detail_side_job" readonly="readonly" class="form-control" id="detail_side_job">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="detail_registration_date">Tanggal Registrasi</label>
					<input type="text" name="detail_registration_date" readonly="readonly" class="form-control" id="detail_registration_date">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="detail_legal_address">Alamat Legal</label>
					<textarea name="detail_legal_address" readonly="readonly" rows="5" class="form-control" id="detail_legal_address"></textarea>
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="detail_domicile_address">Alamat Domisili</label>
					<textarea name="detail_domicile_address" readonly="readonly" rows="5" class="form-control" id="detail_domicile_address"></textarea>
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="detail_office_address">Alamat Kantor</label>
					<textarea name="detail_office_address" readonly="readonly" rows="5" class="form-control" id="detail_office_address"></textarea>
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<span id="spanPrivateDocumentList"></span>
				</div>
			</div>

			</p>
			<br>
			<div class="col-md-12">
				<button data-remodal-action="cancel" class="remodal-cancel">Close</button>   
			</div>
		</div>
		
	</div>

	
	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>
	
	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
	<script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'keyword'			: $('input[name=keyword]').val(),
								'src_type'			: $('select[name=src_type]').val(),
								'src_status'		: $('select[name=src_status]').val(),
								'page'				: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_customer.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );
		var global_regency_id			= '';
		var global_district_id			= '';
		var global_village_id 			= '';
		var global_regency_id_domicile	= '';
		var global_district_id_domicile	= '';
		var global_village_id_domicile	= '';
		var global_regency_id_office	= '';
		var global_district_id_office	= '';
		var global_village_id_office	= '';
		var global_andalan_customer_id 	= '';
		
		$(document).on('confirmation', '.remodal', function() {

            var formData = {
                'customer_id'		: $('#customer_id').val(),
				'customer_name'		: $("#customer_name").val(),
				'customer_type'		: $("#customer_type").val(),
				'id_number'			: $("#id_number").val(),
				'nip'				: $("#nip").val(),
				'npwp'				: $("#npwp").val(),
				'email'				: $("#email").val(),
				'phone_number'		: $("#phone_number").val(),
				'home_phone_number'	: $("#home_phone_number").val(),
				'referral_code'		: $("#referral_code").val(),
				'place_of_birth'	: $("#place_of_birth").val(),
				'date_of_birth'		: $("#date_of_birth").val(),
				'gender'			: $("#gender").val(),
				'mother_name'		: $("#mother_name").val(),
				'occupation'		: $("#occupation").val(),
				'monthly_income'	: $("#monthly_income").val(),
				'additional_income'	: $("#additional_income").val(),
				'side_job'			: $("#side_job").val(),
				
				'address'		: $('input[name=address]').val(),
				'rt'			: $('input[name=rt]').val(),
				'rw'			: $('input[name=rw]').val(),
				'zip_code'		: $('input[name=zip_code]').val(),
				'village_id'	: $('select[name=village_id]').val(),
				'district_id'	: $('select[name=district_id]').val(),
				'regency_id'	: $('select[name=regency_id]').val(),
				'province_id'	: $('select[name=province_id]').val(),
				
				'address_domicile'		: $('input[name=address_domicile]').val(),
				'rt_domicile'			: $('input[name=rt_domicile]').val(),
				'rw_domicile'			: $('input[name=rw_domicile]').val(),
				'zip_code_domicile'		: $('input[name=zip_code_domicile]').val(),
				'village_id_domicile'	: $('select[name=village_id_domicile]').val(),
				'district_id_domicile'	: $('select[name=district_id_domicile]').val(),
				'regency_id_domicile'	: $('select[name=regency_id_domicile]').val(),
				'province_id_domicile'	: $('select[name=province_id_domicile]').val(),
				
				'address_office'		: $('input[name=address_office]').val(),
				'rt_office'				: $('input[name=rt_office]').val(),
				'rw_office'				: $('input[name=rw_office]').val(),
				'zip_code_office'		: $('input[name=zip_code_office]').val(),
				'village_id_office'		: $('select[name=village_id_office]').val(),
				'district_id_office'	: $('select[name=district_id_office]').val(),
				'regency_id_office'		: $('select[name=regency_id_office]').val(),
				'province_id_office'	: $('select[name=province_id_office]').val()
            };

			$body.addClass("loadingClass");
			
			$.ajax({
				type: "POST",
				url: 'ajax_edit_customer.php',
				data: formData
			}).done(function(data) {

				data = $.trim(data);
				$body.removeClass("loadingClass");

				if (data == 'success') {
					$("#mainNotification").html(showNotification("success", "Data konsumen telah disimpan"));
					modalAdd.close();
					searchData(1);
					resetdata();
					setTimeout(clearnotif, 5000);

				} else if (data == 'empty') {
					$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
				} 
			});			
        });
		
		function resetdata() {
			
			$("#customer_id").val(0);
			$("#customer_name").val('');
			$("#address").val('');
			$("#rt").val('');
			$("#rw").val('');
			$("#zip_code").val('');
			$("#province_id").val($("#province_id option:first").val());
			$('#spanCityList').html('<select name="regency_id" id="regency_id" class="form-control"></select>');
			$('#spanDistrictList').html('<select name="district_id" id="district_id" class="form-control"></select>');
			$('#spanVillageList').html('<select name="village_id" id="village_id" class="form-control"></select>');
				
			$("#modal1Title").html('Edit Konsumen');
			
			global_regency_id	= '';
			global_district_id	= '';
			global_village_id 	= '';
		}
		
		function getDetail(customerID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_customer_detail.php',
                data: 'customerID=' + customerID,
                dataType: 'json'
            }).done(function(data) { 
			
				$("#detail_bank_name").val(data.bank_name);
				$("#detail_bank_account_number").val(data.bank_account_number);
				$("#detail_bank_account_holder").val(data.bank_account_holder);
				
				if(data.bank_account_image != '') {
					$("#detail_bank_account_image").html("<a href='user_files/customer_document_image/"+data.bank_account_image+"' target='_blank'><img src='user_files/customer_document_image/"+data.bank_account_image+"' width='150px'></a>");
				}
				else {
					$("#detail_bank_account_image").html('<i>Tidak ada foto</i>');
				}
							
            	$body.removeClass("loadingClass");
            });
        } 
		
		function getDetailAll(customerID) {

			$body.addClass("loadingClass");
			
			$("#detail_customer_name_detail").val('');
			$("#detail_customer_type").val('');
			$("#detail_id_number").val('');
			$("#detail_nip").val('');
			$("#detail_npwp").val('');
			$("#detail_email").val('');
			$("#detail_phone_number").val('');
			$("#detail_home_phone_number").val('');
			$("#detail_referral_code").val('');
			$("#detail_place_of_birth").val('');
			$("#detail_gender").val('');
			$("#detail_mother_name").val('');
			$("#detail_occupation_name").val('');
			$("#detail_monthly_income").val('');
			$("#detail_additional_income").val('');
			$("#detail_side_job").val('');
			$("#detail_registration_date").val('');
			$("#detail_legal_address").val('');
			$("#detail_domicile_address").val('');
			$("#detail_office_address").val('');
			$('#detail_spanPrivateDocumentList').html('');
			$("#divAgentCode").show();
			$("#divPekerjaan").show();
			$("#divNIP").show();
			
            $.ajax({
                type: "POST",
                url: 'ajax_get_customer_detail.php',
                data: 'customerID=' + customerID,
                dataType: 'json'
            }).done(function(data) { 
			
				$("#detail_customer_name").val(data.customer_name);
				$("#detail_customer_type").val(data.customer_type);
				$("#detail_agent_code").val(data.agent_code);
				$("#detail_id_number").val(data.id_number);
				$("#detail_nip").val(data.nip);
				$("#detail_npwp").val(data.npwp);
				$("#detail_email").val(data.email);
				$("#detail_phone_number").val(data.phone_number);
				$("#detail_home_phone_number").val(data.home_phone_number);
				$("#detail_referral_code").val(data.referral_code);
				$("#detail_place_of_birth").val(data.place_of_birth+', '+data.date_of_birth);
				$("#detail_gender").val(data.gender);
				$("#detail_mother_name").val(data.mother_name);
				$("#detail_occupation_name").val(data.occupation_name);
				$("#detail_monthly_income").val(data.monthly_income);
				$("#detail_additional_income").val(data.additional_income);
				$("#detail_side_job").val(data.side_job);
				$("#detail_registration_date").val(data.registration_date);
				$("#detail_legal_address").val(data.legal_address);
				$("#detail_domicile_address").val(data.domicile_address);
				$("#detail_office_address").val(data.office_address);
				$('#spanPrivateDocumentList').html(data.customer_documents);
				if(data.customer_type != 'EMPLOYEE') {
					$("#divNIP").hide();
					$("#divPekerjaan").show();
				}
				else if(data.customer_type == 'EMPLOYEE') {
					$("#divNIP").show();
					$("#divPekerjaan").hide();
				}
				
				if(data.customer_type == 'AGENT') {
					$("#divAgentCode").show();
				}
				else {
					$("#divAgentCode").hide();
				}
							
            	$body.removeClass("loadingClass");
            });
        }
		
		function getedit(customerID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_customer_detail.php',
                data: 'customerID=' + customerID,
                dataType: 'json'
            }).done(function(data) { 
			
				$("#customer_id").val(data.id_customer);
				$("#customer_name").val(data.customer_name);
				$("#customer_type").val(data.customer_type);
				$("#id_number").val(data.id_number);
				$("#nip").val(data.nip);
				$("#npwp").val(data.npwp);
				$("#email").val(data.email);
				$("#phone_number").val(data.phone_number);
				$("#home_phone_number").val(data.home_phone_number);
				$("#referral_code").val(data.referral_code);
				$("#place_of_birth").val(data.place_of_birth);
				$("#date_of_birth").val(data.date_of_birth);
				$("#gender").val(data.gender);
				$("#mother_name").val(data.mother_name);
				$("#occupation").val(data.occupation);
				$("#monthly_income").val(data.monthly_income);
				$("#additional_income").val(data.additional_income);
				$("#side_job").val(data.side_job);
				
				$("#modal1Title").html('Edit Konsumen');
				
				// ===== ALAMAT LEGAL =====
				$("#address").val(data.address);
				$("#rt").val(data.rt);
				$("#rw").val(data.rw);
				$("#zip_code").val(data.zip_code);
				$("#province_id").val(data.province_id);
				$('#spanDistrictList').html('<select name="district_id" id="district_id" class="form-control"></select>');
				$('#spanVillageList').html('<select name="village_id" id="village_id" class="form-control"></select>');				
				
				global_regency_id			= data.regency_id;
				global_district_id			= data.district_id;
				global_village_id 			= data.village_id;
				global_andalan_customer_id 	= data.andalan_customer_id;
				
				getCity(true, 'legal');
				
				// ===== ALAMAT DOMISILI =====
				$("#address_domicile").val(data.address_domicile);
				$("#rt_domicile").val(data.rt_domicile);
				$("#rw_domicile").val(data.rw_domicile);
				$("#zip_code_domicile").val(data.zip_code_domicile);
				$("#province_id_domicile").val(data.province_id_domicile);
				$('#spanDistrictList_domicile').html('<select name="district_id_domicile" id="district_id_domicile" class="form-control"></select>');
				$('#spanVillageList_domicile').html('<select name="village_id_domicile" id="village_id_domicile" class="form-control"></select>');				
				
				global_regency_id_domicile			= data.regency_id_domicile;
				global_district_id_domicile			= data.district_id_domicile;
				global_village_id_domicile 			= data.village_id_domicile;
				
				getCity(true, 'domicile');
				
				// ===== ALAMAT OFFICE =====
				$("#address_office").val(data.address_office);
				$("#rt_office").val(data.rt_office);
				$("#rw_office").val(data.rw_office);
				$("#zip_code_office").val(data.zip_code_office);
				$("#province_id_office").val(data.province_id_office);
				$('#spanDistrictList_office').html('<select name="district_id_office" id="district_id_office" class="form-control"></select>');
				$('#spanVillageList_office').html('<select name="village_id_office" id="village_id_office" class="form-control"></select>');				
				
				global_regency_id_office			= data.regency_id_office;
				global_district_id_office			= data.district_id_office;
				global_village_id_office			= data.village_id_office;
				
				getCity(true, 'office');
				
				if(data.andalan_customer_id == '' || data.andalan_customer_id == null) {
					setInput('enable');
				}
				else {
					setInput('disable');
				}
							
            	$body.removeClass("loadingClass");
            });
        }
		
		function setInput(inputType) {
			
			var disabled = true;
			
			if(inputType == 'enable') disabled = false;
			else disabled = true;
			
			$("#customer_id").prop('disabled', disabled);
			$("#customer_name").prop('disabled', disabled);
			$("#customer_type").prop('disabled', disabled);
			$("#id_number").prop('disabled', disabled);
			$("#nip").prop('disabled', disabled);
			$("#npwp").prop('disabled', disabled);
			$("#email").prop('disabled', disabled);
			$("#phone_number").prop('disabled', disabled);
			$("#home_phone_number").prop('disabled', disabled);
			$("#referral_code").prop('disabled', disabled);
			$("#place_of_birth").prop('disabled', disabled);
			$("#date_of_birth").prop('disabled', disabled);
			$("#gender").prop('disabled', disabled);
			$("#mother_name").prop('disabled', disabled);
			$("#occupation").prop('disabled', disabled);
			$("#monthly_income").prop('disabled', disabled);
			$("#additional_income").prop('disabled', disabled);
			$("#side_job").prop('disabled', disabled);
			
			$("#address").prop('disabled', disabled);
			$("#rt").prop('disabled', disabled);
			$("#rw").prop('disabled', disabled);
			$("#zip_code").prop('disabled', disabled);
			$("#province_id").prop('disabled', disabled);
			$("#district_id").prop('disabled', disabled);
			$("#village_id").prop('disabled', disabled);
			$("#regency_id").prop('disabled', disabled);
			
			$("#address_domicile").prop('disabled', disabled);
			$("#rt_domicile").prop('disabled', disabled);
			$("#rw_domicile").prop('disabled', disabled);
			$("#zip_code_domicile").prop('disabled', disabled);
			$("#province_id_domicile").prop('disabled', disabled);
			$("#district_id_domicile").prop('disabled', disabled);
			$("#village_id_domicile").prop('disabled', disabled);
			$("#regency_id_domicile").prop('disabled', disabled);
			
			$("#address_office").prop('disabled', disabled);
			$("#rt_office").prop('disabled', disabled);
			$("#rw_office").prop('disabled', disabled);
			$("#zip_code_office").prop('disabled', disabled);
			$("#province_id_office").prop('disabled', disabled);
			$("#district_id_office").prop('disabled', disabled);
			$("#village_id_office").prop('disabled', disabled);
			$("#regency_id_office").prop('disabled', disabled);
			
			$("#btnSaveEdit").attr('disabled', disabled);
		}
		
		function getCity(getNext=false, addressType) {

			$body.addClass("loadingClass");

			if(addressType == 'legal')
				var formData = { 'province_id' : $('select[name=province_id]').val(), 'addressType' : addressType };
			else if(addressType == 'domicile')
				var formData = { 'province_id' : $('select[name=province_id_domicile]').val(), 'addressType' : addressType };
			else if(addressType == 'office')
				var formData = { 'province_id' : $('select[name=province_id_office]').val(), 'addressType' : addressType };

            $.ajax({
                type: "POST",
                url: 'ajax_search_city_combo.php',
                data: formData
            }).done(function(data) {
				
				if(addressType == 'legal') {
					$('#spanCityList').html(data);
					$('#spanDistrictList').html('<select name="district_id" id="district_id" class="form-control"></select>');
					$('#spanVillageList').html('<select name="village_id" id="village_id" class="form-control"></select>');
				}
				else if(addressType == 'domicile') {
					$('#spanCityList_domicile').html(data);
					$('#spanDistrictList_domicile').html('<select name="district_id_domicile" id="district_id_domicile" class="form-control"></select>');
					$('#spanVillageList_domicile').html('<select name="village_id_domicile" id="village_id_domicile" class="form-control"></select>');
				}
				else if(addressType == 'office') {
					$('#spanCityList_office').html(data);
					$('#spanDistrictList_office').html('<select name="district_id_office" id="district_id_office" class="form-control"></select>');
					$('#spanVillageList_office').html('<select name="village_id_office" id="village_id_office" class="form-control"></select>');
				}
				
				var disabled = true;
				if(global_andalan_customer_id == '') disabled = false;
				else disabled = true;
				
				$("#district_id").prop('disabled', disabled);
				$("#village_id").prop('disabled', disabled);
				$("#regency_id").prop('disabled', disabled);
				$("#district_id_domicile").prop('disabled', disabled);
				$("#village_id_domicile").prop('disabled', disabled);
				$("#regency_id_domicile").prop('disabled', disabled);
				$("#district_id_office").prop('disabled', disabled);
				$("#village_id_office").prop('disabled', disabled);
				$("#regency_id_office").prop('disabled', disabled);				
				
                $body.removeClass("loadingClass");
				
				if(addressType == 'legal') { 
					if(global_regency_id != '') $('select[name=regency_id]').val(global_regency_id);
				}
				else if(addressType == 'domicile') { 
					if(global_regency_id_domicile != '') $('select[name=regency_id_domicile]').val(global_regency_id_domicile);
				}
				else if(addressType == 'office') {
					if(global_regency_id_office != '') $('select[name=regency_id_office]').val(global_regency_id_office);				
				}
				
				if(getNext) getDistrict(getNext, addressType);
            });
        }
		
		function getDistrict(getNext=false, addressType) {

			$body.addClass("loadingClass");
			
			if(addressType == 'legal')
				var formData = { 'regency_id' : $('select[name=regency_id]').val(), 'addressType' : addressType };
			else if(addressType == 'domicile')
				var formData = { 'regency_id' : $('select[name=regency_id_domicile]').val(), 'addressType' : addressType };
			else if(addressType == 'office')
				var formData = { 'regency_id' : $('select[name=regency_id_office]').val(), 'addressType' : addressType };

            $.ajax({
                type: "POST",
                url: 'ajax_search_district_combo.php',
                data: formData
            }).done(function(data) {
                
				if(addressType == 'legal') {
					$('#spanDistrictList').html(data);
					$('#spanVillageList').html('<select name="village_id" id="village_id" class="form-control"></select>');
				}
				else if(addressType == 'domicile') {
					$('#spanDistrictList_domicile').html(data);
					$('#spanVillageList_domicile').html('<select name="village_id_domicile" id="village_id_domicile" class="form-control"></select>');
				}
				else if(addressType == 'office') {
					$('#spanDistrictList_office').html(data);
					$('#spanVillageList_office').html('<select name="village_id_office" id="village_id_office" class="form-control"></select>');
				}
				
				var disabled = true;
				if(global_andalan_customer_id == '') disabled = false;
				else disabled = true;
				
				$("#district_id").prop('disabled', disabled);
				$("#village_id").prop('disabled', disabled);
				$("#regency_id").prop('disabled', disabled);
				$("#district_id_domicile").prop('disabled', disabled);
				$("#village_id_domicile").prop('disabled', disabled);
				$("#regency_id_domicile").prop('disabled', disabled);
				$("#district_id_office").prop('disabled', disabled);
				$("#village_id_office").prop('disabled', disabled);
				$("#regency_id_office").prop('disabled', disabled);	
				
                $body.removeClass("loadingClass");
							
				if(addressType == 'legal') { 
					if(global_district_id != '') $('select[name=district_id]').val(global_district_id);
				}
				else if(addressType == 'domicile') { 
					if(global_district_id_domicile != '') $('select[name=district_id_domicile]').val(global_district_id_domicile);
				}
				else if(addressType == 'office') {
					if(global_district_id_office != '') $('select[name=district_id_office]').val(global_district_id_office);				
				}
				
				
				if(getNext) getVillage(getNext, addressType);
            });
        }
		
		function getVillage(getNext=false, addressType) {

			$body.addClass("loadingClass");
			
			if(addressType == 'legal')
				var formData = { 'district_id' : $('select[name=district_id]').val(), 'addressType' : addressType };
			else if(addressType == 'domicile')
				var formData = { 'district_id' : $('select[name=district_id_domicile]').val(), 'addressType' : addressType };
			else if(addressType == 'office')
				var formData = { 'district_id' : $('select[name=district_id_office]').val(), 'addressType' : addressType };

            $.ajax({
                type: "POST",
                url: 'ajax_search_village_combo.php',
                data: formData
            }).done(function(data) {
				
                if(addressType == 'legal') {
					$('#spanVillageList').html(data);
				}
				else if(addressType == 'domicile') {
					$('#spanVillageList_domicile').html(data);
				}
				else if(addressType == 'office') {
					$('#spanVillageList_office').html(data);
				}
				
				var disabled = true;
				if(global_andalan_customer_id == '') disabled = false;
				else disabled = true;
				
				$("#district_id").prop('disabled', disabled);
				$("#village_id").prop('disabled', disabled);
				$("#regency_id").prop('disabled', disabled);
				$("#district_id_domicile").prop('disabled', disabled);
				$("#village_id_domicile").prop('disabled', disabled);
				$("#regency_id_domicile").prop('disabled', disabled);
				$("#district_id_office").prop('disabled', disabled);
				$("#village_id_office").prop('disabled', disabled);
				$("#regency_id_office").prop('disabled', disabled);	
				
                $body.removeClass("loadingClass"); 
				
				if(addressType == 'legal') { 
					if(global_village_id != '') $('select[name=village_id]').val(global_village_id);
				}
				else if(addressType == 'domicile') { 
					if(global_village_id_domicile != '') $('select[name=village_id_domicile]').val(global_village_id_domicile);
				}
				else if(addressType == 'office') { 
					if(global_village_id_office != '') $('select[name=village_id_office]').val(global_village_id_office);
				}
            });
        }

        function deleteCustomer(customerID,customerName){

        	if (confirm('Anda yakin akan menghapus konsumen ' + customerName + '?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_delete_customer.php',
                    data: 'customerID=' + customerID
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Data konsumen telah dihapus"));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	} 
                });
            }
        }

		function resetPasswordCustomer(customerID,customerName){

			if (confirm('Anda yakin akan reset password konsumen ' + customerName + '?')) {

				$body.addClass("loadingClass");

				$.ajax({
					type: "POST",
					url: 'ajax_reset_password_customer.php',
					data: 'customerID=' + customerID
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");
					if(data != "empty") {
                    	$("#mainNotification").html(showNotification("success", "Password baru : " + data));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	} 
				});
			}
		}
		
		function saveToExcel() {
			
			var keyword  = $('input[name=keyword]').val();
			var src_type = $('select[name=src_type]').val();
			var src_status = $('select[name=src_status]').val();
			window.open("excel_customer.php?keyword="+keyword+"&src_type="+src_type+"&src_status="+src_status);
		}
		
	</script>

  </body>
</html>
