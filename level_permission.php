<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<style>
	.hummingbird-treeview label {
		font-size : 14px;
	}
</style>
<link href="css/hummingbird-treeview.css" rel="stylesheet">

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Master Data</li>
			  <li>Level Permission</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-unlock-alt"></i> Level Permission</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<select name="user_level" class="form-control">
							<option value="0">Pilih User Level</option>
							<?php 
								$query = "select * from user_levels order by name ASC"; 
								$result = mysqli_query($mysql_connection, $query);
								while ($data = mysqli_fetch_array($result)) {
									echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
								}
							?>
						</select>
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData();"><i class="fa fa-search"></i> Search</button>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->


	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>
    <script src="js/inc-function.js"></script>
	
	<script src="js/hummingbird-treeview.js"></script>

	<script>

		$body = $("body");

		function searchData() {
			
			if($('select[name=user_level]').val() == 0) {
				
				$("#mainNotification").html(showNotification("error", "Silahkan pilih user level terlebih dahulu"));
				setTimeout(clearnotif, 5000);
			}
			else {
				
				$("#mainAlert").alert('close');
				$body.addClass("loadingClass");

				var formData = { 'user_level' : $('select[name=user_level]').val() };

				$.ajax({
					type: "POST",
					url: 'ajax_search_level_permission.php',
					data: formData
				}).done(function(data) { 
					$('#dataSpan').html(data);
					$body.removeClass("loadingClass");
				});
			}
        }		

        function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
		
		function expandAll() {
			$("#treeview").hummingbird("expandAll");
	    }
		
		function collapseAll() {
			$("#treeview").hummingbird("collapseAll");
	    }
		
		function saveData() {
			
			var List = {"id" : [], "dataid" : [], "text" : []};
			$("#treeview").hummingbird("getChecked",{list:List,onlyEndNodes:true});
			
			var listID = List.dataid.join(",");
			
			var formData = {
				'user_level': $('select[name=user_level]').val(),
				'listID'	: listID
			};

			$body.addClass("loadingClass");
			
			$.ajax({
				type: "POST",
				url: 'ajax_save_level_permission.php',
				data: formData
			}).done(function(data) { 

				data = $.trim(data);
				$body.removeClass("loadingClass");
				if (data == 'success') {
					$("#mainNotification").html(showNotification("success", "Data user level permission telah disimpan"));
					setTimeout(clearnotif, 5000);
				} else if (data == 'empty') {
					$("#mainNotification").html(showNotification("error", "Data yang Anda masukkan belum lengkap"));
					setTimeout(clearnotif, 5000);
				} 

			});			
		}

	</script>

  </body>
</html>
