<?php
	include "check-admin-session.php";

	$carTrimID = sanitize_int($_REQUEST["carTrimID"]);
	
	$query 	= "select a.*, c.id_car_brand
			   from car_trims a 
			   left join car_types b on a.id_car_type=b.id_car_type 
			   left join car_brands c on b.id_car_brand=c.id_car_brand 
			   where a.id_car_trim='$carTrimID'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
	
	echo json_encode($data);
?>
