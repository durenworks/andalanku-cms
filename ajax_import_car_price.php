<?php
	include "check-admin-session.php";
	require('plugins/xls/php-excel-reader/excel_reader2.php');
	require('plugins/xls/SpreadsheetReader.php');
	
	$id_car_brand = sanitize_int($_REQUEST['id_car_brand']);
	
	if($id_car_brand == '0') {
		echo "empty";
		exit;
	}
	
	//upload excelFile
	if (!empty($_FILES['excelFile'])) { 
		
		$response = "";
		$file = $_FILES['excelFile'];

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'xls' && $extension<>'xlsx') {
			
			$response = "invalid_file_format";

		} else {

			$folder = "user_files/xls_rate_file";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'andalanku_' . $random . '.'.$extension;
			}
			
			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) { 
			
				//ambil id type
				$arrIdType = array();
				$query 	= "select * from car_types order by name ASC";
				$result = mysqli_query($mysql_connection, $query); 
				while($data = mysqli_fetch_array($result)) {
					$arrIdType[$data['id_car_type']] = strtolower($data['name']);
				}	
				
				$Reader = new SpreadsheetReader($folder . '/' . $newnamefile);
				foreach ($Reader as $lineArray)
				{					
					$assetCode	= sanitize_sql_string($lineArray[0]);
					$typeName	= sanitize_sql_string($lineArray[1]);
					$trim		= sanitize_sql_string($lineArray[2]);
					$price		= sanitize_int($lineArray[3]);
					
					$id_car_type = sanitize_int(array_search(strtolower($typeName), $arrIdType));
					
					//type belum ada, insert dulu
					if($id_car_type == '0') {
						
						$query = "INSERT INTO car_types(id_car_brand, name, is_active) 
								  VALUES ('$id_car_brand', '$typeName','1')";
						mysqli_query($mysql_connection, $query);
						
						//ambil id_car_type terbaru
						$queryCheck		= "SELECT id_car_type from car_types WHERE name='$typeName' order by id_car_type DESC LIMIT 1";
						$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
						$dataCheck		= mysqli_fetch_array($resultCheck);
						$idCarTypeNew	= $dataCheck['id_car_type'];
						$arrIdType[$idCarTypeNew] = strtolower($typeName);
						$id_car_type	= $idCarTypeNew;
					}

                    $query 	= "select id_car_trim from car_trims where id_car_type = '$id_car_type'
                                and name = '$trim' limit 1";
                    $tempresult = mysqli_query($mysql_connection, $query);
                    //printf(mysqli_error($mysql_connection));
                    //var_dump($query);
                    $result = mysqli_fetch_array($tempresult);

                   // var_dump($result);
                    if (!$result) {
                        //echo("insert");
                        $query = "INSERT INTO car_trims(id_car_type, name, price, is_active, asset_code) 
							  VALUES ('$id_car_type', '$trim', '$price', '1', '$assetCode')";

                        mysqli_query($mysql_connection, $query);

                    }
                    else {
                        //echo("update");
                        $idCarTrim = $result['id_car_trim'];
                        $query = "UPDATE car_trims set price = '$price'
							  where id_car_trim='$result[0]'";

                        //var_dump($query);die();
                        mysqli_query($mysql_connection, $query);

                        $query = "DELETE from car_trims 
                                    where name = '$trim' and id_car_trim <> '$idCarTrim' and id_car_type = '$id_car_type' ";
                        //var_dump($query);die;
                        mysqli_query($mysql_connection, $query);
                    }
				}
				
				
				@unlink($folder . '/' . $newnamefile);
				
				echo "success#".$savedData;
				exit;

			} else {
				echo "upload_failed";
				exit;
			}
		}

	
	} else {
		echo "empty";
		exit;
	}
?>