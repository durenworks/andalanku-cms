<?php
	include "check-admin-session.php";

	$id_collateral_claim	= sanitize_int($_REQUEST["id_collateral_claim"]);
	$ready_date				= sanitize_sql_string($_REQUEST["ready_date"]);
	$form_number			= sanitize_sql_string($_REQUEST["form_number"]);

	if ($id_collateral_claim <> 0 && ($ready_date <> '' || $form_number <> '') ) {
		
		$query 	= "select * from collateral_requests where id='$id_collateral_claim'";
		$result = mysqli_query($mysql_connection, $query);
		$data 	= mysqli_fetch_array($result);
		
		$id_customer = $data['customer_id'];
	
		$now = date("Y-m-d H:i:s");
		
		// ON PROCESS
		if($ready_date <> '' && $form_number == '') { 
			
			if(strpos($ready_date, "/") > 0) {
				$tempArray 	= explode("/",$ready_date);
				$ready_date	= $tempArray[2]."-".$tempArray[0]."-".$tempArray[1];
			}
			else if(strpos($ready_date, "-") > 0) {
				$tempArray 	= explode("-",$ready_date);
				$ready_date	= $tempArray[2]."-".$tempArray[1]."-".$tempArray[0];
			}
			
			$query 	= "update collateral_requests set ready_date='$ready_date', status='ON PROCESS' where id='$id_collateral_claim'";
			mysqli_query($mysql_connection, $query);
			
			//==================== INBOX ====================
			$content					 = array();
			$content['ticket_number'] 	 = $data['ticket_number'];
			$content['input_date'] 		 = $data['request_date'];
			$content['status'] 		 	 = 'ON PROCESS';
			$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
			$inbox_message				 .= 'Konsumen yang terhormat, pengajuan Anda sudah kami proses. ';
			$inbox_message				 .= 'Dokumen jaminan Anda dapat diambil pada tanggal : '.date("d-m-Y", strtotime($ready_date));
			$content['message']			 = $inbox_message;
			$content = json_encode($content);
			
			//insert ke tabel inbox
			$now			= date('Y-m-d H:i:s');
			$queryCheck 	= "select id from inbox where content like '%".$data['ticket_number']."%' ";
			$resultCheck	= mysqli_query($mysql_connection, $queryCheck);
			$dataCheck		= mysqli_fetch_array($resultCheck);
			$id_inbox		= $dataCheck['id'];
			$queryInsert 	= "update inbox set date='$now', status='0', content='$content' where id='$id_inbox'";
			mysqli_query($mysql_connection, $queryInsert);
		
			/*$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
							values('$id_customer', '$now', 'collateral request', 'E-Request Pengambilan Jaminan', '0', 
							'$content')";
			mysqli_query($mysql_connection, $queryInsert);*/
			//===============================================
		}
		
		// DONE
		if($ready_date <> '' && $form_number <> '') {
			
			if(strpos($ready_date, "/") > 0) {
				$tempArray 	= explode("/",$ready_date);
				$ready_date	= $tempArray[2]."-".$tempArray[0]."-".$tempArray[1];
			}
			else if(strpos($ready_date, "-") > 0) {
				$tempArray 	= explode("-",$ready_date);
				$ready_date	= $tempArray[2]."-".$tempArray[1]."-".$tempArray[0];
			}
			
			$query 	= "update collateral_requests set form_number='$form_number', status='DONE' where id='$id_collateral_claim'";
			mysqli_query($mysql_connection, $query);
			
			//==================== INBOX ====================
			$content					 = array();
			$content['ticket_number'] 	 = $data['ticket_number'];
			$content['input_date'] 		 = $data['request_date'];
			$content['status'] 		 	 = 'DONE';
			$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
			$inbox_message				 .= 'Konsumen yang terhormat, pengajuan Anda sudah kami proses. ';
			$inbox_message				 .= 'Dokumen Anda telah diambil dengan nomor form pengambilan : '.$form_number;
			$content['message']			 = $inbox_message;
			$content = json_encode($content);
			
			//insert ke tabel inbox
			$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
							values('$id_customer', '$now', 'collateral request', 'E-Request Pengambilan Jaminan', '0', 
							'$content')";
			mysqli_query($mysql_connection, $queryInsert);
			//===============================================
		}
		
        echo 'success';
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>