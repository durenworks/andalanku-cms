<?php
	include "check-admin-session.php";

	$carTypeID = sanitize_int($_REQUEST["carTypeID"]);
	
	echo "<table class='table table-hover'> ";
	
	$queryMedia = "select b.url 
				   FROM car_type_media a 
				   Left JOIN media b on a.id_media=b.id_media 
				   WHERE a.id_car_type='$carTypeID'";
	$resultMedia= mysqli_query($mysql_connection, $queryMedia);
	$i = 0;
	while($dataMedia = mysqli_fetch_array($resultMedia)) {
		
		$url = $dataMedia['url'];
		
		if($i == 0) echo '<tr>';
		
		echo '<td>
				<a href="user_files/media_image/'.$url.'" target="_blank">
					<img src="user_files/media_image/'.$url.'" style="width: 120px;">
				</a>
			  </td>';
		
		$i++;
		
		if($i==3) {
			echo '</tr>';
			$i = 0;
		}
	}
	
	echo "</table>";
?>
