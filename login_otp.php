<?php
	include "check-admin-session.php";

	include "inc-header.php";
	$msg = $_REQUEST['e'];
?>

<body class="login-page">

    <div class="login-box">

      <div class="login-box-body">
		<div class="text-center text-white">

			<table align="center" class="table-responsive">
				<tr>
					<td class="valign_top" width="100%">
						<img src="img/logo.png" alt="Logo">
					</td>
				</tr>
			</table>
			<br>
			<h4>Verifikasi Login</h4>
		</div>
        <p class="login-box-msg">
        	<div class="text-red text-center">
			<?php
				if($msg=="01") echo "Kode OTP harus diisi";
				if($msg=="02") echo "Kode OTP tidak benar";
			?>
			</div>
		</p>
        <form action="login_otp_process" method="post">
          <div class="form-group has-feedback">
			<input type="otp_code" name="otp_code" class="form-control" placeholder="Masukkan Kode OTP" />
            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Lanjut</button>
            </div>
            <div class="col-xs-4">
              &nbsp;
            </div>
            <div class="col-xs-4">
              &nbsp;
            </div><!-- /.col -->
          </div>
        </form>
        <br>

      </div><!-- /.login-box-body -->



    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js" type="text/javascript"></script>

  </body>
</html>
