<?php
	include "check-admin-session.php";

	$type = sanitize_sql_string($_REQUEST["type"]);
	$id	  = sanitize_int($_REQUEST["id"]);

	if($type=='adm') {
		$tableName = 'users';
		$columnName = 'user_id';
	}
	else if($type=='stg') {
		$tableName = 'settings';
		$columnName = 'id_setting';
	}	
	else if($type=='prt') {
		$tableName = 'partners';
		$columnName = 'id_partner';
	}
	else if($type=='crbr') {
		$tableName = 'car_brands';
		$columnName = 'id_car_brand';
	}
	else if($type=='inf') {
		$tableName = 'information_blasts';
		$columnName = 'id';
	}
	else if($type=='usrlvl') {
		$tableName = 'user_levels';
		$columnName = 'id';
	}
	else if($type=='tst') {
		$tableName = 'testimonials';
		$columnName = 'id_testimonial';
	}
	else if($type=='imgsldr') {
		$tableName = 'sliders';
		$columnName = 'id_slider';
	}	
	else if($type=='brnch') {
		$tableName = 'branches';
		$columnName = 'id';
	}	
	
	$query 	= "select * from ".$tableName."
			   where ".$columnName."='".$id."'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);

	echo json_encode($data);
?>
