<?php
	include "check-admin-session.php";
	include "afis_call.php";
	
	$customerID	= sanitize_int($_REQUEST["customerID"]);

	$query_cus = "select c.customer_name,cl.agreement_number,cl.sisa_pinjaman,cl.branch   
			from customers c
			left join agreement_list cl on c.id_customer = cl.customer_id
			where c.id_customer = '$customerID'"; 
	$result_cus = mysqli_query($mysql_connection, $query_cus);
	$data_cus = mysqli_fetch_array($result_cus);

	$sisaPinjaman = number_format($data_cus[sisa_pinjaman],0);

	echo "<div class='col-lg-12' align='left' style='margin-top:10px;'>";
	echo "Agreement No &nbsp;&nbsp;&nbsp;&nbsp; : <strong>". $data_cus[agreement_number]."</strong><br>";
	echo "Nama Customer &nbsp;: <strong>". $data_cus[customer_name]."</strong><br>";
	echo "Sisa Pinjaman &nbsp;&nbsp;&nbsp;&nbsp; : <strong>". $sisaPinjaman."</strong><br><br>";
	echo "</div>";
	
	echo "<table class='table table-hover'>
			  <tr>
				  <th width='10%'>Number</th>
				  <th width='15%'>Date</th>
				  <th width='10%'>Periode</th>
				  <th width='15%'>Due Date</th>
				  <th width='10%'>Status</th>
				  <th width='10%'>Amount</th>
				  <th width='10%'>Fee</th>
				  <th width='10%'>Penalty</th>
				  <th width='10%'>Total</th>
				</tr>";
	$query = "select ph.transaction_code,ph.installment_number,DATE_FORMAT(ph.inquiry_date,'%d-%m-%Y' ) as inquiry_date,
			ph.periode,DATE_FORMAT(ph.due_date, '%d-%m-%Y') AS due_date,ph.amount,
			ph.admin_fee,ph.penalty,ph.total_amount,ph.payment_status  
			from payment_history ph
			where ph.id_customer = '$customerID' and ph.payment_status <> 'INQUIRY' 
			order by ph.inquiry_date ASC"; 
	$result = mysqli_query($mysql_connection, $query);
	while ($data = mysqli_fetch_array($result)) {
		$amount = number_format($data[amount],0);
		$admin_fee = number_format($data[admin_fee],0);
		$penalty = number_format($data[penalty],0);
		$total_amount = number_format($data[total_amount],0);
		
		echo '<tr>
				  <td>'.$data[installment_number].'</td>
				  <td>'.$data[inquiry_date].'</td>
				  <td>'.$data[periode].'</td>
				  <td>'.$data[due_date].'</td>
				  <td>'.$data[payment_status].'</td>
				  <td>'.$amount.'</td>
				  <td>'.$admin_fee.'</td>
				  <td>'.$penalty.'</td>
				  <td>'.$total_amount.'</td>
			  </tr>';
		$i++;
	}

	echo "</table>";
?>