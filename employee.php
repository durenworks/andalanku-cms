<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Konsumen</li>
			  <li>Data Pegawai</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-id-badge"></i> Data Pegawai</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
						<button type="button" class="btn btn-flat btn-success" onClick="saveToExcel();"><i class="fa fa-file-excel-o"></i> Export To Excel</button>
					</div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->
	
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Edit Pegawai</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-6">
				<div class="form-group">
					<label for="customer_name">Nama</label>
					<input type="hidden" id="customer_id" name="customer_id" value="0">
					<input type="text" name="customer_name" placeholder="Nama Lengkap" class="form-control" id="customer_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="nip">NIP</label>
					<input type="text" name="nip" placeholder="NIP" class="form-control" id="nip">
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="address">Alamat</label>
					<input type="text" name="address" placeholder="Alamat" class="form-control" id="address">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="rt">RT</label>
					<input type="text" name="rt" placeholder="RT" class="form-control" id="rt">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="rw">RW</label>
					<input type="text" name="rw" placeholder="RW" class="form-control" id="rw">
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
					<label for="zip_code">Kode Pos</label>
					<input type="text" name="zip_code" placeholder="Kode Pos" class="form-control" id="zip_code">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="province_id">Propinsi</label>
					<select name="province_id" id="province_id" class="form-control" onchange="getCity();">
						<?php 
							$query = "select * from provinces order by name ASC"; 
							$result = mysqli_query($mysql_connection, $query);
							while ($data = mysqli_fetch_array($result)) {
								echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
							}
						?>
					</select>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="regency_id">Kota / Kabupaten</label>
					<span id="spanCityList">
						<select name="regency_id" id="regency_id" class="form-control">
						</select>
					</span>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="district_id">Kecamatan</label>
					<span id="spanDistrictList">
						<select name="district_id" id="district_id" class="form-control">
						</select>
					</span>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="village_id">Kelurahan</label>
					<span id="spanVillageList">
						<select name="village_id" id="village_id" class="form-control">
						</select>
					</span>
				</div>
			</div>

			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
		<button data-remodal-action="confirm" class="remodal-confirm">OK</button>
	</div>

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>
	
	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
	<script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'keyword'	: $('input[name=keyword]').val(),
								'src_type'	: 'EMPLOYEE',
								'page'		: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_customer.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );
		var global_regency_id	= '';
		var global_district_id	= '';
		var global_village_id 	= '';
		
		$(document).on('confirmation', '.remodal', function() {

            var formData = {
                'customer_id'	: $('#customer_id').val(),
				'address'		: $('input[name=address]').val(),
				'rt'			: $('input[name=rt]').val(),
				'rw'			: $('input[name=rw]').val(),
				'zip_code'		: $('input[name=zip_code]').val(),
				'village_id'	: $('select[name=village_id]').val(),
				'district_id'	: $('select[name=district_id]').val(),
				'regency_id'	: $('select[name=regency_id]').val(),
				'province_id'	: $('select[name=province_id]').val()
            };

			$body.addClass("loadingClass");
			
			$.ajax({
				type: "POST",
				url: 'ajax_edit_customer.php',
				data: formData
			}).done(function(data) {

				data = $.trim(data);
				$body.removeClass("loadingClass");

				if (data == 'success') {
					$("#mainNotification").html(showNotification("success", "Data pegawai telah disimpan"));
					modalAdd.close();
					searchData(1);
					resetdata();
					setTimeout(clearnotif, 5000);

				} else if (data == 'empty') {
					$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
				} 
			});			
        });
		
		function resetdata() {
			
			$("#customer_id").val(0);
			$("#customer_name").val('');
			$("#nip").val('');
			$("#address").val('');
			$("#rt").val('');
			$("#rw").val('');
			$("#zip_code").val('');
			$("#province_id").val($("#province_id option:first").val());
			$('#spanCityList').html('<select name="regency_id" id="regency_id" class="form-control"></select>');
			$('#spanDistrictList').html('<select name="district_id" id="district_id" class="form-control"></select>');
			$('#spanVillageList').html('<select name="village_id" id="village_id" class="form-control"></select>');
				
			$("#modal1Title").html('Edit Pegawai');
			
			global_regency_id	= '';
			global_district_id	= '';
			global_village_id 	= '';
		}
		
		function getedit(customerID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_customer_detail.php',
                data: 'customerID=' + customerID,
                dataType: 'json'
            }).done(function(data) { 
			
				$("#customer_id").val(data.id_customer);
				$("#customer_name").val(data.customer_name);
				$("#nip").val(data.nip);
				$("#address").val(data.address);
				$("#rt").val(data.rt);
				$("#rw").val(data.rw);
				$("#zip_code").val(data.zip_code);
				$("#province_id").val(data.province_id);
				$('#spanDistrictList').html('<select name="district_id" id="district_id" class="form-control"></select>');
				$('#spanVillageList').html('<select name="village_id" id="village_id" class="form-control"></select>');
				$("#modal1Title").html('Edit Konsumen');
				
				global_regency_id	= data.regency_id;
				global_district_id	= data.district_id;
				global_village_id 	= data.village_id;
				
				getCity(true);
							
            	$body.removeClass("loadingClass");
            });
        }
		
		function getCity(getNext=false) {

			$body.addClass("loadingClass");

			var formData = { 'province_id' : $('select[name=province_id]').val() };

            $.ajax({
                type: "POST",
                url: 'ajax_search_city_combo.php',
                data: formData
            }).done(function(data) {
                $('#spanCityList').html(data);
				$('#spanDistrictList').html('<select name="district_id" id="district_id" class="form-control"></select>');
				$('#spanVillageList').html('<select name="village_id" id="village_id" class="form-control"></select>');
                $body.removeClass("loadingClass");
				if(global_regency_id != '') $('select[name=regency_id]').val(global_regency_id);				
				if(getNext) getDistrict(getNext);
            });
        }
		
		function getDistrict(getNext=false) {

			$body.addClass("loadingClass");

			var formData = { 'regency_id' : $('select[name=regency_id]').val() };

            $.ajax({
                type: "POST",
                url: 'ajax_search_district_combo.php',
                data: formData
            }).done(function(data) {
                $('#spanDistrictList').html(data);
                $('#spanVillageList').html('<select name="village_id" id="village_id" class="form-control"></select>');
                $body.removeClass("loadingClass");
				if(global_district_id != '') $('select[name=district_id]').val(global_district_id);				
				if(getNext) getVillage(getNext);
            });
        }
		
		function getVillage(getNext=false) {

			$body.addClass("loadingClass");

			var formData = { 'district_id' : $('select[name=district_id]').val() };

            $.ajax({
                type: "POST",
                url: 'ajax_search_village_combo.php',
                data: formData
            }).done(function(data) {
                $('#spanVillageList').html(data);
                $body.removeClass("loadingClass"); 
				if(global_village_id != '') $('select[name=village_id]').val(global_village_id);
            });
        }

        function deleteCustomer(customerID,customerName){

        	if (confirm('Anda yakin akan menghapus pegawai ' + customerName + '?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_delete_customer.php',
                    data: 'customerID=' + customerID
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Data pegawai telah dihapus"));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	} 
                });
            }
        }
		
		function saveToExcel() {
			
			var keyword  = $('input[name=keyword]').val();
			var src_type = 'EMPLOYEE';
			window.open("excel_customer.php?keyword="+keyword+"&src_type="+src_type);
		}
		
	</script>

  </body>
</html>
