<?php
	include "check-admin-session.php";

	$profileUpdateID = sanitize_int($_REQUEST["profileUpdateID"]);
	
	$query 	= "select a.*, b.customer_name, b.place_of_birth, b.date_of_birth 
			   from profile_update_request a 
			   left join customers b on a.user_id=b.id_customer 
			   where a.id='$profileUpdateID'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
	
	$data['request_date'] = date("d-m-Y H:i:s", strtotime($data['request_date']));
	if($data['date_of_birth'] <> '') $data['date_of_birth'] = date("d-m-Y", strtotime($data['date_of_birth']));
	
	$userID = $data['user_id'];	
	
	//cek perubahan data alamat apa saja 
	$queryCheck = "select * from profile_update_request_address where profile_update_request_id='$profileUpdateID' ";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	
	$legalChange	= false;
	$domicileChange	= false;
	$officeChange	= false;
	
	while($dataCheck = mysqli_fetch_array($resultCheck)) {
		
		if($dataCheck['address_type']=='LEGAL') $legalChange = true;
		else if($dataCheck['address_type']=='DOMICILE') $domicileChange = true;
		else if($dataCheck['address_type']=='OFFICE') $officeChange = true;
	}
	
	//========== LEGAL ==========
	$queryAddress = '';
	//Alamat lama
	if($data['status'] == 'APPROVE' && $legalChange) {
		$queryAddress  = "select b.address, b.rt, b.rw,
						  f.name as village_name, e.name as district_name, 
						  d.name as regency_name, c.name as province_name, 
						  b.zip_code 
						  from address_customers a 
						  left join address b on a.address_id=b.id 
						  left join provinces c on b.province_id=c.id 
						  left join regencies d on b.regency_id=d.id
						  left join districts e on b.district_id=e.id
						  left join villages f on b.village_id=f.id
						  where a.address_id='".$data['prev_legal_address_id']."' ";

	}
	else if($legalChange) {
		$queryAddress  = "select b.address, b.rt, b.rw,
						  f.name as village_name, e.name as district_name, 
						  d.name as regency_name, c.name as province_name, 
						  b.zip_code 
						  from address_customers a 
						  left join address b on a.address_id=b.id 
						  left join provinces c on b.province_id=c.id 
						  left join regencies d on b.regency_id=d.id
						  left join districts e on b.district_id=e.id
						  left join villages f on b.village_id=f.id
						  where a.is_active='1' 
						  and a.user_id='$userID'
						  and a.address_type='LEGAL' ";
	}
	
	$resultAddress = mysqli_query($mysql_connection, $queryAddress);
	
	$data['old_address'] = '';
	
	if(mysqli_num_rows($resultAddress) > 0) {
		
		$dataAddress = mysqli_fetch_array($resultAddress);
		
		if($dataAddress['address'] <> '') {
			
			$old_address = $dataAddress['address']."\r\n".
						   'RT '.$dataAddress['rt'].' RW '.$dataAddress['rw']."\r\n".
						   $dataAddress['village_name'].', '.$dataAddress['district_name']."\r\n".
						   $dataAddress['regency_name'].', '.$dataAddress['province_name']."\r\n".
						   $dataAddress['zip_code'];
						   
			$data['old_address'] = $old_address;
		}
	}
	
	//Alamat baru
	$queryAddress  = "select c.address, c.rt, c.rw,
						g.name as village_name, f.name as district_name, 
						e.name as regency_name, d.name as province_name, 
						c.zip_code 
						from profile_update_request a 
						left join profile_update_request_address b on a.id=b.profile_update_request_id
						left join address c on b.address_id=c.id
						left join provinces d on c.province_id=d.id
						left join regencies e on c.regency_id=e.id
						left join districts f on c.district_id=f.id
						left join villages g on c.village_id=g.id
						where a.user_id='$userID'
						and b.address_type='LEGAL' 
						and a.id='$profileUpdateID'";
	$resultAddress = mysqli_query($mysql_connection, $queryAddress);
	
	$data['new_address'] = '';
	
	if(mysqli_num_rows($resultAddress) > 0) {
		
		$dataAddress = mysqli_fetch_array($resultAddress);
		
		if($dataAddress['address'] <> '') {
			
			$new_address = $dataAddress['address']."\r\n".
						   'RT '.$dataAddress['rt'].' RW '.$dataAddress['rw']."\r\n".
						   $dataAddress['village_name'].', '.$dataAddress['district_name']."\r\n".
						   $dataAddress['regency_name'].', '.$dataAddress['province_name']."\r\n".
						   $dataAddress['zip_code'];	
						   
			$data['new_address'] = $new_address;
		}
	}
	
	//========== DOMISILI ==========
	$queryAddress = '';
	//Alamat lama
	if($data['status'] == 'APPROVE' && $domicileChange) {
		$queryAddress  = "select b.address, b.rt, b.rw,
						  f.name as village_name, e.name as district_name, 
						  d.name as regency_name, c.name as province_name, 
						  b.zip_code 
						  from address_customers a 
						  left join address b on a.address_id=b.id 
						  left join provinces c on b.province_id=c.id 
						  left join regencies d on b.regency_id=d.id
						  left join districts e on b.district_id=e.id
						  left join villages f on b.village_id=f.id
						  where a.address_id='".$data['prev_domicile_address_id']."' ";

	}
	else if($domicileChange) {
		$queryAddress  = "select b.address, b.rt, b.rw,
						  f.name as village_name, e.name as district_name, 
						  d.name as regency_name, c.name as province_name, 
						  b.zip_code 
						  from address_customers a 
						  left join address b on a.address_id=b.id 
						  left join provinces c on b.province_id=c.id 
						  left join regencies d on b.regency_id=d.id
						  left join districts e on b.district_id=e.id
						  left join villages f on b.village_id=f.id
						  where a.is_active='1' 
						  and a.user_id='$userID'
						  and a.address_type='DOMICILE' ";
	}
	$resultAddress = mysqli_query($mysql_connection, $queryAddress);
	
	$data['old_address_domicile'] = '';
	
	if(mysqli_num_rows($resultAddress) > 0) {
		
		$dataAddress = mysqli_fetch_array($resultAddress);
		
		if($dataAddress['address'] <> '') {
			
			$old_address = $dataAddress['address']."\r\n".
						   'RT '.$dataAddress['rt'].' RW '.$dataAddress['rw']."\r\n".
						   $dataAddress['village_name'].', '.$dataAddress['district_name']."\r\n".
						   $dataAddress['regency_name'].', '.$dataAddress['province_name']."\r\n".
						   $dataAddress['zip_code'];
						   
			$data['old_address_domicile'] = $old_address;
		}
	}
	
	//Alamat baru
	$queryAddress  = "select c.address, c.rt, c.rw,
						g.name as village_name, f.name as district_name, 
						e.name as regency_name, d.name as province_name, 
						c.zip_code 
						from profile_update_request a 
						left join profile_update_request_address b on a.id=b.profile_update_request_id
						left join address c on b.address_id=c.id
						left join provinces d on c.province_id=d.id
						left join regencies e on c.regency_id=e.id
						left join districts f on c.district_id=f.id
						left join villages g on c.village_id=g.id
						where a.user_id='$userID'
						and b.address_type='DOMICILE' 
						and a.id='$profileUpdateID'";
	$resultAddress = mysqli_query($mysql_connection, $queryAddress);
	
	$data['new_address_domicile'] = '';
	
	if(mysqli_num_rows($resultAddress) > 0) {
		
		$dataAddress = mysqli_fetch_array($resultAddress);
		
		if($dataAddress['address'] <> '') {
			
			$new_address = $dataAddress['address']."\r\n".
						   'RT '.$dataAddress['rt'].' RW '.$dataAddress['rw']."\r\n".
						   $dataAddress['village_name'].', '.$dataAddress['district_name']."\r\n".
						   $dataAddress['regency_name'].', '.$dataAddress['province_name']."\r\n".
						   $dataAddress['zip_code'];	
						   
			$data['new_address_domicile'] = $new_address;
		}
	}
	
	//========== OFFICE ==========
	$queryAddress = '';
	//Alamat lama
	if($data['status'] == 'APPROVE' && $officeChange) {
		$queryAddress  = "select b.address, b.rt, b.rw,
						  f.name as village_name, e.name as district_name, 
						  d.name as regency_name, c.name as province_name, 
						  b.zip_code 
						  from address_customers a 
						  left join address b on a.address_id=b.id 
						  left join provinces c on b.province_id=c.id 
						  left join regencies d on b.regency_id=d.id
						  left join districts e on b.district_id=e.id
						  left join villages f on b.village_id=f.id
						  where a.address_id='".$data['prev_office_address_id']."' ";

	}
	else if($officeChange) {
		$queryAddress  = "select b.address, b.rt, b.rw,
						  f.name as village_name, e.name as district_name, 
						  d.name as regency_name, c.name as province_name, 
						  b.zip_code 
						  from address_customers a 
						  left join address b on a.address_id=b.id 
						  left join provinces c on b.province_id=c.id 
						  left join regencies d on b.regency_id=d.id
						  left join districts e on b.district_id=e.id
						  left join villages f on b.village_id=f.id
						  where a.is_active='1' 
						  and a.user_id='$userID'
						  and a.address_type='OFFICE' ";
	}
	$resultAddress = mysqli_query($mysql_connection, $queryAddress);
	
	$data['old_address_office'] = '';
	
	if(mysqli_num_rows($resultAddress) > 0) {
		
		$dataAddress = mysqli_fetch_array($resultAddress);
		
		if($dataAddress['address'] <> '') {
			
			$old_address = $dataAddress['address']."\r\n".
						   'RT '.$dataAddress['rt'].' RW '.$dataAddress['rw']."\r\n".
						   $dataAddress['village_name'].', '.$dataAddress['district_name']."\r\n".
						   $dataAddress['regency_name'].', '.$dataAddress['province_name']."\r\n".
						   $dataAddress['zip_code'];
						   
			$data['old_address_office'] = $old_address;
		}
	}
	
	//Alamat baru
	$queryAddress  = "select c.address, c.rt, c.rw,
						g.name as village_name, f.name as district_name, 
						e.name as regency_name, d.name as province_name, 
						c.zip_code 
						from profile_update_request a 
						left join profile_update_request_address b on a.id=b.profile_update_request_id
						left join address c on b.address_id=c.id
						left join provinces d on c.province_id=d.id
						left join regencies e on c.regency_id=e.id
						left join districts f on c.district_id=f.id
						left join villages g on c.village_id=g.id
						where a.user_id='$userID'
						and b.address_type='OFFICE' 
						and a.id='$profileUpdateID'";
	$resultAddress = mysqli_query($mysql_connection, $queryAddress);
	
	$data['new_address_office'] = '';
	
	if(mysqli_num_rows($resultAddress) > 0) {
		
		$dataAddress = mysqli_fetch_array($resultAddress);
		
		if($dataAddress['address'] <> '') {
			
			$new_address = $dataAddress['address']."\r\n".
						   'RT '.$dataAddress['rt'].' RW '.$dataAddress['rw']."\r\n".
						   $dataAddress['village_name'].', '.$dataAddress['district_name']."\r\n".
						   $dataAddress['regency_name'].', '.$dataAddress['province_name']."\r\n".
						   $dataAddress['zip_code'];	
						   
			$data['new_address_office'] = $new_address;
		}
	}
	
	echo json_encode($data);
?>
