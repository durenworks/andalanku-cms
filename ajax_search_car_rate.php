<?php
	include "check-admin-session.php";

	$query 	= "select * from car_rates order by id_car_rate ASC ";
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
					<th width='5%'>No</th>
					<th>Tenor</th>
					<th>Rate New</th>
					<th>Flat Rate New</th>
					<th>Rate Used</th>
					<th>Flat Rate Used</th>
					<th>Admin</th>
				</tr>	";

	$i = 1;

	while ($data = mysqli_fetch_array($result)) {
	
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data['tenor'] . '</td>
				  <td>' . number_format($data['rate_new'],2,',','.') . '%</td>
				  <td>' . number_format($data['flat_rate_new'],2,',','.') . ' %</td>
				  <td>' . number_format($data['rate_used'],2,',','.') . '%</td>
				  <td>' . number_format($data['flat_rate_used'],2,',','.') . '%</td>
				  <td>' . number_format($data['admin'],0,',','.') . '</td>				  
				</tr>';
		$i++;
	}

	echo "</table>";
?>
