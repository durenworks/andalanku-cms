<?php
	include "check-admin-session.php";
	
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Data_Komplain_Andalanku.xls");
	
	echo "Data Complain Andalanku<br><br>";

	$status 		= sanitize_sql_string($_REQUEST["src_status"]);
	$category 		= sanitize_sql_string($_REQUEST["src_category"]);
	$searchDate		= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$page 			= sanitize_int($_REQUEST["page"]);

	//var_dump($keyword);

	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";
	} else {
		$startDate	= date("Y-m")."-01 00:00:00";
		$endDate	= date("Y-m")."-31 23:59:59";
	}
			
	$query 	= "select a.*, b.customer_name, b.andalan_customer_id
			   from complains a 
			   left join customers b on a.user_id=b.id_customer 
			   where (message like '%$keyword%') and submitted_date>='$startDate' and submitted_date<='$endDate' ";
	if($status <> '') $query = $query." and a.status='$status' ";
	if($category <> '') $query = $query." and category='$category' ";
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='20%'>Nomor Tiket</th>
				  <th width='10%'>Tanggal Komplain</th>
				  <th width='15%'>Kategori</th>
				  <th width='15%'>Nama</th>				  
				  <th width='15%'>No Handphone</th>
				  <th width='15%'>Nomor Kontrak</th>
				  <th width='15%'>Cabang</th>
				  <th width='30%'>Pesan</th>	
				  <th width='15%'>Status</th>
				</tr>	";

	$i = 1;

	while ($data = mysqli_fetch_array($result)) {
		
		//----- Ambil Nomor Kontrak ------//
		$agreement_number = '';
		$query_agrrement = "select agreement_number, branch from agreement_list where customer_id = '$data[user_id]'";
		$result_agreement = mysqli_query($mysql_connection, $query_agrrement);
		
		while ($data_agreement = mysqli_fetch_assoc($result_agreement)) {

			if ($data_agreement[branch] == '') {
				
				$api_url  	   = $afis_api_url.'/Agreement/ActiveAgrementNumber/'.$data[andalan_customer_id];
				$ch = curl_init();

				curl_setopt($ch,CURLOPT_URL, $api_url);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

				$result_afis = curl_exec($ch);
				$result_afis = json_decode($result_afis,true);

				$responseArray = $result_afis['Response']['Data'];

				
				$nowSQL = date("Y-m-d H:i:s");

				foreach($responseArray as $key=>$value) {
					
					$agreementNo = trim($value['agreementNo']);

					if ($data_agreement[agreement_number] == $agreementNo) {
						$branch = trim($value['branchfullname']);
						
						$query = "update agreement_list set branch='$branch ' where agreement_number='$data_agreement[agreement_number]'";
						mysqli_query($mysql_connection, $query);
					}
				}
			}
			else {
				$branch = $data_agreement[branch];
			}

			if($agreement_number != '') {
				$agreement_number = $agreement_number . ', ' . $data_agreement[agreement_number];
				$branch_stack = $branch_stack . ', ' . $branch;
			}
			else {
				$agreement_number = $data_agreement[agreement_number];
				$branch_stack = $branch;
			}
		}

		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data[ticket_number] . '</td>
				  <td>' . date("d-m-Y H:i:s", strtotime($data[submitted_date])) . '</td>
				  <td>' . $data[category] . '</td>
				  <td>' . $data[customer_name] . '</td>
				  <td>\'' . $data[phone] . '</td>
				  <td>\'' . $agreement_number. '</td>
				  <td>' . $branch_stack. '</td>
				  <td>' . $data [message]. '</td>
				  <td>' . $data[status] . '</td>
			  </tr>';
		$i++;
	}

	echo "</table>";
?>