<?php
	include "check-admin-session.php";

	$loanApplicationID = sanitize_int($_REQUEST["loanApplicationID"]);
	
	$query 	= "select a.*, b.customer_name, b.phone_number, c.name as branch_name, b.customer_name as referal_name  
			   from loan_applications a 
			   left join customers b on a.customer_id=b.id_customer 
			   left join branches c on a.branch_id=c.id
			   where a.id='$loanApplicationID'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result); $data['sql'] = $query;
	
	if($data['loan_application_referal'] == 'Y') {
		$data['customer_name'] = $data['loan_customer_name'];
		$data['phone_number']  = $data['loan_phone_number'];
	} else {
		$data['referal_name'] = '-';
	}

	$data['date'] 			= date("d-m-Y H:i:s", strtotime($data['date']));
	$data['otr']			= number_format($data['otr'],2,',','.');
	$data['dp_amount']		= number_format($data['dp_amount'],2,',','.');
	$data['plafond']		= number_format($data['plafond'],2,',','.');
	$data['survey_date']	= date("d-m-Y H:i:s", strtotime($data['survey_date']));
	
	echo json_encode($data);
?>
