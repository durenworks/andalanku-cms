<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Master Data</li>
			  <li>Cabang</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-building-o"></i> Cabang</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->


	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Edit Cabang</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-12">
				<div class="form-group">
					<label for="name">Nama Cabang</label>
					<input type="hidden" id="branch_id" name="branch_id" value="0">
					<input type="text" name="name" class="form-control" id="name" readonly="readonly">
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="name">Branch manager</label>
					<input type="text" name="branch_manager" class="form-control" id="branch_manager">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="address">Alamat Cabang</label>
					<textarea name="address" class="form-control" id="address" readonly="readonly"></textarea>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="latitude">Latitude</label>
					<input type="text" name="latitude" class="form-control" id="latitude" placeholder="Latitude">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="longitude">Longitude</label>
					<input type="text" name="longitude" class="form-control" id="longitude" placeholder="Longitude">
				</div>
			</div>

			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
		<button data-remodal-action="confirm" class="remodal-confirm">OK</button>
	</div>

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'keyword'	: $('input[name=keyword]').val(),
								'page'		: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_branch.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);

		var modalAdd = $('[data-remodal-id=modal]').remodal( );

        $(document).on('confirmation', '.remodal', function() {

            var formData = {
                'branch_id'	: $('#branch_id').val(),
				'branch_manager' : $('input[name=branch_manager]').val(),
				'latitude'	: $('input[name=latitude]').val(),
				'longitude'	: $('input[name=longitude]').val()
            };

			$body.addClass("loadingClass");

			if ($("#branch_id").val() != '0') {
				
				$.ajax({
					type: "POST",
					url: 'ajax_edit_branch.php',
					data: formData
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data cabang telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					}

				});
			}
			
        });

		function resetdata() {
            $("#branch_id").val(0);
			$("#name").val("");
			$("#branch_manager").val("");
			$("#address").val("");
            $("#latitude").val("");
			$("#longitude").val("");
            $("#modal1Title").html('Edit Cabang');
            $("#addNotification").html("");
        }

        function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

		function getedit(branchID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_detail.php',
                data: 'type=brnch&id=' + branchID,
                dataType: 'json'
            }).done(function(data) { 
			
				$("#branch_id").val(data.id);
				$("#name").val(data.name);
				$("#branch_manager").val(data.branch_manager);
				$("#address").val(data.address+"\n"+data.city+", "+data.provinsi+" "+data.zipcode);
				$("#latitude").val(data.latitude);
				$("#longitude").val(data.longitude);
				$("#modal1Title").html('Edit Cabang');
            	$body.removeClass("loadingClass");
            });
        }

        $(document).on('closing', '.remodal', function(e) {
            resetdata();
        });


	</script>

  </body>
</html>
