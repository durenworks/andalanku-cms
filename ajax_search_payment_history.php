<?php
	include "check-admin-session.php";

	$status 		= sanitize_sql_string($_REQUEST["src_status"]);
	$searchDate		= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$page 			= sanitize_int($_REQUEST["page"]);

	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";

		
	} else {
		$startDate	= date("Y-m-d")." 00:00:00";
		$endDate	= date("Y-m-d")." 23:59:59";		
	}

	$sql_date	= "and ph.inquiry_date between '$startDate' and '$endDate'";
	
	
	if($status!='') {
		$sql_status = "and ph.payment_status = '$status'";
	}
	
	if($keyword!='') {
		$sql_key = "and (c.customer_name like '%$keyword%' or cl.agreement_number like '%$keyword%')";
	}

	$query 			= "select COUNT(*) as num
						from transaction_installment ph
						left join espay_payment ep on ep.payment_ref = ph.espay_payment_reff
						left join customers c on ph.id_customer = c.id_customer 
						left join agreement_list cl on ph.id_customer = cl.customer_id and ph.contract_no = cl.agreement_number 
						where c.andalan_customer_id is not null and ph.payment_status <> 'INQUIRY' 
						$sql_key $sql_date $sql_status";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select cl.agreement_number,cl.branch,cl.sisa_pinjaman,c.customer_name,c.phone_number,
			ph.id_customer,ph.transaction_code,ph.installment_number,
			DATE_FORMAT(ph.inquiry_date,'%d-%m-%Y %H:%i' ) as inquiry_date,
			ph.periode,DATE_FORMAT(ph.due_date, '%d-%m-%Y') AS due_date,ph.amount,
			DATE_FORMAT(ep.payment_datetime,'%d-%m-%Y %H:%i' ) as payment_date,
			ph.admin_fee,ph.penalty,ph.total_amount,ph.payment_status , ph.installment_number as angsuran 
			from transaction_installment ph
			left join espay_payment ep on ep.payment_ref = ph.espay_payment_reff
			left join customers c on ph.id_customer = c.id_customer 
			left join agreement_list cl on ph.id_customer = cl.customer_id and ph.contract_no = cl.agreement_number 
			where c.andalan_customer_id is not null and ph.payment_status <> 'INQUIRY' 
			$sql_key $sql_date $sql_status 
			order by payment_date DESC,c.customer_name LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='8%'>Nomor Kontrak</th>
				  <th width='10%'>Nama</th>
				  <th width='8%'>Telefon</th>
				  <th width='10%'>Branch</th>
				  <th width='8%'>Angsuran</th>
				  <th width='10%'>Transaction Code</th> 
				  <th width='8%'>Inquiry Date</th>
				  <th width='8%'>Payment Date</th>
				  <th width='8%'>Total Amount</th>	
				  <th width='12%'>Payment Status</th>				  			  
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {
		
		$customer_id = $data['id_customer'];
		$sisa_pinjaman = number_format($data[sisa_pinjaman],0);
		$total_amount = number_format($data[total_amount],0);
		if ($data['payment_status'] == "PAYMENT"){ $payment_status = "Menunggu Pembayaran"; }
		else if ($data['payment_status'] == "PAYMENT DONE"){ $payment_status = "Dibayar"; }
		else{ $payment_status = "Pembayaran Gagal"; }

		echo '<tr>
				  <td align="right">'.$i.'.</th>
				  <td>' . $data[agreement_number] . '</td>
				  <td>' . $data[customer_name] . '</td>
				  <td>' . $data[phone_number] . '</td>
				  <td>' . $data[branch] . '</td>
				  <td>' .  $data[angsuran] . '</td>	
				  <td>' . $data[transaction_code] . '</td> 
				  <td>' . $data[inquiry_date] . '</td>
				  <td>' . $data[payment_date] . '</td>
				  <td>' . $total_amount . '</td>
				  <td>' . $payment_status . '</td>				  
			  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>