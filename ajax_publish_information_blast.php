<?php
	include "check-admin-session.php";
	
	$userID		= $_SESSION['userID'];
	$id_info 	= sanitize_int($_REQUEST["infoID"]);

	if ($id_info <> '0') {
		
		$queryInfo 	= "select * from information_blasts where id='$id_info'";
		$resultInfo	= mysqli_query($mysql_connection, $queryInfo);
		$dataInfo	= mysqli_fetch_array($resultInfo);
		
		$title				= $dataInfo["title"];
		$content_message	= $dataInfo["content"];	
		
		// simpan history
		$now = date("Y-m-d H:i:s");
		
        $query = "INSERT INTO information_blast_history(info_id, published_date, user_id, title, content) 
				  VALUES ('$id_info', '$now', '$userID', '$title', '$content_message')";
        mysqli_query($mysql_connection, $query);
		
		//==================== INBOX ====================
		$content			= array();
		$content['message']	= $content_message;
		
		$content = json_encode($content);
		
		//insert ke tabel inbox (customer_id = -1 untuk semua customer)
		$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
						values('-1', '$now', 'information blast', '$title', '0', 
						'$content')";
		mysqli_query($mysql_connection, $queryInsert);
		//===============================================

		//==================== FCM PUSH NOTIF ============
		$json_data = [
			"to" => '/topics/ALL_REGISTERED_USER',
			"notification" => [
				"body" => $content_message,
				"title" => $title,
				"icon" => "https://devapi.andalanku.id/img/ic_notif_transparant.png",
				"click_action" => "OPEN_INBOX"
			]
		];
		$data = json_encode($json_data);
		//FCM API end-point
		$url = 'https://fcm.googleapis.com/fcm/send';
		//api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
		$server_key = 'AAAAS-tTObY:APA91bEMdHLHqYlKKwbS0B8GN2cy84hJn-PqsPV-Xsr4_SoKFjp96ccTw9BwNBsY48Gz176_Dot409_pGVhtSO9FjNTOKSJLltLvAiaaXRTYmmh21shmOIoXIEAH4j1z6vybmGU6K-is';
		//header with content_type api key
		$headers = array(
			'Content-Type:application/json',
			'Authorization:key='.$server_key
		);
		//CURL request to route notification to FCM connection server (provided by Google)
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$result = curl_exec($ch);
		//echo $result;
		if ($result === FALSE) {
			echo 'empty';
			die('Oops! FCM Send Error: ' . curl_error($ch));
		}
		curl_close($ch);
		//================================================

		echo 'success';
		exit;
		
	} else {
		echo "empty";
		exit;
	}
?>
