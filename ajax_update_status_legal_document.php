<?php
	include "check-admin-session.php";

	$userID				= $_SESSION['userID'];
	$loginTypeName		= $_SESSION['loginTypeName'];
	$id_legal_document	= sanitize_int($_REQUEST["id_legal_document"]);
	$legal_type			= sanitize_sql_string($_REQUEST["legal_type"]);
	$status				= sanitize_sql_string($_REQUEST["status"]);
	
	if($loginTypeName <> 'SUPERADMIN') {
		echo 'insufficient_access';
		exit;
	}

	if ($id_legal_document <> '0' && $status <> '') {
	
		$now = date("Y-m-d H:i:s");	

		if($status == 'PUBLISH') {
			
			$query = "	UPDATE legal_documents set is_published='1', 
						published_by='$userID', published_date='$now' 
						where id_legal_document='$id_legal_document' ";
			mysqli_query($mysql_connection, $query);
			
			// update yang lain menjadi is_published = false untuk tipe yang sama
			$query = "	UPDATE legal_documents set is_published='0', 
						published_by=null , published_date=null  
						where id_legal_document!='$id_legal_document' and type='$legal_type' ";
			mysqli_query($mysql_connection, $query);
		}
		else {
			
			if($status == 'APPROVE') $status = 'APPROVED';
			else $status = 'REJECTED';
				
			$query = "	UPDATE legal_documents set status='$status', 
						status_change_by='$userID', status_change_date='$now' 
						where id_legal_document='$id_legal_document' ";
			mysqli_query($mysql_connection, $query);
		}

		echo 'success';
		exit;
		
	} else {
		echo "empty";
		exit;
	}
?>
