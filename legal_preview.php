<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";
	
	$fullname 		= $_SESSION['fullname'];
	$id_legal_document = sanitize_int($_REQUEST['id']);
	
	$query 	= "select a.*, b.fullname as input_by_name, 
			   c.fullname as edit_by_name, 
			   d.fullname as status_change_by_name,
			   e.fullname as published_by_name 
			   from legal_documents a 
			   left join users b on a.input_by=b.user_id  
			   left join users c on a.edit_by=c.user_id 
			   left join users d on a.status_change_by=d.user_id 
			   left join users e on a.published_by=e.user_id 
			   where id_legal_document='$id_legal_document' ";
	$result = mysqli_query($mysql_connection, $query);
	$data   = mysqli_fetch_array($result);
	
	if($data['type'] == 'TOS') {
		
		$legal_type 	= "TOS";
		$legal_link 	= "tos";
		$legal_caption	= "Term of Service";
	}
	else {
		
		$legal_type 	= "TOS";
		$legal_link 	= "tos";
		$legal_caption	= "Term of Service";
	}
	
	if($data['is_published'] == '1') $is_published = 'Sudah Terbit';
	else $is_published = 'Belum Terbit';
?>

<link href="plugins/summernote/summernote.css" rel="stylesheet">

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
         <!-- Main content -->
          <section class="content">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Preview <?php echo $legal_caption; ?></h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>
				
				<div class="row form-group">
					<div class="col-md-3">
						<label>Tanggal Input</label>
						<input type="text" value="<?php echo $data['date']; ?>" readonly="true" class="form-control"/>
					</div>
					<div class="col-md-3">
						<label>Diinput Oleh</label>
						<input type="text" value="<?php echo $data['input_by_name']; ?>" readonly="true" class="form-control"/>
					</div>
					<div class="col-md-3">
						<label>Tanggal Edit</label>
						<input type="text" value="<?php echo $data['edit_date']; ?>" readonly="true" class="form-control"/>
					</div>
					<div class="col-md-3">
						<label>Diedit Oleh</label>
						<input type="text" value="<?php echo $data['edit_by_name']; ?>" readonly="true" class="form-control"/>
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-md-3">
						<label>Status</label>
						<input type="text" value="<?php echo $data['status']; ?>" readonly="true" class="form-control"/>
					</div>
					<div class="col-md-3">
						<label>Diupdate Oleh</label>
						<input type="text" value="<?php echo $data['status_change_by_name']; ?>" readonly="true" class="form-control"/>
					</div>
					<div class="col-md-3">
						<label>Tanggal Update</label>
						<input type="text" value="<?php echo $data['status_change_date']; ?>" readonly="true" class="form-control"/>
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-md-3">
						<label>Status Publikasi</label>
						<input type="text" value="<?php echo $is_published; ?>" readonly="true" class="form-control"/>
					</div>
					<div class="col-md-3">
						<label>Dipublish Oleh</label>
						<input type="text" value="<?php echo $data['published_by_name']; ?>" readonly="true" class="form-control"/>
					</div>
					<div class="col-md-3">
						<label>Tanggal Publish</label>
						<input type="text" value="<?php echo $data['published_date']; ?>" readonly="true" class="form-control"/>
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-md-12">
						<label>Isi <?php echo $legal_caption; ?></label>
						<textarea name="content" id="content" rows="8" class="form-control" placeholder="Isi <?php echo $legal_caption; ?>"><?php echo $data['content']; ?></textarea>
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-12 text-center">
						<button type="button" style="width:100px" class="btn btn-warning btn-large" onclick="cancel();">Tutup</button>
					</div>
				</div>

				<br><br><br>

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

    <script src="js/inc-function.js"></script>
	
	<script src="plugins/summernote/summernote.js"></script>
	
    <script type="text/javascript">
	
		$(document).ready(function() {
		  $('#content').summernote({
			height: 300
		  });
		  
		   $('#content').summernote('disable');
		});

    	function cancel() {
    		window.close();
    	}
		
    </script>

  </body>
</html>
