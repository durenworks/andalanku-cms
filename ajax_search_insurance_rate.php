<?php
	include "check-admin-session.php";

	$query 	= "select * from insurance_rates order by id_insurance_rate ASC ";
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
					<th width='5%'>No</th>
					<th>Asuransi</th>
					<th>OTR Range Min</th>
					<th>OTR Range Max</th>
					<th>Wilayah 1</th>
					<th>Wilayah 2</th>
					<th>Wilayah 3</th>
				</tr>	";

	$i = 1;

	while ($data = mysqli_fetch_array($result)) {
	
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data['asuransi'] . '</td>
				  <td>' . number_format($data['otr_range_min'],0,',','.') . '</td>
				  <td>' . number_format($data['otr_range_max'],0,',','.') . '</td>
				  <td>' . number_format($data['wilayah_1'],2,',','.') . ' %</td>
				  <td>' . number_format($data['wilayah_2'],2,',','.') . ' %</td>
				  <td>' . number_format($data['wilayah_3'],2,',','.') . ' %</td>
				</tr>';
		$i++;
	}

	echo "</table>";
?>
