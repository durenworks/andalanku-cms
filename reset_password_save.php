<?php

include "inc-db.php";
include "sanitize.inc.php";

$captcha=$_POST['g-recaptcha-response'];
$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcTPJoUAAAAABrzQhyvx7wdkpZEGlL76U9cQGq_&response=".$captcha);
$g_response = json_decode($response);
if($g_response->success!==true) { 
	//invalid captcha
	$location = "location:reset_password?c=".$password_code."&e=05";
	header($location);
	exit;
}

$password_code		= sanitize_sql_string(trim($_REQUEST["password_code"]));
$password			= sanitize_sql_string(trim($_REQUEST["password"]));
$password_confirm	= sanitize_sql_string(trim($_REQUEST["password_confirm"]));

if ($password_code <> '' and $password <> '' and $password_confirm <> '') {

	if($password <> $password_confirm) {
		header("location:reset_password?c=".$password_code."&e=02");
		exit;
	}

	if(strlen($password) < 8) {
		header("location:reset_password?c=".$password_code."&e=03");
		exit;
	}
	
	$queryCheck = "select id_customer from customers where password_code='$password_code'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) == 0) {
		
		header("location:reset_password?c=".$password_code."&e=04");
		exit;
	}
	
	$password 	  = md5($password);
	$dataCheck	  = mysqli_fetch_array($resultCheck);
	$id_customer  = $dataCheck['id_customer'];
	
	$queryUpdate  = "update customers set password='$password', password_code='' where id_customer='$id_customer'";
	$resultUpdate = mysqli_query($mysql_connection, $queryUpdate);
	
	header("location:reset_password?c=".$password_code."&e=06");
	exit;

} else {

    header("location:reset_password?c=".$password_code."&e=01");
    exit;
}
?>
