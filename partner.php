<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Display</li>
			  <li>Partner</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-handshake-o"></i> Partner</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
						<a href="#modal">
							<button type="button" class="btn btn-primary  btn-flat " ><i class="fa fa-plus"></i> Tambah Partner</button>
						</a>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->


	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Tambah Partner</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-12">
				<div class="form-group">
					<label for="name">Nama</label>
					<input type="hidden" id="id_partner" name="id_partner" value="0">
					<input type="hidden" id="oldImg" name="oldImg" value="">
					<input type="text" name="name" placeholder="Nama" class="form-control" id="name">
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="url">URL</label>
					<input type="text" name="url" placeholder="URL" class="form-control" id="url">
				</div>
			</div>

			<div class="col-md-12">
				<label>Gambar</label>
				<table width="100%" class='table'>
					<tr>
						<td width="150px" align="center">
							<img id="imageView1" src="img/none.png" style="width: 90px;" alt="Gambar"/>
						</td>
						<td>
							<input type='file' name="imageInput1" id="imageInput1" accept='image/*'/>
						</td>
					</tr>
				</table>
			</div>

			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
		<button data-remodal-action="confirm" class="remodal-confirm">OK</button>
	</div>

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'keyword'	: $('input[name=keyword]').val(),
								'page'		: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_partner.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);

		var modalAdd = $('[data-remodal-id=modal]').remodal( );

        $(document).on('confirmation', '.remodal', function() {
			
			var imageInput1 = $('#imageInput1').prop('files')[0];
			
            var form_data = new FormData();
			form_data.append('id_partner', $('input[name=id_partner]').val());
          	form_data.append('oldImg', $('input[name=oldImg]').val());
          	form_data.append('name', $('input[name=name]').val());
			form_data.append('url', $('input[name=url]').val());
			form_data.append('imageInput1', imageInput1);
            

			$body.addClass("loadingClass");

			if ($("#id_partner").val() == '0') {
				$.ajax({
					type: "POST",
					url: 'ajax_add_partner.php',
					processData: false,
					contentType: false,
					data: form_data
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data partner telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					} else if (data == 'name_exist') {
						$('#addNotification').html(showNotification("error", "Nama partner sudah digunakan"));
					}
				});
			} else {
				$.ajax({
					type: "POST",
					url: 'ajax_edit_partner.php',
					processData: false,
					contentType: false,
					data: form_data
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data partner telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					} else if (data == 'name_exist') {
						$('#addNotification').html(showNotification("error", "Nama partner sudah digunakan"));
					}

				});
			}
			
        });

		function resetdata() {
            $("#id_partner").val(0);
            $("#name").val("");
			$("#url").val("");
			$('#imageInput1').val(""); 
			$("#imageView1").attr('src', '');
            $("#modal1Title").html('Tambah Partner');
            $("#addNotification").html("");
        }

        function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

		function getedit(partnerID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_detail.php',
                data: 'type=prt&id=' + partnerID,
                dataType: 'json'
            }).done(function(data) { 
			
				$("#id_partner").val(data.id_partner);
				$("#oldImg").val(data.image);
				$("#name").val(data.name);
				$("#url").val(data.url);
				$("#imageView1").attr('src', 'user_files/partner_image/'+data.image);
				$("#modal1Title").html('Edit Partner');
				
				$body.removeClass("loadingClass");
            });
        }

        function deletePartner(partnerID, partnerImg, partnerName){

        	if (confirm('Anda yakin akan menghapus partner ' + partnerName + '?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_delete_partner.php',
                    data: 'partnerID=' + partnerID + '&partnerImg=' + partnerImg
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Data partner telah dihapus"));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	} else if (data == 'in_use') {
						$('#mainNotification').html(showNotification("error", "Partner "+partnerName+" tidak dapat dihapus"));
					}
                });
            }
        }

        $(document).on('closing', '.remodal', function(e) {
            resetdata();
        });

		function readURL(input, view) {

		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $(view).attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imageInput1").change(function(){
		    readURL(this, "#imageView1");
		});
	</script>

  </body>
</html>
