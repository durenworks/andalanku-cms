<?php
	include "check-admin-session.php";

	$user_level	= sanitize_int($_REQUEST["user_level"]);
	
	$queryMenu	= "select * from menus where level=1 order by menu_order ASC";
	$resultMenu	= mysqli_query($mysql_connection, $queryMenu);
	
	echo '<button type="button" class="btn btn-sm btn-warning btn-flat " onClick="expandAll();"><i class="fa fa-plus"></i> Expand All</button>
		  <button type="button" class="btn btn-sm btn-primary btn-flat " onClick="collapseAll();"><i class="fa fa-minus"></i> Collapse All</button>
		  &nbsp;&nbsp;&nbsp;
		  <button type="button" class="btn btn-sm btn-success btn-flat " onClick="saveData();"><i class="fa fa-save"></i> Simpan</button>
		  <hr style="border:1px solid #cccccc;">';
		  
	echo '<br>';
	echo '<div id="treeview_container" class="hummingbird-treeview" style="height: auto; overflow-y: scroll;">
			<ul id="treeview" class="hummingbird-base">
				<li>
					<i class="fa fa-plus"></i>
					<label>
						<input id="0" data-id="0" type="checkbox" /> Check / Uncheck All
					</label>
					<ul>
			';
	
	while ($dataMenu = mysqli_fetch_array($resultMenu)) {
		
		$parentID = $dataMenu['id'];
		
					echo '<li> <i class="fa fa-plus" style="padding-right:10px;"></i> ';
					echo '<label>
							<input id="'.$dataMenu['id'].'" data-id="'.$dataMenu['id'].'" type="checkbox" /> '.$dataMenu['name'].'
						  </label>
						  <ul>';
						  
		$queryChild	= "select * from menus where level=2 and parent_id='$parentID' order by menu_order ASC";
		$resultChild= mysqli_query($mysql_connection, $queryChild);		
		while ($dataChild = mysqli_fetch_array($resultChild)) {		
		
					echo '<li> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					echo '<label>
							<input class="hummingbird-end-node" 
								id="'.$dataChild['id'].'" 
								data-id="'.$dataChild['id'].'" type="checkbox"  /> '.$dataChild['name'].'
						  </label>
						  </li>';
		}
					
					echo '</ul>
						  </li>';
	}
	
	echo '			</ul>
				</li>
			</ul>
		  </div>';
	echo '<br>';
	echo '<hr style="border:1px solid #cccccc;">
		  <button type="button" class="btn btn-sm btn-warning btn-flat " onClick="expandAll();"><i class="fa fa-plus"></i> Expand All</button>
		  <button type="button" class="btn btn-sm btn-primary btn-flat " onClick="collapseAll();"><i class="fa fa-minus"></i> Collapse All</button>
		  &nbsp;&nbsp;&nbsp;
		  <button type="button" class="btn btn-sm btn-success btn-flat " onClick="saveData();"><i class="fa fa-save"></i> Simpan</button>';
?>

<script>
	
	$("#treeview").hummingbird();
	
	<?php 
		//ambil data permission saat ini 
		$queryPermission	= "select * from user_level_menu where user_level_id='$user_level'";
		$resultPermission	= mysqli_query($mysql_connection, $queryPermission);
		
		while ($dataPermission = mysqli_fetch_array($resultPermission)) {
			
			echo '$("#treeview").hummingbird("checkNode",{attr:"id",name: "'.$dataPermission['menu_id'].'",expandParents:false});';
		}
	?>

</script>
