<?php
	include "check-admin-session.php";
	
	$id_car_trim	= sanitize_int($_REQUEST["id_car_trim"]);
	$id_car_type	= sanitize_int($_REQUEST["id_car_type"]);
	$asset_code		= sanitize_sql_string($_REQUEST["asset_code"]);
	$name			= sanitize_sql_string($_REQUEST["name"]);
	$price			= sanitize_int($_REQUEST["price"]);
	$is_active		= sanitize_int($_REQUEST["is_active"]);
	
	if ($id_car_trim <> '0' && $id_car_type <> '0' && $name <> '' && $price <> '0') {
		
		$queryCheck		= "SELECT id_car_trim from car_trims 
						   WHERE id_car_type='$id_car_type' and name='$name' and id_car_trim!='$id_car_trim' ";
		$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck ) > 0) {
			echo "name_exist";
			exit;
		}
		
        $query = "UPDATE car_trims SET id_car_type='$id_car_type', 
				  name='$name', price='$price', is_active='$is_active', 
				  asset_code='$asset_code' 
				  where id_car_trim='$id_car_trim' ";
        mysqli_query($mysql_connection, $query);
		
        echo 'success'; 
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>