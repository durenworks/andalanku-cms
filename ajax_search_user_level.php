<?php
	include "check-admin-session.php";

	$keyword	= sanitize_sql_string($_REQUEST["keyword"]);
	$page 		= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(*) as num
					   from user_levels
					   where name like '%$keyword%' ";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select * 
			  from user_levels 
			  where name like '%$keyword%' 
			  order by name ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='5%'>No</th>
				  <th width='10%'>Nama Level</th>
				  <th width='85%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1); 

	while ($data = mysqli_fetch_array($result)) {

		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . @$data[name] . '</td>
				  <td align="center">
					<a href="#modal" onclick="getedit(' . @$data[id] . ')">Edit</a> | 
					<a href="#" onclick="deleteUserLevel(' . @$data[id] . ',\'' . @$data[name] . '\')">Delete</a>
				  </td>
				  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>