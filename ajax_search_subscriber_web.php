<?php
	include "check-admin-session.php";

	$searchDate	= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword	= sanitize_sql_string($_REQUEST["keyword"]);
	$page 		= sanitize_int($_REQUEST["page"]);
	
	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";
	} else {
		$startDate	= date("Y-m")."-01 00:00:00";
		$endDate	= date("Y-m-t")." 23:59:59";
	}
	
	$query 			= "select COUNT(*) as num
					   from subscribers 
					   where product!='none' and (name like '%$keyword%' or email like '%$keyword%' or phone like '%$keyword%')  
					   and created_at>='$startDate' and created_at<='$endDate'";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select * 
			  from subscribers 
			  where product!='none' and (name like '%$keyword%' or email like '%$keyword%' or phone like '%$keyword%') 
			  and created_at>='$startDate' and created_at<='$endDate' 
			  order by created_at ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query); 

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='15%'>Tanggal</th>
				  <th width='25%'>Nama</th>
				  <th width='20%'>Email</th>
				  <th width='10%'>Telepon</th>
				  <th width='10%'>Product</th>
				  <th width='10%'></th>
				  <th width='10%'></th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . date("d-m-Y H:i:s", strtotime($data[created_at])) . '</td>
				  <td>' . @$data[name] . '</td>
				  <td>' . @$data[email] . '</td>
				  <td>' . @$data[phone] . '</td>
				  <td>' . @$data[product] . '</td>
				  <td>
					<a href="#modal" onclick="viewDetail('.$data[id_subscriber].')"><i class="fa fa-search-plus"></i> View Detail</a>
				  </td>
				  <td>
					<a href="#" onclick="deleteSubscriber(' . @$data[id_subscriber] . ',\'' . @$data[name] . '\')" title="Hapus"><i class="fa fa-trash"></i> Delete</a>
				  </td>				  
			  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>