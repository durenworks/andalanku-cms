<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";
	
	$fullname 		= $_SESSION['fullname'];
	$legal_type 	= "Privacy Policy";
	$legal_link 	= "privacypolicy";
	$legal_caption	= "Privacy Policy";
?>

<link href="plugins/summernote/summernote.css" rel="stylesheet">

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Display</li>
              <li><a href="<?php echo $legal_link; ?>"><?php echo $legal_caption; ?></a></li>
              <li>Tambah <?php echo $legal_caption; ?></a></li>
            </ol>
          </section>
		  <br>
          <!-- Main content -->
          <section class="content">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Tambah <?php echo $legal_caption; ?></h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>
				
				<input type="hidden" name="legal_type" id="legal_type" value="<?php echo $legal_type; ?>" />
				<input type="hidden" name="legal_link" id="legal_link" value="<?php echo $legal_link; ?>" />
				<input type="hidden" name="legal_caption" id="legal_caption" value="<?php echo $legal_caption; ?>" />
				
				<div class="row form-group">
					<div class="col-md-12">
						<textarea name="content" id="content" rows="8" class="form-control" placeholder="Isi <?php echo $legal_caption; ?>"></textarea>
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-12 text-center">
						<button type="button" style="width:100px" class="btn btn-primary btn-large" onclick="save();">Simpan</button>
						<button type="button" style="width:100px" class="btn btn-warning btn-large" onclick="cancel();">Batal</button>
					</div>
				</div>

				<br><br><br>

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

    <script src="js/inc-function.js"></script>
	
	<script src="plugins/summernote/summernote.js"></script>
	
    <script type="text/javascript">

    	$body = $("body");
		
		$(document).ready(function() {
		  $('#content').summernote({
			height: 300, 
			placeholder: 'Isi '+$('input[name=legal_caption]').val()
		  });
		});
		
    	function save() {

    		$body.addClass("loadingClass");

    		var form_data = new FormData();
          	form_data.append('legal_type', $('input[name=legal_type]').val());
			form_data.append('content', $('textarea[name=content]').val());
			form_data.append('_ref', '<?php echo md5('privacypolicy#'.date('d-m-Y')); ?>');
			
			$.ajax({
				type: "POST",
                url: 'ajax_add_legal_document.php',
                processData: false,
                contentType: false,
                data: form_data
			}).done(function(data) {

				data = $.trim(data); 
				$body.removeClass("loadingClass");

				if (data == 'success') {
					$("#mainNotification").html(showNotification("success", "Data Sudah Disimpan"));
					resetdata();
					setTimeout(clearnotif, 5000);

				} else if (data == 'empty') {
					$('#mainNotification').html(showNotification("error", "Input Tidak Lengkap"));
				} 

				$('html, body').animate({scrollTop: '0px'}, 0);
			});

    	}

    	function cancel() {
    		window.location = $('input[name=legal_link]').val();
    	}

    	function resetdata() {

			$('#content').summernote('reset');
    	}

    	function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

    </script>

  </body>
</html>
