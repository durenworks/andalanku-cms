<?php
	include "check-admin-session.php";
	include "plugins/image-resize/lib/ImageResize.php";
	
	function uploadImage1($file) {

		$response = "";

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/slider_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'Banner_Apps_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'Banner_Apps_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

				//resize image
				$image = new \Eventviva\ImageResize($folder . '/' . $newnamefile);
				$image->resize(1125, 1170,  $allow_enlarge = True);
				$image->save($folder . '/' . $newnamefile);

			} else {
				$response = "";
			}
		}

		return $response;
	}

	function uploadImage2($file) {

		$response = "";

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/slider_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'Banner_Web_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'Banner_Web_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

				//resize image
				$image = new \Eventviva\ImageResize($folder . '/' . $newnamefile);
				$image->resizeToBestFit(1125, 525);
				$image->save($folder . '/' . $newnamefile);

			} else {
				$response = "";
			}
		}

		return $response;
	}

	$id_slider 	= sanitize_int($_REQUEST["id_slider"]);
	$id_promo 	= sanitize_int($_REQUEST["id_promo"]);
	$oldImg 	= sanitize_sql_string($_REQUEST["oldImg"]);
	$oldImgWeb 	= sanitize_sql_string($_REQUEST["oldImgWeb"]);

	if ($id_slider <> '0' && (!empty($_FILES['imageInput1']) || $oldImg<>'') && (!empty($_FILES['imageInput2']) || $oldImgWeb<>'') ) {
		
		//upload gambar dulu
		$imageFileName1 = "";
		if (!empty($_FILES['imageInput1'])) {
			$imageFileName1 = uploadImage1($_FILES['imageInput1']);
			if($imageFileName1 != "") {
				//hapus gambar yang lama
				@unlink("user_files/slider_image/".$oldImg);
			}
		} else {
			$imageFileName1 = "";
		}

		$imageFileName2 = "";
		if (!empty($_FILES['imageInput2'])) {
			$imageFileName2 = uploadImage2($_FILES['imageInput2']);
			if($imageFileName2 != "") {
				//hapus gambar yang lama
				@unlink("user_files/slider_image/".$oldImgWeb);
			}
		} else {
			$imageFileName2 = "";
		}

		$query = "UPDATE sliders set id_promo='$id_promo' ";
		if($imageFileName1 <> '') $query = $query." ,image='$imageFileName1' ";
		if($imageFileName2 <> '') $query = $query." ,image_web='$imageFileName2' ";
		$query = $query." where id_slider='$id_slider' ";
		mysqli_query($mysql_connection, $query);

		echo 'success';
		exit;
		
	} else {
		echo "empty";
		exit;
	}
?>
