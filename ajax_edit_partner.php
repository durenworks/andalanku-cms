<?php
	include "check-admin-session.php";
	include "plugins/image-resize/lib/ImageResize.php";
	
	function uploadImage($file) {

		$response = "";

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/partner_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'andalanku_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

				//resize image
				$image = new \Eventviva\ImageResize($folder . '/' . $newnamefile);
				$image->resizeToBestFit(800, 600);
				$image->save($folder . '/' . $newnamefile);

			} else {
				$response = "";
			}
		}

		return $response;
	}

	$id_partner 	= sanitize_int($_REQUEST["id_partner"]);
	$name 			= sanitize_sql_string($_REQUEST["name"]);
	$url			= sanitize_sql_string($_REQUEST["url"]);
	$oldImg 		= sanitize_sql_string($_REQUEST["oldImg"]);

	if ($id_partner <> '0' && $name <> '') {
		
		$queryCheck		= "SELECT id_partner from partners WHERE name='$name' and id_partner!='$id_partner'";
		$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck ) > 0) {
			echo "name_exist";
			exit;
		}

		//upload gambar dulu
		$imageFileName1 = "";
		if (!empty($_FILES['imageInput1'])) {
			$imageFileName1 = uploadImage($_FILES['imageInput1']);
			if($imageFileName1 != "") {
				//hapus gambar yang lama
				@unlink("user_files/partner_image/".$oldImg);
			}
		} else {
			$imageFileName1 = "";
		}

		$query = "UPDATE partners set name='$name', url='$url' ";
		if($imageFileName1 <> '') $query = $query." ,image='$imageFileName1' ";
		$query = $query." where id_partner='$id_partner' ";
		mysqli_query($mysql_connection, $query);

		echo 'success';
		exit;
		
	} else {
		echo "empty";
		exit;
	}
?>
