<?php
	include "check-admin-session.php";

	$loginTypeName	= $_SESSION['loginTypeName'];	
	$legal_type		= sanitize_sql_string($_REQUEST["legal_type"]);
	$legal_caption	= sanitize_sql_string($_REQUEST["legal_caption"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$page 			= sanitize_int($_REQUEST["page"]);

	$query 		= "select COUNT(a.id_legal_document) as num
				   from legal_documents a
				   left join users b on a.input_by=b.user_id 
				   where (a.content like '%$keyword%') and a.type='$legal_type' ";

	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;

	$query 	= "select a.*, b.fullname as input_by_name
			   from legal_documents a
			   left join users b on a.input_by=b.user_id 
			   where (a.content like '%$keyword%') and a.type='$legal_type'
			   order by id_legal_document ASC LIMIT $start,$limit";
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
					<th width='5%'>No</th>
					<th width='10%'>Tanggal</th>
					<th width='15%'>Diinput oleh</th>
					<th width='15%'>Detail</th>
					<th width='10%'>Status</th>
					<th width='12%'>Status Publikasi</th>
					<th width='10%'>Edit</th>
					<th>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {
		
		$buttonApprove = '';		
		if($loginTypeName == 'SUPERADMIN') {
			
			if($data['status'] == 'PENDING') {
				$buttonApprove = '<button type="button" class="btn btn-xs btn-success" onclick="processData(\''. $data['id_legal_document'].'\', \'APPROVE\')"><i class="fa fa-check"></i> APPROVE</button>
									&nbsp;
								  <button type="button" class="btn btn-xs btn-danger" onclick="processData(\''. $data['id_legal_document'].'\', \'REJECT\')"><i class="fa fa-remove"></i> REJECT</button>';
			}
			else if($data['status'] == 'APPROVED') {
				
				if($data['is_published'] == '0')
					$buttonApprove = '<button type="button" class="btn btn-xs btn-warning" onclick="processData(\''. $data['id_legal_document'].'\', \'PUBLISH\')"><i class="fa fa-globe"></i> PUBLISH</button>';
			}
		}

		if($data['is_published'] == '1') $is_published = 'Sudah Terbit';
		else $is_published = 'Belum Terbit';
		
		if($data['is_active'] == '1') $is_active = 'Aktif';
		else $is_active = 'Tidak Aktif';
		
		if($legal_type == 'TOS') $linkEdit = 'edit_tos';
		else $linkEdit = 'edit_privacy_policy';
		
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . date("d-m-Y H:i:s", strtotime($data['date'])) . '</td>
				  <td>' . $data['input_by_name'] . '</td>
				  <td>
					<a href="legal_preview?id='. $data['id_legal_document'] .'" title="Preview" target="_blank">
						<i class="fa fa-search-plus"></i> Preview Detail
					</a>
				  </td>
				  <td>' . $data['status'] . '</td>
				  <td>' . $is_published . '</td>
				  <td>
					<a href="'.$linkEdit.'?id='. $data['id_legal_document'] .'" title="Edit">Edit</a>
				  </td>
				  <td align="center">
					'. $buttonApprove .'
				  </td>
				</tr>';
		$i++;
	}

	echo "</table>";

	include "inc-paging.php";
?>
