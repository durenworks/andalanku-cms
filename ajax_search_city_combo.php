<?php
	include "check-admin-session.php";

	$province_id = sanitize_sql_string($_REQUEST["province_id"]);
	$addressType = sanitize_sql_string($_REQUEST["addressType"]);

	$query = "select * 
			  from regencies 
			  where province_id='$province_id' 
			  order by name ASC"; 
	$result = mysqli_query($mysql_connection, $query);
	
	if($addressType <> '') {
	
		$addrType = '';
		if($addressType == 'domicile') $addrType = '_domicile';
		else if($addressType == 'office') $addrType = '_office';

		echo "<select name='regency_id".$addrType."' id='regency_id".$addrType."' class='form-control' onchange='getDistrict(\"false\", \"".$addressType."\");'>";

		$i = ($page*$limit) - ($limit-1);

		while ($data = mysqli_fetch_array($result)) {

			echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
		}

		echo "</select>";
	
	} else {
		
		echo "<select name='regency_id' id='regency_id' class='form-control'>";
		echo "<option value=''>Semua Kota / Kabupaten</option>";
		
		while ($data = mysqli_fetch_array($result)) {

			echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
		}

		echo "</select>";
	}
?>