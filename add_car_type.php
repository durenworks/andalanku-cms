<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";
?>

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Car Prices</li>
			  <li><a href="car_type">Tipe Mobil</a></li>
              <li>Tambah Tipe Mobil</a></li>
            </ol>
          </section>
		  <br>
          <!-- Main content -->
          <section class="content">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Tambah Tipe Mobil</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row form-group">
					<div class="col-md-4">
						<label>Merk Mobil</label>
						<select name="id_car_brand" class="form-control" id="id_car_brand">
							<?php 
								$query = "select * from car_brands order by name asc";
								$result= mysqli_query($mysql_connection, $query);
								while($data = mysqli_fetch_array($result)) {
									echo '<option value="'.$data['id_car_brand'].'">'.$data['name'].'</option>';
								}
							?>
						</select>
					</div>
					<div class="col-md-4">
						<label>Nama</label>
						<input type="text" name="typeName" id="typeName" class="form-control" placeholder="Nama Tipe Mobil"/>
					</div>
					<div class="col-md-4">
						<label for="is_active">Status</label>
						<select name="is_active" class="form-control" id="is_active">
							<option value="1">Aktif</option>
							<option value="0">Tidak Aktif</option>
						</select>
					</div>
					<div class="col-md-12">
						<hr style="border-color:#bbbbbb;">
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-md-12">
						<button type="button" class="btn btn-primary  btn-flat " onClick="addImage();"><i class="fa fa-plus"></i> Tambah Gambar</button>
					</div>
				</div>

				<span id="spanImageTable"></span>

				<div class="row form-group">
					<div class="col-md-12 text-center">
						<button type="button" style="width:100px" class="btn btn-primary btn-large" onclick="save();">Simpan</button>
						<button type="button" style="width:100px" class="btn btn-warning btn-large" onclick="cancel();">Batal</button>
					</div>
				</div>

				<br><br><br>

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

    <script src="js/inc-function.js"></script>

    <script type="text/javascript">

    	$body = $("body");
		
		var rowNumber = 0;
		var rowCount  = 0;
		
		function addImage() {
			
			rowNumber++;
			rowCount++;
			
			var spanImageRowName 		= 'spanImageRow_'+rowNumber;
			var imageViewName 			= 'imageView_'+rowNumber;
			var answerImageName 		= 'answerImage_'+rowNumber;
			
			var input = "<span id='"+ spanImageRowName +"'>"+
						"<div class='row form-group'> "+
						"<div class='col-md-4'>"+
						"<table width='100%' class='table'>"+
							"<tr>"+
								"<td width='150px' align='center'>"+
									"<img id='"+ imageViewName +"' src='img/none.png' style='width: 90px;' alt='Gambar'/>"+
								"</td>"+
								"<td>"+
									"<input type='file' name='"+ answerImageName +"' id='"+ answerImageName +"' accept='image/*' onchange='readURL(this, \"#"+ imageViewName +"\")'/>"+
								"</td>"+
							"</tr>"+
						"</table>"+
						"</div>"+
						"<div class='col-md-1'>"+
						"<button type='button' class='btn btn-danger btn-large' onclick='removeImage("+ rowNumber +");'> X </button>"+
						"</div>"+
						"</div>"+
						"</span>";
						
			$('#spanImageTable').append(input);
		}
		
		function removeImage(rowNum) {
			
			if (confirm('Hapus gambar?')) {
				
				$('#spanImageRow_'+rowNum).html('');
				rowCount--;
			}
		}
		
		function readURL(input, view) { 
			
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $(view).attr('src', e.target.result); 
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

    	function save() {

    		$body.addClass("loadingClass");

    		var form_data = new FormData();
			form_data.append('id_car_brand', $('select[name=id_car_brand]').val());
          	form_data.append('typeName', $('input[name=typeName]').val());
			form_data.append('is_active', $('select[name=is_active]').val());
			form_data.append('image_row_count', rowCount);
			form_data.append('_ref', '<?php echo md5('car_type#'.date('d-m-Y')); ?>');
			
			for(var i=0; i<rowCount; i++) {
				
				var n = i+1;
				var answerImageName = 'answerImage_'+n;
				
				form_data.append(answerImageName, $('#'+answerImageName).prop('files')[0]);
			}
			
			$.ajax({
				type: "POST",
                url: 'ajax_add_car_type.php',
                processData: false,
                contentType: false,
                data: form_data
			}).done(function(data) {

				data = $.trim(data); 
				$body.removeClass("loadingClass");

				if (data == 'success') {
					$("#mainNotification").html(showNotification("success", "Data Sudah Disimpan"));
					resetdata();
					setTimeout(clearnotif, 5000);

				} else if (data == 'empty') {
					$('#mainNotification').html(showNotification("error", "Input Tidak Lengkap"));
				} else if (data == 'car_type_exist') {
					$('#mainNotification').html(showNotification("error", "Nama tipe mobil sudah ada"));
				} else if (data == 'incorrect_file_type') {
					$('#mainNotification').html(showNotification("error", "File gambar harus berupa file jpg atau png atau gif"));
				}

				$('html, body').animate({scrollTop: '0px'}, 0);
			});

    	}

    	function cancel() {
    		window.location = 'car_type';
    	}

    	function resetdata() {

    		$("#typeName").val("");
			$("#is_active").val($("#is_active option:first").val());
			$("#id_car_brand").val($("#id_car_brand option:first").val());
			$('#spanImageTable').html('');
			rowCount	= 0;
			rowNumber	= 0;
    	}

    	function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

    	


    </script>

  </body>
</html>
