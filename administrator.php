<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Master Data</li>
			  <li>Administrator</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user-secret"></i> Administrator</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<select name="src_level" id="src_level" class="form-control">
							<option value="">Semua User Level</option>
							<?php 
								$query = "select * from user_levels order by name ASC"; 
								$result = mysqli_query($mysql_connection, $query);
								while ($data = mysqli_fetch_array($result)) {
									echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
								}
							?>
						</select>
					</div>
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
						<a href="#modal">
							<button type="button" class="btn btn-primary  btn-flat " ><i class="fa fa-plus"></i> Tambah Administrator</button>
						</a>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->


	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Tambah Administrator</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-6">
				<div class="form-group">
					<label for="fullname">Nama</label>
					<input type="hidden" id="administrator_id" name="administrator_id" value="0">
					<input type="text" name="fullname" placeholder="Name" class="form-control" id="fullname">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="phone_number">Nomor HP</label>
					<input type="text" name="phone_number" placeholder="Nomor HP" class="form-control" id="phone_number">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="username">Username</label>
					<input type="text" name="username" placeholder="Username" class="form-control" id="username">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="password">Password <span id="passwdNotif"></span></label>
					<input type="password" name="password" onblur="checkPassword();" placeholder="Password" class="form-control" id="password">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="passwordConfirm">Konfirmasi Password <span id="passwdNotifConfirm"></span></label>
					<input type="password" name="passwordConfirm" placeholder="Konfirmasi Password" class="form-control" id="passwordConfirm">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="level">Level</label>
					<select name="level" id="level" class="form-control">
						<?php 
							$query = "select * from user_levels order by name ASC"; 
							$result = mysqli_query($mysql_connection, $query);
							while ($data = mysqli_fetch_array($result)) {
								echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
							}
						?>
					</select>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="status">Status</label>
					<select name="status" class="form-control" id="status">
						<option value="ACTIVE">ACTIVE</option>
						<option value="NOT ACTIVE">NOT ACTIVE</option>
					</select>
				</div>
			</div>

			<div class="col-md-12">
			<span id="passwdLabel"></span>
			<br>
			<i class="fa fa-info-circle"></i> <i>Password minimal 8 karakter, harus mengandung huruf kecil, huruf besar, angka, dan karakter spesial</i>
			<br><br>
			</div>
			
			<div class="col-md-12">
				<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
				<button data-remodal-action="confirm" class="remodal-confirm">OK</button>
			</div>

			</p>
		</div>
		<br>
		
	</div>

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'src_level'	: $('select[name=src_level]').val(),
								'keyword'	: $('input[name=keyword]').val(),
								'page'		: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_administrator.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);

		var modalAdd = $('[data-remodal-id=modal]').remodal( );

        $(document).on('confirmation', '.remodal', function() {

            var formData = {
                'administrator_id'				: $('#administrator_id').val(),
				'administrator_name'			: $('input[name=fullname]').val(),
				'administrator_phone_number'	: $('input[name=phone_number]').val(),
				'administrator_username'		: $('input[name=username]').val(),
				'administrator_password'		: $('input[name=password]').val(),
				'administrator_passwordConfirm'	: $('input[name=passwordConfirm]').val(),
				'administrator_level'			: $('select[name=level]').val(),
				'administrator_status'			: $('select[name=status]').val()
            };

			if($("#password").val() != $("#passwordConfirm").val()) {
				$('#addNotification').html(showNotification("error", "Password dan konfirmasi password tidak sama"));
			}
			else {

				$body.addClass("loadingClass");

				if ($("#administrator_id").val() == '0') {
					$.ajax({
						type: "POST",
						url: 'ajax_add_administrator.php',
						data: formData
					}).done(function(data) {

						data = $.trim(data);
						$body.removeClass("loadingClass");

						if (data == 'success') {
							$("#mainNotification").html(showNotification("success", "Data administrator telah disimpan"));
							modalAdd.close();
							searchData(1);
							$('#dataSpan').html(data);
							resetdata();
							setTimeout(clearnotif, 5000);

						} else if (data == 'empty') {
							$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
						} else if (data == 'username_exist') {
							$('#addNotification').html(showNotification("error", "Username sudah digunakan"));
						} else if (data == 'password_not_match') {
							$('#addNotification').html(showNotification("error", "Password dan konfirmasi password tidak sama"));
						} else if (data == 'invalid_password') {
							$('#addNotification').html(showNotification("error", "Password minimal 8 karakter, harus mengandung huruf kecil, huruf besar, angka, dan karakter spesial"));
						} else if (data == 'superadmin_phone_empty') {
							$('#addNotification').html(showNotification("error", "Level Superadmin harus menggunakan nomor hp"));
						}
					});
				} else {
					$.ajax({
						type: "POST",
						url: 'ajax_edit_administrator.php',
						data: formData
					}).done(function(data) {

						data = $.trim(data);
						$body.removeClass("loadingClass");

						if (data == 'success') {
							$("#mainNotification").html(showNotification("success", "Data administrator telah disimpan"));
							modalAdd.close();
							searchData(1);
							$('#dataSpan').html(data);
							resetdata();
							setTimeout(clearnotif, 5000);

						} else if (data == 'empty') {
							$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
						} else if (data == 'username_exist') {
							$('#addNotification').html(showNotification("error", "Username sudah digunakan"));
						} else if (data == 'password_not_match') {
							$('#addNotification').html(showNotification("error", "Password dan konfirmasi password tidak sama"));
						} else if (data == 'invalid_password') {
							$('#addNotification').html(showNotification("error", "Password minimal 8 karakter, harus mengandung huruf kecil, huruf besar, angka, dan karakter spesial"));
						} else if (data == 'superadmin_phone_empty') {
							$('#addNotification').html(showNotification("error", "Level Superadmin harus menggunakan nomor hp"));
						}
					});
				}
			}
        });

		function resetdata() {
            $("#administrator_id").val(0);
            $("#fullname").val("");
			$("#phone_number").val("");
			$("#username").val("");
			$("#password").val("");
			$("#passwordConfirm").val("");
			$("#level").val($("#level option:first").val());
			$("#status").val($("#status option:first").val());
            $("#modal1Title").html('Tambah Administrator');
            $("#addNotification").html("");
			$("#passwdNotif").html("");
			$("#passwdNotifConfirm").html("");
			$("#passwdLabel").html("");
        }

        function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

		function getedit(administratorID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_detail.php',
                data: 'type=adm&id=' + administratorID,
                dataType: 'json'
            }).done(function(data) { 
							$("#administrator_id").val(data.user_id);
							$("#status").val(data.status);
							$("#level").val(data.level);
							$("#username").val(data.username);
							$("#fullname").val(data.fullname);
							$("#phone_number").val(data.phone_number);
							$("#modal1Title").html('Edit Administrator');
							$("#passwdNotif").html("<font color='red'><i>*</i></font>");
							$("#passwdNotifConfirm").html("<font color='red'><i>*</i></font>");
							$("#passwdLabel").html("<font color='red'><i>* Biarkan kosong jika tidak akan mengubah password</i></font><br>&nbsp;");

            	$body.removeClass("loadingClass");
            });
        }

        function deleteAdministrator(administratorID,administratorName){

        	if (confirm('Anda yakin akan menghapus administrator ' + administratorName + '?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_delete_administrator.php',
                    data: 'administratorID=' + administratorID
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Data administrator telah dihapus"));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	} else if (data == 'in_use') {
						$('#mainNotification').html(showNotification("error", "Administrator "+administratorName+" tidak dapat dihapus"));
					}
                });
            }
        }

        $(document).on('closing', '.remodal', function(e) {
            resetdata();
        });

		function checkPassword() {
			
			var password = $("#password").val();
			
			if (password.length < 8) {
				$('#addNotification').html(showNotification("error", "Password minimal 8 karakter"));
				return;
			}
			
			if (!password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
				$('#addNotification').html(showNotification("error", "Password harus mengandung huruf kecil, huruf besar, dan angka"));
				return;
			}
			
			if (!password.match(/([0-9])/)) {
				$('#addNotification').html(showNotification("error", "Password harus mengandung huruf kecil, huruf besar, dan angka"));
				return;
			}
			
			if (!password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
				$('#addNotification').html(showNotification("error", "Password harus mengandung karakter spesial"));
				return;
			}
			
			$("#addNotification").html('');
		}

	</script>

  </body>
</html>
