<?php	
	include "check-admin-session.php";
	include "inc-header-admin.php";
	
	$fullname  = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>
	  
      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Edit Profile</li>
            </ol>
          </section>
		  <br>	
		  
          <!-- Main content -->
          <section class="content">
		  
			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-lock"></i> Edit Profile</h3>
              </div>
              <div class="box-body">
			  
				<span id="mainNotification"></span>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label for="old_pass">Password Lama</label>
						<input type="password" name="old_pass" placeholder="Password Lama" class="form-control" id="old_pass">  
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label for="new_pass">Password Baru</label>
						<input type="password" name="new_pass" placeholder="Password Baru" class="form-control" id="new_pass"> 
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label for="new_pass_confirm">Konfirmasi Password Baru</label>
						<input type="password" name="new_pass_confirm" placeholder="Konfirmasi Password Baru" class="form-control" id="new_pass_confirm">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<button type="button" class="btn btn-primary  btn-flat " onClick="updatePassword();"><i class="fa fa-save"></i> Simpan</button>
					</div>
				</div>
				
				<br>
				
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->
		  
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
	  
      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->

	<?php include "custom_loading.php"; ?>
	
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>
	
	<script src="js/inc-function.js"></script>
	
	<script> 
	
		$body = $("body");
	
		function updatePassword() {
			
			$body.addClass("loadingClass");
			
			var form_data = new FormData();            
            form_data.append('old_pass', $('input[name=old_pass]').val());
            form_data.append('new_pass', $('input[name=new_pass]').val());
            form_data.append('new_pass_confirm', $('input[name=new_pass_confirm]').val());
            
			var new_passs = $('input[name=new_pass]').val();
			
			if(new_passs=="") {
				$body.removeClass("loadingClass");
				alert('Password baru harus diisi');
			} 
			else if($('input[name=new_pass]').val() != $('input[name=new_pass_confirm]').val()) {
				$body.removeClass("loadingClass");
				alert('Password baru dan konfirmasi password baru harus sama');
			} 
			else {
				
	            $.ajax({
					type: "POST",
                    url: 'ajax_update_password.php',
                    processData: false,
                    contentType: false,
                    data: form_data
				}).done(function(data) {
	                
	                data = $.trim(data);
	                $body.removeClass("loadingClass");
					
	                if(data =='empty') {
	                	alert('Data yang Anda masukkan tidak lengkap');
	                }
	                else if(data =='password_not_match') {
	                	alert('Password baru dan konfirmasi password baru harus sama');
	                }
	                else if(data =='invalid_old_pass') {
	                	alert('Password lama tidak benar');
	                }
	                else if(data =='success') {
	                	alert('Data telah disimpan');
	                	window.location = 'profile.php';
	                } 
	            });
	        }
        }
	</script>
	
  </body>
</html>
