<?php
	include "check-admin-session.php";
	
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Data_PPOB_Transaction_History.xls");
	
	echo "Data Payment History<br><br>";

	$transaction_status = sanitize_sql_string($_REQUEST["src_transaction_status"]);
	$ayopop_status 		= sanitize_sql_string($_REQUEST["src_ayopop_status"]);
	$searchDate			= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 			= sanitize_sql_string($_REQUEST["keyword"]);
	$page 				= sanitize_int($_REQUEST["page"]);

	//var_dump($keyword);

	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";

		$sql_date	= "and pt.transaction_date between '$startDate' and '$endDate'";
	} else {
		$startDate	= date("Y-m")."-01 00:00:00";
		$endDate	= date("Y-m")."-31 23:59:59";		
	}

	if($transaction_status!='') {
		$sql_transaction_status = "and pt.transaction_status = '$transaction_status'";
	}
	
	if($ayopop_status!='') {
		$sql_ayopop_status = "and pt.ayopop_status = '$ayopop_status'";
	}

	if($keyword!='') {
		$sql_key = "and (c.customer_name like '%$keyword%' or pt.customer_name like '%$keyword%' or pt.order_id like '%$keyword%')";
	}

	$query = "select pt.order_id,pt.customer_name,c.customer_name AS name,pt.account_number,
		pt.product_name,pt.amount,pt.transaction_status,pt.ayopop_status
		from ppob_transaction pt
		left join customers c on pt.customer_id = c.id_customer 
		where c.andalan_customer_id is not null and pt.transaction_status <> 'INQUIRY' 
		$sql_key $sql_date $sql_transaction_status $sql_ayopop_status
		order by pt.transaction_date DESC,c.customer_name";
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
            <tr>
              <th width='2%'>No</th>
              <th>Nomor Transaksi</th>
              <th>Nama Customer</th>
              <th>Nama</th>
              <th>ID Pelanggan</th>
              <th>Produk</th>
              <th>Total Tagihan</th> 
              <th>Status Bayar</th> 
              <th>Status Transaksi</th>
            </tr>";

	$i = 1;

	while ($data = mysqli_fetch_array($result)) {
		
		$total_amount = number_format($data[amount],0);
		if ($data['transaction_status'] == "PAYMENT"){ $transaction_status = "Menunggu Pembayaran"; }
		else if ($data['transaction_status'] == "PAYMENT DONE"){ $transaction_status = "Dibayar"; }
		else{ $transaction_status = "Pembayaran Gagal"; }

		echo '<tr>
				  <td align="right">'.$i.'.</th>
				  <td>' . $data[order_id] . '</td>
				  <td>' . $data[customer_name] . '</td>
				  <td>' . $data[name] . '</td>
				  <td>' . $data[account_number] . '</td>	
				  <td>' . $data[product_name] . '</td> 
				  <td>' . $total_amount . '</td>
				  <td>' . $transaction_status . '</td>
				  <td>' . $data[ayopop_status] . '</td> 		  
			  </tr>';
		$i++;
	}

	echo "</table>";
?>