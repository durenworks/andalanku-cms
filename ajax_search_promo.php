<?php
	include "check-admin-session.php";

	$id_promo_type	= sanitize_int($_REQUEST["id_promo_type"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$page 			= sanitize_int($_REQUEST["page"]);

	$query 			= "select COUNT(a.id_promo) as num
					   from promos a 
					   left join promo_type b on a.id_promo_type=b.id_promo_type 
					   where (title like '%$keyword%' or content like '%$keyword%') ";
	if($id_promo_type <> '0') $query = $query." and a.id_promo_type='$id_promo_type' ";

	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;

	$query 	= "select a.*, b.promo_type_name 
			   from promos a 
			   left join promo_type b on a.id_promo_type=b.id_promo_type 
 			   where (title like '%$keyword%' or content like '%$keyword%') ";
	if($id_promo_type <> '0') $query = $query." and a.id_promo_type='$id_promo_type' ";
	$query = $query." order by id_promo ASC LIMIT $start,$limit";
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
					<th width='2%'>No</th>
					<th width='15%'>Tanggal Berlaku</th>
					<th width='10%'>Tipe Promo</th>
					<th width='200px'>Gambar</th>
					<th width='20%'>Judul Promo</th>
					<th>Deskripsi</th>
					<th width='15%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		$content = strip_tags($data[content]);
		$content = substr($content, 0, 200);
		$content = $content."...";
		
		if($data[promo_start_date]<>'' && $data[promo_expired_date]<>'' && $data[promo_start_date]<>'0000-00-00' && $data[promo_expired_date]<>'0000-00-00')
			$availableDate = date("d-m-Y", strtotime($data[promo_start_date])) .' s/d '. date("d-m-Y", strtotime($data[promo_expired_date]));
		else 
			$availableDate = '';
		
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $availableDate . '</td>
				  <td>' . $data[promo_type_name] . '</td>
				  <td><img src="user_files/promo_image/'. $data[image] .'" alt=" " style="width: 200px;" /></td>
				  <td>' . $data[title] . '</td>
				  <td>' . $content . '</td>
				  <td align="center">
					<a href="edit_promo?id='. $data[id_promo] .'" title="Edit">Edit</a> |
					<a href="#" onclick="deletePromo(' . $data[id_promo] . ',\'' . $data[image] . '\',\'' . $data[title] . '\')" title="Hapus">Delete</a>
				  </td>
				</tr>';
		$i++;
	}

	echo "</table>";

	include "inc-paging.php";
?>
