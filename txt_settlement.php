<?php
	ob_clean();

	include "inc-db.php";
	include "sanitize.inc.php";

	$date = date("Ymd");
	$file = "temp_files/settlement_". $date . ".txt";
	$txt = fopen($file, "w") or die("Unable to open file!");
	
	$searchDate		= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);

	$sql_key = "";
	$sql_date = "";
	$sql_status = "";

	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";

		$sql_date	= "and es.rq_datetime between '$startDate' and '$endDate'";
	} else {
		$startDate	= date("Y-m")."-01 00:00:00";
		$endDate	= date("Y-m")."-31 23:59:59";		
	}
	
	if($keyword!='') {
			$sql_key = "and (c.customer_name like '%$keyword%' or ti.agreement_number like '%$keyword%' or 
						ti.transaction_code like '%$keyword%' or ep.payment_ref like '%$keyword%')";
	}

	$query = "select DISTINCT
	DATE_FORMAT(es.rq_datetime,'%Y-%m-%d %H:%i:%s' ) as tanggal,
	ti.contract_no as nopel, c.customer_name as nama, 
	ti.installment_number as angsuran, ti.tenor as tenor,
	ti.amount as tagihan, ti.admin_fee as admin, ti.penalty as denda, 
	ti.total_amount as jmlbayar, ti.afis_reference_no as mst_ref,
	ti.espay_payment_reff as mitra_ref, c.phone_number as hppel
	from espay_settlement es
	left join espay_settlement_data esd on es.rq_uuid = esd.rq_uuid
	left join espay_payment ep on esd.tx_id = ep.payment_ref
	left join transaction_installment ti on ep.payment_ref = ti.espay_payment_reff
	left join customers c on ti.id_customer = c.id_customer 
	where ti.afis_reference_no != ''
			$sql_key $sql_date $sql_status"; 

	$result = mysqli_query($mysql_connection, $query);

	$header = "tanggal|kodemitra|nopel|nama|angsuran_ke|tenor|tagihan|admin|denda|jmlbyr|mitra_reff|mst_ref|merchant|hppel\n";
	fwrite($txt, $header);
	while ($data = mysqli_fetch_array($result)) {
		$tempText =  $data['tanggal'] . '|' . 
				'AFICP' . '|' . 
				$data['nopel'] . '|' . 
				$data['nama'] . '|' . 
				$data['angsuran'] . '|' . 
				$data['tenor'] . '|' . 
				$data['tagihan'] . '|' . 
				$data['admin'] . '|' . 
				$data['denda'] . '|' . 
				$data['jmlbayar'] . '|' . 
				$data['mitra_ref'] . '|' . 
				$data['mst_ref'] . '|' . 
				'22691' . '|' . 
				$data['hppel'];
		$content = $tempText . "\n";
		fwrite($txt, $content);
	}

	
	fclose($txt);

	header('Content-Description: File Transfer');
	header('Content-Disposition: attachment; filename='.basename($file));
	ob_end_clean(); 
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($file));
	header("Content-Type: text/plain");

	//ob_clean();//finalizar buffer
	if (ob_get_contents()) ob_end_clean();
    flush(); //descarregar o buffer acumulado
    readfile($file); //ler arquivo/enviar
    unlink($file); //deletar arquivo temporario
    exit();
?>