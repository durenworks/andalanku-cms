<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Display</li>
			  <li>Image Slider</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-file-image-o"></i> Image Slider</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-6z">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
						<a href="#modal">
							<button type="button" class="btn btn-primary  btn-flat " ><i class="fa fa-plus"></i> Tambah Image Slider</button>
						</a>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->


	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Tambah Image Slider</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-12">
				<input type="hidden" id="id_slider" name="id_slider" value="0">
				<input type="hidden" id="oldImg" name="oldImg" value="">
				<label>Gambar Banner Apps</label>
				<table width="100%" class='table'>
					<tr>
						<td width="150px" align="center">
							<img id="imageView1" src="img/none.png" style="width: 90px;" alt="Gambar Apps"/>
						</td>
						<td>
							<input type='file' name="imageInput1" id="imageInput1" accept='image/x-png, image/jpeg'/>
						</td>
					</tr>
				</table>
			</div>

			<div class="col-md-12">
				<i style="color:red">Ukuran gambar yang direkomendasikan : 1125 x 1170 px</i>
				<br>&nbsp;
			</div>

			<div class="col-md-12">
				<input type="hidden" id="oldImgWeb" name="oldImgWeb" value="">
				<label>Gambar Banner Web</label>
				<table width="100%" class='table'>
					<tr>
						<td width="150px" align="center">
							<img id="imageView2" src="img/none.png" style="width: 90px;" alt="Gambar Web"/>
						</td>
						<td>
							<input type='file' name="imageInput2" id="imageInput2" accept='image/x-png, image/jpeg' />
						</td>
					</tr>
				</table>
			</div>

			<div class="col-md-12">
				<i style="color:red">Ukuran gambar yang direkomendasikan : 1125 x 525 px</i>
				<br>&nbsp;
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label>Promo</label>
					<select name="id_promo" id="id_promo" class="form-control">
						<option value=''></option>
						<?php
							$now = date("Y-m-d");
							$query 	= "select *
									   from promos
									   where promo_start_date <= '$now' and promo_expired_date >= '$now'
									   order by id_promo ASC ";
							$result = mysqli_query($mysql_connection, $query);
							while ($data = mysqli_fetch_array($result)) {
								echo '<option value="'.$data['id_promo'].'">'.$data['title'].'</option>';
							}
						?>
					</select>
				</div>
			</div>

			<div class="col-md-12">
				<i>Ukuran gambar yang direkomendasikan : 1125 x 1170 px</i>
				<br>&nbsp;
			</div>

			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
		<button data-remodal-action="confirm" class="remodal-confirm">OK</button>
	</div>

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = { 'page'	: searchPage };

            $.ajax({
                type: "POST",
                url: 'ajax_search_slider.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);

		var modalAdd = $('[data-remodal-id=modal]').remodal( );

        $(document).on('confirmation', '.remodal', function() {

			var imageInput1 = $('#imageInput1').prop('files')[0];
			var imageInput2 = $('#imageInput2').prop('files')[0];

            var form_data = new FormData();
			form_data.append('id_slider', $('input[name=id_slider]').val());
			form_data.append('oldImg', $('input[name=oldImg]').val());
			form_data.append('oldImgWeb', $('input[name=oldImgWeb]').val());
          	form_data.append('id_promo', $('select[name=id_promo]').val());
			form_data.append('imageInput1', imageInput1);
			form_data.append('imageInput2', imageInput2);


			$body.addClass("loadingClass");

			if ($("#id_slider").val() == '0') {
				$.ajax({
					type: "POST",
					url: 'ajax_add_slider.php',
					processData: false,
					contentType: false,
					data: form_data
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data slider telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					}
				});
			} else {
				$.ajax({
					type: "POST",
					url: 'ajax_edit_slider.php',
					processData: false,
					contentType: false,
					data: form_data
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data slider telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					}

				});
			}

        });

		function resetdata() {
            $("#id_slider").val(0);
            $("#id_promo").val("");
			$('#imageInput1').val("");
			$('#imageInput2').val("");
			$("#imageView1").attr('src', '');
			$("#imageView2").attr('src', '');
            $("#modal1Title").html('Tambah Image Slider');
            $("#addNotification").html("");
        }

        function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

		function getedit(sliderID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_detail.php',
                data: 'type=imgsldr&id=' + sliderID,
                dataType: 'json'
            }).done(function(data) {

				$("#id_slider").val(data.id_slider);
				$("#oldImg").val(data.image);
				$("#oldImgWeb").val(data.image_web);
				$("#id_promo").val(data.id_promo);
				$("#imageView1").attr('src', 'user_files/slider_image/'+data.image);
				$("#imageView2").attr('src', 'user_files/slider_image/'+data.image_web);
				$("#modal1Title").html('Edit Image Slider');

				$body.removeClass("loadingClass");
            });
        }

        function deleteImageSlider(sliderID, sliderImg, sliderWeb){

        	if (confirm('Anda yakin akan menghapus gambar ini?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_delete_slider.php',
                    data: 'sliderID=' + sliderID + '&sliderImg=' + sliderImg + '&sliderWeb=' + sliderWeb
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Gambar telah dihapus"));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	}
                });
            }
        }

        $(document).on('closing', '.remodal', function(e) {
            resetdata();
        });

		function readURL(input, view) {

		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $(view).attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imageInput1").change(function(){
		    readURL(this, "#imageView1");
		});

		$("#imageInput2").change(function(){
		    readURL(this, "#imageView2");
		});
	</script>

  </body>
</html>
