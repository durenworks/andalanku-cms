<?php
	include "inc-header.php";
	$msg = $_REQUEST['e'];
?>

<body class="login-page">

    <div class="login-box">

      <div class="login-box-body">
		<div class="text-center text-white">

			<table align="center" class="table-responsive">
				<tr>
					<td class="valign_top" width="100%">
						<img src="img/logo.png" alt="Logo">
					</td>
				</tr>
			</table>
			<br>
			<h4>Login</h4>
		</div>
        <p class="login-box-msg">
        	<div class="text-red text-center">
			<?php
				if($msg=="01") echo "Username dan password harus diisi";
				else if($msg=="02") echo "Login Gagal. Periksa kembali username dan password Anda";
				else if($msg=="03") echo "Anda harus login terlebih dahulu";
				else if($msg=="04") echo "Invalid captcha response";
			?>
			</div>
		</p>
        <form action="login_process" method="post">
          <div class="form-group has-feedback">
			<input type="username" name="username" class="form-control" placeholder="Username" />
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password" />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
		  <div class="form-group has-feedback">
            <div class="g-recaptcha" data-sitekey="6LcTPJoUAAAAAGBU6QmVaTpIVNq0aeYt7S4MeDOo"></div>
          </div>
          <div class="row">
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Login</button>
            </div>
            <div class="col-xs-4">
              &nbsp;
            </div>
            <div class="col-xs-4">
              &nbsp;
            </div><!-- /.col -->
          </div>
        </form>
        <br>

      </div><!-- /.login-box-body -->



    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js" type="text/javascript"></script>
	
	<script src='https://www.google.com/recaptcha/api.js'></script>

  </body>
</html>
