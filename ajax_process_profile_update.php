<?php
	include "check-admin-session.php";
	include "afis_call.php";

	$userID				= $_SESSION['userID'];
	$id_profile_update	= sanitize_int($_REQUEST["id_profile_update"]);
	$customerID			= sanitize_int($_REQUEST["customer_id"]);
	$status				= sanitize_sql_string($_REQUEST["status"]);

	if ($id_profile_update <> '0' && $customerID <> '0' && $status <> '') {
	
		$now = date("Y-m-d H:i:s");	
		
		$queryRequest 	= "select * from profile_update_request where id='$id_profile_update'";
		$resultRequest	= mysqli_query($mysql_connection, $queryRequest);
		$dataRequest 	= mysqli_fetch_array($resultRequest);
		
		//ambil alamat yang sebelumnya 
		$queryAddress  = "select address_id from address_customers 
						  where is_active='1' and user_id='$customerID' and address_type='LEGAL' ";
		$resultAddress = mysqli_query($mysql_connection, $queryAddress);
		$dataAddress   = mysqli_fetch_array($resultAddress);
		$prev_legal_address_id = $dataAddress['address_id'];
		if ($prev_legal_address_id == '') {
			$prev_legal_address_id = 0;
		}
		
		$queryAddress  = "select address_id from address_customers 
						  where is_active='1' and user_id='$customerID' and address_type='DOMICILE' ";
		$resultAddress = mysqli_query($mysql_connection, $queryAddress);
		$dataAddress   = mysqli_fetch_array($resultAddress);
		$prev_domicile_address_id = $dataAddress['address_id'];
		if ($prev_domicile_address_id == '') {
			$prev_domicile_address_id = 0;
		}
		
		$queryAddress  = "select address_id from address_customers 
						  where is_active='1' and user_id='$customerID' and address_type='OFFICE' ";
		$resultAddress = mysqli_query($mysql_connection, $queryAddress);
		$dataAddress   = mysqli_fetch_array($resultAddress);
		$prev_office_address_id = $dataAddress['address_id'];
		if ($prev_office_address_id == '') {
			$prev_office_address_id = 0;
		}
				
		$query = "	UPDATE profile_update_request set status='$status', 
					status_change_by='$userID', status_change_date='$now',
					prev_legal_address_id='$prev_legal_address_id',
					prev_domicile_address_id='$prev_domicile_address_id',
					prev_office_address_id='$prev_office_address_id' 
					where id='$id_profile_update' ";
		//echo $query;
		//exit;
		mysqli_query($mysql_connection, $query);		

		if($status == 'APPROVE') {
						
			$query = "	UPDATE customers set home_phone_number='".$dataRequest['phone']."', 
						area_code='".$dataRequest['area_code']."', 
						phone_number='".$dataRequest['mobile_phone']."' 
						where id_customer='$customerID' ";
			mysqli_query($mysql_connection, $query);
			
			$query = "	UPDATE address_customers set is_active='0' 
						where user_id='$customerID' ";
			mysqli_query($mysql_connection, $query);
			
			$query 	= "select address_id, address_type 
					   from profile_update_request_address 
					   where profile_update_request_id='$id_profile_update'";
			$result = mysqli_query($mysql_connection, $query);
			while($data = mysqli_fetch_array($result)) {
				
				$address_id 	= $data['address_id'];
				$address_type	= $data['address_type'];
				
				$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
							VALUES('$address_type', '$customerID', '$address_id', '1') ";
				mysqli_query($mysql_connection, $query);
			}
			
			
			//==================================== AFIS CALL ====================================
			$query = "select andalan_customer_id from customers where id_customer='$customerID'";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$andalan_customer_id = $data['andalan_customer_id'];
			
			$afis_api_url	= $afis_api_url.'/Supplier/pembaharuan';
			
			$queryAddress  = "select b.address, b.rt, b.rw,
							  f.name as village_name, e.name as district_name, 
							  d.name as regency_name, c.name as province_name, 
							  b.zip_code 
							  from profile_update_request_address a 
							  left join address b on a.address_id=b.id 
							  left join provinces c on b.province_id=c.id 
							  left join regencies d on b.regency_id=d.id
							  left join districts e on b.district_id=e.id
							  left join villages f on b.village_id=f.id
							  where a.profile_update_request_id='$id_profile_update' 
							  and a.address_type='LEGAL' ";
	
	
			$resultAddress = mysqli_query($mysql_connection, $queryAddress);
			
			if(mysqli_num_rows($resultAddress) > 0) {
				
				$dataAddress = mysqli_fetch_array($resultAddress);
				
				$body_param					= array();
				$body_param['CustomerId']	= $andalan_customer_id;
				$body_param['AddressType']	= 'L';
				$body_param['Address']		= $dataAddress['address'];
				$body_param['RT']			= $dataAddress['rt'];
				$body_param['RW']			= $dataAddress['rw'];
				$body_param['Kelurahan']	= $dataAddress['village_name'];
				$body_param['Kecamatan']	= $dataAddress['district_name'];
				$body_param['City']			= $dataAddress['regency_name'];
				$body_param['ZipCode']		= $dataAddress['zip_code'];
				$body_param['AreaPhone']	= $dataRequest['area_code'];
				$body_param['PhoneNo']		= $dataRequest['phone'];
				$body_param['MobileNo']		= $dataRequest['mobile_phone'];
				
				$jsonData = json_encode($body_param);
				$queryUpdate = "UPDATE profile_update_request SET log_legal = '$jsonData' WHERE id = '$id_profile_update'";
				mysqli_query($mysql_connection, $queryUpdate);
				
				$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_param));
				$api_response['afis_response'] 	= $afis_response;	
		
				if ($afis_response->StatusCode == 500) {
					
					$queryUpdate = "UPDATE profile_update_request SET status_legal = 0 WHERE id = '$id_profile_update'";
					mysqli_query($mysql_connection, $queryUpdate);
				} 
				else {
					$queryUpdate = "UPDATE profile_update_request SET status_legal = 1 WHERE id = '$id_profile_update'";
					mysqli_query($mysql_connection, $queryUpdate);
				}
			}
			//===================
			$queryAddress  = "select b.address, b.rt, b.rw,
							  f.name as village_name, e.name as district_name, 
							  d.name as regency_name, c.name as province_name, 
							  b.zip_code 
							  from profile_update_request_address a 
							  left join address b on a.address_id=b.id 
							  left join provinces c on b.province_id=c.id 
							  left join regencies d on b.regency_id=d.id
							  left join districts e on b.district_id=e.id
							  left join villages f on b.village_id=f.id
							  where a.profile_update_request_id='$id_profile_update' 
							  and a.address_type='DOMICILE' ";
	
	
			$resultAddress = mysqli_query($mysql_connection, $queryAddress);
			
			if(mysqli_num_rows($resultAddress) > 0) {
				
				$dataAddress = mysqli_fetch_array($resultAddress);
				
				$body_param					= array();
				$body_param['CustomerId']	= $andalan_customer_id;
				$body_param['AddressType']	= 'D';
				$body_param['Address']		= $dataAddress['address'];
				$body_param['RT']			= $dataAddress['rt'];
				$body_param['RW']			= $dataAddress['rw'];
				$body_param['Kelurahan']	= $dataAddress['village_name'];
				$body_param['Kecamatan']	= $dataAddress['district_name'];
				$body_param['City']			= $dataAddress['regency_name'];
				$body_param['ZipCode']		= $dataAddress['zip_code'];
				$body_param['AreaPhone']	= $dataRequest['area_code'];
				$body_param['PhoneNo']		= $dataRequest['phone'];
				$body_param['MobileNo']		= $dataRequest['mobile_phone'];
				
				$jsonData = json_encode($body_param);
				$queryUpdate = "UPDATE profile_update_request SET log_domicile = '$jsonData' WHERE id = '$id_profile_update'";
				mysqli_query($mysql_connection, $queryUpdate);
				
				$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_param));
				$api_response['afis_response'] 	= $afis_response;	
		
				if ($afis_response->StatusCode == 500) {
					
					$queryUpdate = "UPDATE profile_update_request SET status_domicile = 0 WHERE id = '$id_profile_update'";
					mysqli_query($mysql_connection, $queryUpdate);
				} 
				else {
					$queryUpdate = "UPDATE profile_update_request SET status_domicile = 1 WHERE id = '$id_profile_update'";
					mysqli_query($mysql_connection, $queryUpdate);
				}
			}
			
			//===================
			
			$queryAddress  = "select b.address, b.rt, b.rw,
							  f.name as village_name, e.name as district_name, 
							  d.name as regency_name, c.name as province_name, 
							  b.zip_code 
							  from profile_update_request_address a 
							  left join address b on a.address_id=b.id 
							  left join provinces c on b.province_id=c.id 
							  left join regencies d on b.regency_id=d.id
							  left join districts e on b.district_id=e.id
							  left join villages f on b.village_id=f.id
							  where a.profile_update_request_id='$id_profile_update' 
							  and a.address_type='OFFICE' ";
	
	
			$resultAddress = mysqli_query($mysql_connection, $queryAddress);
			
			if(mysqli_num_rows($resultAddress) > 0) {
				
				$dataAddress = mysqli_fetch_array($resultAddress);
				
				$body_param					= array();
				$body_param['CustomerId']	= $andalan_customer_id;
				$body_param['AddressType']	= 'O';
				$body_param['Address']		= $dataAddress['address'];
				$body_param['RT']			= $dataAddress['rt'];
				$body_param['RW']			= $dataAddress['rw'];
				$body_param['Kelurahan']	= $dataAddress['village_name'];
				$body_param['Kecamatan']	= $dataAddress['district_name'];
				$body_param['City']			= $dataAddress['regency_name'];
				$body_param['ZipCode']		= $dataAddress['zip_code'];
				$body_param['AreaPhone']	= $dataRequest['area_code'];
				$body_param['PhoneNo']		= $dataRequest['phone'];
				$body_param['MobileNo']		= $dataRequest['mobile_phone'];
				
				$jsonData = json_encode($body_param);
				$queryUpdate = "UPDATE profile_update_request SET log_office = '$jsonData' WHERE id = '$id_profile_update'";
				mysqli_query($mysql_connection, $queryUpdate);
				
				$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_param));
				$api_response['afis_response'] 	= $afis_response;	
		
				if ($afis_response->StatusCode == 500) {
					
					$queryUpdate = "UPDATE profile_update_request SET status_office = 0 WHERE id = '$id_profile_update'";
					mysqli_query($mysql_connection, $queryUpdate);
				} 
				else {
					$queryUpdate = "UPDATE profile_update_request SET status_office = 1 WHERE id = '$id_profile_update'";
					mysqli_query($mysql_connection, $queryUpdate);
				}
			}
			//==================================== AFIS CALL ====================================
		} 
		
		//==================== INBOX ====================
		$content					 = array();
		$content['ticket_number'] 	 = $dataRequest['ticket_number'];
		$content['input_date'] 		 = $dataRequest['request_date'];
		$content['status'] 		 	 = $status;
		$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
		$inbox_message				 .= 'Konsumen yang terhormat, terima kasih atas kepercayaan Anda kepada Andalan Finance. ';
		
		if($status == 'APPROVE')
			$inbox_message .= 'Permohonan pembaharuan data diri Anda telah disetujui.';
		
		if($status == 'REJECT')
			$inbox_message .= 'Permohonan pembaharuan data diri Anda belum dapat kami setujui. Untuk informasi lebih lanjut silahkan hubungi Customer Service kami. ';
		
		$content['message']			 = $inbox_message;
		$content = json_encode($content);
		
		//insert ke tabel inbox
		$now			= date('Y-m-d H:i:s');
		$queryCheck 	= "select id from inbox where content like '%".$dataRequest['ticket_number']."%' ";
		$resultCheck	= mysqli_query($mysql_connection, $queryCheck);
		$dataCheck		= mysqli_fetch_array($resultCheck);
		$id_inbox		= $dataCheck['id'];
		$queryInsert 	= "update inbox set date='$now', status='0', content='$content' where id='$id_inbox'";
		mysqli_query($mysql_connection, $queryInsert);
		//===============================================

		echo 'success';
		exit;
		
	} else {
		echo "empty";
		exit;
	}
?>
