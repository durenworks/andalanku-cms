<?php
ob_clean();
$myfile = fopen("temp_files/newfile.txt", "w") or die("Unable to open file!");
$txt = "John Doe\n";
fwrite($myfile, $txt);
$txt = "Jane Doe\n";
fwrite($myfile, $txt);
fclose($myfile);

header('Content-Description: File Transfer');
	header('Content-Disposition: attachment; filename='.basename($myfile));
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($myfile));
	header("Content-Type: text/plain");
	readfile($myfile);
?>