<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname 		= $_SESSION['fullname'];
?>

<!-- daterange picker -->
<link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<link href="plugins/datepicker/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
<link href="plugins/timepicker/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Konsumen</li>
			  <li>Pengambilan Jaminan</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-check-square-o"></i> Pengambilan Jaminan</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row form-group">
					<div class="col-md-6">
						<div class="input-group">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input class="form-control pull-right active" name="searchDate" id="searchDate" type="text" placeholder="Tanggal">
						</div>
					</div>
					<div class="col-md-3">
						<select name="src_status" class="form-control" id="src_status">
							<option value=''>Semua Status</option>
							<option value='INPUT REQUEST'>INPUT REQUEST</option>
							<option value='ON PROCESS'>ON PROCESS</option>
							<option value='DONE'>DONE</option>
						</select>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-6">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword (Nama / Nomor Telepon)" />
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Cari</button>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->
	
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Detail Pengambilan Jaminan</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-6">
				<div class="form-group">
					<label for="ticket_number">Nomor Tiket</label>
					<input type="hidden" id="id_collateral_claim" name="id_collateral_claim" value="0">
					<input type="text" name="ticket_number" readonly="readonly" class="form-control" id="ticket_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="request_date">Tanggal Permintaan</label>
					<input type="text" name="request_date" readonly="readonly" class="form-control" id="request_date">
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="customer_name">Nama</label>
					<input type="text" name="customer_name" readonly="readonly" class="form-control" id="customer_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="phone_number">Telepon</label>
					<input type="text" name="phone_number" readonly="readonly" class="form-control" id="phone_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="agreement_number">Nomor Kontrak</label>
					<input type="text" name="agreement_number" readonly="readonly" class="form-control" id="agreement_number">
				</div>
			</div>			
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="taking_date">Tanggal Pengambilan</label>
					<input type="text" name="taking_date" readonly="readonly" class="form-control" id="taking_date">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="status">Status</label>
					<input type="text" name="status" readonly="readonly" class="form-control" id="status">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="ready_date">Tanggal Bisa Diambil</label>
					<input type="text" name="ready_date" class="form-control" id="ready_date">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="form_number">Nomor Form Pengambilan</label>
					<input type="text" name="form_number" class="form-control" id="form_number">
				</div>
			</div>
			
			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Tutup</button>
		<button data-remodal-action="confirm" class="remodal-confirm">Simpan</button>
	</div>


    <?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>

	<!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
	<script src="plugins/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.js" type="text/javascript"></script>
	
	<script>

		$body = $("body");
		
		$('#searchDate').daterangepicker({"autoApply" : true});

		function searchData(searchPage) {

			$body.addClass("loadingClass");

            var formData = {
								'src_product'	: $('select[name=src_product]').val(),
								'src_status'	: $('select[name=src_status]').val(),
								'searchDate'	: $('input[name=searchDate]').val(),
								'keyword'		: $('input[name=keyword]').val(),
								'page'			: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_collateral_claim.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }
		
		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );
		
		function viewDetail(collateralClaimID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_collateral_claim_detail.php',
                data: 'collateralClaimID=' + collateralClaimID,
                dataType: 'json'
            }).done(function(data) { 
			
				$("#id_collateral_claim").val(data.id);
				$("#ticket_number").val(data.ticket_number);
				$("#request_date").val(data.request_date);
				$("#customer_name").val(data.customer_name);
				$("#phone_number").val(data.phone_number);
				$("#agreement_number").val(data.agreement_number);
				$("#taking_date").val(data.taking_date);
				$("#status").val(data.status);
				$("#ready_date").val(data.ready_date);
				$("#form_number").val(data.form_number);
				
            	$body.removeClass("loadingClass");
                searchData();
            });
        }
		
		$(document).on('confirmation', '.remodal', function() {
			
            if(confirm("Update data Pengambilan Jaminan ?")) {
				
				var formData = {
					'id_collateral_claim'	: $('#id_collateral_claim').val(),
					'ready_date'			: $('input[name=ready_date]').val(),
					'form_number'			: $('input[name=form_number]').val()
				};
				
				$body.addClass("loadingClass");
				
				$.ajax({
					type: "POST",
					url: 'ajax_edit_collateral_claim.php',
					data: formData
				}).done(function(data) { 

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data pengambilan jaminan telah disimpan"));
						modalAdd.close();
						searchData(1);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					} else if (data == 'already_solved') {
						$('#addNotification').html(showNotification("error", "Proses gagal. Status pengambilan jaminan sudah DONE"));
					} 
				});
			}
        });
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
		
		$('#ready_date').datepicker({});
	</script>

  </body>
</html>
