<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>

<!-- daterange picker -->
<link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<link href="plugins/datepicker/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
<link href="plugins/timepicker/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Konsumen</li>
			  <li>Histori Pembayaran</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-file-text-o"></i> Histori Pembayaran</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword (Nama / Nomor Kontrak)" />
					</div>

          <div class="col-md-3">
						<div class="input-group">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input class="form-control pull-right active" name="searchDate" id="searchDate" type="text" placeholder="Tanggal">
						</div>
					</div>

          <div class="col-md-3">
						<select name="src_status" class="form-control" id="src_status">
              <option value='PAYMENT DONE'>Dibayar</option>
							<option value='PAYMENT'>Menunggu Pembayaran</option>
              <option value='PAYMENT FAILED'>Pembayaran Gagal</option>
						</select>
					</div>

					<div class="col-md-3">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
            <button style="margin-left:15px;" type="button" class="btn btn-flat btn-success" onClick="saveToExcel();"><i class="fa fa-file-excel-o"></i> Export To Excel</button>
					</div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->
	
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div class="col-lg-12">
			<h4 id="modal1Title">Detail Histori Pembayaran Konsumen</h4>
			<p id="modal1Desc"><span id="addNotification"></span>
			
			<div class="col-md-12" style="width:116%; margin-left:-50px;">
				<div class="form-group">
					<span id="spanPaymentHistoryList"></span>
				</div>
			</div>
			
			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Tutup</button>
	</div>


	<?php include "custom_loading.php"; ?>

  <!-- jQuery 2.1.4 -->
  <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
  <!-- Bootstrap 3.3.2 JS -->
  <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <!-- SlimScroll -->
  <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <!-- FastClick -->
  <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
  <!-- AdminLTE App -->
  <script src="js/app.min.js" type="text/javascript"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="js/demo.js" type="text/javascript"></script>

	<script>
      window.REMODAL_GLOBALS = {
          NAMESPACE: 'remodal',
          DEFAULTS: {
              hashTracking: true,
              closeOnConfirm: false,
              closeOnCancel: true,
              closeOnEscape: true,
              closeOnOutsideClick: false,
              modifier: ''
          }
      };
  </script>
  <script src="plugins/remodal/remodal.js"></script>
	<script src="js/inc-function.js"></script>
  
  <!-- date-range-picker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
  <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
  <script src="plugins/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="plugins/timepicker/bootstrap-timepicker.js" type="text/javascript"></script>

	<script>

		$body = $("body");

    $('#searchDate').daterangepicker({"autoApply" : true});

		function searchData(searchPage) {

      $body.addClass("loadingClass");            

			var formData = {
                'src_status'	: $('select[name=src_status]').val(),
								'searchDate'	: $('input[name=searchDate]').val(),
                'keyword'	: $('input[name=keyword]').val(),
								'page'		: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_payment_history.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );
		
		function viewContract(customerID) {

			$body.addClass("loadingClass");

            var formData = { 'customerID'	: customerID };

            $.ajax({
                type: "POST",
                url: 'ajax_search_payment_history_customer.php',
                data: formData
            }).done(function(data) {
                $('#spanPaymentHistoryList').html(data);
                $body.removeClass("loadingClass");
            });
        }

    function saveToExcel() {
			
			var src_status 	= $('select[name=src_status]').val();
			var searchDate	= $('input[name=searchDate]').val();
			var keyword		= $('input[name=keyword]').val();
		
			//alert($('select[name=src_status]').val());
			window.open("excel_payment_history.php?src_status="+$('select[name=src_status]').val()+"&searchDate="+$('input[name=searchDate]').val()+"&keyword="+$('input[name=keyword]').val());
    }
    
		
	</script>

  </body>
</html>
