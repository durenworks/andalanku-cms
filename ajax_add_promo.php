<?php

	$_ref = $_REQUEST['_ref'];
	if($_ref == md5('promo#'.date('d-m-Y'))) {
		$currentURL = 'promo';
	}

	include "check-admin-session.php";
	include "plugins/image-resize/lib/ImageResize.php";

	function uploadImage($file) {

		$response = "";

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/promo_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'andalanku_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

				//resize image
				$image = new \Eventviva\ImageResize($folder . '/' . $newnamefile);
				$image->resize(1080, 607, $allow_enlarge = True);
				$image->save($folder . '/' . $newnamefile);

			} else {
				$response = "";
			}
		}

		return $response;
	}

	$title 				= sanitize_sql_string($_REQUEST["title"]);
	$id_promo_type		= sanitize_sql_string($_REQUEST["id_promo_type"]);
	$content 			= sanitize_sql_string($_REQUEST["content"]);
	$promo_start_date 	= sanitize_sql_string($_REQUEST["promo_start_date"]);
	$promo_expired_date = sanitize_sql_string($_REQUEST["promo_expired_date"]);
	
	
	$tempArray 	= explode("/",$promo_start_date);
	$promo_start_date	= $tempArray[2]."-".$tempArray[0]."-".$tempArray[1];
	
	$tempArray 	= explode("/",$promo_expired_date);
	$promo_expired_date	= $tempArray[2]."-".$tempArray[0]."-".$tempArray[1];

	if ($title <> '' && $content <> '') {

		//upload gambar dulu
		$imageFileName1 = "";
		if (!empty($_FILES['imageInput1'])) {
			$imageFileName1 = uploadImage($_FILES['imageInput1']);
		} else {
			$imageFileName1 = "";
		}

		$content = str_replace("\r\n","<br />",$content );

		$now = date("Y-m-d H:i:s");
		
		$slug = strtolower($title);
		$slug = str_replace(" ", "-", $slug);
		$i = 0;
		$slug_exist = true;
		$new_slug	= $slug;
		
		while($slug_exist) {
			
			$query = "select * from promos where slug='$new_slug'";
			$result= mysqli_query($mysql_connection, $query);
			if(mysqli_num_rows($result) == 0) {
				$slug_exist = false;
			}
			else {
				$i++;
				$new_slug = $slug.'-'.str_pad($i, 2, '0', STR_PAD_LEFT);
			}
		}

		$query = "	INSERT INTO promos(promo_date, promo_start_date, promo_expired_date, 
					title, content, image, slug, id_promo_type)
					VALUES ('$now', '$promo_start_date', '$promo_expired_date', 
					'$title', '$content', '$imageFileName1', '$new_slug', '$id_promo_type')";
		mysqli_query($mysql_connection, $query);
		
		$inbox_content = $content;
		
		//==================== INBOX ====================
		/*$content			= array();
		$content['message']	= $inbox_content;
		
		if($imageFileName1 <> '') $content['image'] = $backend_url.'/'.$promo_image_folder.'/'.$imageFileName1;
		else $content['image'] = '';
		
		$content = json_encode($content);
		
		//insert ke tabel inbox (customer_id = -1 untuk semua customer)
		$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
						values('-1', '$now', 'promo', '$title', '0', 
						'$content')";
		mysqli_query($mysql_connection, $queryInsert);*/
		//===============================================

		echo 'success';
		exit;
		
	} else {
		echo "empty";
		exit;
	}
?>
