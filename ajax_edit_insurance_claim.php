<?php
	include "check-admin-session.php";

	$id_insurance_claim	= sanitize_int($_REQUEST["id_insurance_claim"]);
	$pic				= sanitize_sql_string($_REQUEST["pic"]);

	if ($id_insurance_claim <> 0 && $pic <> '') {
		
		//cek dulu apakah sudah on handle 
		$query 	= "select * from insurance_claims where id='$id_insurance_claim'";
		$result = mysqli_query($mysql_connection, $query);
		$data 	= mysqli_fetch_array($result);
		
		$id_customer = $data['customer_id'];
		
		if($data['status'] == 'ON HANDLE') {
			echo 'already_solved';
			exit; 
		}
	
		$now = date("Y-m-d H:i:s");
		
		$query 	= "update insurance_claims set status='ON HANDLE', pic='$pic'    
				   where id='$id_insurance_claim'";
		mysqli_query($mysql_connection, $query);
		
		//==================== INBOX ====================
		$content					 = array();
		$content['ticket_number'] 	 = $data['ticket_number'];
		$content['input_date'] 		 = $data['claim_date'];
		$content['status'] 		 	 = 'ON HANDLE';
		$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
		$inbox_message				 .= 'Konsumen yang terhormat, terima kasih atas kepercayaan Anda kepada Andalan Finance. ';
		$inbox_message				 .= 'Kami telah menindaklanjuti permintaan Anda. Permintaan Anda akan ditangani oleh '.$pic;
		$content['message']			 = $inbox_message;
		$content = json_encode($content);
		
		//insert ke tabel inbox
		$now			= date('Y-m-d H:i:s');
		$queryCheck 	= "select id from inbox where content like '%".$data['ticket_number']."%' ";
		$resultCheck	= mysqli_query($mysql_connection, $queryCheck);
		$dataCheck		= mysqli_fetch_array($resultCheck);
		$id_inbox		= $dataCheck['id'];
		$queryInsert 	= "update inbox set date='$now', status='0', content='$content' where id='$id_inbox'";
		mysqli_query($mysql_connection, $queryInsert);
		
		/*$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
						values('$id_customer', '$now', 'insurance claim', 'E-Claim Asuransi', '0', 
						'$content')";
		mysqli_query($mysql_connection, $queryInsert);*/
		//===============================================
		
        echo 'success';
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>