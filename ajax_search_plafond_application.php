<?php
	include "check-admin-session.php";

	$status 		= sanitize_sql_string($_REQUEST["src_status"]);
	$searchDate		= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$page 			= sanitize_int($_REQUEST["page"]);
	
	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";
	} else {
		$startDate	= date("Y-m")."-01 00:00:00";
		$endDate	= date("Y-m")."-31 23:59:59";
	}

	$query 	= "select COUNT(id) as num
				from plafond_applications a 
				left join customers b on a.customer_id=b.id_customer 
				where (ticket_number like '%$keyword%' or customer_name like '%$keyword%' or phone_number like '%$keyword%') 
				and application_date>='$startDate' and application_date<='$endDate' ";
	if($status <> '') $query = $query." and status='$status' ";			  

	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;

	$query 	= "select a.*, b.customer_name, b.phone_number 
				from plafond_applications a 
				left join customers b on a.customer_id=b.id_customer 
				where (ticket_number like '%$keyword%' or customer_name like '%$keyword%' or phone_number like '%$keyword%') 
				and application_date>='$startDate' and application_date<='$endDate' ";
	if($status <> '') $query = $query." and status='$status' ";			  	
	$query = $query." order by application_date DESC LIMIT $start,$limit";
	
	$result = mysqli_query($mysql_connection, $query); 

	echo "<table class='table table-hover'>
			  <tr>
					<th width='5%'>No</th>
					<th width='10%'>Nomor Tiket</th>
					<th width='10%'>Tanggal Pengajuan</th>
					<th width='10%'>Nama Konsumen</th>
					<th width='12%'>Penghasilan Per Bulan</th>
					<th width='12%'>Penghasilan Tambahan</th>
					<th width='12%'>Pekerjaan Sampingan</th>
					<th width='12%'>Jumlah Plafond</th>
					<th width='8%'>Status</th>
					<th width='8%'>Detail</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {
		
		//cek apakah ada dokumen yang diupload
		$queryDocs	= 'select * from plafond_application_documents where plafond_application_id='.$data[id];
		$resultDocs	= mysqli_query($mysql_connection, $queryDocs);
		if(mysqli_num_rows($resultDocs) > 0) {
			$downloadLink = '<br><a href="download_document?type=plafond&id='.$data[id].'&ticket='.$data[ticket_number].'" target="_blank"><i class="fa fa-paperclip"></i> Dokumen</a>';
		}
		else {
			$downloadLink = '';
		}
		
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data[ticket_number] . $downloadLink . '</td>
				  <td>' . date("d-m-Y H:i:s", strtotime($data[application_date])) . '</td>
				  <td>' . $data[customer_name] . '<br>' . $data[phone_number] . '</td>
				  <td>Rp ' . number_format($data[monthly_income],2,',','.') . '</td>
				  <td>Rp ' . number_format($data[additional_income],2,',','.') . '</td>
				  <td>' . $data[side_job] . '</td>
				  <td>Rp ' . number_format($data[plafond_amount],2,',','.') . '</td>
				  <td>' . $data[status] . '</td>
				  <td>
					<a href="#modal" onclick="viewDetail('.$data[id].')"><i class="fa fa-search-plus"></i> View Detail</a>
				  </td>
				</tr>';
		$i++;
	}

	echo "</table>";

	include "inc-paging.php";
?>
