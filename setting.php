<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Master Data</li>
			  <li>Setting</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-gears"></i> Setting</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->


	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Edit Setting</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-12">
				<div class="form-group">
					<label for="setting_key">Setting Name</label>
					<input type="hidden" id="id_setting" name="id_setting" value="0">
					<input type="text" name="setting_key" placeholder="Setting Name" class="form-control" id="setting_key" readonly="readonly">
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="setting_value">Setting Value</label>
					<input type="text" name="setting_value" placeholder="Setting Value" class="form-control" id="setting_value">
				</div>
			</div>

			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
		<button data-remodal-action="confirm" class="remodal-confirm">OK</button>
	</div>

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData() {

			$body.addClass("loadingClass");

			var formData = {
								'keyword'	: $('input[name=keyword]').val()
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_setting.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData();

		var modalAdd = $('[data-remodal-id=modal]').remodal( );

        $(document).on('confirmation', '.remodal', function() {

            var formData = {
                'id_setting'					: $('#id_setting').val(),
				'setting_key'					: $('input[name=setting_key]').val(),
				'setting_value'					: $('input[name=setting_value]').val()
            };


			$body.addClass("loadingClass");

			$.ajax({
				type: "POST",
				url: 'ajax_edit_setting.php',
				data: formData
			}).done(function(data) {

				data = $.trim(data);
				$body.removeClass("loadingClass");

				if (data == 'success') {
					$("#mainNotification").html(showNotification("success", "Data setting telah disimpan"));
					modalAdd.close();
					searchData();
					$('#dataSpan').html(data);
					resetdata();
					setTimeout(clearnotif, 5000);

				} else if (data == 'empty') {
					$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
				} 

			});
			
        });

		function resetdata() {
            $("#id_setting").val(0);
            $("#setting_key").val("");
			$("#setting_value").val("");			
            $("#addNotification").html("");
			$("#setting_value").attr("placeholder", "Setting Value");
        }

        function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

		function getedit(settingID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_detail.php',
                data: 'type=stg&id=' + settingID,
                dataType: 'json'
            }).done(function(data) { 
			
				$("#id_setting").val(data.id_setting);
				$("#setting_key").val(data.setting_key);
				$("#setting_value").val(data.setting_value);

				if(data.setting_key == 'Call Center') $("#setting_value").attr("placeholder", "+62 21 xxxxxx");
				else if(data.setting_key == 'Email Info') $("#setting_value").attr("placeholder", "email@domain.com");
				else if(data.setting_key == 'Facebook') $("#setting_value").attr("placeholder", "http://www.facebook.com/yourname");
				else if(data.setting_key == 'Instagram') $("#setting_value").attr("placeholder", "http://www.instagram.com/yourname");
				else if(data.setting_key == 'Whatsapp Number') $("#setting_value").attr("placeholder", "+628xxxxxxxxx");
				else if(data.setting_key == 'Youtube') $("#setting_value").attr("placeholder", "https://www.youtube.com/channel/yourchannel");

            	$body.removeClass("loadingClass");
            });
        }

        $(document).on('closing', '.remodal', function(e) {
            resetdata();
        });


	</script>

  </body>
</html>
