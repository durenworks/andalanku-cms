<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>

<!-- daterange picker -->
<link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<link href="plugins/datepicker/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
<link href="plugins/timepicker/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Konsumen</li>
			  <li>Broadcast Informasi</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-bullhorn"></i> Broadcast Informasi</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row form-group">
					<div class="col-md-6">
						<div class="input-group">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input class="form-control pull-right active" name="searchDate" id="searchDate" type="text" placeholder="Tanggal">
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
						<a href="#modal">
							<button type="button" class="btn btn-primary  btn-flat " ><i class="fa fa-plus"></i> Tambah Broadcast Informasi</button>
						</a>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->


	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Tambah Broadcast Informasi</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-12">
				<div class="form-group">
					<label for="title">Judul Informasi</label>
					<input type="hidden" id="id_info" name="id_info" value="0">
					<input type="text" name="title" placeholder="Judul Informasi" class="form-control" id="title">
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="content">Isi Informasi</label>
					<textarea name="content" placeholder="Isi Informasi" rows="5" class="form-control" id="content"></textarea>
				</div>
			</div>

			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
		<button data-remodal-action="confirm" class="remodal-confirm">OK</button>
	</div>

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>
	
	<!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
	<script src="plugins/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.js" type="text/javascript"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'keyword'	: $('input[name=keyword]').val(),
								'page'		: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_information_blast.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);
		
		$('#searchDate').daterangepicker({"autoApply" : true});

		var modalAdd = $('[data-remodal-id=modal]').remodal( );

        $(document).on('confirmation', '.remodal', function() {
			
			var form_data = new FormData();
			form_data.append('id_info', $('input[name=id_info]').val());
          	form_data.append('title', $('input[name=title]').val());
			form_data.append('content', $('textarea[name=content]').val());
            
			$body.addClass("loadingClass");

			if ($("#id_info").val() == '0') {
				$.ajax({
					type: "POST",
					url: 'ajax_add_information_blast.php',
					processData: false,
					contentType: false,
					data: form_data
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data broadcast informasi telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					}
				});
			} else {
				$.ajax({
					type: "POST",
					url: 'ajax_edit_information_blast.php',
					processData: false,
					contentType: false,
					data: form_data
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data broadcast informasi telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					}

				});
			}
			
        });

		function resetdata() {
            $("#id_info").val(0);
            $("#title").val("");
			$("#content").val("");
            $("#modal1Title").html('Tambah Broadcast Informasi');
            $("#addNotification").html("");
        }

        function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

		function getedit(infoID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_detail.php',
                data: 'type=inf&id=' + infoID,
                dataType: 'json'
            }).done(function(data) { 
			
				$("#id_info").val(data.id);
				$("#title").val(data.title);
				$("#content").val(data.content);
				$("#modal1Title").html('Edit Broadcast Informasi');
				
				$body.removeClass("loadingClass");
            });
        }

        function deleteInfo(infoID, title){

        	if (confirm('Anda yakin akan menghapus informasi ' + title + '?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_delete_information_blast.php',
                    data: 'infoID=' + infoID  
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Data broadcast informasi telah dihapus"));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	} else if (data == 'in_use') {
						$('#mainNotification').html(showNotification("error", "Informasi tidak dapat dihapus"));
					}
                });
            }
        }
		
		function publishInfo(infoID, title){

        	if (confirm('Anda yakin akan mempublikasikan informasi ' + title + '?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_publish_information_blast.php',
                    data: 'infoID=' + infoID  
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Data broadcast informasi telah dipublikasikan"));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	} 
					else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					}
                });
            }
        }

        $(document).on('closing', '.remodal', function(e) {
            resetdata();
        });
	</script>

  </body>
</html>
