<?php 

	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";

	session_start();
	
	$username	= $_SESSION['username'];
	$password	= $_SESSION['password'];
	
	$query	= "select * from users
    		  where username='$username' and 
    		  password='$password' and status='ACTIVE'";
	
	$result	= mysqli_query($mysql_connection, $query); 

	if(mysqli_num_rows($result)==0) {
		
		header("location:index.php?e=03");
		exit;
	}
	
	if($currentURL == '') {
		$currentURL = basename($_SERVER['PHP_SELF']); 
		$currentURL = str_replace('.php','',$currentURL);
	}
	
	if(strpos($currentURL, 'ajax_') !== false || 
	   strpos($currentURL, 'excel_') !== false || 
	   strpos($currentURL, 'legal_preview') !== false || 
	   strpos($currentURL, 'add_') !== false || 
	   strpos($currentURL, 'edit_') !== false || 
	   strpos($currentURL, 'download_document') !== false ||
	   strpos($currentURL, 'direct_message_detail') !== false) 
	{
		$currentURL = basename($_SERVER['HTTP_REFERER']); 
		$currentURL = str_replace('.php','',$currentURL); 
	}
	
	$arraySkipURL = array("dashboard", "profile", "ajax_get_detail", "login_otp", 
						  "login_otp_process", "ajax_search_dm_detail", "tracking", 
						  "agent_pks_pdf");
	
	if(in_array($currentURL, $arraySkipURL)) {
		//skip page permission check
	}
	else {
		
		$loginType	= $_SESSION['loginType'];
		
		$query = "select a.id  
				  from user_level_menu a 
				  left join menus b on a.menu_id=b.id 
				  where a.user_level_id='$loginType' 
				  and b.url='$currentURL'"; 
		$result	= mysqli_query($mysql_connection, $query); 
		
		if(mysqli_num_rows($result) == 0) {
			echo "<center><br><br><br><br><b>
					Maaf, Anda tidak berhak mengakses halaman ini.<br>
					<a href='#' onClick='window.history.back();'>Back</a>
				  </b></center>";
			exit;
		}
	}
	
	// untuk keperluan paging
	$limit 			= 10; 
	$adjacents 		= 1;	
?>