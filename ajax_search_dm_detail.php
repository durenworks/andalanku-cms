<?php
	//include "check-admin-session.php";	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	$customer_id			= sanitize_sql_string($_REQUEST["customer_id"]);
	
	$query 			= "SELECT id, sender_id, title,
						DATE_FORMAT(date, '%a, %e %b %k:%i') dateonly, 
						DATE_FORMAT(date, '%a, %e %b') forcompare,
						DATE_FORMAT(date,'%k:%i') timeonly, 
						content, status
						FROM   inbox 
						WHERE (sender_id = $customer_id and customer_id = -2)
						OR (sender_id = -2 and customer_id = $customer_id)
						AND sender_id IS NOT NULL
						ORDER BY date ASC
						";
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-borderless'>";
	$empty = true;
	while ($data = mysqli_fetch_array($result)) {

		$queryMedia = "SELECT COUNT(id) as num FROM inbox_media WHERE inbox_id = '$data[id]'";
		$resultMedia = mysqli_query($mysql_connection, $queryMedia);
		$dataMedia 			= mysqli_fetch_array($resultMedia);
		$total_media 	= $dataMedia['num'];

		$content = json_decode($data['content']);
		$message = $content->message;
		

		$secondDate = date('D, j M');
		
		if ($data['dateonly'] != $secondDate) {
			$time = $data['dateonly'];
		}
		else {
			$time = $data['timeonly'];
		}

		$rightDate = '';
		$leftDate = '';
		

		if ($data['sender_id'] == '-2') {
			$leftDate = '';
			$rightDate = '<p class="text-right">'. $time .'</b>';
			$message = '<p class="text-right">'. $data['title'] . '<br><br>' . strip_tags($message) .'</p>';
		}
		else {
			$rightDate = '';
			if ($total_media > 0) {
				$rightDate = '<a href="download_document?type=dm&id='.$data['id'].'" target="_blank"><i class="fa fa-paperclip"></i> Attachment</a>';
			}
			$leftDate = '<b>' . $time . '</b>';
			$message = '<b>' . $data['title'] . '<br><br>' . $message . '</b>';
		}

		if (!$empty) {
			$empty = true;
		}

		echo '<tr>';
				  
		echo '		  
				  <td width="13%">'. @$leftDate .'</td>
				  <td width = "77%">'. @$message .'</td>
				  <td width="10%">' . @$rightDate . '</td>
			  </tr>';
	}

	$queryUpdate = "update inbox set status = 1 WHERE (sender_id = $customer_id and customer_id = -2)";
	mysqli_query($mysql_connection, $queryUpdate);

	echo "</table>";
?>