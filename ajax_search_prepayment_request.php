<?php
	include "check-admin-session.php";

	$status 		= sanitize_sql_string($_REQUEST["src_status"]);
	$searchDate		= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$page 			= sanitize_int($_REQUEST["page"]);
	
	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";
	} else {
		$startDate	= date("Y-m")."-01 00:00:00";
		$endDate	= date("Y-m")."-31 23:59:59";
	}

	$query 	= "select COUNT(a.id) as num
				from prepayment_requests a 
				left join customers b on a.customer_id=b.id_customer 
				where (b.customer_name like '%$keyword%' or b.phone_number like '%$keyword%') 
				and request_date>='$startDate' and request_date<='$endDate' ";
	if($status <> '') $query = $query." and status='$status' ";
					  

	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;

	$query 	= "select a.*, b.customer_name, b.phone_number   
			   from prepayment_requests a 
			   left join customers b on a.customer_id=b.id_customer 
			   where (b.customer_name like '%$keyword%' or b.phone_number like '%$keyword%') 
			   and request_date>='$startDate' and request_date<='$endDate'  ";
	if($status <> '') $query = $query." and status='$status' ";
	$query = $query." order by request_date ASC LIMIT $start,$limit"; 
	
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
					<th width='5%'>No</th>
					<th width='10%'>Nomor Tiket</th>
					<th width='10%'>Tanggal Permintaan</th>
					<th width='15%'>Nama Konsumen</th>
					<th width='10%'>Telepon</th>
					<th width='10%'>Nomor Kontrak</th>
					<th width='15%'>Tanggal Pelunasan Maju</th>
					<th width='10%'>Status</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data['ticket_number'] . '</td>
				  <td>' . date("d-m-Y H:i:s", strtotime($data['request_date'])) . '</td>
				  <td>' . $data['customer_name'] . '</td>
				  <td>' . $data['phone_number'] . '</td>
				  <td>' . $data['agreement_number'] . '</td>
				  <td>' . date("d-m-Y", strtotime($data['prepayment_date'])) . '</td>
				  <td>' . $data['status'] . '</td>
				  <td>
					<a href="#modal" onclick="viewDetail('.$data[id].')" class="btn btn-sm btn-warning"><i class="fa fa-file-text-o"></i> View Detail</a>
				  </td>
			  </tr>';
		$i++;
	}

	echo "</table>";

	include "inc-paging.php";
?>
