<?php
	include "check-admin-session.php";

	$status 		= sanitize_sql_string($_REQUEST["src_status"]);
	$searchDate		= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$province_id	= sanitize_int($_REQUEST["province_id"]);
	$regency_id		= sanitize_int($_REQUEST["regency_id"]);
	$page 			= sanitize_int($_REQUEST["page"]);
	
	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";
	} else {
		$startDate	= date("Y-m")."-01 00:00:00";
		$endDate	= date("Y-m-t")." 23:59:59";
	}

	$query 	= "select COUNT(*) as num
				from register_agent_history a 
				left join customers b on a.id_customer=b.id_customer 
				where (agent_name like '%$keyword%' or ticket_number like '%$keyword%') 
				and register_agent_date>='$startDate' and register_agent_date<='$endDate' ";
	if($status <> '') $query = $query." and a.status='$status' ";
	if($province_id <> '0') $query = $query." and a.province_id='$province_id' ";
	if($regency_id <> '') $query = $query." and a.regency_id='$regency_id' ";
					  

	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;

	$query 	= "select a.* 
				from register_agent_history a 
				left join customers b on a.id_customer=b.id_customer 
				where (agent_name like '%$keyword%' or ticket_number like '%$keyword%') 
				and register_agent_date>='$startDate' and register_agent_date<='$endDate' ";
	if($status <> '') $query = $query." and a.status='$status' ";
	if($province_id <> '0') $query = $query." and a.province_id='$province_id' ";
	if($regency_id <> '') $query = $query." and a.regency_id='$regency_id' ";
	$query = $query." order by register_agent_date ASC LIMIT $start,$limit"; 
	
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
					<th width='5%'>No</th>
					<th width='8%'>Nomor Tiket</th>
					<th width='12%'>Tanggal</th>
					<th width='10%'>NIK Upline</th>
					<th width='15%'>Nama</th>
					<th>Alamat</th>
					<th width='10%'>Status</th>
					<th width='12%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {
		
		$queryAddress  = "select a.address, a.rt, a.rw, a.zip_code,  
						  e.name as village_name, d.name as district_name, 
						  c.name as regency_name, b.name as province_name  						  
						  from register_agent_history a  
						  left join provinces b on a.province_id=b.id 
						  left join regencies c on a.regency_id=c.id
						  left join districts d on a.district_id=d.id
						  left join villages e on a.village_id=e.id
						  where a.id_register_agent='".$data['id_register_agent']."' ";
		$resultAddress = mysqli_query($mysql_connection, $queryAddress);
		$dataAddress = mysqli_fetch_array($resultAddress);
		
		$downloadLink = '<br><a href="download_document?type=agent_registration&id='.$data['id_register_agent'].'&ticket='.$data['ticket_number'].'" target="_blank"><i class="fa fa-paperclip"></i> Dokumen</a>';
		
		/*if($data[status] == 'APPROVED')
			$downloadLinkPDF = '<br><a href="agent_pks_pdf?id='.$data['id_register_agent'].'" target="_blank"><i class="fa fa-file-pdf-o"></i> Download PKS</a>';
		else 
			$downloadLinkPDF = '';*/
		
		if($data[status] == 'APPROVED') {
			
			$filename = 'Perjanjian_Kerjasama_Agen_'.$data['ticket_number'].'.pdf';
			
			$downloadLinkPDF = '<br><a href="user_files/inbox_files/'.$filename.'" target="_blank"><i class="fa fa-file-pdf-o"></i> Download PKS</a>';
		}
		else 
			$downloadLinkPDF = '';
		
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data[ticket_number] . $downloadLink . '</td>
				  <td>' . date("d-m-Y H:i:s", strtotime($data[register_agent_date])) . '</td>
				  <td>' . $data[nik_upline] . '</td>
				  <td>' . $data[agent_name] . '</td>
				  <td>' 
						. $dataAddress[address] . '
						RT '.$dataAddress[rt].' / RW '.$dataAddress[rw].' <br>
						'.$dataAddress[village_name].', '.$dataAddress[district_name].', <br>
						'.$dataAddress[regency_name].', '.$dataAddress[province_name].' '.$dataAddress[zip_code].'
				  </td>
				  <td>' . $data[status] . $downloadLinkPDF . '</td>
				  <td>
					<a href="#modal" onclick="viewDetail('.$data[id_register_agent].')"><i class="fa fa-search-plus"></i> View Detail</a>
				  </td>
				</tr>';
		$i++;
	}

	echo "</table>";

	include "inc-paging.php";
?>
