<?php
	include "check-admin-session.php";

	$insuranceClaimID = sanitize_int($_REQUEST["insuranceClaimID"]);
	
	$query 	= "select * from insurance_claims where id='$insuranceClaimID'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
	$id_customer = $data['customer_id'];
	
	if($data['status'] == 'SUBMITTED') {
		
		$now = date("Y-m-d H:i:s");
		
		$query 	= "update insurance_claims set status='ON PROCESS' 
				   where id='$insuranceClaimID'";
		mysqli_query($mysql_connection, $query);
		
		//==================== INBOX ====================
		$content					 = array();
		$content['ticket_number'] 	 = $data['ticket_number'];
		$content['input_date'] 		 = $data['claim_date'];
		$content['status'] 		 	 = 'ON PROCESS';
		$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
		$inbox_message				 .= 'Konsumen yang terhormat, terima kasih atas kepercayaan Anda kepada Andalan Finance. ';
		$inbox_message				 .= 'Kami telah menerima permintaan Anda dan saat ini permintaan Anda sedang kami tindak lanjuti.';
		$content['message']			 = $inbox_message;
		$content = json_encode($content);
		
		//insert ke tabel inbox
		$now			= date('Y-m-d H:i:s');
		$queryCheck 	= "select id from inbox where content like '%".$data['ticket_number']."%' ";
		$resultCheck	= mysqli_query($mysql_connection, $queryCheck);
		$dataCheck		= mysqli_fetch_array($resultCheck);
		$id_inbox		= $dataCheck['id'];
		$queryInsert 	= "update inbox set date='$now', status='0', content='$content' where id='$id_inbox'";
		mysqli_query($mysql_connection, $queryInsert);
		
		/*$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
						values('$id_customer', '$now', 'insurance claim', 'E-Claim Asuransi', '0', 
						'$content')";
		mysqli_query($mysql_connection, $queryInsert);*/
		//===============================================
	}
	
	$query 	= "select a.*, b.customer_name, b.phone_number  
			   from insurance_claims a 
			   left join customers b on a.customer_id=b.id_customer 
			   where a.id='$insuranceClaimID'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
	
	$data['claim_date']	= date("d-m-Y H:i:s", strtotime($data['claim_date']));
	$data['event_date']	= date("d-m-Y", strtotime($data['event_date']));
	
	echo json_encode($data);
?>
