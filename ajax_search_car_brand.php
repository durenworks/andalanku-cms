<?php
	include "check-admin-session.php";

	$keyword	= sanitize_sql_string($_REQUEST["keyword"]);
	$page 		= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(*) as num
					   from car_brands
					   where name like '%$keyword%' ";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select * 
			  from car_brands
			  where name like '%$keyword%'  
			  order by name ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='5%'>No</th>
				  <th width='30%'>Gambar</th>
				  <th width='40%'>Nama</th>
				  <th width='10%'>Status</th>
				  <th width='15%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		if($data['is_active'] == '1') $status = 'Aktif';
		else $status = 'Tidak Aktif';
	
		echo '<tr>
				  <td>'.$i.'</th>
				  <td><img src="user_files/car_brand_image/' . @$data[image] . '" width="120px" alt=" "></td>
				  <td>' . @$data[name] . '</td>
				  <td>' . $status . '</td>
				  <td align="center">
					<a href="#modal" onclick="getedit(' . @$data[id_car_brand] . ')">Edit</a> | 
					<a href="#" onclick="deleteCarBrand(' . @$data[id_car_brand] . ',\'' . @$data[image] . '\',\'' . @$data[name] . '\')" title="Hapus">Delete</a>
				  </td>
				  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>