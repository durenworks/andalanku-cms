<?php
	include "check-admin-session.php";

	$carTypeID = sanitize_int($_REQUEST["carTypeID"]);

	if ($carTypeID <> 0) {
		
		$queryDelete = "DELETE FROM car_trims WHERE id_car_type='$carTypeID'";
		mysqli_query($mysql_connection, $queryDelete);
		
		$queryMedia = "select a.id_media, b.url 
					   FROM car_type_media a 
					   Left JOIN media b on a.id_media=b.id_media 
					   WHERE a.id_car_type='$carTypeID'";
		$resultMedia= mysqli_query($mysql_connection, $queryMedia);
		$idMediaList= '';
		while($dataMedia = mysqli_fetch_array($resultMedia)) {
			
			$idMedia		= $dataMedia['id_media'];
			$idMediaList	= $idMediaList.$idMedia.',';
			$url			= $dataMedia['url'];
			
			@unlink("user_files/media_image/".$url);
		}
		
		if($idMediaList<>'') $idMediaList = substr($idMediaList,0,strlen($idMediaList)-1);
		
		$queryDelete = "DELETE FROM media WHERE id_media in ( ".$idMediaList." )";
		mysqli_query($mysql_connection, $queryDelete);
		
		$queryDelete = "DELETE FROM car_type_media WHERE id_car_type='$carTypeID'";
		mysqli_query($mysql_connection, $queryDelete);
				

		$query = "DELETE FROM car_types WHERE id_car_type='$carTypeID'";
		mysqli_query($mysql_connection, $query);

		echo 'success';

	} else {
		echo "empty";
	}
?>
