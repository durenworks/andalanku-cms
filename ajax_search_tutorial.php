<?php
	include "check-admin-session.php";

	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$is_active		= sanitize_sql_string($_REQUEST["src_is_active"]);
	$page 			= sanitize_int($_REQUEST["page"]);

	$query 			= "select COUNT(*) as num
					   from tutorials 
					   where question like '%$keyword%' ";
	if($is_active <> '') $query = $query." and is_active='$is_active' ";

	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;

	$query 	= "select *
			   from tutorials 
			   where question like '%$keyword%' ";
	if($is_active <> '') $query = $query." and is_active='$is_active' ";
	$query = $query." order by id_tutorial ASC LIMIT $start,$limit";
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
					<th width='5%'>No</th>
					<th>Pertanyaan</th>
					<th width='15%'>Status</th>
					<th width='20%'>Jawaban</th>
					<th width='15%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		if($data['is_active'] == 'TRUE') $status = 'Aktif';
		else $status = 'Tidak Aktif';
	
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . @$data[question] . '</td>
				  <td>' . @$status . '</td>
				  <td><a href="#modal" onclick="getAnswer('. @$data[id_tutorial] .');">Daftar Jawaban</a></td>
				  <td align="center">
					<a href="edit_tutorial?id=' . @$data[id_tutorial] . '">Edit</a> | 
					<a href="#" onclick="deleteTutorial(' . @$data[id_tutorial] . ',\'' . @$data[question] . '\')">Delete</a>
				  </td>
				</tr>';
		$i++;
	}

	echo "</table>";

	include "inc-paging.php";
?>
