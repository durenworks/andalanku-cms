<?php
	include "check-admin-session.php";
	
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Data_Payment_History.xls");
	
	echo "Data Payment History<br><br>";

	$status 		= sanitize_sql_string($_REQUEST["src_status"]);
	$searchDate		= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$page 			= sanitize_int($_REQUEST["page"]);

	//var_dump($keyword);

	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";

		$sql_date	= "and ph.inquiry_date between '$startDate' and '$endDate'";
	} else {
		$startDate	= date("Y-m")."-01 00:00:00";
		$endDate	= date("Y-m")."-31 23:59:59";		
	}
	
	if($status!='') {
		$sql_status = "and ph.payment_status = '$status'";
	}
	
	if($keyword!='') {
		$sql_key = "and (c.customer_name like '%$keyword%' or cl.agreement_number like '%$keyword%')";
    }

	$query = "select cl.agreement_number,cl.branch,cl.sisa_pinjaman,c.customer_name,c.phone_number,
        ph.id_customer,ph.transaction_code,ph.installment_number,
        DATE_FORMAT(ph.inquiry_date,'%d-%m-%Y %H:%i' ) as inquiry_date,
        ph.periode,DATE_FORMAT(ph.due_date, '%d-%m-%Y') AS due_date,ph.amount,
        ph.admin_fee,ph.penalty,ph.total_amount,ph.payment_status  
        from payment_history ph
        left join customers c on ph.id_customer = c.id_customer 
        left join agreement_list cl on ph.id_customer = cl.customer_id and ph.contract_no = cl.agreement_number 
        where c.andalan_customer_id is not null and ph.payment_status <> 'INQUIRY' 
        $sql_key $sql_date $sql_status 
        order by ph.inquiry_date,c.customer_name ASC";
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
            <tr>
              <th width='2%'>No</th>
              <th>Nomor Kontrak</th>
              <th>Nama</th>
              <th>Telefon</th>
              <th>Branch</th>
              <th>Sisa Pinjaman</th>
              <th>Transaction Code</th> 
              <th>Installment Number</th> 
              <th>Periode</th> 
              <th>Inquiry Date</th>
              <th>Due Date</th>
              <th>Amount</th>
              <th>Admin Fee</th>
              <th>Penalty</th>
              <th>Total Amount</th>	
              <th>Payment Status</th>
            </tr>";

	$i = 1;

	while ($data = mysqli_fetch_array($result)) {
		
		$customer_id = $data['id_customer'];
        $sisa_pinjaman = number_format($data[sisa_pinjaman],0);
        $amount = number_format($data[amount],0);
        $admin_fee = number_format($data[admin_fee]);
        $penalty = number_format($data[penalty]);
		$total_amount = number_format($data[total_amount],0);
		if ($data['payment_status'] == "PAYMENT"){ $payment_status = "Menunggu Pembayaran"; }
		else if ($data['payment_status'] == "PAYMENT DONE"){ $payment_status = "Dibayar"; }
		else{ $payment_status = "Pembayaran Gagal"; }

		echo '<tr>
				  <td align="right">'.$i.'.</th>
				  <td>' . $data[agreement_number] . '</td>
				  <td>' . $data[customer_name] . '</td>
				  <td>' . $data[phone_number] . '</td>
				  <td>' . $data[branch] . '</td>
				  <td>' . $data[sisa_pinjaman] . '</td>	
                  <td>' . $data[transaction_code] . '</td> 
                  <td>' . $data[installment_number] . '</td> 
                  <td>' . $data[periode] . '</td> 
                  <td>' . $data[inquiry_date] . '</td>
                  <td>' . $data[due_date] . '</td> 
                  <td>' . $data[amount] . '</td>
                  <td>' . $data[admin_fee] . '</td>
                  <td>' . $data[penalty] . '</td> 
				  <td>' . $data[total_amount] . '</td>
				  <td>' . $payment_status . '</td>
			  </tr>';
		$i++;
	}

	echo "</table>";
?>