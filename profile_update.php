<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname 		= $_SESSION['fullname'];
?>

<!-- daterange picker -->
<link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<link href="plugins/datepicker/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
<link href="plugins/timepicker/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Konsumen</li>
			  <li>Pembaharuan Data</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-vcard-o"></i> Permintaan Pembaharuan Data Konsumen</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-6">
						<div class="input-group">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input class="form-control pull-right active" name="searchDate" id="searchDate" type="text" placeholder="Tanggal">
						</div>
					</div>
					<div class="col-md-2">
						<select name="src_status" class="form-control" id="src_status">
							<option value=''>Semua Status</option>
							<option value='PROCESS'>PROCESS</option>
							<option value='APPROVE'>APPROVE</option>
							<option value='REJECT'>REJECT</option>
						</select>
					</div>
					<div class="col-md-2">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-2">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Cari</button>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->
	
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Detail Permintaan Pembaharuan Data Konsumen</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-6">
				<div class="form-group">
					<label for="ticket_number">Nomor Tiket</label>
					<input type="hidden" id="id_profile_update" name="id_profile_update" value="0">
					<input type="hidden" id="customer_id" name="customer_id" value="0">
					<input type="text" name="ticket_number" readonly="readonly" class="form-control" id="ticket_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="request_date">Tanggal Input</label>
					<input type="text" name="request_date" readonly="readonly" class="form-control" id="request_date">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="customer_name">Nama</label>
					<input type="text" name="customer_name" readonly="readonly" class="form-control" id="customer_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="mobile_phone">Handphone</label>
					<input type="text" name="mobile_phone" readonly="readonly" class="form-control" id="mobile_phone">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="area_code">Kode Area</label>
					<input type="text" name="area_code" readonly="readonly" class="form-control" id="area_code">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="phone">Nomor Telepon</label>
					<input type="text" name="phone" readonly="readonly" class="form-control" id="phone">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="old_address_domicile">Alamat Domisili Lama</label>
					<textarea name="old_address_domicile" readonly="readonly" rows="5" class="form-control" id="old_address_domicile"></textarea>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="new_address_domicile">Alamat Domisili Baru</label>
					<textarea name="new_address_domicile" readonly="readonly" rows="5" class="form-control" id="new_address_domicile"></textarea>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="old_address_office">Alamat Kantor Lama</label>
					<textarea name="old_address_office" readonly="readonly" rows="5" class="form-control" id="old_address_office"></textarea>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="new_address_office">Alamat Kantor Baru</label>
					<textarea name="new_address_office" readonly="readonly" rows="5" class="form-control" id="new_address_office"></textarea>
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<span id="spanBtnProcess"></span>
				</div>
			</div>
			
			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Tutup</button>
	</div>


    <?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>

	<!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
	<script src="plugins/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.js" type="text/javascript"></script>
	
	<script>

		$body = $("body");
		
		$('#searchDate').daterangepicker({"autoApply" : true});

		function searchData(searchPage) {

			$body.addClass("loadingClass");

            var formData = {
								'src_status'	: $('select[name=src_status]').val(),
								'searchDate'	: $('input[name=searchDate]').val(),
								'keyword'		: $('input[name=keyword]').val(),
								'page'			: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_profile_update.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }
		
		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );
		
		function viewDetail(profileUpdateID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_profile_update_detail.php',
                data: 'profileUpdateID=' + profileUpdateID,
                dataType: 'json'
            }).done(function(data) { 

				$("#id_profile_update").val(data.id);
				$("#customer_id").val(data.user_id);
				$("#ticket_number").val(data.ticket_number);
				$("#request_date").val(data.request_date);
				$("#customer_name").val(data.customer_name);
				$("#old_address_domicile").val(data.old_address_domicile);
				$("#new_address_domicile").val(data.new_address_domicile);
				$("#old_address_office").val(data.old_address_office);
				$("#new_address_office").val(data.new_address_office);
				$("#area_code").val(data.area_code);
				$("#phone").val(data.phone);
				$("#mobile_phone").val(data.mobile_phone);
				
				if(data.status == 'PROCESS') {
					var btn = '<button type="button" class="btn btn-flat btn-lg btn-success" onclick="processData(\'APPROVE\')">APPROVE</button>';
					btn = btn + ' &nbsp;&nbsp;&nbsp; ';
					btn = btn + '<button type="button" class="btn btn-flat btn-lg btn-danger" onclick="processData(\'REJECT\')">REJECT</button>';
					btn = btn + ' <br>&nbsp;&nbsp;&nbsp; ';
					$("#spanBtnProcess").html(btn);
				}
				else {
					$("#spanBtnProcess").html('');
				}
				
            	$body.removeClass("loadingClass");
            });
        }
		
		function processData(status) {
			
			if(confirm(status+' permintaan pembaharuan data konsumen ?')) {
								
				$body.addClass("loadingClass");

				var form_data = new FormData();
				form_data.append('id_profile_update', $('input[name=id_profile_update]').val());
				form_data.append('customer_id', $('input[name=customer_id]').val());
				form_data.append('status', status);
				
				$.ajax({
					type: "POST",
					url: 'ajax_process_profile_update.php',
					processData: false,
					contentType: false,
					data: form_data
				}).done(function(data) { console.log(data);

					data = $.trim(data); 
					
					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data Sudah Disimpan"));
						modalAdd.close();
						setTimeout(clearnotif, 5000);
						searchData(1);						
					} else if (data == 'empty') {
						$('#mainNotification').html(showNotification("error", "Input Tidak Lengkap"));
					} 
				});
			}
		}
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
	</script>

  </body>
</html>
