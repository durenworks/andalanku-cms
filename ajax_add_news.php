<?php

	$_ref = $_REQUEST['_ref'];
	if($_ref == md5('news#'.date('d-m-Y'))) {
		$currentURL = 'news';
	}
	
	include "check-admin-session.php";
	include "plugins/image-resize/lib/ImageResize.php";

	function uploadImage($file) {

		$response = "";

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/news_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'andalanku_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

				//resize image
				$image = new \Eventviva\ImageResize($folder . '/' . $newnamefile);
				$image->resize(1080, 607, $allow_enlarge = True);
				$image->save($folder . '/' . $newnamefile);

			} else {
				$response = "";
			}
		}

		return $response;
	}

	$title 		= sanitize_sql_string($_REQUEST["title"]);
	$content 	= sanitize_sql_string($_REQUEST["content"]);

	if ($title <> '' && $content <> '') {

		//upload gambar dulu
		$imageFileName1 = "";
		if (!empty($_FILES['imageInput1'])) {
			$imageFileName1 = uploadImage($_FILES['imageInput1']);
		} else {
			$imageFileName1 = "";
		}

		$content = str_replace("\r\n","<br />",$content );

		$now = date("Y-m-d H:i:s");
		
		$slug = strtolower(preg_replace("/[^A-Za-z0-9 ]/", "",  $title));
		$slug = str_replace(" ", "-", $slug);
		$i = 0;
		$slug_exist = true;
		$new_slug	= $slug;
		
		while($slug_exist) {
			
			$query = "select * from news where slug='$new_slug'";
			$result= mysqli_query($mysql_connection, $query);
			if(mysqli_num_rows($result) == 0) {
				$slug_exist = false;
			}
			else {
				$i++;
				$new_slug = $slug.'-'.str_pad($i, 2, '0', STR_PAD_LEFT);
			}
		}
		

		$query = "	INSERT INTO news(news_date, title, content, image, slug)
					VALUES ('$now', '$title', '$content', '$imageFileName1', '$new_slug')";
		mysqli_query($mysql_connection, $query);

		echo 'success';
		exit;
		
	} else {
		echo "empty";
		exit;
	}
?>
