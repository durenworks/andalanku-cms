<?php
	include "check-admin-session.php";

	$prepaymentReqID = sanitize_int($_REQUEST["prepaymentReqID"]);	
	
	$query 	= "select a.*, b.customer_name, b.phone_number  
			   from prepayment_requests a 
			   left join customers b on a.customer_id=b.id_customer 
			   where a.id='$prepaymentReqID'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
	
	$data['request_date']		= date("d-m-Y H:i:s", strtotime($data['request_date']));
	$data['prepayment_date']	= date("d-m-Y", strtotime($data['prepayment_date']));
	
	echo json_encode($data);
?>
