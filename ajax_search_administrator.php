<?php
	include "check-admin-session.php";

	$src_level 	= sanitize_sql_string($_REQUEST["src_level"]);
	$keyword	= sanitize_sql_string($_REQUEST["keyword"]);
	$page 		= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(a.*) as num
					   from users a 
					   left join user_levels b on a.level=b.id 
					   where (fullname like '%$keyword%' or username like '%$keyword%') ";
	if($src_level <> '') $query = $query." and level='$src_level' ";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select a.*, b.name as user_level_name 
			  from users a 
			  left join user_levels b on a.level=b.id 
			  where (fullname like '%$keyword%' or username like '%$keyword%') ";
	if($src_level <> '') $query = $query." and level='$src_level' ";
	$query = $query." order by fullname ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='20%'>Nama</th>
				  <th width='20%'>Nomor HP</th>
				  <th width='15%'>Username</th>
				  <th width='15%'>Level</th>
				  <th width='15%'>Status</th>
				  <th width='15%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . @$data[fullname] . '</td>
				  <td>' . @$data[phone_number] . '</td>
				  <td>' . @$data[username] . '</td>
				  <td>' . @$data[user_level_name] . '</td>
				  <td>' . @$data[status] . '</td>
				  <td align="center">
							  <a href="#modal" onclick="getedit(' . @$data[user_id] . ')">Edit</a> | 
							  <a href="#" onclick="deleteAdministrator(' . @$data[user_id] . ',\'' . @$data[fullname] . '\')">Delete</a>
							  </td>
				  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>