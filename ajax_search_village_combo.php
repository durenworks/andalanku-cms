<?php
	include "check-admin-session.php";

	$district_id = sanitize_sql_string($_REQUEST["district_id"]);
	$addressType = sanitize_sql_string($_REQUEST["addressType"]);

	$query = "select * 
			  from villages 
			  where district_id='$district_id' 
			  order by name ASC"; 
	$result = mysqli_query($mysql_connection, $query);
	
	$addrType = '';
	if($addressType == 'domicile') $addrType = '_domicile';
	else if($addressType == 'office') $addrType = '_office';

	echo "<select name='village_id".$addrType."' id='village_id".$addrType."' class='form-control'>";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
	}

	echo "</select>";
?>