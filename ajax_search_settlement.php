<?php
	include "check-admin-session.php";

	$searchDate		= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$page 			= sanitize_int($_REQUEST["page"]);

	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";

	} else {
		$startDate	= date("Y-m-d")." 00:00:00";
		$endDate	= date("Y-m-d")." 23:59:59";		
	}

	$sql_date	= "and es.rq_datetime between '$startDate' and '$endDate'";
	
	if($keyword!='') {
			$sql_key = "and (c.customer_name like '%$keyword%' or ti.contract_no like '%$keyword%' or 
						ti.transaction_code like '%$keyword%' or ep.payment_ref like '%$keyword%')";
	}

	$query 			= "select DISTINCT COUNT(*) as num
						from espay_settlement es
						left join espay_settlement_data esd on es.rq_uuid = esd.rq_uuid
						left join espay_payment ep on esd.tx_id = ep.payment_ref
						left join transaction_installment ti on ep.payment_ref = ti.espay_payment_ref
						left join customers c on ti.id_customer = c.id_customer
						where esd.payment_id like 'INS%' and
						$sql_key $sql_date $sql_status";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select DISTINCT
			DATE_FORMAT(es.rq_datetime,'%d-%m-%Y %H:%i' ) as tanggal,
			ti.contract_no as nopel, c.customer_name as nama, 
			ti.installment_number as angsuran, ti.tenor as tenor,
			ti.amount as tagihan, ti.admin_fee as admin, ti.penalty as denda, 
			ti.total_amount as jmlbayar, ti.afis_reference_no as mitra_ref,
			ti.espay_payment_reff as mst_ref, c.phone_number as hppel,
			ti.transaction_code
			from espay_settlement es
			left join espay_settlement_data esd on es.rq_uuid = esd.rq_uuid
			left join espay_payment ep on esd.tx_id = ep.payment_ref
			left join transaction_installment ti on ep.payment_ref = ti.espay_payment_reff
			left join customers c on ti.id_customer = c.id_customer 
			where ti.afis_reference_no != '' and
			esd.payment_id like 'INS%'
			$sql_key $sql_date $sql_status"; 

	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='8%'>Tanggal</th>
				  <th width='8%'>No. Transaksi</th>
				  <th width='8%'>No. Kontrak</th>
				  <th width='10%'>Nama</th>
				  <th width='5%'>Angsuran</th>
				  <th width='5%'>Tenor</th>
				  <th width='8%'>Tagihan</th>
				  <th width='5%'>Admin</th> 
				  <th width='8%'>Denda</th>
				  <th width='8%'>Jml Bayar</th>	
				  <th width='12%'>AFI Ref</th>				  			  
				  <th width='10%'>Espay Ref</th>
				  <th width='10%'>No HP</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {
		echo '<tr>
				  <td align="right">'.$i.'.</th>
				  <td>' . $data[tanggal] . '</td>
				  <td>' . $data[transaction_code] . '</td>
				  <td>' . $data[nopel] . '</td>
				  <td>' . $data[nama] . '</td>
				  <td>' . $data[angsuran] . '</td>
				  <td>' . $data[tenor] . '</td>	
				  <td>' . number_format($data[tagihan],0) . '</td> 
				  <td>' . number_format($data[admin],0) . '</td>
				  <td>' . number_format($data[denda],0) . '</td>
				  <td>' .number_format($data[jmlbayar],0) . '</td>
				  <td>' .$data[mitra_ref] . '</td>
				  <td>' .$data[mst_ref] . '</td>
				  <td>' .$data[hppel] . '</td>				  
			  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>