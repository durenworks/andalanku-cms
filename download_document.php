<?php
	//include "check-admin-session.php";
	include "inc-db.php";
	include "sanitize.inc.php";

	$type			= sanitize_sql_string($_REQUEST["type"]);
	$id				= sanitize_int($_REQUEST["id"]);
	$ticket_number	= sanitize_sql_string($_REQUEST["ticket"]);
	$zip_folder		= 'user_files/zip_files/';
	
	//================ PENGAJUAN KREDIT
	if($type=='loan') {
		
		$queryDocs	= 'select * from loan_application_documents where loan_application_id='.$id;
		$resultDocs	= mysqli_query($mysql_connection, $queryDocs);
		if(mysqli_num_rows($resultDocs) > 0) {
			
			$files = array();
			
			while($dataDocs = mysqli_fetch_array($resultDocs)) {
				
				$files[] = $dataDocs['image'];
			}
			
			$zipname = 'Loan_Application_Documents_'.$ticket_number.'.zip';
			$zip = new ZipArchive;
			$zip->open($zip_folder.$zipname, ZipArchive::CREATE);
			foreach ($files as $file) {
			  $zip->addFile($loan_application_document_image_folder.'/'.$file, $file);
			}
			$zip->close();
			
			header('Content-Type: application/zip');
			header('Content-disposition: attachment; filename='.$zipname);
			header('Content-Length: ' . filesize($zip_folder.$zipname));
			ob_end_clean();
			readfile($zip_folder.$zipname);
			
			ignore_user_abort(true);
			unlink($zip_folder.$zipname);
			if (connection_aborted()) {
				unlink($zip_folder.$zipname);
			}
		}
	}
	//================ COMPLAIN
	else if($type=='complain') {
		
		$queryDocs	= 'select b.url 
					   from complain_media a 
					   left join media b on a.id_media=b.id_media 
					   where a.id_complain='.$id;
		$resultDocs	= mysqli_query($mysql_connection, $queryDocs);
		if(mysqli_num_rows($resultDocs) > 0) {
			
			$files = array();
			
			while($dataDocs = mysqli_fetch_array($resultDocs)) {
				
				$files[] = $dataDocs['url'];
			}
			
			$zipname = 'Complain_Media_'.$ticket_number.'.zip';
			$zip = new ZipArchive;
			$zip->open($zip_folder.$zipname, ZipArchive::CREATE);
			foreach ($files as $file) {
			  $zip->addFile($media_image_folder.'/'.$file, $file);
			}
			$zip->close();
			
			header('Content-Type: application/zip');
			header('Content-disposition: attachment; filename='.$zipname);
			header('Content-Length: ' . filesize($zip_folder.$zipname));
			ob_end_clean();
			readfile($zip_folder.$zipname);
			
			ignore_user_abort(true);
			unlink($zip_folder.$zipname);			
			if (connection_aborted()) {
				unlink($zip_folder.$zipname);
			}
		}
	}
	//================ INSURANCE CLAIM
	else if($type=='ins_claim') {
		
		$queryDocs	= 'select b.url 
					   from insurance_claim_media a 
					   left join media b on a.id_media=b.id_media 
					   where a.id_claim='.$id;
		$resultDocs	= mysqli_query($mysql_connection, $queryDocs);
		if(mysqli_num_rows($resultDocs) > 0) {
			
			$files = array();
			
			while($dataDocs = mysqli_fetch_array($resultDocs)) {
				
				$files[] = $dataDocs['url'];
			}
			
			$zipname = 'Insurance_Claim_Media_'.$ticket_number.'.zip';
			$zip = new ZipArchive;
			$zip->open($zip_folder.$zipname, ZipArchive::CREATE);
			foreach ($files as $file) {
			  $zip->addFile($media_image_folder.'/'.$file, $file);
			}
			$zip->close();
			
			header('Content-Type: application/zip');
			header('Content-disposition: attachment; filename='.$zipname);
			header('Content-Length: ' . filesize($zip_folder.$zipname));
			ob_end_clean();
			readfile($zip_folder.$zipname);
			
			ignore_user_abort(true);
			unlink($zip_folder.$zipname);			
			if (connection_aborted()) {
				unlink($zip_folder.$zipname);
			}
		}
	}
	//================ PENGAJUAN PLAFOND
	if($type=='plafond') {
		
		$queryDocs	= 'select * from plafond_application_documents where plafond_application_id='.$id;
		$resultDocs	= mysqli_query($mysql_connection, $queryDocs);
		if(mysqli_num_rows($resultDocs) > 0) {
			
			$files = array();
			
			while($dataDocs = mysqli_fetch_array($resultDocs)) {
				
				$files[] = $dataDocs['image'];
			}
			
			$zipname = 'Plafond_Application_Documents_'.$ticket_number.'.zip';
			$zip = new ZipArchive;
			$zip->open($zip_folder.$zipname, ZipArchive::CREATE);
			foreach ($files as $file) {
			  $zip->addFile($plafond_application_document_image_folder.'/'.$file, $file);
			}
			$zip->close();
			
			header('Content-Type: application/zip');
			header('Content-disposition: attachment; filename='.$zipname);
			header('Content-Length: ' . filesize($zip_folder.$zipname));
			ob_end_clean();
			readfile($zip_folder.$zipname);
			
			ignore_user_abort(true);
			unlink($zip_folder.$zipname);
			if (connection_aborted()) {
				unlink($zip_folder.$zipname);
			}
		}
	}

	//================ DIRECT MESSAGE
	if($type=='dm') {
		
		$queryInbox = "select customer_name, andalan_customer_id, DATE_FORMAT(date, '%d%m%H%i') formatedDate from inbox
						LEFT JOIN customers on id_customer = sender_id
						WHERE id = '$id'";

		$result = mysqli_query($mysql_connection, $queryInbox);
		$data = mysqli_fetch_assoc($result);
		$name = $data['customer_name'];

		if ($data['andalan_customer_id'] != '') {
			$name = $name . '-' . $data['andalan_customer_id'];	
		}

		$queryDocs	= 'select * from inbox_media where inbox_id='.$id;

		$resultDocs	= mysqli_query($mysql_connection, $queryDocs);
		if(mysqli_num_rows($resultDocs) > 0) {
			
			$files = array();
			
			while($dataDocs = mysqli_fetch_array($resultDocs)) {
				$files[] = $dataDocs['file_name'];
			}
			
			$zipname = 'Direct_Message_'.$name.'_'.$data['formatedDate'].'.zip';
			$zip = new ZipArchive;
			$zip->open($zip_folder.$zipname, ZipArchive::CREATE);
			foreach ($files as $file) {

			  $zip->addFile($inbox_file_folder.'/'.$file, $file);
			}
			$zip->close();

			ob_end_clean();
			header('Content-Type: application/zip');
			header('Content-disposition: attachment; filename='.$zipname);
			header('Content-Length: ' . filesize($zip_folder.$zipname));
			ob_end_clean();
			readfile($zip_folder.$zipname);
			
			ignore_user_abort(true);
			unlink($zip_folder.$zipname);
			if (connection_aborted()) {
				unlink($zip_folder.$zipname);
			}
		}
	}
	//================ AGENT REGISTRATION
	if($type=='agent_registration') {
		
		$query 	= "select id_card_image, npwp_image, bank_account_image, id_customer  
				   from register_agent_history
				   WHERE id_register_agent = '$id'";
		$result = mysqli_query($mysql_connection, $query);
		$data 	= mysqli_fetch_assoc($result);
		$customerID = $data['id_customer'];
		
		$queryDocs = "select * from customer_documents where customer_id='$customerID' and is_active='1' ";
		$resultDocs= mysqli_query($mysql_connection, $queryDocs);
		$arrayCurrentDocs = array();
		
		while ($dataDocs = mysqli_fetch_array($resultDocs)) {
			
			$arrayCurrentDocs[$dataDocs['type']] = $dataDocs['image'];
		}
		
		if(mysqli_num_rows($result) > 0) {
			
			$files = array();
			
			if($data['id_card_image'] <> '')
				$files[] = $data['id_card_image'];	
			else {
				$files[] = $arrayCurrentDocs['FOTO KTP'];
			}
			
			if($data['npwp_image'] <> '') 
				$files[] = $data['npwp_image'];	
			else {
				$files[] = $arrayCurrentDocs['FOTO NPWP'];
			}
			
			if($data['bank_account_image'] <> '')
				$files[] = $data['bank_account_image'];	
			else {
				$files[] = $arrayCurrentDocs['FOTO BUKU REKENING BANK'];
			}			
			
			$zipname = 'Agent_Registration_Documents_'.$ticket_number.'.zip';
			$zip = new ZipArchive;
			$zip->open($zip_folder.$zipname, ZipArchive::CREATE);
			foreach ($files as $file) {
			  if($file<>'')	$zip->addFile($customer_document_image_folder.'/'.$file, $file);
			}
			$zip->close();
			
			header('Content-Type: application/zip');
			header('Content-disposition: attachment; filename='.$zipname);
			header('Content-Length: ' . filesize($zip_folder.$zipname));
			ob_end_clean();
			readfile($zip_folder.$zipname);
			
			ignore_user_abort(true);
			unlink($zip_folder.$zipname);
			if (connection_aborted()) {
				unlink($zip_folder.$zipname);
			}
		}
	}
?>