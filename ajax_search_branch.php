<?php
	include "check-admin-session.php";

	$keyword	= sanitize_sql_string($_REQUEST["keyword"]);
	$page 		= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(*) as num
					   from branches
					   where name like '%$keyword%' or address like '%$keyword%' 
					   or city like '%$keyword%' or provinsi like '%$keyword%' ";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select * 
			  from branches  
			  where name like '%$keyword%' or address like '%$keyword%' 
			  or city like '%$keyword%' or provinsi like '%$keyword%' 
			  order by name ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='5%'>No</th>
				  <th width='20%'>Nama Cabang</th>
				  <th width='10%'>Branch Manager</th>
				  <th>Alamat</th>
				  <th width='10%'>Latitude</th>
				  <th width='10%'>Longitude</th>
				  <th width='10%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1); 

	while ($data = mysqli_fetch_array($result)) {

		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data[name] . '</td>
				  <td>' . $data[branch_manager] . '</td>
				  <td>' . $data[address] . '<br>
					  ' . $data[city] . ', ' . $data[provinsi] . ' ' . $data[zipcode] . '<br>
					  Telp : ' . $data[phone] . '<br>
					  Fax : ' . $data[fax] . '<br>
				  </td>
				  <td>' . $data[latitude] . '</td>
				  <td>' . $data[longitude] . '</td>
				  <td align="center">
					<a href="#modal" onclick="getedit(' . $data[id] . ')">Edit</a>
				  </td>
				  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>