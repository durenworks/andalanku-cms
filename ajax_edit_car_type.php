<?php

	$_ref = $_REQUEST['_ref'];
	if($_ref == md5('car_type#'.date('d-m-Y'))) {
		$currentURL = 'car_type';
	}

	include "check-admin-session.php";
	include "plugins/image-resize/lib/ImageResize.php";
	
	function uploadImage($file) {

		$response = "";

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/media_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'andalanku_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

				//resize image
				$image = new \Eventviva\ImageResize($folder . '/' . $newnamefile);
				$image->resizeToBestFit(800, 600);
				$image->save($folder . '/' . $newnamefile);

			} else {
				$response = "";
			}
		}

		return $response;
	}

	$id_car_type	= sanitize_int($_REQUEST["id_car_type"]);
	$id_car_brand	= sanitize_int($_REQUEST["id_car_brand"]);
	$typeName 		= sanitize_sql_string($_REQUEST["typeName"]);
	$is_active		= sanitize_sql_string($_REQUEST["is_active"]);
	$image_row_count= sanitize_int($_REQUEST["image_row_count"]);

	if ($id_car_type <> '0' && $id_car_brand <> '0' && $typeName <> '' &&  $is_active <> '' ) {
		
		$queryCheck		= "SELECT id_car_type from car_types WHERE name='$typeName' and id_car_type<>'$id_car_type'";
		$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck ) > 0) {
			echo "car_type_exist";
			exit;
		}
	
        $query = "UPDATE car_types SET id_car_brand='$id_car_brand', name='$typeName', is_active='$is_active'
				  where id_car_type='$id_car_type'";
        mysqli_query($mysql_connection, $query);
		
		for($i=0; $i<$image_row_count; $i++) {
			
			$n = $i+1;
			$answerImageName 	= 'answerImage_'.$n;
			$mediaIDName 		= 'media_id_'.$n;
			$mediaTypeName 		= 'media_type_'.$n;
			
			$imageFileName = "";
			if (!empty($_FILES[$answerImageName])) {
				$imageFileName = uploadImage($_FILES[$answerImageName]);
			} else {
				$imageFileName = "";
			}
			
			$id_media			= sanitize_int($_REQUEST[$mediaIDName]);
			$mediaType			= sanitize_sql_string($_REQUEST[$mediaTypeName]);
			
			if($mediaType=='NEW') {
				
				$query = "	INSERT INTO media(url) VALUES ('$imageFileName')";
				mysqli_query($mysql_connection, $query);
				
				//ambil id_media terbaru
				$queryCheck		= "SELECT id_media from media WHERE url='$imageFileName' order by id_media DESC LIMIT 1";
				$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
				$dataCheck		= mysqli_fetch_array($resultCheck);
				$idMediaNew		= $dataCheck['id_media'];
				
				$query = "	INSERT INTO car_type_media(id_car_type, id_media) VALUES ('$id_car_type', '$idMediaNew')";
				mysqli_query($mysql_connection, $query);
			}
			else {
				
				if($imageFileName <> '') {
					
					//hapus dulu gambar media yang lama
					$queryCheck		= "SELECT url from media WHERE id_media='$id_media'";
					$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
					$dataCheck		= mysqli_fetch_array($resultCheck);
					$url			= $dataCheck['url'];
					
					@unlink("user_files/media_image/".$url);
				
					$query = "UPDATE media SET url='$imageFileName' where id_media='$id_media' ";
					mysqli_query($mysql_connection, $query);
				}
			}			
		}
		
        echo 'success';
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>