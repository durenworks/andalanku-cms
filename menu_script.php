<?php
	include "check-admin-session.php";
	
	echo "Start...<br><br>";
	
	$query = "delete from menus";
	mysqli_query($mysql_connection, $query);
	
	$query = "	INSERT INTO menus(id, name, url, icon, parent_id, level, menu_order)
				VALUES 
				
				(100, 'Master Data', '#', 'fa fa-database', 0, 1, 1),
				(101, 'User Level', 'user_level', 'fa fa-tasks', 100, 2, 1),
				(102, 'Level Permission', 'level_permission', 'fa fa-unlock-alt', 100, 2, 2),
				(103, 'Administrator', 'administrator', 'fa fa-user-secret', 100, 2, 3),
				(104, 'Subscriber', 'subscriber', 'fa fa-users', 100, 2, 4),
				(105, 'Settings', 'setting', 'fa fa-gears', 100, 2, 5),
				
				(200,  'Display', '#', 'fa fa-tv', 0, 1, 2),
				(201,  'Berita', 'news', 'fa fa-newspaper-o', 200, 2, 1),
				(203,  'Promo', 'promo', 'fa fa-tags', 200, 2, 2),
				(204,  'Partner', 'partner', 'fa fa-handshake-o', 200, 2, 3),
				(205,  'FAQ', 'faq', 'fa fa-question-circle-o', 200, 2, 4),
				(206, 'Term of Service', 'tos', 'fa fa-file-text-o', 200, 2, 5),
				(207, 'Privacy Policy', 'privacypolicy', 'fa fa-file-text-o', 200, 2, 6),
				
				
				(300,  'Rates', '#', 'fa fa-bar-chart', 0, 1, 3),
				(301,  'Insurance Rate', 'insurance_rate', 'fa fa-shield', 300, 2, 1),
				(302,  'Car Rate', 'car_rate', 'fa fa-car', 300, 2, 2),
				(303,  'Rate', 'rate', 'fa fa-bar-chart', 300, 2, 3),
				
				(400,  'Car Prices', '#', 'fa fa-car', 0, 1, 4),
				(401,  'Merk Mobil', 'car_brand', 'fa fa-car', 400, 2, 1),
				(402,  'Tipe Mobil', 'car_type', 'fa fa-car', 400, 2, 2),
				(403,  'Model Mobil', 'car_trim', 'fa fa-car', 400, 2, 3),
				(404,  'Export / Import', 'car_exim', 'fa fa-file-excel-o', 400, 2, 4),
				
				(500,  'Konsumen', '#', 'fa fa-id-badge', 0, 1, 5),
				(501,  '&nbsp;&nbsp;Data Konsumen', 'customer', 'fa fa-id-badge', 500, 2, 1),
				(502,  'Pembaharuan Data', 'profile_update', 'fa fa-vcard-o', 500, 2, 2),
				(503,  'Pengajuan Kredit', 'loan_application', 'fa fa-shopping-bag', 500, 2, 3),
				(504,  '&nbsp;Klaim Asuransi', 'insurance_claim', 'fa fa-shield', 500, 2, 4),
				(505,  'Pengambilan Jaminan', 'collateral_claim', 'fa fa-check-square-o', 500, 2, 5),
				(506,  'Dokumen Kontrak', 'contract_document', 'fa fa-file-text-o', 500, 2, 6),
				(507,  'Dokumen Pribadi', 'private_document', 'fa fa-file-text-o', 500, 2, 7),
				(508,  'History Pembayaran', 'payment_history', 'fa fa-file-text-o', 500, 2, 8),
				(509,  'Komplain', 'complain', 'fa fa-exclamation-triangle', 500, 2, 9),
				(510,  'Broadcast Informasi', 'information_blast', 'fa fa-bullhorn', 500, 2, 10)			
				";
	mysqli_query($mysql_connection, $query);
	
	echo "Finish...<br><br>";
?>