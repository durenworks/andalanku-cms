<?php
	include "check-admin-session.php";

	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$page 			= sanitize_int($_REQUEST["page"]);

	$query 			= "select COUNT(*) as num
					   from news
					   where title like '%$keyword%' or content like '%$keyword%' ";

	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;

	$query 	= "select *
			   from news
 			   where title like '%$keyword%' or content like '%$keyword%'
			   order by id_news ASC LIMIT $start,$limit";
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
					<th width='5%'>No</th>
					<th width='10%'>Tanggal</th>
					<th width='120px'>Gambar</th>
					<th width='25%'>Judul Berita</th>
					<th>Isi Berita</th>
					<th width='15%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		$content = strip_tags(@$data[content]);
		//$content = str_replace("<br />","",$content);
		$content = substr($content, 0, 200);
		$content = $content."...";

		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . @$data[news_date] . '</td>
				  <td><img src="user_files/news_image/'. @$data[image] .'" alt=" " style="width: 120px;" /></td>
				  <td>' . @$data[title] . '</td>
				  <td>' . $content . '</td>
				  <td align="center">
					<a href="edit_news?id='. @$data[id_news] .'" title="Edit">Edit</a> |
					<a href="#" onclick="deleteNews(' . @$data[id_news] . ',\'' . @$data[image] . '\',\'' . @$data[title] . '\')" title="Hapus">Delete</a>
				  </td>
				</tr>';
		$i++;
	}

	echo "</table>";

	include "inc-paging.php";
?>
