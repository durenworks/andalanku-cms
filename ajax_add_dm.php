<?php	
	//include "check-admin-session.php";
	include "inc-db.php";
	include "sanitize.inc.php";
	include "plugins/image-resize/lib/ImageResize.php";

	function uploadFile($file) {

		$response = "";

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'pdf') {

			$response = "";

		} else {

			$folder = "user_files/inbox_files";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'andalanku_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

				//resize image
				//$image = new \Eventviva\ImageResize($folder . '/' . $newnamefile);
				//$image->resizeToBestFit(1080, 607);
				//$image->save($folder . '/' . $newnamefile);

			} else {
				$response = "";
			}
		}

		return $response;
	}


	$subject 		= sanitize_sql_string($_REQUEST["subject"]);
	$message 	= sanitize_sql_string($_REQUEST["message"]);
	$destination_id = sanitize_sql_string($_REQUEST["dest_id"]);

	if ($subject <> '' && $message <> '' && $destination_id <> '') {

		//upload file dulu
		$fileName1 = "";
		if (!empty($_FILES['fileInput1'])) {
			$fileName1 = uploadFile($_FILES['fileInput1']);
		} else {
			$fileName1 = "";
		}

		$message = str_replace("\r\n","<br />",$message );

		$content					 = array();
		$content['ticket_number'] 	 = '';
		$content['input_date'] 		 = $now;
		$content['status'] 		 	 = '';
		$content['message']			 = $message;
		$content = json_encode($content);

		$now = date("Y-m-d H:i:s");
		
		$queryInsert = "insert into inbox(customer_id, sender_id, 
					date, type, title, content, status) 
					values('$destination_id', '-2', '$now',
					'direct message', '$subject', '$content', '0')";
		mysqli_query($mysql_connection, $queryInsert);

		if ($fileName1 != "") {
			//ambil id yang terakhir
			$query = "select id from inbox where customer_id='$destination_id' 
						and sender_id = '-2' 
						and date = '$now'";
		
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_inbox = $data['id'];

			$queryInsert = "insert into inbox_media (inbox_id, file_name, file_type)
									values('$id_inbox','$fileName1', 'pdf')";
				mysqli_query($mysql_connection, $queryInsert);

			}
		echo 'success';
		exit;
		
	} else {
		echo "empty";
		exit;
	}
?>
