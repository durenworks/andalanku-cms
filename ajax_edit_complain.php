<?php
	include "check-admin-session.php";

	$userID			= $_SESSION['userID'];
	$id_complain	= sanitize_int($_REQUEST["id_complain"]);
	$solution 		= sanitize_sql_string($_REQUEST["solution"]);

	if ($id_complain <> 0 && $solution <> '') {
		
		//cek dulu apakah sudah solved 
		$query 	= "select status from complains where id_complain='$id_complain'";
		$result = mysqli_query($mysql_connection, $query);
		$data 	= mysqli_fetch_array($result);
		if($data['status'] == 'SOLVED') {
			echo 'already_solved';
			exit; 
		}
	
		$now = date("Y-m-d H:i:s");
		
		$query 	= "update complains set status='SOLVED', solved_date='$now', solved_by='$userID', solution='$solution'  
				   where id_complain='$id_complain'";
		mysqli_query($mysql_connection, $query);
		
		$queryDetail = "select * from complains where id_complain='$id_complain'";
		$resultDetail= mysqli_query($mysql_connection, $queryDetail);
		$dataDetail  = mysqli_fetch_array($resultDetail);
		$id_customer = $dataDetail['user_id'];
		
		$queryMedia 	= "select b.url 
						   from complain_media a 
						   left join media b on a.id_media=b.id_media 
						   where id_complain='$id_complain'";
		$resultMedia	= mysqli_query($mysql_connection, $queryMedia);
		$arrayMediaURL 	= array();
		$i = 1;
		while($dataMedia = mysqli_fetch_array($resultMedia) ) {
			
			$arrayMediaURL[$i] = $backend_url.'/'.$media_image_folder.'/'.$dataMedia['url'];
			$i++;
		}
		
		//==================== INBOX ====================
		$content					 = array();
		$content['ticket_number'] 	 = $dataDetail['ticket_number'];
		$content['category'] 	 	 = $dataDetail['category'];
		$content['input_date'] 		 = $dataDetail['submitted_date'];
		$content['status'] 		 	 = 'SOLVED';
		$content['message'] 		 = $dataDetail['message'];
		$content['processed_status'] = 'SOLVED';
		$content['processed_date']   = $dataDetail['on_process_date'];
		$content['solved_status'] 	 = 'SOLVED';
		$content['solved_date']   	 = $dataDetail['solved_date'];
		$content['complain_media']   = $arrayMediaURL;
		$content = json_encode($content);
		
		//insert ke tabel inbox
		$now			= date('Y-m-d H:i:s');
		$queryCheck 	= "select id from inbox where content like '%".$dataDetail['ticket_number']."%' ";
		$resultCheck	= mysqli_query($mysql_connection, $queryCheck);
		$dataCheck		= mysqli_fetch_array($resultCheck);
		$id_inbox		= $dataCheck['id'];
		$queryInsert 	= "update inbox set date='$now', status='0', content='$content' where id='$id_inbox'";
		mysqli_query($mysql_connection, $queryInsert);
		
		/*$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
						values('$id_customer', '$now', 'e complain', 'E-Complain', '0', 
						'$content')";
		mysqli_query($mysql_connection, $queryInsert);*/
		//===============================================
		
        echo 'success';
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>