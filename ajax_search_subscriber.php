<?php
	include "check-admin-session.php";

	$keyword	= sanitize_sql_string($_REQUEST["keyword"]);
	$page 		= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(*) as num
					   from subscribers
					   where product='none' and (name like '%$keyword%' or email like '%$keyword%' or phone like '%$keyword%') ";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select * 
			  from subscribers
			  where product='none' and (name like '%$keyword%' or email like '%$keyword%' or phone like '%$keyword%')  
			  order by created_at ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='15%'>Tanggal</th>
				  <th width='25%'>Nama</th>
				  <th width='20%'>Email</th>
				  <th width='10%'>Telepon</th>
				  <th width='10%'></th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . date("d-m-Y H:i:s", strtotime($data[created_at])) . '</td>
				  <td>' . @$data[name] . '</td>
				  <td>' . @$data[email] . '</td>
				  <td>' . @$data[phone] . '</td>
				  <td>
					<a href="#" onclick="deleteSubscriber(' . @$data[id_subscriber] . ',\'' . @$data[name] . '\')" title="Hapus"><i class="fa fa-trash"></i> Delete</a>
				  </td>
			  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>