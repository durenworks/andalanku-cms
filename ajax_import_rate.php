<?php
	include "check-admin-session.php";
	
	require('plugins/xls/php-excel-reader/excel_reader2.php');
	require('plugins/xls/SpreadsheetReader.php');
	
	$rate_type = sanitize_sql_string($_REQUEST['rate_type']);
	
	//upload excelFile
	if (!empty($_FILES['excelFile'])) { 
		
		$response = "";
		$file = $_FILES['excelFile'];

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'xls' && $extension<>'xlsx') {
			
			$response = "invalid_file_format";

		} else {

			$folder = "user_files/xls_rate_file";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'andalanku_' . $random . '.'.$extension;
			}
			
			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) { 

				//hapus dulu data yang lama
				if($rate_type == 'insurance_rate')  $query = "delete from insurance_rates"; 
				else if($rate_type == 'car_rate')  $query = "delete from car_rates";
				else if($rate_type == 'rates')  $query = "delete from rates";				
				
				mysqli_query($mysql_connection, $query);
				
				$Reader = new SpreadsheetReader($folder . '/' . $newnamefile);
				foreach ($Reader as $lineArray)
				{					
				
					if($rate_type == 'insurance_rate') {
						
						$asuransi		= sanitize_sql_string($lineArray[0]);
						$otr_range_min	= sanitize_int($lineArray[1]);
						$otr_range_max	= sanitize_int($lineArray[2]);
						$wilayah_1		= sanitize_sql_string($lineArray[3]);
						$wilayah_2		= sanitize_sql_string($lineArray[4]);
						$wilayah_3		= sanitize_sql_string($lineArray[5]);
						
						$wilayah_1 		= str_replace("%","",$wilayah_1);
						$wilayah_2 		= str_replace("%","",$wilayah_2);
						$wilayah_3 		= str_replace("%","",$wilayah_3);
						
						if(strtolower($asuransi) <> 'asuransi') {
							
							$query = "INSERT INTO insurance_rates(asuransi, otr_range_min, otr_range_max, 
									  wilayah_1, wilayah_2, wilayah_3) 
									  VALUES ('$asuransi', '$otr_range_min', '$otr_range_max', 
									  '$wilayah_1', '$wilayah_2', '$wilayah_3')";
							mysqli_query($mysql_connection, $query);
						}
					}
					
					else if($rate_type == 'car_rate') {
						
						$tenor			= sanitize_sql_string($lineArray[0]);
						$rate_new		= sanitize_sql_string($lineArray[1]);
						$flat_rate_new	= sanitize_sql_string($lineArray[2]);
						$rate_used		= sanitize_sql_string($lineArray[3]);
						$flat_rate_used	= sanitize_sql_string($lineArray[4]);
						$admin			= sanitize_int($lineArray[5]);
						
						
						$rate_new 		= str_replace("%","",$rate_new);
						$rate_used 		= str_replace("%","",$rate_used);
						$flat_rate_new	= str_replace("%","",$flat_rate_new);
						$flat_rate_used	= str_replace("%","",$flat_rate_used);
						
						if(strtolower($tenor) <> 'tenor') {
							
							$query = "INSERT INTO car_rates(tenor, rate_new, flat_rate_new, 
									  rate_used, flat_rate_used, admin) 
									  VALUES ('$tenor', '$rate_new', '$flat_rate_new', 
									  '$rate_used', '$flat_rate_used', '$admin')";
							mysqli_query($mysql_connection, $query);
						}
					}
				
					else if($rate_type == 'rate') {
						
						$tenor		= sanitize_sql_string($lineArray[0]);
						$rate		= sanitize_sql_string($lineArray[1]);
						$admin		= sanitize_int($lineArray[2]);
						
						$rate 		= str_replace("%","",$rate);
						
						if(strtolower($tenor) <> 'tenor') {
							
							$query = "INSERT INTO rates(tenor, rate, admin) 
									  VALUES ('$tenor', '$rate', '$admin')";
							mysqli_query($mysql_connection, $query);
						}
					}
				}
				
				
				@unlink($folder . '/' . $newnamefile);
				
				echo "success#".$savedData;
				exit;

			} else {
				echo "upload_failed";
				exit;
			}
		}

	
	} else {
		echo "empty";
		exit;
	}
?>