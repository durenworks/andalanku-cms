<?php
	include "check-admin-session.php";

	$keyword		= sanitize_sql_string($_REQUEST["keyword"]);
	$src_car_brand	= sanitize_sql_string($_REQUEST["src_car_brand"]);
	$src_car_type	= sanitize_sql_string($_REQUEST["src_car_type"]);
	$page 			= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(*) as num
					   from car_trims a
					   left join car_types b on a.id_car_type=b.id_car_type 
					   left join car_brands c on b.id_car_brand=c.id_car_brand 
					   where (a.name like '%$keyword%' or b.name like '%$keyword%') ";
	if($src_car_type<>'') $query = $query." and a.id_car_type='$src_car_type' ";
	if($src_car_brand<>'') $query = $query." and b.id_car_brand='$src_car_brand' ";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select a.*, b.name as car_type_name, c.name as car_brand_name 
			  from car_trims a
		      left join car_types b on a.id_car_type=b.id_car_type 
		      left join car_brands c on b.id_car_brand=c.id_car_brand 
		      where (a.name like '%$keyword%' or b.name like '%$keyword%') ";
	if($src_car_type<>'') $query = $query." and a.id_car_type='$src_car_type' ";
	if($src_car_brand<>'') $query = $query." and b.id_car_brand='$src_car_brand' ";
	$query = $query." order by c.name, b.name, a.name ASC LIMIT $start,$limit"; 
	
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='5%'>No</th>
				  <th width='10%'>Merk Mobil</th>
				  <th width='15%'>Tipe Mobil</th>
				  <th width='20%'>Nama Model Mobil</th>
				  <th width='15%'>Asset Code</th>
				  <th width='15%'>Harga</th>
				  <th width='8%'>Status</th>
				  <th width='12%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		if($data['is_active'] == '1') $status = 'Aktif';
		else $status = 'Tidak Aktif';
	
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . @$data[car_brand_name] . '</td>
				  <td>' . @$data[car_type_name] . '</td>
				  <td>' . @$data[name] . '</td>
				  <td>' . @$data[asset_code] . '</td>
				  <td align="left">' . number_format($data['price'],2,',','.') . '</td>
				  <td>' . $status . '</td>
				  <td align="center">
					<a href="#modal" onclick="getedit(' . @$data[id_car_trim] . ')">Edit</a> | 
					<a href="#" onclick="deleteCarTrim(' . @$data[id_car_trim] . ',\'' . @$data[name] . '\')" title="Hapus">Delete</a>
				  </td>
				  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>