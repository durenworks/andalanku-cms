<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname 		= $_SESSION['fullname'];
?>

<!-- daterange picker -->
<link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<link href="plugins/datepicker/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
<link href="plugins/timepicker/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Konsumen</li>
			  <li>Klaim Asuransi</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-shield"></i> Klaim Asuransi</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row form-group">
					<div class="col-md-6">
						<div class="input-group">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input class="form-control pull-right active" name="searchDate" id="searchDate" type="text" placeholder="Tanggal">
						</div>
					</div>
					<div class="col-md-3">
						<select name="src_status" class="form-control" id="src_status">
							<option value=''>Semua Status</option>
							<option value='SUBMITTED'>SUBMITTED</option>
							<option value='ON PROCESS'>ON PROCESS</option>
							<option value='ON HANDLE'>ON HANDLE</option>
						</select>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-6">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword (Nama / Nomor Telepon / Nomor Tiket)" />
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Cari</button>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->
	
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Detail Klaim Asuransi</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-6">
				<div class="form-group">
					<label for="ticket_number">Nomor Tiket</label>
					<input type="hidden" id="id_insurance_claim" name="id_insurance_claim" value="0">
					<input type="text" name="ticket_number" readonly="readonly" class="form-control" id="ticket_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="claim_date">Tanggal Klaim</label>
					<input type="text" name="claim_date" readonly="readonly" class="form-control" id="claim_date">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="customer_name">Nama</label>
					<input type="text" name="customer_name" readonly="readonly" class="form-control" id="customer_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="phone_number">Telepon</label>
					<input type="text" name="phone_number" readonly="readonly" class="form-control" id="phone_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="agreement_number">Nomor Kontrak</label>
					<input type="text" name="agreement_number" readonly="readonly" class="form-control" id="agreement_number">
				</div>
			</div>			
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="claim_type">Tipe Asuransi</label>
					<input type="text" name="claim_type" readonly="readonly" class="form-control" id="claim_type">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="event_case">Jenis Kejadian</label>
					<input type="text" name="event_case" readonly="readonly" class="form-control" id="event_case">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="event_date">Tanggal Kejadian</label>
					<input type="text" name="event_date" readonly="readonly" class="form-control" id="event_date">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="status">Status</label>
					<input type="text" name="status" readonly="readonly" class="form-control" id="status">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="location">Lokasi Kejadian</label>
					<input type="text" name="location" readonly="readonly" class="form-control" id="location">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="notes">Catatan</label>
					<input type="text" name="notes" readonly="readonly" class="form-control" id="notes">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="pic">PIC</label>
					<input type="text" name="pic" class="form-control" id="pic">
				</div>
			</div>
			
			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Tutup</button>
		<button data-remodal-action="confirm" class="remodal-confirm">Selesai Proses</button>
	</div>

	<div class="remodal" data-remodal-id="modalMedia" role="dialog" aria-labelledby="modalMediaTitle" aria-describedby="modalMediaDesc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Detail Media Klaim Asuransi</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<span id="spanMediaList"></span>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Tutup</button>
	</div>

    <?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>

	<!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
	<script src="plugins/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.js" type="text/javascript"></script>
	
	<script>

		$body = $("body");
		
		$('#searchDate').daterangepicker({"autoApply" : true});

		function searchData(searchPage) {

			$body.addClass("loadingClass");

            var formData = {
								'src_product'	: $('select[name=src_product]').val(),
								'src_status'	: $('select[name=src_status]').val(),
								'searchDate'	: $('input[name=searchDate]').val(),
								'keyword'		: $('input[name=keyword]').val(),
								'page'			: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_insurance_claim.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }
		
		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );
		
		function viewDetail(insuranceClaimID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_insurance_claim_detail.php',
                data: 'insuranceClaimID=' + insuranceClaimID,
                dataType: 'json'
            }).done(function(data) {

                let event_case = '';
                let claim_type = '';

                if (data.claim_type === 'TLO'){
                    claim_type = 'TOTAL LOSS'
                }
                else if (data.claim_type === 'ARK'){
                    claim_type = 'ALL RISK'
                }

                if (data.event_case === 'A'){
                    event_case = 'ACCIDENT'
                }
                else if (data.event_case === 'ST'){
                    event_case = 'STOLEN'
                }
                if (data.event_case === 'TLO'){
                    event_case = 'TOTAL LOSS'
                }


                $("#id_insurance_claim").val(data.id);
                $("#ticket_number").val(data.ticket_number);
                $("#claim_date").val(data.claim_date);
                $("#customer_name").val(data.customer_name);
                $("#phone_number").val(data.phone_number);
                $("#agreement_number").val(data.agreement_number);
                $("#claim_type").val(claim_type);
                $("#event_case").val(event_case);
                $("#event_date").val(data.event_date);
                $("#status").val(data.status);
                $("#location").val(data.location);
                $("#notes").val(data.notes);
                $("#pic").val(data.pic);

                if(data.status == 'ON HANDLE') $('input[name="pic"]').attr('readonly', true);
                else $('input[name="pic"]').attr('readonly', false);

                $body.removeClass("loadingClass");

                searchData(1);

            });
        }
		
		function viewMedia(insuranceClaimID) {
			
			$body.addClass("loadingClass");
			
			$('#spanMediaList').html('');

            var formData = { 'insuranceClaimID'	: insuranceClaimID };

            $.ajax({
                type: "POST",
                url: 'ajax_search_insurance_claim_media.php',
                data: formData
            }).done(function(data) {
                $('#spanMediaList').html(data);
                $body.removeClass("loadingClass");
            });
		}
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
		
		$(document).on('confirmation', '.remodal', function() {
			
			if($('input[name=status]').val() == 'ON HANDLE') {
				
				alert('Proses gagal. Status klaim asuransi sudah ON HANDLE');
				return false;
			}
			

            if(confirm("Update status klaim asuransi menjadi ON HANDLE?")) {
				
				var formData = {
					'id_insurance_claim': $('#id_insurance_claim').val(),
					'pic'				: $('input[name=pic]').val()
				};
				
				$body.addClass("loadingClass");
				
				$.ajax({
					type: "POST",
					url: 'ajax_edit_insurance_claim.php',
					data: formData
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data klaim asuransi telah disimpan"));
						modalAdd.close();
						searchData(1);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					} else if (data == 'already_solved') {
						$('#addNotification').html(showNotification("error", "Proses gagal. Status klaim asuransi sudah ON HANDLE"));
					} 
				});
			}
        });
	</script>

  </body>
</html>
