<?php
	include "check-admin-session.php";

	$keyword	= sanitize_sql_string($_REQUEST["keyword"]);
	$page 		= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(*) as num
					   from partners
					   where name like '%$keyword%' or url like '%$keyword%' ";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select * 
			  from partners
			  where name like '%$keyword%' or url like '%$keyword%' 
			  order by name ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='15%'>Gambar</th>
				  <th width='20%'>Nama</th>
				  <th width='20%'>URL</th>
				  <th width='15%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		echo '<tr>
				  <td>'.$i.'</th>
				  <td><img src="user_files/partner_image/' . @$data[image] . '" width="120px" alt=" "></td>
				  <td>' . @$data[name] . '</td>
				  <td>' . @$data[url] . '</td>
				  <td align="center">
					<a href="#modal" onclick="getedit(' . @$data[id_partner] . ')">Edit</a> | 
					<a href="#" onclick="deletePartner(' . @$data[id_partner] . ',\'' . @$data[image] . '\',\'' . @$data[name] . '\')" title="Hapus">Delete</a>
				  </td>
				  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>