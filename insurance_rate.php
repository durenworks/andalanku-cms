<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Rates</li>
			  <li>Insurance Rate</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-shield"></i> Insurance Rate</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row form-group">
					<div class="col-md-4">
						<input type="file" name="excelFile" id="excelFile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-4">
						<button type="button" class="btn btn-warning btn-flat " onClick="submitUpload();"><i class="fa fa-file-excel-o"></i> Import Excel</button>
					</div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->
	
    <?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>
    <script src="js/inc-function.js"></script>

	<script>
	
		$body = $("body");

		function searchData() {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_search_insurance_rate.php'
            }).done(function(data) { 
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData();	

		function submitUpload() {

			$body.addClass("loadingClass");
			
			var excelFile = $('#excelFile').prop('files')[0];

			var form_data = new FormData();
			form_data.append('rate_type', 'insurance_rate');
			form_data.append('excelFile', excelFile);

            $.ajax({
                type: "POST",
                url: 'ajax_import_rate.php',
                processData: false,
				contentType: false,
				data: form_data
            }).done(function(data) { 
				
                $body.removeClass("loadingClass");	
				
				$('#excelFile').val("");		
				
				data = $.trim(data);
				
                if (data == 'success') {
					$("#mainNotification").html(showNotification("success", "Data telah disimpan"));
				} else if (data == 'invalid_file_format') {
					$("#mainNotification").html(showNotification("success", "File yang diupload harus merupakan file XLS atau XLSX"));
				} else if (data == 'upload_failed') {
					$("#mainNotification").html(showNotification("success", "Upload file gagal, cobalah beberapa saat lagi"));
				} else if (data == 'empty') {
					$("#mainNotification").html(showNotification("success", "Silahkan pilih file XLS atau XLSX terlebih dahulu"));
				}
				
				searchData();	
            });
        }
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

	</script>

  </body>
</html>
