<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname 		= $_SESSION['fullname'];
?>

<!-- daterange picker -->
<link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<link href="plugins/datepicker/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
<link href="plugins/timepicker/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Konsumen</li>
			  <li>Pendaftaran Agent</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-address-card"></i> Pendaftaran Agent</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row form-group">
					<div class="col-md-6">
						<div class="input-group">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input class="form-control pull-right active" name="searchDate" id="searchDate" type="text" placeholder="Tanggal">
						</div>
					</div>
					<div class="col-md-3">
						<select name="src_status" class="form-control" id="src_status">
							<option value=''>Semua Status</option>
							<option value='PENDING'>PENDING</option>
							<option value='APPROVED'>APPROVED</option>
							<option value='REJECTED'>REJECTED</option>
							<option value='FAILED'>FAILED</option>
						</select>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-3">
						<select name="province_id" id="province_id" class="form-control" onchange="getCity();">
							<option value=''>Semua Propinsi</option>
							<?php 
								$query = "select * from provinces order by name ASC";
								$result= mysqli_query($mysql_connection, $query);
								while($data = mysqli_fetch_array($result)) {
									
									echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
								}
							?>
						</select>
					</div>
					<div class="col-md-3">
						<span id="spanRegencyID">
						<select name="regency_id" id="regency_id" class="form-control">
							<option value=''>Semua Kota / Kabupaten</option>
						</select>
						</span>
					</div>
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Cari</button>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->
	
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Detail Pendaftaran Agent</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-6">
				<div class="form-group">
					<label for="ticket_number">Nomor Tiket</label>
					<input type="hidden" name="id_register_agent" id="id_register_agent" value="0">
					<input type="text" name="ticket_number" readonly="readonly" class="form-control" id="ticket_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="status">Status</label>
					<input type="text" name="status" readonly="readonly" class="form-control" id="status">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="register_agent_date">Tanggal Pendaftaran</label>
					<input type="text" name="register_agent_date" readonly="readonly" class="form-control" id="register_agent_date">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="agent_name">Nama</label>
					<input type="text" name="agent_name" readonly="readonly" class="form-control" id="agent_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="nik_upline">NIK Upline</label>
					<input type="text" name="nik_upline" readonly="readonly" class="form-control" id="nik_upline">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="upline_name">Nama Upline</label>
					<input type="text" name="upline_name" readonly="readonly" class="form-control" id="upline_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="gender">Jenis Kelamin</label>
					<input type="text" name="gender" readonly="readonly" class="form-control" id="gender">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="place_of_birth">Tempat, Tanggal Lahir</label>
					<input type="text" name="place_of_birth" readonly="readonly" class="form-control" id="place_of_birth">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="id_card_number">Nomor Identitas</label>
					<input type="text" name="id_card_number" readonly="readonly" class="form-control" id="id_card_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" name="email" readonly="readonly" class="form-control" id="email">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="phone_number">Handphone</label>
					<input type="text" name="phone_number" readonly="readonly" class="form-control" id="phone_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="home_phone_number">Telepon Rumah</label>
					<input type="text" name="home_phone_number" readonly="readonly" class="form-control" id="home_phone_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="occupation_name">Pekerjaan</label>
					<input type="text" name="occupation_name" readonly="readonly" class="form-control" id="occupation_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="npwp">NPWP</label>
					<input type="text" name="npwp" readonly="readonly" class="form-control" id="npwp">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="npwp">Kerabat AFI</label>
					<input type="text" name="kerabat_afi" readonly="readonly" class="form-control" id="kerabat_afi">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="npwp">Jabatan Kerabat AFI</label>
					<input type="text" name="jabatan_kerabat_afi" readonly="readonly" class="form-control" id="jabatan_kerabat_afi">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="bank_name">Nama Bank</label>
					<input type="text" name="bank_name" readonly="readonly" class="form-control" id="bank_name">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="bank_account_number">Nomor Rekening</label>
					<input type="text" name="bank_account_number" readonly="readonly" class="form-control" id="bank_account_number">
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="bank_account_holder">Nama Pemilik Rekening</label>
					<input type="text" name="bank_account_holder" readonly="readonly" class="form-control" id="bank_account_holder">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					&nbsp;
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="legal_address">Alamat Legal</label>
					<textarea name="legal_address" readonly="readonly" rows="5" class="form-control" id="legal_address"></textarea>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label for="domicile_address">Alamat Domisili</label>
					<textarea name="domicile_address" readonly="readonly" rows="5" class="form-control" id="domicile_address"></textarea>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="lama_tinggal_tahun">Lama Tinggal (Tahun)</label>
					<input type="text" name="lama_tinggal_tahun" readonly="readonly" class="form-control" id="lama_tinggal_tahun">
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="lama_tinggal_bulan">Lama Tinggal (Bulan)</label>
					<input type="text" name="lama_tinggal_bulan" readonly="readonly" class="form-control" id="lama_tinggal_bulan">
				</div>
			</div>			
			
			<div class="col-md-12">
				<div class="form-group">
					<span id="spanBtnProcess"></span>
				</div>
			</div>
			
			</p>
		</div>
		<br>
		<div class="col-md-12">
			<button data-remodal-action="cancel" class="remodal-cancel">Tutup</button>
		</div>
	</div>


    <?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>

	<!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
	<script src="plugins/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="plugins/timepicker/bootstrap-timepicker.js" type="text/javascript"></script>
	
	<script>

		$body = $("body");
		
		$('#searchDate').daterangepicker({"autoApply" : true});

		function searchData(searchPage) {

			$body.addClass("loadingClass");

            var formData = {
								'src_status'	: $('select[name=src_status]').val(),
								'searchDate'	: $('input[name=searchDate]').val(),
								'keyword'		: $('input[name=keyword]').val(),
								'province_id'	: $('select[name=province_id]').val(),
								'regency_id'	: $('select[name=regency_id]').val(),
								'page'			: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_agent_registration.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }
		
		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );
		
		function viewDetail(agentRegistrationID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_agent_registration_detail.php',
                data: 'agentRegistrationID=' + agentRegistrationID,
                dataType: 'json'
            }).done(function(data) { 

				$("#id_register_agent").val(data.id_register_agent);
				$("#ticket_number").val(data.ticket_number);
				$("#status").val(data.status);
				$("#register_agent_date").val(data.register_agent_date);
				$("#nik_upline").val(data.nik_upline);
				$("#upline_name").val(data.upline_name);
				$("#agent_name").val(data.agent_name);
				$("#gender").val(data.gender);
				$("#place_of_birth").val(data.place_of_birth);
				$("#id_card_number").val(data.id_card_number);
				$("#email").val(data.email);
				$("#phone_number").val(data.phone_number);
				$("#home_phone_number").val(data.area_code+'-'+data.home_phone_number);
				$("#occupation_name").val(data.occupation_name);
				$("#npwp").val(data.npwp);
				$("#kerabat_afi").val(data.kerabat_afi);
				$("#jabatan_kerabat_afi").val(data.kerabat_afi_jabatan);
				$("#bank_name").val(data.bank_name);
				$("#bank_account_number").val(data.bank_account_number);
				$("#bank_account_holder").val(data.bank_account_holder);
				$("#legal_address").val(data.legal_address);
				$("#domicile_address").val(data.domicile_address);
				$("#lama_tinggal_tahun").val(data.lama_tinggal_tahun);
				$("#lama_tinggal_bulan").val(data.lama_tinggal_bulan);
				
				if(data.status == 'PENDING') {
					var btn = '<button type="button" class="btn btn-flat btn-lg btn-success" onclick="processData(\'APPROVED\')">APPROVE</button>';
					btn = btn + ' &nbsp;&nbsp;&nbsp; ';
					btn = btn + '<button type="button" class="btn btn-flat btn-lg btn-danger" onclick="processData(\'REJECTED\')">REJECT</button>';
					btn = btn + ' <br>&nbsp;&nbsp;&nbsp; ';
					$("#spanBtnProcess").html(btn);
				}
				else {
					$("#spanBtnProcess").html('');
				}
								
            	$body.removeClass("loadingClass");
            });
        }
		
		function getCity() {
		
			$body.addClass("loadingClass");

            var formData = { 'province_id'	: $('select[name=province_id]').val(), addressType : '' };

            $.ajax({
                type: "POST",
                url: 'ajax_search_city_combo.php',
                data: formData
            }).done(function(data) {
                $('#spanRegencyID').html(data);
                $body.removeClass("loadingClass");
            });
		}
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
		
		function processData(status) {
			
			var caption = '';
			if(status == 'APPROVED') caption = 'Setujui';
			else if(status == 'REJECTED') caption = 'Tolak';
			
			if(confirm(caption+' permintaan pendaftaran agen ?')) {
								
				$body.addClass("loadingClass");

				var form_data = new FormData();
				form_data.append('id_register_agent', $('input[name=id_register_agent]').val());
				form_data.append('status', status);
				
				$.ajax({
					type: "POST",
					url: 'ajax_process_agent_registration.php',
					processData: false,
					contentType: false,
					data: form_data
				}).done(function(data) {

					data = $.trim(data);  
					
					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Proses Berhasil. Data sudah disimpan"));						
					} else if (data == 'empty') {
						$('#mainNotification').html(showNotification("error", "Input Tidak Lengkap"));
					} else {
						$('#mainNotification').html(showNotification("error", data));
					} 
					
					modalAdd.close();
					setTimeout(clearnotif, 5000);
					searchData(1);
				});
			}
		}
	</script>

  </body>
</html>
