<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Subscriber</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-users"></i> Subscriber</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
						<button type="button" class="btn btn-flat btn-success" onClick="saveToExcel();"><i class="fa fa-file-excel-o"></i> Export To Excel</button>
					</div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>
	<script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'keyword'	: $('input[name=keyword]').val(),
								'page'		: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_subscriber.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);
		
		function saveToExcel() {
			
			var keyword = $('input[name=keyword]').val();
			window.open("excel_subscriber.php?keyword="+keyword+"&type=s");
		}
		
		function deleteSubscriber(subscriberID,subscriberName){

			if (confirm('Apakah Anda yakin akan menghapus subscriber ' + subscriberName +' ?')) {

				$body.addClass("loadingClass");

				var formData = { 'subscriberID'	: subscriberID };

				$.ajax({
					type: "POST",
					url: 'ajax_delete_subscriber.php',
					data: formData
				}).done(function(data) {
					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data Sudah Dihapus"));
						searchData(1);
						setTimeout(clearnotif, 5000);
					}
				});
			}
		}
		
	</script>

  </body>
</html>
