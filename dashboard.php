<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";
	
	$periode = sanitize_sql_string($_REQUEST['periode']);
	
	if($periode == '') $periode = 'Harian';
?>

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>
	  
      <!-- Full Width Column -->
      <div class="content-wrapper">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            </ol>
          </section>
		  <br>	
          <!-- Main content -->
          <section class="content">
            <div class="box box-default">
              <div class="box-header with-border">
                <img src="img/logo.png" style="width:200px;">
              </div>
              <div class="box-body">
			  
				<form action="dashboard" method="POST">
				<div class="row form-group">
					<div class="col-md-2">
						<label>Periode tampilan grafik : </label>
						<select name="periode" class="form-control">
							<option value="Harian" <?php if($periode=='Harian') echo 'selected'; ?>>Harian</option>
							<option value="Mingguan" <?php if($periode=='Mingguan') echo 'selected'; ?>>Mingguan</option>
							<option value="Bulanan" <?php if($periode=='Bulanan') echo 'selected'; ?>>Bulanan</option>
						</select>
					</div>
					<div class="col-md-2">
						<label>&nbsp;</label>
						<input type="submit" class="btn btn-success form-control" value="Tampilkan">
					</div>
				</div>
				</form>
			  
                <div class="row form-group">
					<div class="col-md-6 col-sm-12">
						<canvas id="userChart" width="100%" ></canvas>
					</div>
					<div class="col-md-6 col-sm-12">
						<canvas id="refferantorChart" width="100%" ></canvas>
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-md-6 col-sm-12">
						<canvas id="loanChart" width="100%" ></canvas>
					</div>
					<div class="col-md-6 col-sm-12">
						<canvas id="complainChart" width="100%" ></canvas>
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-md-6 col-sm-12">
						<canvas id="insuranceChart" width="100%" ></canvas>
					</div>
					<div class="col-md-6 col-sm-12">
						<canvas id="collateralChart" width="100%" ></canvas>
					</div>
				</div>
				
				<?php 
					if($periode == 'Harian') {
					
						$startDate 	= date('d-m-Y', strtotime("-30 day"));
						$loopDate	= $startDate;
						$endDate	= date('d-m-Y');
						$label		= '';
						$emptyArrayData	= array();
						
						while(strtotime($loopDate) <= strtotime($endDate)) {
							
							$label = $label."'".date('d', strtotime($loopDate))."',";
							$indexArray = date('Y-m-d', strtotime($loopDate));
							$emptyArrayData[$indexArray] = '0';
							$loopDate 	= date('d-m-Y', strtotime("+1 day", strtotime($loopDate)));
						}
						
						$label = substr($label, 0, strlen($label)-1);
						
						$startDateSQL	= date('Y-m-d', strtotime($startDate)).' 00:00:00';
						$endDateSQL		= date('Y-m-d', strtotime($endDate)).' 23:59:59';
						
						//============================ 1. PENDAFTARAN USER ============================  
						$query	= "select date(registration_date) as reg_date, count(id_customer) as jml 
								   from customers 
								   where registration_date>='$startDateSQL' and registration_date<='$endDateSQL' 
								   group by date(registration_date) ";
						$result = mysqli_query($mysql_connection, $query);
						$arrayData1 = $emptyArrayData;
						while($data = mysqli_fetch_array($result)) {
							$tempDate = $data['reg_date'];
							$arrayData1[$tempDate] = $data['jml'];
						}
						
						$dataContent1 = implode(',',$arrayData1);
						$title1 = "'Jumlah Pendaftaran User ( Periode ".$startDate." s/d ".$endDate." )'";
						
						//============================ 2. REFFERANTOR ============================  
						$query	= "select date(date) as reg_date, count(id) as jml 
								   from loan_applications 
								   where loan_application_referal='Y' and date>='$startDateSQL' and date<='$endDateSQL' 
								   group by date(date) ";
						$result = mysqli_query($mysql_connection, $query); 
						$arrayData2 = $emptyArrayData;
						while($data = mysqli_fetch_array($result)) {
							$tempDate = $data['reg_date'];
							$arrayData2[$tempDate] = $data['jml'];
						}
						
						$dataContent2 = implode(',',$arrayData2);
						$title2 = "'Jumlah Refferantor ( Periode ".$startDate." s/d ".$endDate." )'";
						
						//============================ 3. PEMOHON APLIKASI ============================  
						$query	= "select date(date) as reg_date, count(id) as jml 
								   from loan_applications 
								   where date>='$startDateSQL' and date<='$endDateSQL' 
								   group by date(date) ";
						$result = mysqli_query($mysql_connection, $query);
						$arrayData3 = $emptyArrayData;
						while($data = mysqli_fetch_array($result)) {
							$tempDate = $data['reg_date'];
							$arrayData3[$tempDate] = $data['jml'];
						}
						
						$dataContent3 = implode(',',$arrayData3);
						$title3 = "'Jumlah Pemohon Aplikasi ( Periode ".$startDate." s/d ".$endDate." )'";
						
						//============================ 4. KOMPLAIN ============================  
						$query	= "select date(submitted_date) as reg_date, count(id_complain) as jml 
								   from complains 
								   where submitted_date>='$startDateSQL' and submitted_date<='$endDateSQL' 
								   group by date(submitted_date) ";
						$result = mysqli_query($mysql_connection, $query);
						$arrayData4 = $emptyArrayData;
						while($data = mysqli_fetch_array($result)) {
							$tempDate = $data['reg_date'];
							$arrayData4[$tempDate] = $data['jml'];
						}
						
						$dataContent4 = implode(',',$arrayData4); 
						$title4 = "'Jumlah Komplain ( Periode ".$startDate." s/d ".$endDate." )'";
						
						//============================ 5. KLAIM ASURANSI ============================  
						$query	= "select date(claim_date) as reg_date, count(id) as jml 
								   from insurance_claims 
								   where claim_date>='$startDateSQL' and claim_date<='$endDateSQL' 
								   group by date(claim_date) ";
						$result = mysqli_query($mysql_connection, $query);
						$arrayData5 = $emptyArrayData;
						while($data = mysqli_fetch_array($result)) {
							$tempDate = $data['reg_date'];
							$arrayData5[$tempDate] = $data['jml'];
						}
						
						$dataContent5 = implode(',',$arrayData5); 
						$title5 = "'Jumlah Klaim Asuransi ( Periode ".$startDate." s/d ".$endDate." )'";
						
						//============================ 6. PENGAMBILAN BPKB ============================  
						$query	= "select date(request_date) as reg_date, count(id) as jml 
								   from collateral_requests 
								   where request_date>='$startDateSQL' and request_date<='$endDateSQL' 
								   group by date(request_date) ";
						$result = mysqli_query($mysql_connection, $query);
						$arrayData6 = $emptyArrayData;
						while($data = mysqli_fetch_array($result)) {
							$tempDate = $data['reg_date'];
							$arrayData6[$tempDate] = $data['jml'];
						}
						
						$dataContent6 = implode(',',$arrayData6); 
						$title6 = "'Jumlah Pengambilan BPKB ( Periode ".$startDate." s/d ".$endDate." )'";
					}
				
					else if($periode == 'Mingguan') {
						
						$arrLabel 		= array();
						$arrStartDate	= array();
						$arrEndDate		= array();
						
						for($i=0; $i<12; $i++) {
							
							if($i==0) {
								$day = date('w') - 1;
								$startDateSQL	= date('Y-m-d', strtotime('-'.$day.' days'));
								$endDateSQL 	= date('Y-m-d', strtotime('+'.(6-$day).' days'));
								$arrStartDate[$i]	= $startDateSQL.' 00:00:00';
								$arrEndDate[$i]		= $endDateSQL.' 23:59:59';
							}
							else {
								$endDateSQL   = date('Y-m-d', strtotime('-1 days', strtotime($startDateSQL)));
								$startDateSQL = date('Y-m-d', strtotime('-6 days', strtotime($endDateSQL)));
								$arrStartDate[$i]	= $startDateSQL.' 00:00:00';
								$arrEndDate[$i]		= $endDateSQL.' 23:59:59';
							}
							
							$arrLabel[] = "'".date('d M', strtotime($startDateSQL))." - ".date('d M', strtotime($endDateSQL))."'";
						}
						$arrLabel = array_reverse($arrLabel);
						$label = implode(',',$arrLabel);						
					}
				
					else if($periode == 'Bulanan') {
						
						$arrLabel 		= array();
						$arrStartDate	= array();
						$arrEndDate		= array();
						
						for($i=0; $i<12; $i++) {
							
							$year = date('Y');
							$month= date('n') - $i; 
							
							if($month <= 0) {
								$month = $month + 12;
								$year  = $year - 1;
							}
							
							$startDateSQL = date('Y-m-d', strtotime($year.'-'.$month.'-1'));
							$endDateSQL   = date('Y-m-d', strtotime($year.'-'.$month.'-31'));
							$arrStartDate[$i]	= $startDateSQL.' 00:00:00';
							$arrEndDate[$i]		= $endDateSQL.' 23:59:59'; 
							
							$arrLabel[] = "'".date('M Y', strtotime($startDateSQL))."'";
						}
						$arrLabel = array_reverse($arrLabel);
						$label = implode(',',$arrLabel);
					}	
					
					if($periode == 'Mingguan' || $periode == 'Bulanan') {
						//============================ 1. PENDAFTARAN USER ============================  
						$arrayData = array();
						for($i=0; $i<12; $i++) {
							
							$startDateSQL	= $arrStartDate[$i];
							$endDateSQL		= $arrEndDate[$i];
														
							$query	= "select count(id_customer) as jml 
									   from customers 
									   where registration_date>='$startDateSQL' and registration_date<='$endDateSQL' ";
							$result = mysqli_query($mysql_connection, $query); 
							
							while($data = mysqli_fetch_array($result)) {
								$arrayData[] = $data['jml'];
							}
						}
						
						$arrayData = array_reverse($arrayData);
						$dataContent1 = implode(',',$arrayData);
						$title1 = "'Jumlah Pendaftaran User'";
						
						//============================ 2. REFFERANTOR ============================  
						$arrayData = array();
						for($i=0; $i<12; $i++) {
							
							$startDateSQL	= $arrStartDate[$i];
							$endDateSQL		= $arrEndDate[$i];
														
							$query	= "select count(id) as jml 
									   from loan_applications  
									   where loan_application_referal='Y' and date>='$startDateSQL' and date<='$endDateSQL' ";
							$result = mysqli_query($mysql_connection, $query); 
							
							while($data = mysqli_fetch_array($result)) {
								$arrayData[] = $data['jml'];
							}
						}
						
						$arrayData = array_reverse($arrayData);
						$dataContent2 = implode(',',$arrayData);
						$title2 = "'Jumlah Refferantor'";
						
						//============================ 3. PEMOHON APLIKASI ============================  
						$arrayData = array();
						for($i=0; $i<12; $i++) {
							
							$startDateSQL	= $arrStartDate[$i];
							$endDateSQL		= $arrEndDate[$i];
														
							$query	= "select count(id) as jml 
									   from loan_applications 
									   where date>='$startDateSQL' and date<='$endDateSQL' ";
							$result = mysqli_query($mysql_connection, $query); 
							
							while($data = mysqli_fetch_array($result)) {
								$arrayData[] = $data['jml'];
							}
						}
						
						$arrayData = array_reverse($arrayData);
						$dataContent3 = implode(',',$arrayData);
						$title3 = "'Jumlah Pemohon Aplikasi'";
						
						//============================ 4. KOMPLAIN ============================  
						$arrayData = array();
						for($i=0; $i<12; $i++) {
							
							$startDateSQL	= $arrStartDate[$i];
							$endDateSQL		= $arrEndDate[$i];
														
							$query	= "select count(id_complain) as jml 
									   from complains 
									   where submitted_date>='$startDateSQL' and submitted_date<='$endDateSQL' ";
							$result = mysqli_query($mysql_connection, $query); 
							
							while($data = mysqli_fetch_array($result)) {
								$arrayData[] = $data['jml'];
							}
						}
						
						$arrayData = array_reverse($arrayData);
						$dataContent4 = implode(',',$arrayData);
						$title4 = "'Jumlah Komplain'";
						
						//============================ 5. KLAIM ASURANSI ============================ 
						$arrayData = array();
						for($i=0; $i<12; $i++) {
							
							$startDateSQL	= $arrStartDate[$i];
							$endDateSQL		= $arrEndDate[$i];
														
							$query	= "select count(id) as jml 
									   from insurance_claims 
									   where claim_date>='$startDateSQL' and claim_date<='$endDateSQL' ";
							$result = mysqli_query($mysql_connection, $query); 
							
							while($data = mysqli_fetch_array($result)) {
								$arrayData[] = $data['jml'];
							}
						}
						
						$arrayData = array_reverse($arrayData);
						$dataContent5 = implode(',',$arrayData);
						$title5 = "'Jumlah Klaim Asuransi'";
						
						//============================ 6. PENGAMBILAN BPKB ============================  
						$arrayData = array();
						for($i=0; $i<12; $i++) {
							
							$startDateSQL	= $arrStartDate[$i];
							$endDateSQL		= $arrEndDate[$i];
														
							$query	= "select count(id) as jml 
									   from collateral_requests 
									   where request_date>='$startDateSQL' and request_date<='$endDateSQL' ";
							$result = mysqli_query($mysql_connection, $query); 
							
							while($data = mysqli_fetch_array($result)) {
								$arrayData[] = $data['jml'];
							}
						}
						
						$arrayData = array_reverse($arrayData);
						$dataContent6 = implode(',',$arrayData);
						$title6 = "'Jumlah Pengambilan BPKB'";
					}
				?>
				
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->
	  
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>
	
	<script src="plugins/chartjs/Chart.min.js"></script>
	<script src="plugins/chartjs/utils.js"></script>
	
	<script>
		
		var ctx = document.getElementById('userChart').getContext('2d');
		var userChart = new Chart(ctx, {
			type: 'line',
			data: {
				labels: [<?php echo $label; ?>],
				datasets: [{
					label: <?php echo $title1; ?>,
					fill: false,
					borderColor: window.chartColors.blue,
					data: [<?php echo $dataContent1; ?>],
					borderWidth: 2
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
		
		var ctx2 = document.getElementById('refferantorChart').getContext('2d');
		var refferantorChart = new Chart(ctx2, {
			type: 'line',
			data: {
				labels: [<?php echo $label; ?>],
				datasets: [{
					label: <?php echo $title2; ?>,
					fill: false,
					borderColor: window.chartColors.red,
					data: [<?php echo $dataContent2; ?>],
					borderWidth: 2
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
		
		var ctx3 = document.getElementById('loanChart').getContext('2d');
		var loanChart = new Chart(ctx3, {
			type: 'line',
			data: {
				labels: [<?php echo $label; ?>],
				datasets: [{
					label: <?php echo $title3; ?>,
					fill: false,
					borderColor: window.chartColors.green,
					data: [<?php echo $dataContent3; ?>],
					borderWidth: 2
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
		
		var ctx4 = document.getElementById('complainChart').getContext('2d');
		var complainChart = new Chart(ctx4, {
			type: 'line',
			data: {
				labels: [<?php echo $label; ?>],
				datasets: [{
					label: <?php echo $title4; ?>,
					fill: false,
					borderColor: window.chartColors.orange,
					data: [<?php echo $dataContent4; ?>],
					borderWidth: 2
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
		
		var ctx5 = document.getElementById('insuranceChart').getContext('2d');
		var insuranceChart = new Chart(ctx5, {
			type: 'line',
			data: {
				labels: [<?php echo $label; ?>],
				datasets: [{
					label: <?php echo $title5; ?>,
					fill: false,
					borderColor: window.chartColors.yellow,
					data: [<?php echo $dataContent5; ?>],
					borderWidth: 2
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
		
		var ctx6 = document.getElementById('collateralChart').getContext('2d');
		var collateralChart = new Chart(ctx6, {
			type: 'line',
			data: {
				labels: [<?php echo $label; ?>],
				datasets: [{
					label: <?php echo $title6; ?>,
					fill: false,
					borderColor: window.chartColors.purple,
					data: [<?php echo $dataContent6; ?>],
					borderWidth: 2
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	</script>
	
  </body>
</html>
