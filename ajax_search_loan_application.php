<?php
	include "check-admin-session.php";

	$product 			= sanitize_sql_string($_REQUEST["src_product"]);
	$status 			= sanitize_sql_string($_REQUEST["src_status"]);
	$searchDate			= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 			= sanitize_sql_string($_REQUEST["keyword"]);
	$src_pengajuan		= sanitize_sql_string($_REQUEST["src_pengajuan"]);
	$src_customer_type	= sanitize_sql_string($_REQUEST["src_customer_type"]);
	$page 				= sanitize_int($_REQUEST["page"]);
	
	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";
	} else {
		$startDate	= date("Y-m")."-01 00:00:00";
		$endDate	= date("Y-m")."-31 23:59:59";
	}

	$query 	= "select COUNT(a.id) as num
				from loan_applications a 
				left join customers b on a.customer_id=b.id_customer 
				where (ticket_number like '%$keyword%' or loan_customer_name like '%$keyword%' or loan_phone_number like '%$keyword%') 
				and date>='$startDate' and date<='$endDate' and is_active='1' ";
	if($status <> '') $query = $query." and a.status='$status' ";
	if($product <> '') $query = $query." and product='$product' ";
	
	if($src_pengajuan == 'Agent') $query = $query." and loan_application_referal='Y' ";	
	else if($src_pengajuan == 'Pribadi') $query = $query." and loan_application_referal='N' ";	
	
	if($src_customer_type == 'EMPLOYEE') $query = $query." and customer_type='EMPLOYEE' ";	
	else if($src_customer_type == 'NON EMPLOYEE') $query = $query." and customer_type!='EMPLOYEE' ";	
	
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;

	$query 	= "select a.*, b.customer_name, b.nip   
				from loan_applications a
				left join customers b on a.customer_id=b.id_customer 
				where (ticket_number like '%$keyword%' or loan_customer_name like '%$keyword%' or loan_phone_number like '%$keyword%') 
				and date>='$startDate' and date<='$endDate' and a.is_active='1' ";
	if($status <> '') $query = $query." and a.status='$status' ";
	if($product <> '') $query = $query." and product='$product' ";	

	if($src_pengajuan == 'Agent') $query = $query." and loan_application_referal='Y' ";	
	else if($src_pengajuan == 'Pribadi') $query = $query." and loan_application_referal='N' ";	
	
	if($src_customer_type == 'EMPLOYEE') $query = $query." and customer_type='EMPLOYEE' ";	
	else if($src_customer_type == 'NON EMPLOYEE') $query = $query." and customer_type!='EMPLOYEE' ";
	
	$query = $query." order by date DESC LIMIT $start,$limit";
	
	$result = mysqli_query($mysql_connection, $query); 

	echo "<table class='table table-hover'>
			  <tr>
					<th width='5%'>No</th>
					<th width='10%'>Nomor Tiket</th>
					<th width='10%'>Tanggal</th>
					<th width='10%'>Nama Konsumen</th>
					<th width='10%'>Nama Referal</th>
					<th width='10%'>Telepon</th>
					<th width='8%'>Produk</th>
					<th width='12%'>OTR</th>
					<th width='12%'>Jumlah DP</th>
					<th width='8%'>Status</th>
					<th width='8%'>Detail</th>
					<th width='8%'>Hapus</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {
		
		if($data['loan_application_referal'] == 'Y') {
			$referal_name = $data['customer_name'];
			if($src_customer_type == 'EMPLOYEE') $nip_referal = '<br>NIP : '.$data['nip'];
			if($src_customer_type == 'EMPLOYEE') $nip_customer = '';
		} else {
			$referal_name = '-';
			if($src_customer_type == 'EMPLOYEE') $nip_referal = '';
			if($src_customer_type == 'EMPLOYEE') $nip_customer = '<br>NIP : '.$data['nip'];
		}
		
		//cek apakah ada dokumen yang diupload
		$queryDocs	= 'select * from loan_application_documents where loan_application_id='.$data[id];
		$resultDocs	= mysqli_query($mysql_connection, $queryDocs);
		if(mysqli_num_rows($resultDocs) > 0) {
			$downloadLink = '<br><a href="download_document?type=loan&id='.$data[id].'&ticket='.$data[ticket_number].'" target="_blank"><i class="fa fa-paperclip"></i> Dokumen</a>';
		}
		else {
			$downloadLink = '';
		}
		
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data[ticket_number] . $downloadLink . '</td>
				  <td>' . date("d-m-Y H:i:s", strtotime($data[date])) . '</td>
				  <td>' . $data[loan_customer_name] . $nip_customer . '</td>	  
				  <td>' . $referal_name . $nip_referal . '</td>
				  <td>' . $data[loan_phone_number] . '</td>
				  <td>' . $data[product] . '</td>
				  <td>Rp ' . number_format($data[otr],2,',','.') . '</td>
				  <td>Rp ' . number_format($data[dp_amount],2,',','.') . '</td>
				  <td>' . $data[status] . '</td>
				  <td>
					<a href="#modal" onclick="viewDetail('.$data[id].')"><i class="fa fa-search-plus"></i> View Detail</a>
				  </td>
				  <td>
					<a href="#" onclick="deleteLoanApplication('.$data[id].', \''.$data[ticket_number].'\')"><i class="fa fa-trash"></i> Hapus</a>
				  </td>
				</tr>';
		$i++;
	}

	echo "</table>";

	include "inc-paging.php";
?>
