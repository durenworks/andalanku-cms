<?php
	
	$_ref = $_REQUEST['_ref'];
	if($_ref == md5('faq#'.date('d-m-Y'))) {
		$currentURL = 'faq';
	}

	include "check-admin-session.php";
	include "plugins/image-resize/lib/ImageResize.php";
	
	function uploadImage($file) {

		$response = "";

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/faq_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'andalanku_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

				//resize image
				$image = new \Eventviva\ImageResize($folder . '/' . $newnamefile);
				$image->resizeToBestFit(800, 600);
				$image->save($folder . '/' . $newnamefile);

			} else {
				$response = "";
			}
		}

		return $response;
	}
	
	$question 		= sanitize_sql_string($_REQUEST["question"]);
	$is_active		= sanitize_sql_string($_REQUEST["is_active"]);
	$anser_row_count= sanitize_int($_REQUEST["anser_row_count"]);
	
	if ($question <> '' &&  $is_active <> '' ) {
			
		$queryCheck		= "SELECT id_faq from faq WHERE question='$question'";
		$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck ) > 0) {
			echo "question_exist";
			exit;
		}
	
		$query = "INSERT INTO faq(question, is_active) 
				  VALUES ('$question','$is_active')";
        mysqli_query($mysql_connection, $query);
		
		//ambil id_faq terbaru
		$queryCheck		= "SELECT id_faq from faq WHERE question='$question' order by id_faq DESC LIMIT 1";
		$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
		$dataCheck		= mysqli_fetch_array($resultCheck);
		$idNew			= $dataCheck['id_faq'];
		
		for($i=0; $i<$anser_row_count; $i++) {
			
			$n = $i+1;
			$answerImageName 		= 'answerImage_'.$n;
			$answerDescriptionName 	= 'answerDescription_'.$n;
			$answerStatusName 		= 'is_active_answer_'.$n;
			
			$imageFileName = "";
			if (!empty($_FILES[$answerImageName])) {
				$imageFileName = uploadImage($_FILES[$answerImageName]);
			} else {
				$imageFileName = "";
			}
			
			$status = sanitize_sql_string($_REQUEST[$answerStatusName]);
			$answerDescription = sanitize_sql_string($_REQUEST[$answerDescriptionName]);
			$answerDescription = str_replace("\r\n","<br />",$answerDescription );
			
			$query = "	INSERT INTO faq_answers(id_faq, image, description, is_active)
						VALUES ('$idNew', '$imageFileName', '$answerDescription', '$status')";
			mysqli_query($mysql_connection, $query);
		}
		
        echo 'success'; 
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>