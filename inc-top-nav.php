      <?php
      	$fullname	= $_SESSION['fullname'];
    $user_level	= $_SESSION['loginType'];
    
    $queryInbox = "select count(id) as count from inbox where customer_id = -2 and status = 0";
    $resultInbox	= mysqli_query($mysql_connection, $queryInbox);
    $resultInbox = mysqli_fetch_assoc($resultInbox);
    $count = $resultInbox['count'];
      ?>

      <header class="main-header">
        <nav class="navbar navbar-static-top bg-blue-2">
          <div class="container">
            <div class="navbar-header">
              <a href="dashboard" class="navbar-brand">
              	<div class="text-white" style="display: inline-block;">
                  <img src="img/logo_sm.png" alt="Logo">
              	</div>
              </a>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
              </button>
            </div>

			<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
				<ul class="nav navbar-nav">
			<?php 
				$queryParent	= "select distinct(b.parent_id) as menu_id 
									   from user_level_menu a 
									   left join menus b on a.menu_id=b.id 
									   where user_level_id='$user_level'";
				$resultParent	= mysqli_query($mysql_connection, $queryParent);
				
				while ($dataParent = mysqli_fetch_array($resultParent)) {
					
					$parentID  = $dataParent['menu_id'];
					$queryMenu = "select * from menus where id='$parentID' order by menu_order ASC";
					$resultMenu= mysqli_query($mysql_connection, $queryMenu);
					$dataMenu  = mysqli_fetch_array($resultMenu);
					
					echo '<li class="dropdown">
							<a href="'.$dataMenu['url'].'" class="dropdown-toggle" data-toggle="dropdown"><i class="'.$dataMenu['icon'].'"></i> '.$dataMenu['name'].'<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">';
					
					$queryChild = "select b.* 
								   from user_level_menu a 
								   left join menus b on a.menu_id=b.id 
								   where b.parent_id='$parentID' and a.user_level_id='$user_level' 
								   order by b.menu_order ASC";
					$resultChild= mysqli_query($mysql_connection, $queryChild);
					while ($dataChild = mysqli_fetch_array($resultChild)) {
						
						echo '<li><a href="'.$dataChild['url'].'"><i class="'.$dataChild['icon'].'"></i> '.$dataChild['name'].'</a></li>';
					}
					
					echo '	</ul>';
					echo '</li>';
				}
			?>
				</ul>
			</div>			
					
			
            <!-- Navbar Right Menu -->
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <!-- User Account Menu -->
                  <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <!-- The user image in the navbar-->
                      	<img src='img/user2-160x160.jpg' class='img-circle' alt='User Image' style='width:16px;'/>
                      <!-- hidden-xs hides the username on small devices so only the image appears. -->
                      <span class="hidden-xs"><?php echo $fullname; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- The user image in the menu -->
                      <li class="user-header">
                        <img src='img/user2-160x160.jpg' class='img-circle' alt='User Image'/>
                        <p>
                          <?php echo $fullname; ?>
                        </p>
                      </li>

                      <!-- Menu Footer-->
                      <li class="user-footer">
						            <div class="pull-left">
                          <a href="profile" class="btn btn-default btn-flat"><i class="fa fa-lock"></i> Edit Profile</a>
                        </div>
                        <div class="pull-right">
                          <a href="logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Logout</a>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
              <div class="navbar-custom-menu">
                <?php 
                if($count > 0) {
                  echo "<button type='button' class='btn btn-danger' style='height:50px' onclick='window.location=\"direct_message.php\"' >New Message <span class='badge badge-light'>". $count ."</span></button>";
                }
                else {
                  echo "<button type='button' class='btn btn-primary' style='height:50px' onclick='window.location=\"direct_message.php\"'>New Message <span class='badge badge-light'>". $count ."</span></button>";
                }
                ?>
              </div><!-- /.navbar-custom-menu -->
          </div><!-- /.container-fluid -->
        </nav>
      </header>
