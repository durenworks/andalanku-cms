<?php
	include "check-admin-session.php";

	$agentRegistrationID = sanitize_int($_REQUEST["agentRegistrationID"]);
	
	$query 	= "select a.*, j.name as occupation_name,  
	
			   e.name as village_name, d.name as district_name, 
			   c.name as regency_name, b.name as province_name,
			   
			   i.name as village_name_domicile, h.name as district_name_domicile, 
			   g.name as regency_name_domicile, f.name as province_name_domicile  
			   
			   from register_agent_history a
			   
			   left join provinces b on a.province_id=b.id 
			   left join regencies c on a.regency_id=c.id
			   left join districts d on a.district_id=d.id
			   left join villages e on a.village_id=e.id
			   
			   left join provinces f on a.province_id_domicile=f.id 
			   left join regencies g on a.regency_id_domicile=g.id
			   left join districts h on a.district_id_domicile=h.id
			   left join villages i on a.village_id_domicile=i.id
			   
			   left join occupations j on a.occupation_id=j.id 
			   
			   where a.id_register_agent='$agentRegistrationID' ";
			   
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
	
	$data['register_agent_date'] = date("d-m-Y H:i:s", strtotime($data['request_date']));
	
	if($data['date_of_birth'] <> '') $data['date_of_birth'] = date("d-m-Y", strtotime($data['date_of_birth']));
	$data['place_of_birth'] = $data['place_of_birth'].', '.$data['date_of_birth'];
	
	$data['legal_address'] = $data[address] . ' RT '.$data[rt].' / RW '.$data[rw]."\r\n".$data[village_name].', '.$data[district_name]."\r\n".$data[regency_name].', '.$data[province_name]."\r\n".$data[zip_code];
	$data['domicile_address'] = $data[address_domicile] . ' RT '.$data[rt_domicile].' / RW '.$data[rw_domicile]."\r\n".$data[village_name_domicile].', '.$data[district_name_domicile]."\r\n".$data[regency_name_domicile].', '.$data[province_name_domicile]."\r\n".$data[zip_code_domicile];
		
	echo json_encode($data);
?>
