<?php
	include "check-admin-session.php";

	$keyword	= sanitize_sql_string($_REQUEST["keyword"]);
	$page 		= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(*) as num
					   from customers
					   where (customer_name like '%$keyword%' or phone_number like '%$keyword%') 
					   and andalan_customer_id is not null ";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select * 
			  from customers
			  where (customer_name like '%$keyword%' or phone_number like '%$keyword%') 
			  and andalan_customer_id is not null 
			  order by customer_name ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='15%'>Nama</th>
				  <th width='15%'>Tanggal Lahir</th>
				  <th width='10%'>Telepon</th>
				  <th width='10%'>HP</th>
				  <th>Alamat</th>
				  <th width='15%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {
		
		$customer_id = $data['id_customer'];
		
		$queryAddress  = "select b.address, b.rt, b.rw,
						  f.name as village_name, e.name as district_name, 
						  d.name as regency_name, c.name as province_name, 
						  b.zip_code 
						  from address_customers a 
						  left join address b on a.address_id=b.id 
						  left join provinces c on b.province_id=c.id 
						  left join regencies d on b.regency_id=d.id
						  left join districts e on b.district_id=e.id
						  left join villages f on b.village_id=f.id
						  where a.is_active='1' 
						  and a.user_id='$customer_id'
						  and a.address_type='DOMICILE' ";
		$resultAddress = mysqli_query($mysql_connection, $queryAddress);
		
		$address = '';
		
		if(mysqli_num_rows($resultAddress) > 0) {
			
			$dataAddress = mysqli_fetch_array($resultAddress);
			$address 	 = $dataAddress['address'].' RT '.$dataAddress['rt'].' RW '.$dataAddress['rw']."<br>".
						   $dataAddress['village_name'].', '.$dataAddress['district_name']."<br>".
						   $dataAddress['regency_name'].', '.$dataAddress['province_name'].' '.$dataAddress['zip_code'];
		}

		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data[customer_name] . '</td>
				  <td>' . date("d-m-Y", strtotime($data[date_of_birth])) . '</td>
				  <td>' . $data[home_phone_number] . '</td>
				  <td>' . $data[phone_number] . '</td>
				  <td>' . $address . '</td>
				  <td>
					<a href="#modal" onclick="viewDocument(\''.$customer_id.'\')"><i class="fa fa-search-plus"></i> View Document</a>
				  </td>
			  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>