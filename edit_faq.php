<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$faqID = sanitize_int($_REQUEST["id"]);

	$query 	= "select * from faq where id_faq='$faqID'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
?>

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Display</li>
              <li><a href="faq">FAQ</a></li>
              <li>Edit FAQ</a></li>
            </ol>
          </section>
		  <br>
          <!-- Main content -->
          <section class="content">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Edit FAQ</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<input type="hidden" name="id_faq" id="id_faq" value="<?php echo $faqID; ?>" />

				<div class="row form-group">
					<div class="col-md-8">
						<label>Pertanyaan</label>
						<input type="text" name="question" id="question" class="form-control" placeholder="Pertanyaan" value="<?php echo $data['question']; ?>"/>
					</div>
					<div class="col-md-4">
						<label for="is_active">Status</label>
						<select name="is_active" class="form-control" id="is_active">
							<option value="TRUE" <?php if($data['is_active']=='TRUE') echo 'selected'; ?> >Aktif</option>
							<option value="FALSE" <?php if($data['is_active']=='FALSE') echo 'selected'; ?> >Tidak Aktif</option>
						</select>
					</div>
					<div class="col-md-12">
						<hr style="border-color:#bbbbbb;">
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-md-12">
						<button type="button" class="btn btn-primary  btn-flat " onClick="addAnswer();"><i class="fa fa-plus"></i> Tambah Jawaban</button>
					</div>
				</div>
				
				<span id="spanAnswerTable"></span>

				<div class="row form-group">
					<div class="col-md-12 text-center">
						<button type="button" id="btnSave" style="width:100px" class="btn btn-primary btn-large" onclick="save();">Simpan</button>
						<button type="button" style="width:100px" class="btn btn-warning btn-large" onclick="cancel();">Batal</button>
					</div>
				</div>

				<br><br><br>

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

    <script src="js/inc-function.js"></script>

    <script type="text/javascript">

		var rowNumber = 0;
		var rowCount  = 0;
		
		function addAnswer(parIdAnswer=0, parImage='', parDescription='', parStatus='', parAnswerType='NEW') {
			
			rowNumber++;
			rowCount++;
			
			var spanAnswerRowName 		= 'spanAnswerRow_'+rowNumber;
			var imageViewName 			= 'imageView_'+rowNumber;
			var answerImageName 		= 'answerImage[]';
			var answerDescriptionName 	= 'answerDescription[]';
			var answerStatusName 		= 'is_active_answer[]';
			var answerIDName 			= 'answer_id[]';
			var answerTypeName 			= 'answer_type[]';
			
			if(parImage != '') parImage = 'user_files/faq_image/'+parImage;
			else parImage = '#';
			
			var selectedTrue	= '';
			var selectedFalse	= '';
			
			if(parStatus == 'TRUE') selectedTrue = 'selected';
			if(parStatus == 'FALSE') selectedFalse = 'selected';
			
			var input = "<span id='"+ spanAnswerRowName +"'>"+
						"<input type='hidden' name='"+ answerIDName +"' id='"+ answerIDName +"' value='"+ parIdAnswer +"' />"+
						"<input type='hidden' name='"+ answerTypeName +"' id='"+ answerTypeName +"' value='"+ parAnswerType +"' />"+
						"<div class='row form-group'> "+
						"<div class='col-md-4'>"+
						"<table width='100%' class='table'>"+
							"<tr>"+
								"<td width='150px' align='center'>"+
									"<img id='"+ imageViewName +"' src='"+parImage+"' style='width: 90px;' alt='Gambar'/>"+
								"</td>"+
								"<td>"+
									"<input type='file' name='"+ answerImageName +"' id='"+ answerImageName +"' accept='image/*' onchange='readURL(this, \"#"+ imageViewName +"\")'/>"+
								"</td>"+
							"</tr>"+
						"</table>"+
						"</div>"+
						"<div class='col-md-5'>"+
						"<textarea name='"+ answerDescriptionName +"' id='"+ answerDescriptionName +"' rows='4' class='form-control' placeholder='Deskripsi' >"+parDescription+"</textarea>"+						
						"</div>"+
						"<div class='col-md-2'>"+
						"<select name='"+ answerStatusName +"' class='form-control' id='is_active'>"+
							"<option value='TRUE' "+ selectedTrue +">Aktif</option>"+
							"<option value='FALSE' "+ selectedFalse +">Tidak Aktif</option>"+
						"</select>"+						
						"</div>"+
						"<div class='col-md-1'>"+
						"<button type='button' class='btn btn-danger btn-large' onclick='removeAnswer("+ rowNumber +", "+ parIdAnswer +");'> X </button>"+
						"</div>"+
						"</div>"+
						"</span>";
						
			$('#spanAnswerTable').append(input);
		}
		
		function removeAnswer(rowNum, idFaqAnswer=0) {
			
			if (confirm('Hapus jawaban?')) {
				
				if(idFaqAnswer != 0) {					

					$body.addClass("loadingClass");
					
					var form_data = new FormData();
					form_data.append('idFaqAnswer', idFaqAnswer);
					form_data.append('_ref', '<?php echo md5('faq#'.date('d-m-Y')); ?>');

					$.ajax({
						type: "POST",
						url: 'ajax_delete_faq_answer.php',
						processData: false,
						contentType: false,
						data: form_data
					}).done(function(data) {

						data = $.trim(data);
						$body.removeClass("loadingClass");

						if(data == "success") {
							$('#spanAnswerRow_'+rowNum).html('');
							rowCount--;
						} 
					});
				}
				else {
					
					$('#spanAnswerRow_'+rowNum).html('');
					rowCount--;
				}				
			}
		}
		
		function readURL(input, view) { 
			
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $(view).attr('src', e.target.result); 
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}
	
    	$body = $("body");

    	function save() {

    		$body.addClass("loadingClass");

    		var form_data = new FormData();
			form_data.append('id_faq', $('input[name=id_faq]').val());
          	form_data.append('question', $('input[name=question]').val());
			form_data.append('is_active', $('select[name=is_active]').val());
			form_data.append('anser_row_count', rowCount);
			form_data.append('_ref', '<?php echo md5('faq#'.date('d-m-Y')); ?>');
			
			var n = 1			
			$('input[name^="answerImage"]').each(function() {				
				var answerImageName = 'answerImage_'+n;
				form_data.append(answerImageName, $(this).prop('files')[0]);
				n++;
			});
			
			n = 1			
			$('textarea[name^="answerDescription"]').each(function() {				
				var answerDescriptionName = 'answerDescription_'+n;
				form_data.append(answerDescriptionName, $(this).val());
				n++;
			});
			
			n = 1			
			$('select[name^="is_active_answer"]').each(function() {				
				var answerStatusName = 'is_active_answer_'+n;
				form_data.append(answerStatusName, $(this).val());
				n++;
			});
			
			n = 1			
			$('input[name^="answer_id"]').each(function() {				
				var answerIDName = 'answer_id_'+n;
				form_data.append(answerIDName, $(this).val());
				n++;
			});
			
			n = 1			
			$('input[name^="answer_type"]').each(function() {				
				var answerTypeName = 'answer_type_'+n;
				form_data.append(answerTypeName, $(this).val());
				n++;
			});

			$.ajax({
				type: "POST",
				url: 'ajax_edit_faq.php',
				processData: false,
				contentType: false,
				data: form_data
			}).done(function(data) {

				data = $.trim(data); 
				$body.removeClass("loadingClass");

				if (data == 'success') {
					
					$("#mainNotification").html(showNotification("success", "Data Sudah Disimpan"));
					$("#btnSave").attr("disabled", true);
					setTimeout(cancel, 2000);

				} else if (data == 'empty') {
					$('#mainNotification').html(showNotification("error", "Input Tidak Lengkap"));
				} else if (data == 'incorrect_file_type') {
					$('#mainNotification').html(showNotification("error", "File gambar artikel harus berupa file jpg atau png atau gif"));
				}

				$('html, body').animate({scrollTop: '0px'}, 0);
			});

    	}

    	function cancel() {
    		window.location = 'faq';
    	}

    	function clearnotif() {
			$("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
		}
		
		<?php 
			// ambil daftar jawaban faq
			$query 	= "select * from faq_answers where id_faq='".$faqID."'";
			$result = mysqli_query($mysql_connection, $query);
			
			while($dataAnswer = mysqli_fetch_array($result)) {
				
				$description= $dataAnswer['description'];
				$description= str_replace("<br />", "\r\n", $description);
				
				echo " addAnswer('".$dataAnswer['id_answer']."', '".$dataAnswer['image']."', '".$description."', '".$dataAnswer['is_active']."', 'OLD'); ";
			}
		?>

	</script>

  </body>
</html>
