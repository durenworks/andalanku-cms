<?php
	include "check-admin-session.php";
	include "plugins/image-resize/lib/ImageResize.php";
	
	function uploadImage($file) {

		$response = "";

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/testimonial_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'andalanku_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

				//resize image
				$image = new \Eventviva\ImageResize($folder . '/' . $newnamefile);
				$image->resizeToBestFit(800, 600);
				$image->save($folder . '/' . $newnamefile);

			} else {
				$response = "";
			}
		}

		return $response;
	}

	$id_testimonial = sanitize_int($_REQUEST["id_testimonial"]);
	$name 			= sanitize_sql_string($_REQUEST["name"]);
	$profession		= sanitize_sql_string($_REQUEST["profession"]);
	$testimonial	= sanitize_sql_string($_REQUEST["testimonial"]);
	$oldImg 		= sanitize_sql_string($_REQUEST["oldImg"]);

	if ($id_testimonial <> '0' && $name <> '' && $testimonial <> '') {
		
		//upload gambar dulu
		$imageFileName1 = "";
		if (!empty($_FILES['imageInput1'])) {
			$imageFileName1 = uploadImage($_FILES['imageInput1']);
			if($imageFileName1 != "") {
				//hapus gambar yang lama
				@unlink("user_files/testimonial_image/".$oldImg);
			}
		} else {
			$imageFileName1 = "";
		}

		$query = "UPDATE testimonials set name='$name', profession='$profession', testimonial='$testimonial' ";
		if($imageFileName1 <> '') $query = $query." ,image='$imageFileName1' ";
		$query = $query." where id_testimonial='$id_testimonial' ";
		mysqli_query($mysql_connection, $query);

		echo 'success';
		exit;
		
	} else {
		echo "empty";
		exit;
	}
?>
