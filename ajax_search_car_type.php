<?php
	include "check-admin-session.php";

	$keyword		= sanitize_sql_string($_REQUEST["keyword"]);
	$src_car_brand	= sanitize_sql_string($_REQUEST["src_car_brand"]);
	$page 			= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(*) as num
					   from car_types a 
					   left join car_brands b on a.id_car_brand=b.id_car_brand 
					   where a.name like '%$keyword%' ";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select a.*, b.name as car_brand_name
			  from car_types a 
			  left join car_brands b on a.id_car_brand=b.id_car_brand 
			  where a.name like '%$keyword%'
			  order by b.name, a.name ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='5%'>No</th>
				  <th width='20%'>Merk Mobil</th>
				  <th width='35%'>Nama Tipe Mobil</th>
				  <th width='15%'>Gambar</th>
				  <th width='10%'>Status</th>
				  <th width='15%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		if($data['is_active'] == '1') $status = 'Aktif';
		else $status = 'Tidak Aktif';
	
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . @$data[car_brand_name] . '</td>
				  <td>' . @$data[name] . '</td>
				  <td><a href="#modal" onclick="getImages('. @$data[id_car_type] .');">Lihat Gambar</a></td>
				  <td>' . $status . '</td>
				  <td align="center">
					<a href="edit_car_type?id=' . @$data[id_car_type] . '">Edit</a> | 
					<a href="#" onclick="deleteCarType(' . @$data[id_car_type] . ',\'' . @$data[name] . '\')" title="Hapus">Delete</a>
				  </td>
				  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>