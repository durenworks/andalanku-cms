-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2019 at 04:08 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `andalanku`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `province_id` int(11) DEFAULT NULL,
  `regency_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `village_id` varchar(12) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `rt` varchar(5) DEFAULT NULL,
  `rw` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

-- --------------------------------------------------------

--
-- Table structure for table `address_customers`
--

CREATE TABLE `address_customers` (
  `id` int(11) NOT NULL,
  `address_type` varchar(15) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address_customers`
--

-- --------------------------------------------------------

--
-- Table structure for table `occupations`
--

CREATE TABLE `occupations` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `occupations`
--

INSERT INTO `occupations` (`id`, `name`, `is_active`) VALUES
(1, 'Karyawan Swasta', 1),
(2, 'Pegawai Negeri Sipil', 1),
(3, 'TNI / Polri', 1),
(4, 'Wirausaha', 1),
(5, 'Profesional', 1);

-- --------------------------------------------------------

--
-- Table structure for table `profile_update_request`
--

CREATE TABLE `profile_update_request` (
  `id` int(11) NOT NULL,
  `ticket_number` varchar(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `request_date` datetime DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `id_card_image` varchar(100) DEFAULT NULL,
  `status` enum('PROCESS','APPROVE','REJECT') DEFAULT 'PROCESS',
  `last_update` datetime DEFAULT NULL,
  `status_change_by` int(11) DEFAULT NULL,
  `status_change_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_update_request`
--

-- --------------------------------------------------------

--
-- Table structure for table `profile_update_request_address`
--

CREATE TABLE `profile_update_request_address` (
  `id` int(11) NOT NULL,
  `address_type` varchar(15) DEFAULT NULL,
  `profile_update_request_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_update_request_address`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `address_customers`
--
ALTER TABLE `address_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `occupations`
--
ALTER TABLE `occupations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_update_request`
--
ALTER TABLE `profile_update_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_update_request_address`
--
ALTER TABLE `profile_update_request_address`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `address_customers`
--
ALTER TABLE `address_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `occupations`
--
ALTER TABLE `occupations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `profile_update_request`
--
ALTER TABLE `profile_update_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `profile_update_request_address`
--
ALTER TABLE `profile_update_request_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
