-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2019 at 11:37 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `andalanku`
--

-- --------------------------------------------------------

ALTER TABLE `users` CHANGE `level` `level` INT NULL DEFAULT NULL;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `url` varchar(30) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `menu_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `url`, `icon`, `parent_id`, `level`, `menu_order`) VALUES
(100, 'Master Data', '#', 'fa fa-database', 0, 1, 1),
(101, 'User Level', 'user_level', 'fa fa-tasks', 100, 2, 1),
(102, 'Level Permission', 'level_permission', 'fa fa-unlock-alt', 100, 2, 2),
(103, 'Administrator', 'administrator', 'fa fa-user-secret', 100, 2, 3),
(104, 'Subscriber', 'subscriber', 'fa fa-users', 100, 2, 4),
(105, 'Settings', 'setting', 'fa fa-gears', 100, 2, 5),
(200, 'Display', '#', 'fa fa-tv', 0, 1, 2),
(201, 'Berita', 'news', 'fa fa-newspaper-o', 200, 2, 1),
(203, 'Promo', 'promo', 'fa fa-tags', 200, 2, 2),
(204, 'Partner', 'partner', 'fa fa-handshake-o', 200, 2, 3),
(205, 'FAQ', 'faq', 'fa fa-question-circle-o', 200, 2, 4),
(206, 'Term of Service', 'tos', 'fa fa-file-text-o', 200, 2, 5),
(207, 'Privacy Policy', 'privacypolicy', 'fa fa-file-text-o', 200, 2, 6),
(300, 'Rates', '#', 'fa fa-bar-chart', 0, 1, 3),
(301, 'Insurance Rate', 'insurance_rate', 'fa fa-shield', 300, 2, 1),
(302, 'Car Rate', 'car_rate', 'fa fa-car', 300, 2, 2),
(303, 'Rate', 'rate', 'fa fa-bar-chart', 300, 2, 3),
(400, 'Car Prices', '#', 'fa fa-car', 0, 1, 4),
(401, 'Merk Mobil', 'car_brand', 'fa fa-car', 400, 2, 1),
(402, 'Tipe Mobil', 'car_type', 'fa fa-car', 400, 2, 2),
(403, 'Model Mobil', 'car_trim', 'fa fa-car', 400, 2, 3),
(404, 'Export / Import', 'car_exim', 'fa fa-file-excel-o', 400, 2, 4),
(500, 'Konsumen', '#', 'fa fa-id-badge', 0, 1, 5),
(501, '&nbsp;&nbsp;Data Konsumen', 'customer', 'fa fa-id-badge', 500, 2, 1),
(502, 'Pembaharuan Data', 'profile_update', 'fa fa-vcard-o', 500, 2, 2),
(503, 'Pengajuan Kredit', 'loan_application', 'fa fa-shopping-bag', 500, 2, 3),
(504, '&nbsp;Klaim Asuransi', 'insurance_claim', 'fa fa-shield', 500, 2, 4),
(505, 'Pengambilan Jaminan', 'collateral_claim', 'fa fa-check-square-o', 500, 2, 5),
(506, 'Dokumen Kontrak', 'contract_document', 'fa fa-file-text-o', 500, 2, 6),
(507, 'Dokumen Pribadi', 'private_document', 'fa fa-file-text-o', 500, 2, 7),
(508, 'History Pembayaran', 'payment_history', 'fa fa-file-text-o', 500, 2, 8),
(509, 'Komplain', 'complain', 'fa fa-exclamation-triangle', 500, 2, 9),
(510, 'Broadcast Informasi', 'information_blast', 'fa fa-bullhorn', 500, 2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `user_levels`
--

CREATE TABLE `user_levels` (
  `id` int(11) NOT NULL,
  `name` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_levels`
--

INSERT INTO `user_levels` (`id`, `name`) VALUES
(1, 'SUPERADMIN'),
(2, 'ADMINISTRATOR');

-- --------------------------------------------------------

--
-- Table structure for table `user_level_menu`
--

CREATE TABLE `user_level_menu` (
  `id` int(11) NOT NULL,
  `user_level_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `access_type` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_level_menu`
--

INSERT INTO `user_level_menu` (`id`, `user_level_id`, `menu_id`, `access_type`) VALUES
(217, 1, 101, NULL),
(218, 1, 102, NULL),
(219, 1, 103, NULL),
(220, 1, 104, NULL),
(221, 1, 105, NULL),
(222, 1, 201, NULL),
(223, 1, 203, NULL),
(224, 1, 204, NULL),
(225, 1, 205, NULL),
(226, 1, 206, NULL),
(227, 1, 207, NULL),
(228, 1, 301, NULL),
(229, 1, 302, NULL),
(230, 1, 303, NULL),
(231, 1, 401, NULL),
(232, 1, 402, NULL),
(233, 1, 403, NULL),
(234, 1, 404, NULL),
(235, 1, 501, NULL),
(236, 1, 502, NULL),
(237, 1, 503, NULL),
(238, 1, 504, NULL),
(239, 1, 505, NULL),
(240, 1, 506, NULL),
(241, 1, 507, NULL),
(242, 1, 508, NULL),
(243, 1, 509, NULL),
(244, 1, 510, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_levels`
--
ALTER TABLE `user_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_level_menu`
--
ALTER TABLE `user_level_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=511;

--
-- AUTO_INCREMENT for table `user_levels`
--
ALTER TABLE `user_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_level_menu`
--
ALTER TABLE `user_level_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
