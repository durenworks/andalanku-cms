-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2019 at 04:01 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `andalanku`
--

-- --------------------------------------------------------

ALTER TABLE `customers` ADD `place_of_birth` VARCHAR(20) NULL AFTER `last_login`, 
ADD `date_of_birth` DATE NULL AFTER `place_of_birth`, 
ADD `mother_name` VARCHAR(50) NULL AFTER `date_of_birth`, 
ADD `occupation` INT NULL AFTER `mother_name`, 
ADD `id_card_image` VARCHAR(50) NULL AFTER `occupation`, 
ADD `monthly_income` DOUBLE NULL AFTER `id_card_image`, 
ADD `additional_income` DOUBLE NULL AFTER `monthly_income`, 
ADD `side_job` VARCHAR(50) NULL AFTER `additional_income`, 
ADD `last_activity` DATETIME NULL AFTER `side_job`, 
ADD `last_latitude` DECIMAL(10,8) NULL AFTER `last_activity`, 
ADD `last_longitude` DECIMAL(10,8) NULL AFTER `last_latitude`;

ALTER TABLE `customers` ADD `home_phone_number` VARCHAR(20) NULL AFTER `last_login`;

-- --------------------------------------------------------

--
-- Table structure for table `customer_documents`
--

CREATE TABLE `customer_documents` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `loan_application_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `loan_applications`
--

CREATE TABLE `loan_applications` (
  `id` int(11) NOT NULL,
  `ticket_number` varchar(10) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `product` varchar(10) DEFAULT NULL,
  `otr` double DEFAULT NULL,
  `dp_percent` double DEFAULT NULL,
  `dp_amount` double DEFAULT NULL,
  `plafond` double DEFAULT NULL,
  `tenor` int(11) DEFAULT NULL,
  `insurance` varchar(10) DEFAULT NULL,
  `region` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `loan_application_status`
--

CREATE TABLE `loan_application_status` (
  `id` int(11) NOT NULL,
  `loan_application_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer_documents`
--
ALTER TABLE `customer_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_applications`
--
ALTER TABLE `loan_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_application_status`
--
ALTER TABLE `loan_application_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer_documents`
--
ALTER TABLE `customer_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `loan_applications`
--
ALTER TABLE `loan_applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `loan_application_status`
--
ALTER TABLE `loan_application_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
