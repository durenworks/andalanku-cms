-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2019 at 01:41 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `andalanku`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_keys`
--

CREATE TABLE `api_keys` (
  `id_api_key` int(11) NOT NULL,
  `api_key` varchar(32) DEFAULT NULL,
  `api_secret` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `api_keys`
--

INSERT INTO `api_keys` (`id_api_key`, `api_key`, `api_secret`) VALUES
(1, '1BA3A9444B0C4EB8F344EDB588E5101E', '82A9AD292A4FAF4B9C029BDB88D2DAB0');

-- --------------------------------------------------------

--
-- Table structure for table `car_brands`
--

CREATE TABLE `car_brands` (
  `id_car_brand` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_brands`
--

INSERT INTO `car_brands` (`id_car_brand`, `name`, `image`, `is_active`) VALUES
(9, 'Toyota', 'andalanku_35b8a22f77019577.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_rates`
--

CREATE TABLE `car_rates` (
  `id_car_rate` int(11) NOT NULL,
  `tenor` int(11) DEFAULT NULL,
  `rate_new` decimal(6,2) DEFAULT NULL,
  `flat_rate_new` decimal(6,2) DEFAULT NULL,
  `rate_used` decimal(6,2) NOT NULL,
  `flat_rate_used` decimal(6,2) NOT NULL,
  `admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_rates`
--

INSERT INTO `car_rates` (`id_car_rate`, `tenor`, `rate_new`, `flat_rate_new`, `rate_used`, `flat_rate_used`, `admin`) VALUES
(24, 1, '15.00', '8.31', '22.34', '12.51', 4550000),
(25, 2, '15.00', '8.18', '21.39', '11.85', 4550000),
(26, 3, '15.00', '8.27', '21.94', '12.46', 4550000),
(27, 4, '16.00', '9.01', '22.14', '12.90', 4550000),
(28, 5, '16.50', '9.32', '0.00', '0.00', 4550000);

-- --------------------------------------------------------

--
-- Table structure for table `car_trims`
--

CREATE TABLE `car_trims` (
  `id_car_trim` int(11) NOT NULL,
  `id_car_type` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_trims`
--

INSERT INTO `car_trims` (`id_car_trim`, `id_car_type`, `name`, `price`, `is_active`) VALUES
(8, 13, 'G MT 1200 cc', 120000000, 1),
(9, 13, 'TRD MT 1200 cc', 135000000, 1),
(10, 13, 'G AT 1200cc', 125000000, 1),
(11, 13, 'TRD AT 1200 cc', 137000000, 1),
(16, 13, 'E AT 1200cc', 115000000, 1),
(17, 13, 'E MT 1200cc', 109000000, 1),
(18, 12, 'G AT 1500cc', 155000000, 1),
(19, 12, 'G MT 1500cc', 149000000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_types`
--

CREATE TABLE `car_types` (
  `id_car_type` int(11) NOT NULL,
  `id_car_brand` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_types`
--

INSERT INTO `car_types` (`id_car_type`, `id_car_brand`, `name`, `is_active`) VALUES
(12, 9, 'Avanza', 1),
(13, 9, 'Agya', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type_media`
--

CREATE TABLE `car_type_media` (
  `id_car_type_media` int(11) NOT NULL,
  `id_car_type` int(11) DEFAULT NULL,
  `id_media` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type_media`
--

INSERT INTO `car_type_media` (`id_car_type_media`, `id_car_type`, `id_media`) VALUES
(28, 12, 31),
(29, 12, 32),
(32, 12, 35),
(33, 13, 36),
(34, 13, 37),
(35, 13, 38);

-- --------------------------------------------------------

--
-- Table structure for table `complains`
--

CREATE TABLE `complains` (
  `id_complain` int(11) NOT NULL,
  `ticket_number` varchar(10) DEFAULT NULL,
  `category` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `message` text,
  `status` enum('SUBMITTED','ON PROCESS','SOLVED') DEFAULT 'SUBMITTED',
  `submitted_date` datetime DEFAULT NULL,
  `on_process_date` datetime DEFAULT NULL,
  `processed_by` int(11) DEFAULT NULL,
  `solved_date` datetime DEFAULT NULL,
  `solved_by` int(11) DEFAULT NULL,
  `solution` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `complains`
--

INSERT INTO `complains` (`id_complain`, `ticket_number`, `category`, `user_id`, `phone`, `message`, `status`, `submitted_date`, `on_process_date`, `processed_by`, `solved_date`, `solved_by`, `solution`) VALUES
(11, 'C177208810', 'Marketing', 2, '08176884055', 'Test e-complain', 'ON PROCESS', '2019-05-06 09:54:39', '2019-05-08 10:51:01', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `complain_media`
--

CREATE TABLE `complain_media` (
  `id_complain_media` int(11) NOT NULL,
  `id_complain` int(11) DEFAULT NULL,
  `id_media` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `complain_media`
--

INSERT INTO `complain_media` (`id_complain_media`, `id_complain`, `id_media`) VALUES
(6, 11, 6),
(7, 11, 7);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id_customer` int(11) NOT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `id_number` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `referral_code` varchar(20) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `verification_code` varchar(50) DEFAULT NULL,
  `email_verification` varchar(15) DEFAULT NULL,
  `sms_otp` varchar(6) DEFAULT NULL,
  `sms_verification` varchar(15) DEFAULT NULL,
  `pin` varchar(4) DEFAULT NULL,
  `andalan_customer_id` varchar(32) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id_customer`, `customer_name`, `id_number`, `email`, `password`, `phone_number`, `referral_code`, `status`, `verification_code`, `email_verification`, `sms_otp`, `sms_verification`, `pin`, `andalan_customer_id`, `last_login`) VALUES
(2, 'Handi Artiawan', '3310020505840002', 'handie.artiawan@gmail.com', '25d55ad283aa400af464c76d713c07ad', '08176884055', NULL, 'ACTIVE', 'eea5b1b9d88f60977334b419e11e8bbc', 'NOT VERIFIED', '922389', 'VERIFIED', '8704', NULL, '2019-04-21 14:32:33');

-- --------------------------------------------------------

--
-- Table structure for table `customer_updates`
--

CREATE TABLE `customer_updates` (
  `id_customer_update` int(11) NOT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `request_date` datetime DEFAULT NULL,
  `address_type` varchar(2) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `rt` varchar(5) DEFAULT NULL,
  `rw` varchar(5) DEFAULT NULL,
  `kelurahan` varchar(20) DEFAULT NULL,
  `kecamatan` varchar(20) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `mobile_number` varchar(20) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_updates`
--

INSERT INTO `customer_updates` (`id_customer_update`, `id_customer`, `request_date`, `address_type`, `address`, `rt`, `rw`, `kelurahan`, `kecamatan`, `city`, `zip_code`, `phone_number`, `mobile_number`, `status`) VALUES
(3, 2, '2019-04-16 09:21:55', 'D', 'Jerukan', '07', '02', 'Karangturi', 'Gantiwarno', 'Klaten', '55282', '08176884055', '0274-488118', 'PENDING');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id_faq` int(11) NOT NULL,
  `question` varchar(100) DEFAULT NULL,
  `is_active` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id_faq`, `question`, `is_active`) VALUES
(1, 'Bagaimana cara menjadi nasabah Andalan?', 'TRUE'),
(2, 'Bagaimana cara mengajukan kredit mobil?', 'TRUE');

-- --------------------------------------------------------

--
-- Table structure for table `faq_answers`
--

CREATE TABLE `faq_answers` (
  `id_answer` int(11) NOT NULL,
  `id_faq` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `is_active` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq_answers`
--

INSERT INTO `faq_answers` (`id_answer`, `id_faq`, `image`, `description`, `is_active`) VALUES
(1, 1, '', 'Pendaftaran nasabah bisa dilakukan secara online maupun offline', 'TRUE'),
(2, 1, '', 'Pendaftaran online bisa dilakukan melalui website Andalan Finance', 'TRUE'),
(5, 1, '', 'Pendaftaran offline bisa dilakukan dengan cara datang langsung ke kantor cabang Andalan Finance terdekat', 'TRUE'),
(6, 2, 'andalanku_8773dba463193ca2.jpg', 'Silahkan menghubungi customer service Andalan Finance atau melalui aplikasi Andalanku', 'TRUE');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_rates`
--

CREATE TABLE `insurance_rates` (
  `id_insurance_rate` int(11) NOT NULL,
  `asuransi` varchar(5) DEFAULT NULL,
  `otr_range_min` int(11) DEFAULT NULL,
  `otr_range_max` int(11) DEFAULT NULL,
  `wilayah_1` decimal(6,2) DEFAULT NULL,
  `wilayah_2` decimal(6,2) DEFAULT NULL,
  `wilayah_3` decimal(6,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurance_rates`
--

INSERT INTO `insurance_rates` (`id_insurance_rate`, `asuransi`, `otr_range_min`, `otr_range_max`, `wilayah_1`, `wilayah_2`, `wilayah_3`) VALUES
(11, 'TLO', 0, 125000000, '4.20', '3.59', '2.78'),
(12, 'TLO', 125000001, 250000000, '2.94', '2.72', '2.96'),
(13, 'TLO', 200000001, 400000000, '2.40', '2.29', '1.97'),
(14, 'TLO', 400000001, 800000000, '1.32', '1.32', '1.25'),
(15, 'TLO', 800000001, 1410065407, '1.16', '1.16', '1.16'),
(16, 'ARK', 0, 125000000, '0.56', '0.78', '0.56'),
(17, 'ARK', 125000001, 250000000, '0.69', '0.53', '0.48'),
(18, 'ARK', 200000001, 400000000, '0.46', '0.42', '0.35'),
(19, 'ARK', 400000001, 800000000, '0.30', '0.30', '0.27'),
(20, 'ARK', 800000001, 1410065407, '0.24', '0.24', '0.24');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id_media` int(11) NOT NULL,
  `url` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id_media`, `url`) VALUES
(6, 'andalanku_64e714d81ed4592f.png'),
(7, 'andalanku_2e881a1b7cb05f1d.png'),
(31, 'andalanku_01306fe5565b5da2.jpg'),
(32, 'andalanku_0fcc4bbe3bd652c4.jpg'),
(35, 'andalanku_7ae70a5f68fc74b5.jpg'),
(36, 'andalanku_2058353324f6f3d1.png'),
(37, 'andalanku_bcebe4b9d0f9f1f4.jpg'),
(38, 'andalanku_a75eceea4bb041c2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id_news` int(11) NOT NULL,
  `news_date` datetime DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `content` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `slug` varchar(220) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id_news`, `news_date`, `title`, `content`, `image`, `slug`) VALUES
(5, '2019-03-27 15:15:44', 'Andalan Finance', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br /><br />Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', 'andalanku_c31233c82ce75850.jpg', 'andalan-finance');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id_partner` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id_partner`, `name`, `url`, `image`) VALUES
(4, 'Nasmoco', 'http://baru.nasmoco.co.id/', 'andalanku_148a69aeb78637a8.jpg'),
(5, 'Cars World', 'https://carsworld.id/', 'andalanku_4adbca12124c9f24.png');

-- --------------------------------------------------------

--
-- Table structure for table `promos`
--

CREATE TABLE `promos` (
  `id_promo` int(11) NOT NULL,
  `promo_date` datetime DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `content` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `slug` varchar(220) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promos`
--

INSERT INTO `promos` (`id_promo`, `promo_date`, `title`, `content`, `image`, `slug`) VALUES
(2, '2019-03-27 15:35:11', 'Promo Andalan 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br /><br />Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', 'andalanku_0d628b5d09051799.jpg', 'promo-andalan-1');

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `id_rate` int(11) NOT NULL,
  `tenor` int(11) DEFAULT NULL,
  `rate` decimal(6,2) DEFAULT NULL,
  `admin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id_rate`, `tenor`, `rate`, `admin`) VALUES
(5, 1, '21.00', 3500000),
(6, 2, '21.50', 3500000),
(7, 3, '22.00', 3500000),
(8, 4, '22.50', 3500000);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id_setting` int(11) NOT NULL,
  `setting_key` varchar(50) DEFAULT NULL,
  `setting_value` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id_setting`, `setting_key`, `setting_value`) VALUES
(3, 'Facebook', ''),
(4, 'Instagram', NULL),
(5, 'Youtube', NULL),
(6, 'Call Center', ''),
(7, 'Email Info', ''),
(8, 'Whatsapp Number', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id_subscriber` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `product` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id_subscriber`, `name`, `email`, `phone`, `product`) VALUES
(2, 'Heru Wahono', 'heru.wahono@gmail.com', '08882794385', 'New Car'),
(3, 'Andakara Prastawa', 'prastawa@gmail.com', '081392291546', 'Used Car'),
(4, 'Daniel Wenas', 'd.wenasss@gmail.com', '08178665044', 'Dana Tunai');

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id_token` int(11) NOT NULL,
  `api_key` varchar(32) DEFAULT NULL,
  `token_number` varchar(32) DEFAULT NULL,
  `token_expired` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tokens`
--

INSERT INTO `tokens` (`id_token`, `api_key`, `token_number`, `token_expired`) VALUES
(15, '1BA3A9444B0C4EB8F344EDB588E5101E', '812bf7664a59846ac614e3edde09450d', '2019-05-09 15:25:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `level` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `fullname`, `level`, `status`) VALUES
(1, 'superadmin', '82a3f212c95c1516907f27e1220c6f13', 'Superadmin Andalanku', 'SUPERADMIN', 'ACTIVE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api_keys`
--
ALTER TABLE `api_keys`
  ADD PRIMARY KEY (`id_api_key`);

--
-- Indexes for table `car_brands`
--
ALTER TABLE `car_brands`
  ADD PRIMARY KEY (`id_car_brand`);

--
-- Indexes for table `car_rates`
--
ALTER TABLE `car_rates`
  ADD PRIMARY KEY (`id_car_rate`);

--
-- Indexes for table `car_trims`
--
ALTER TABLE `car_trims`
  ADD PRIMARY KEY (`id_car_trim`);

--
-- Indexes for table `car_types`
--
ALTER TABLE `car_types`
  ADD PRIMARY KEY (`id_car_type`);

--
-- Indexes for table `car_type_media`
--
ALTER TABLE `car_type_media`
  ADD PRIMARY KEY (`id_car_type_media`);

--
-- Indexes for table `complains`
--
ALTER TABLE `complains`
  ADD PRIMARY KEY (`id_complain`);

--
-- Indexes for table `complain_media`
--
ALTER TABLE `complain_media`
  ADD PRIMARY KEY (`id_complain_media`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `customer_updates`
--
ALTER TABLE `customer_updates`
  ADD PRIMARY KEY (`id_customer_update`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id_faq`);

--
-- Indexes for table `faq_answers`
--
ALTER TABLE `faq_answers`
  ADD PRIMARY KEY (`id_answer`);

--
-- Indexes for table `insurance_rates`
--
ALTER TABLE `insurance_rates`
  ADD PRIMARY KEY (`id_insurance_rate`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id_media`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id_news`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id_partner`);

--
-- Indexes for table `promos`
--
ALTER TABLE `promos`
  ADD PRIMARY KEY (`id_promo`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id_rate`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id_subscriber`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id_token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api_keys`
--
ALTER TABLE `api_keys`
  MODIFY `id_api_key` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `car_brands`
--
ALTER TABLE `car_brands`
  MODIFY `id_car_brand` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `car_rates`
--
ALTER TABLE `car_rates`
  MODIFY `id_car_rate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `car_trims`
--
ALTER TABLE `car_trims`
  MODIFY `id_car_trim` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `car_types`
--
ALTER TABLE `car_types`
  MODIFY `id_car_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `car_type_media`
--
ALTER TABLE `car_type_media`
  MODIFY `id_car_type_media` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `complains`
--
ALTER TABLE `complains`
  MODIFY `id_complain` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `complain_media`
--
ALTER TABLE `complain_media`
  MODIFY `id_complain_media` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id_customer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `customer_updates`
--
ALTER TABLE `customer_updates`
  MODIFY `id_customer_update` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id_faq` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `faq_answers`
--
ALTER TABLE `faq_answers`
  MODIFY `id_answer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `insurance_rates`
--
ALTER TABLE `insurance_rates`
  MODIFY `id_insurance_rate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id_media` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id_news` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id_partner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `promos`
--
ALTER TABLE `promos`
  MODIFY `id_promo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `id_rate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id_subscriber` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id_token` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
