<?php
	include "check-admin-session.php";
	include "plugins/image-resize/lib/ImageResize.php";
	
	function uploadImage1($file1) {

		$response = "";

		$path_parts = pathinfo($file1["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/slider_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'Banner_Apps_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'Banner_Apps_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file1['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

				//resize image
				$image = new \Eventviva\ImageResize($folder . '/' . $newnamefile);
				$image->resize(1125, 1170,  $allow_enlarge = True);
				$image->save($folder . '/' . $newnamefile);

			} else {
				$response = "";
			}
		}

		return $response;
	}

	function uploadImage2($file2) {

		$response = "";

		$path_parts = pathinfo($file2["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/slider_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'Banner_Web_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'Banner_Web_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file2['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

				//resize image
				$image = new \Eventviva\ImageResize($folder . '/' . $newnamefile);
				$image->resize(1125, 525,  $allow_enlarge = True);
				$image->save($folder . '/' . $newnamefile);

			} else {
				$response = "";
			}
		}

		return $response;
	}

	$id_promo = sanitize_int($_REQUEST["id_promo"]);

	if (!empty($_FILES['imageInput1']) and !empty($_FILES['imageInput2'])) {
		
		//upload gambar dulu
		$imageFileName1 = "";
		$imageFileName2 = "";
		if (!empty($_FILES['imageInput1'])) {
			$imageFileName1 = uploadImage1($_FILES['imageInput1']);
		} else {
			$imageFileName1 = "";
		}

		if (!empty($_FILES['imageInput2'])) {
			$imageFileName2 = uploadImage2($_FILES['imageInput2']);
		} else {
			$imageFileName2 = "";
		}
		
        $query = "INSERT INTO sliders(image, image_web, id_promo) 
				  VALUES ('$imageFileName1', '$imageFileName2', '$id_promo')";
        mysqli_query($mysql_connection, $query);
		
        echo 'success'; 
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>