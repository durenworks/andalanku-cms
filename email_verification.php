<?php
include "inc-header.php";
include "inc-db.php";
include "sanitize.inc.php";

$verification_code	= sanitize_sql_string(trim($_REQUEST["c"]));
$err = '';

if ($verification_code <> '') {

	$queryCheck = "select id_customer from customers where verification_code='$verification_code'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) == 0) {
		
		$err = 'invalid_code';
	}
	else {

		$dataCheck	  = mysqli_fetch_array($resultCheck);
		$id_customer  = $dataCheck['id_customer'];
	
		$queryUpdate  = "update customers set email_verification='VERIFIED', verification_code='' 
						 where id_customer='$id_customer' ";
		$resultUpdate = mysqli_query($mysql_connection, $queryUpdate);
	}
} else {

    $err = 'invalid_code';
}
?>

<body class="login-page">

    <div class="login-box">

      <div class="login-box-body">
		<div class="text-center">

			<table align="center" class="table-responsive">
				<tr>
					<td class="valign_top" width="100%">
						<img src="img/logo.png" alt="Logo">
					</td>
				</tr>
			</table>
			<br>
			<h4>Verifikasi Email</h4>
		</div>
        <p class="login-box-msg">
        	
			<?php
				if($err=="invalid_code") {
					echo '<div class="text-green text-center">';
					echo "Terima kasih telah melakukan verifikasi alamat email Anda. <br>
						  Sekarang Anda dapat menikmati semua fitur yang ada di dalam aplikasi Andalanku.<br><br>";
					echo '</div>';
				}
				else if($err=="invalid_code") {
					echo '<div class="text-red text-center">';
					echo "Kode verifikasi tidak benar. Silahkan periksa kembali email Anda.<br><br>";
					echo '</div>';
					echo '<div class="text-center">';
					echo "<a href='#'><button type='button' class='btn btn-flat btn-primary'>Kirim Ulang Email Aktivasi</button></a>";
					echo '</div>';
				}
			?>
			
		</p>
        <br>

      </div><!-- /.login-box-body -->



    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js" type="text/javascript"></script>
	
	<script src='https://www.google.com/recaptcha/api.js'></script>

  </body>
</html>