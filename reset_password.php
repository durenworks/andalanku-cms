<?php
	include "inc-header.php";
	$msg = $_REQUEST['e'];
	$password_code = $_REQUEST['c'];
?>

<body class="login-page">

    <div class="login-box">

      <div class="login-box-body">
		<div class="text-center">

			<table align="center" class="table-responsive">
				<tr>
					<td class="valign_top" width="100%">
						<img src="img/logo.png" alt="Logo">
					</td>
				</tr>
			</table>
			<br>
			<h4>Reset Password</h4>
		</div>
        <p class="login-box-msg">
        	<div class="text-red text-center">
			<?php
				if($msg=="01") echo "Data yang Anda masukkan tidak lengkap";
				else if($msg=="02") echo "Password dan Konfirmasi Password tidak sama";
				else if($msg=="03") echo "Password minimal 8 karakter";
				else if($msg=="04") echo "Reset password gagal. Silahkan periksa kembali email Anda";
				else if($msg=="05") echo "Invalid captcha response";
				else if($msg=="06") echo "Reset password berhasil. Silahkan login kembali di aplikasi Andalanku.";
				else if($msg=="") echo "Silahkan masukkan password baru Anda";
			?>
			</div>
		</p>
		<?php
			if($msg!="06") {
		?>
				<form action="reset_password_save" method="post">
				  <input type="hidden" name="password_code" value="<?php echo $password_code; ?>" />
				  <div class="form-group has-feedback">
					<input type="password" name="password" class="form-control" placeholder="Password Baru" />
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<input type="password" name="password_confirm" class="form-control" placeholder="Konfirmasi Password Baru" />
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				  </div>
				  <div class="form-group has-feedback">
					<div class="g-recaptcha" data-sitekey="6LcTPJoUAAAAAGBU6QmVaTpIVNq0aeYt7S4MeDOo"></div>
				  </div>
				  <div class="row">
					<div class="col-xs-4">
					  <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-save"></i> Simpan</button>
					</div>
					<div class="col-xs-4">
					  &nbsp;
					</div>
					<div class="col-xs-4">
					  &nbsp;
					</div><!-- /.col -->
				  </div>
				</form>
		<?php 
			}
		?>
        <br>

      </div><!-- /.login-box-body -->



    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js" type="text/javascript"></script>
	
	<script src='https://www.google.com/recaptcha/api.js'></script>

  </body>
</html>
