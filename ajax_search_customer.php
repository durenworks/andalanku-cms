<?php
	include "check-admin-session.php";

	$keyword			= sanitize_sql_string($_REQUEST["keyword"]);
	$src_type			= sanitize_sql_string($_REQUEST["src_type"]);
	$src_status			= sanitize_sql_string($_REQUEST["src_status"]);
	$page 				= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(*) as num
					   from customers
					   where (customer_name like '%$keyword%' or phone_number like '%$keyword%' or email like '%$keyword%') ";
	
	if($src_type=='CUSTOMER') $query = $query." and customer_type='CUSTOMER' ";
	else if($src_type == 'AGENT') $query = $query." and customer_type='AGENT' ";	
	else if($src_type == 'EMPLOYEE') $query = $query." and customer_type='EMPLOYEE' ";	

	if ($src_status == 'new' ) $query = $query." and andalan_customer_id is null ";
	else if ($src_status == 'existing') $query = $query . " and andalan_customer_id is not null";

	$query = $query." and is_active ";
	
	$result 		= mysqli_query($mysql_connection, $query);
	//echo $query;
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select * 
			  from customers
			  left join agreement_list al on id_customer = customer_id
			  where (customer_name like '%$keyword%' or phone_number like '%$keyword%' or email like '%$keyword%' or al.agreement_number like'%$keyword%') ";
	
	if($src_type=='CUSTOMER') $query = $query." and customer_type='CUSTOMER' ";
	else if($src_type == 'AGENT') $query = $query." and customer_type='AGENT' ";	
	else if($src_type == 'EMPLOYEE') $query = $query." and customer_type='EMPLOYEE' ";	

	if ($src_status == 'new' ) $query = $query." and andalan_customer_id is null ";
	else if ($src_status == 'existing') $query = $query . " and andalan_customer_id is not null";

	$query = $query." and is_active ";
	
	$query = $query." order by customer_name ASC LIMIT $start,$limit"; 
	//var_dump($query);die();
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='2%'>No</th>";
	
	if($src_type == 'EMPLOYEE') {
		echo "<th width='8%'>NIP</th>";
	}
	else {
		echo "<th width='8%'>Tipe</th>";
	}
				  
	echo "		  <th width='15%'>Nama</th>
				  <th width='15%'>Email</th>
				  <th width='10%'>Telepon</th>
				  <th width='15%'>Login Terakhir</th>
				  <th width='10%'>No. Kontrak</th>
				  <th width='10%'>Tracking</th>
				  <th width='10%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {
		
		if($data['customer_type'] == 'AGENT') {
			$detailLink = '<a href="#modalDetail" onclick="getDetail(' . @$data['id_customer'] . ')"><i class="fa fa-book"></i> Rekening</a>';
		}
		else {
			$detailLink = '';
		}

		echo '<tr>
				  <td>'.$i.'</th>';
				  
				  
		if($src_type == 'EMPLOYEE') {
			echo '<td>' . @$data[nip] . '</td>';
		}
		else {
			echo '<td>' . @$data[customer_type] . '<br>'.$detailLink.'</td>';
		}
	
		//----- Ambil Nomor Kontrak ------//
		$agreement_number = '';
		$branch_stack = '';
		$query_agrrement = "select agreement_number, branch from agreement_list where customer_id = '$data[id_customer]'";
		//echo $query_agrrement; die();
		$result_agreement = mysqli_query($mysql_connection, $query_agrrement);
		//var_dump($result_agreement);die();
		//$agreement_number = $query_agrrement;
		
		while ($data_agreement = mysqli_fetch_assoc($result_agreement)) {
			
			if ($data_agreement[branch] == '') {
				
				$api_url	= $afis_api_url.'/Agreement/ActiveAgrementNumber/'.$data[andalan_customer_id];
				$ch 		= curl_init();

				curl_setopt($ch, CURLOPT_URL, $api_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

				$result_afis = curl_exec($ch);
				$result_afis = json_decode($result_afis,true);

				$responseArray = $result_afis['Response']['Data'];

				foreach($responseArray as $key=>$value) {
					
					$agreementNo = trim($value['agreementNo']);

					if ($data_agreement[agreement_number] == $agreementNo) {
						$branch = trim($value['branchfullname']);
						
						$query = "update agreement_list set branch='$branch ' where agreement_number='$data_agreement[agreement_number]'";
						mysqli_query($mysql_connection, $query);
					}
				}
			}
			else {
				$branch = $data_agreement[branch];
			}

			if($agreement_number != '') {
				$agreement_number = $agreement_number . ', ' . $data_agreement[agreement_number];
				$branch_stack = $branch_stack . ', ' . $branch;
			}
			else {
				$agreement_number = $data_agreement[agreement_number];
				$branch_stack = $branch;
			}
		}
		echo '		  
				  	<td><a href="#modalDetailAll" onclick="getDetailAll(' . @$data['id_customer'] . ')">' . @$data['customer_name'] . '</a></td>
				  	<td>' . @$data['email'] . '</td>
				  	<td>' . @$data['phone_number'] . '</td>
				  	<td>' . @$data['last_login'] . '</td>
				  	<td>' . $agreement_number . '</td>
				  	<td><a href="tracking?id='.$data['id_customer'].'" target="_blank">Lihat Peta</a></td>
				  	<td align="center"> 
						<a href="#" onclick="resetPasswordCustomer(' . @$data['id_customer'] . ',\'' . @$data['customer_name'] . '\')">Reset Password</a>
					</td>
					<td align="center"> 
						<a href="direct_message_detail.php?id='. @$data['id_customer'] . '">Kirim Pesan</a>
					</td>

			  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>