<?php
	include "check-admin-session.php";

	$collateralClaimID = sanitize_int($_REQUEST["collateralClaimID"]);	
	
	$query 	= "select a.*, b.customer_name, b.phone_number  
			   from collateral_requests a 
			   left join customers b on a.customer_id=b.id_customer 
			   where a.id='$collateralClaimID'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
	
	if($data['status'] == 'INPUT REQUEST') {
		
		$now = date("Y-m-d H:i:s");
		
		$query 	= "update collateral_requests set status='ON PROCESS' 
				   where id='$collateralClaimID'";
		mysqli_query($mysql_connection, $query);
		
		//==================== INBOX ====================
		$content					 = array();
		$content['ticket_number'] 	 = $data['ticket_number'];
		$content['input_date'] 		 = $now;
		$content['status'] 		 	 = 'ON PROCESS';
		$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
		$inbox_message				 .= 'Konsumen yang terhormat, pengajuan Anda saat ini sudah kami proses. ';
		$inbox_message				 .= 'Untuk jadwal pengambilan adalah maksimal 7 hari setelah pengajuan ini.';
		$content['message']			 = $inbox_message;
		$content = json_encode($content);
		
		//insert ke tabel inbox
		$now			= date('Y-m-d H:i:s');
		$queryCheck 	= "select id from inbox where content like '%".$data['ticket_number']."%' ";
		$resultCheck	= mysqli_query($mysql_connection, $queryCheck);
		$dataCheck		= mysqli_fetch_array($resultCheck);
		$id_inbox		= $dataCheck['id'];
		$queryInsert 	= "update inbox set date='$now', status='0', content='$content' where id='$id_inbox'";
		mysqli_query($mysql_connection, $queryInsert);
		
		/*$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
						values('$id_customer', '$now', 'collateral request', 'E-Request Pengambilan Jaminan', '0', 
						'$content')";
		mysqli_query($mysql_connection, $queryInsert);*/
		//===============================================
	}
	
	$data['request_date']	= date("d-m-Y H:i:s", strtotime($data['request_date']));
	$data['taking_date']	= date("d-m-Y", strtotime($data['taking_date']));
	
	if($data['ready_date'] <> '' && $data['ready_date'] <> '0000-00-00') {
		$data['ready_date']	= date("d-m-Y", strtotime($data['ready_date']));
	}
	
	echo json_encode($data);
?>
