<?php
	include "check-admin-session.php";
	
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Data_Harga_Mobil_Andalanku.xls");
	
	echo "<b>Data Harga Mobil Andalanku</b><br><br>";

	$id_car_brand = sanitize_int($_REQUEST["id_car_brand"]);
			
	$query = "select * from car_brands where 1 ";
	if($id_car_brand <> '0') $query = $query." and id_car_brand='$$id_car_brand' ";		  
	$query = $query."order by name ASC "; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table border='1'>";

	while ($data = mysqli_fetch_array($result)) {

		echo '<tr><td colspan="3"><b>'.$data['name'].'<b></td></tr>';
		
		$id_car_brand = $data['id_car_brand'];
		
		$queryType = "select * from car_types where id_car_brand='$id_car_brand' order by name ASC";
		$resultType= mysqli_query($mysql_connection, $queryType);
		while($dataType = mysqli_fetch_array($resultType)) {
			
			$id_car_type = $dataType['id_car_type'];
			
			$queryTrim = "select * from car_trims where id_car_type='$id_car_type' order by name ASC";
			$resultTrim= mysqli_query($mysql_connection, $queryTrim);
			while($dataTrim = mysqli_fetch_array($resultTrim)) {
				echo '<tr>
					  <td width="200px">'.$dataType['name'].'</td>
					  <td width="200px">'.$dataTrim['name'].'</td>
					  <td width="200px">'.number_format($dataTrim['price'],2,',','.').'</td>
					  </tr>';
			}
		}
	}

	echo "</table>";
?>