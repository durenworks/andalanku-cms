<?php
	include "check-admin-session.php";
	include "plugins/image-resize/lib/ImageResize.php";
	
	function uploadImage($file) {

		$response = "";

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/car_brand_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'andalanku_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

				//resize image
				$image = new \Eventviva\ImageResize($folder . '/' . $newnamefile);
				$image->resizeToBestFit(800, 600);
				$image->save($folder . '/' . $newnamefile);

			} else {
				$response = "";
			}
		}

		return $response;
	}

	$name		= sanitize_sql_string($_REQUEST["name"]);
	$is_active	= sanitize_sql_string($_REQUEST["is_active"]);

	if ($name <> '') {
		
		$queryCheck		= "SELECT id_car_brand from car_brands WHERE name='$name'";
		$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck ) > 0) {
			echo "name_exist";
			exit;
		}
		
		//upload gambar dulu
		$imageFileName1 = "";
		if (!empty($_FILES['imageInput1'])) {
			$imageFileName1 = uploadImage($_FILES['imageInput1']);
		} else {
			$imageFileName1 = "";
		}
		
        $query = "INSERT INTO car_brands(name, image, is_active) 
				  VALUES ('$name', '$imageFileName1', '$is_active')";
        mysqli_query($mysql_connection, $query);
		
        echo 'success'; 
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>