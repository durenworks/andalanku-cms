<?php
	//create_pks(16);
	
	function create_pks($id_register_agent) {
		
		ob_start();

		//include "check-admin-session.php";
		include "inc-db.php";
		include "plugins/tcpdf/tcpdf.php";
	
		//$id_register_agent	= sanitize_int($_REQUEST["id"]);
	
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Andalanku.id');
		$pdf->SetTitle('Perjanjian Kerjasama Agen');
		$pdf->SetSubject('Perjanjian Kerjasama Agen');

		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, 20, PDF_MARGIN_RIGHT);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 9);

		// add a page
		$pdf->AddPage();
		
		$dayName 	= getDayName();
		$today		= date("d-m-Y");
		
		$queryRequest 	= "select a.*, b.name as occupation_name 
						  from register_agent_history a 
						  left join occupations b on a.occupation_id=b.id 
						  where id_register_agent='$id_register_agent'";
		$resultRequest	= mysqli_query($mysql_connection, $queryRequest);
		$dataRequest 	= mysqli_fetch_array($resultRequest);
		$id_customer	= $dataRequest['id_customer'];
		
		$queryCustomer  = "select * 
						   from customers 
						   where id_customer='".$id_customer."' ";
		$resultCustomer = mysqli_query($mysql_connection, $queryCustomer);
		$dataCustomer 	= mysqli_fetch_array($resultCustomer);
		
		$queryAddress  = "SELECT a.address_domicile, a.rt_domicile, a.rw_domicile, a.zip_code_domicile, 
							e.name AS village_name, d.name AS district_name, 
							c.name AS regency_name, b.name AS province_name 
							FROM register_agent_history a  
							LEFT JOIN provinces b ON a.province_id_domicile=b.id 
							LEFT JOIN regencies c ON a.regency_id_domicile=c.id 
							LEFT JOIN districts d ON a.district_id_domicile=d.id 
							LEFT JOIN villages e ON a.village_id_domicile=e.id 
							WHERE id_register_agent='$id_register_agent' ";
		$resultAddress = mysqli_query($mysql_connection, $queryAddress);
		$dataAddress = mysqli_fetch_array($resultAddress);	
		
		$query  = "select branch_manager from branches where id ='".$dataRequest['branch_id']."' ";
		$result = mysqli_query($mysql_connection, $query);
		$data   = mysqli_fetch_array($result);	
		$branchManagerName = $data['branch_manager'];
		
		if($dataRequest['kerabat_afi'] == 'TIDAK') {
			$kerabat_afi = 'TIDAK';
			$nama_kerabat_afi = '-';
			$jabatan_kerabat_afi = '-';
		}
		else {
			$kerabat_afi = 'YA';
			$nama_kerabat_afi = $dataRequest['kerabat_afi'];
			$jabatan_kerabat_afi = $dataRequest['kerabat_afi_jabatan'];
		}
		
		$html = '
		<p style="text-align: center;"><b><u>
			FORM PENDAFTARAN AGEN PEMASARAN<br>
			PT ANDALAN FINANCE INDONESIA
		</u></b></p>
		<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>
		
		<table width="100%">
			<tr>
				<td width="25%">Nama Lengkap</td>
				<td width="2%">:</td>
				<td width="25%">'.$dataCustomer['customer_name'].' ('.$dataCustomer['gender'].')</td>
				<td width="6%">&nbsp;</td>
				<td width="20%">Rekening</td>
				<td width="2%"> </td>
				<td width="25%"> </td>
			</tr>
			<tr>
				<td>No. Identitas (KTP/SIM) </td>
				<td>:</td>
				<td>'.$dataRequest['id_card_number'].'</td>
				<td>&nbsp;</td>
				<td>&nbsp;&nbsp;- No.Rek</td>
				<td>:</td>
				<td>'.$dataRequest['bank_account_number'].'</td>
			</tr>
			<tr>
				<td>Tempat / Tanggal Lahir</td>
				<td>:</td>
				<td>'.$dataCustomer['place_of_birth'].' / '.date('d-m-Y', strtotime($dataCustomer['date_of_birth'])).'</td>
				<td>&nbsp;</td>
				<td>&nbsp;&nbsp;- Bank</td>
				<td>:</td>
				<td>'.$dataRequest['bank_name'].'</td>
			</tr>
			<tr>
				<td>No. Telp / HP</td>
				<td>:</td>
				<td>'.$dataCustomer['phone_number'].'</td>
				<td>&nbsp;</td>
				<td>&nbsp;&nbsp;- A.n</td>
				<td>:</td>
				<td>'.$dataRequest['bank_account_holder'].'</td>
			</tr>
			<tr>
				<td>Alamat (sesuai identitas)</td>
				<td>:</td>
				<td>'.$dataRequest['address'].'</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>RT / RW</td>
				<td>:</td>
				<td>'.$dataRequest['rt'].' / '.$dataRequest['rw'].'</td>
				<td>&nbsp;</td>
				<td>Kode Pos</td>
				<td>:</td>
				<td>'.$dataRequest['zip_code'].'</td>
				
			</tr>
			<tr>
				<td>Pekerjaan</td>
				<td>:</td>
				<td>'.$dataRequest['occupation_name'].'</td>
				
			</tr>
			<tr>
				<td>No. NPWP</td>
				<td>:</td>
				<td>'.$dataRequest['npwp'].'</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Alamat Domisili</td>
				<td>:</td>
				<td>'.$dataAddress['address_domicile'].'</td>
				<td>&nbsp;</td>
				<td>RT / RW</td>
				<td>:</td>
				<td>'.$dataAddress['rt_domicile'].' / '.$dataAddress['rw_domicile'].'</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>'.$dataAddress['village_name'].', '.$dataAddress['district_name'].', '.$dataAddress['regency_name'].', '.$dataAddress['province_name'].'</td>
				<td>&nbsp;</td>
				<td>Kode Pos</td>
				<td>:</td>
				<td>'.$dataAddress['zip_code_domicile'].'</td>
			</tr>
			<tr>
				<td>Alamat Email</td>
				<td>:</td>
				<td>'.$dataRequest['email'].'</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<br><br>
		Dengan ini saya menyatakan dengan sebenarnya hal-hal sebagai berikut : 
		<br><br>
		<table width="100%">
		<tr>
			<td width="3%">1.</td>
			<td width="97%">
				Bahwa saya memahami PT Andalan Finance Indonesia (<b>"Andalan Finance"</b>) adalah suatu perusahaan yang bergerak dalam bidang jasa pembiayaan dengan kegiatan usaha antara lain Pembiayaan Multiguna.
			</td>
		</tr>
		<tr>
			<td width="3%">2.</td>
			<td width="97%">
				Pembiayaan Multiguna adalah pembiayaan barang dan/atau jasa yang diperlukan oleh debitur untuk pemakaian/konsumsi dan bukan untuk keperluan usaha atau aktivitas produktif dalam jangka waktu yang diperjanjikan berdasarkan kebutuhan debitur dengan cara fasilitas dana atau pembelian barang/jasa dengan pembayaran kembali oleh debitur secara angsuran.
			</td>
		</tr>
		<tr>
			<td width="3%">3.</td>
			<td width="97%">
				3.	Bahwa saya adalah perseorangan yang memiliki kemampuan dan bersedia melakukan kerja sama dengan Andalan Finance untuk memasarkan fasilitas Pembiayaan Multiguna yang ditawarkan oleh Andalan Finance kepada para calon debitur (<b>"Kerja Sama"</b>). 
			</td>
		</tr>
		<tr>
			<td width="3%">4.</td>
			<td width="97%">
				Bahwa saya memahami debitur adalah badan usaha atau orang perseorangan yang menerima pembiayaan Multiguna dari Andalan Finance.
			</td>
		</tr>
		<tr>
			<td width="3%">5.</td>
			<td width="97%">
				5.	Bahwa   dalam rangka Kerja Sama saya setuju memenuhi syarat dan ketentuan agen pemasaran yang disyaratkan oleh Andalan Finance (<b>"Syarat dan Ketentuan"</b>) dan dengan ini mendaftarkan diri sebagai agen pemasaran Andalan Finance (<b>"Agen"</b>) dan oleh karenanya setuju serta berjanji untuk tunduk pada  Syarat dan Ketentuan Agen Pemasaran Andalan Finance sebagai berikut : 
			</td>
		</tr>
		</table>
		<br><br>
		<p style="text-align: center;"><b><u>
			SYARAT DAN KETENTUAN AGEN PEMASARAN<br>
			PT ANDALAN FINANCE INDONESIA  ("ANDALAN FINANCE")<br>
			("SYARAT DAN KETENTUAN")<br>
		</u></b></p>
		
		<br><br>
		<p style="text-align: center;"><b>PASAL 1<br>TUGAS DAN FUNGSI</b></p>
		<table width="100%">
		<tr>
			<td width="3%">1.</td>
			<td width="97%">
				Agen setuju dan berjanji untuk memasarkan fasilitas Pembiayaan Multiguna yang disediakan oleh Andalan Finance kepada calon debitur.
			</td>
		</tr>
		<tr>
			<td width="3%">2.</td>
			<td width="97%">
				Agen  wajib menaati dan mematuhi seluruh dan setiap ketentuan yang termaktub di Syarat dan Ketentuan ini.
			</td>
		</tr>
		</table>
		
		<br><br>
		<p style="text-align: center;"><b>PASAL 2<br>HAK DAN KEWAJIBAN</b></p>
		<table width="100%">
		<tr>
			<td width="3%">1.</td>
			<td width="97%">
				Agen wajib mencapai target sebagaimana telah ditentukan oleh Andalan Finance.  
			</td>
		</tr>
		<tr>
			<td width="3%">2.</td>
			<td width="97%">
				Agen setuju dalam hal Agen tidak dapat memenuhi prestasi yang telah ditentukan, maka Andalan Finance berhak mengakhiri Kerja Sama dengan Agen.
			</td>
		</tr>
		<tr>
			<td width="3%">3.</td>
			<td width="97%">
				Agen berhak memperoleh imbalan jasa yang besarnya telah ditentukan oleh Andalan Finance berdasarkan target yang harus dicapai oleh Agen sebagaimana dimaksud dalam butir 1 di atas, yang dihitung berdasarkan aplikasi yang disetujui oleh Andalan Finance dan telah dilakukan pencairan dana atas fasilitas Pembiayaan Multiguna yang diberikan oleh Andalan Finance tersebut.
			</td>
		</tr>
		<tr>
			<td width="3%">4.</td>
			<td width="97%">
				Target dan imbalan jasa akan dituangkan dalam dokumen terpisah, yang disepakati oleh Agen dan Andalan Finance.
			</td>
		</tr>
		<tr>
			<td width="3%">5.</td>
			<td width="97%">
				Agen setuju bahwa  Andalan Finance berhak menerima dan/atau menolak seluruh dan/atau setiap Formulir Aplikasi Permohonan Pembiayaan (FPP) yang diajukan dan direkomendasikan oleh Agen, semata-mata berdasarkan pertimbangan Andalan Finance sendiri.  
			</td>
		</tr>
		<tr>
			<td width="3%">6.</td>
			<td width="97%">
				Agen berhak atas pembayaran uang imbalan jasa dari Andalan Finance sebagaimana dimaksud ayat 3 di atas  pada tanggal 7 setiap bulannya. 
			</td>
		</tr>
		<tr>
			<td width="3%">7.</td>
			<td width="97%">
				Agen setuju teknis pelaksanaan imbalan jasa lebih lanjut akan ditentukan oleh Andalan Finance dan sewaktu-waktu dapat berubah sesuai dengan kebijakan dari Andalah Finance.
			</td>
		</tr>
		</table>

		
		<br><br>
		<p style="text-align: center;"><b>PASAL 3<br>BATASAN-BATASAN</b></p>
		<table width="100%">
		<tr>
			<td width="3%">1.</td>
			<td width="97%">
				Agen dilarang dan/ atau tidak berhak untuk:
				<br>
				<table width="100%">
				<tr>
				<td width="3%">a)</td>
				<td width="97%">
					Meminta dan/atau menerima angsuran dan/atau pembayaran dari Debitur.
				</td>
				</tr>
				<tr>
				<td width="3%">b)</td>
				<td width="97%">
					Meminta dan/ atau menerima uang pengurusan/ komisi/ hadiah dalam bentuk apapun dari debitur.
				</td>
				</tr>
				<tr>
				<td width="3%">c)</td>
				<td width="97%">
					Meminta dan/ atau menerima asli dokumen/ data (-data) dari Calon Debitur.
				</td>
				</tr>
				<tr>
				<td width="3%">d)</td>
				<td width="97%">
					Memberikan informasi yang menyesatkan kepada calon debitur, misalnya: "bisa mempercepat proses pembiayaan", "pasti disetujui" dan lain-lain.
				</td>
				</tr>
				<tr>
				<td width="3%">e)</td>
				<td width="97%">
					Mengubah/memalsukan data (-data) Calon Debitur, atau membujuk/mengajarkan calon debitur supaya memalsukan data (-data) yang diberikan kepada Andalan Finance.
				</td>
				</tr>
				<tr>
				<td width="3%">f)</td>
				<td width="97%">
					Melakukan promosi dengan memakai nama Andalan Finance dan/ atau logo ANDALANKU untuk semua bentuk media promosi seperti, iklan koran, web internet, poster, dll, selain media promosi resmi yang diizinkan oleh Andalan Finance.
				</td>
				</tr>
				<tr>
				<td width="3%">g)</td>
				<td width="97%">
					Melakukan kegiatan pemasaran/promosi kepada calon debitur maupun debitur tanpa ada persetujuan tertulis dari Andalan Finance.
				</td>
				</tr>
				<tr>
				<td width="3%">h)</td>
				<td width="97%">
					Melakukan hal-hal lain yang dianggap merugikan Andalan Finance.
				</td>
				</tr>
				</table>
				<br>&nbsp;
			</td>
		</tr>
		<tr>
			<td width="3%">2.</td>
			<td width="97%">
				Agen tidak berhak bertindak untuk dan atas nama serta mewakili kepentingan Andalan Finance, menggunakan fasilitas serta segala sesuatu yang berhubungan dengan Andalan Finance.		
			</td>
		</tr>
		<tr>
			<td width="3%">2.</td>
			<td width="97%">
				Apabila Agen melanggar batasan-batasan/larangan-larangan pada ayat 1 tersebut di atas dan/atau tidak melaksanakan seluruh dan setiap ketentuan yang terdapat di dalam Syarat dan Ketentuan ini, meskipun Agen berhasil mencapai target sebagaimana dimaksud Pasal 2 ayat 1, maka Andalah Finance  berhak untuk mengakhiri Kerja Sama dengan Agen sesuai dengan ketentuan Pasal 4 di bawah ini.    			
			</td>
		</tr>
		</table>
		
		
		<br><br>
		<p style="text-align: center;"><b>PASAL 4<br>JANGKA WAKTU DAN PENGAKHIRAN KERJA SAMA</b></p>
		<table width="100%">
		<tr>
			<td width="3%">1.</td>
			<td width="97%">
				Kerja sama ini mulai berlaku sejak disetujuinya Syarat dan Ketentuan ini oleh Agen  dan akan berakhir apabila kerja sama ini diakhiri dan/ atau dibatalkan sebagaimana dimaksud dalam ayat 2 di bawah ini. 
			</td>
		</tr>
		<tr>
			<td width="3%">2.</td>
			<td width="97%">
				Agen maupun Andalan Finance berhak untuk mengakhiri kerja sama ini sewaktu-waktu dengan pemberitahuan kepada pihak lainnya selambat-lambatnya dalam waktu 3 (tiga) hari kalender sebelum pengakhiran Kerja Sama ini efektif berlaku, berdasarkan pertimbangan masing-masing pihak.		
			</td>
		</tr>
		<tr>
			<td width="3%">3.</td>
			<td width="97%">
				Dalam hal Kerja Sama diakhiri dan/ atau dibatalkan, maka Agen sepakat dan setuju untuk mengesampingkan ketentuan pada Pasal 1266 dan 1267 Kitab Undang-undang Hukum Perdata.
			</td>
		</tr>
		</table>
	
		
		<br><br>
		<p style="text-align: center;"><b>PASAL 5<br>AKIBAT PENGAKHIRAN PERJANJIAN</b></p>
		<table width="100%">
		<tr>
			<td colspan="2">
				Agen menyetujui bahwa dalam hal Kerja Sama ini diakhiri sebagaimana dimaksud Pasal 4 di atas, maka:
			</td>
		</tr>
		<tr>
			<td width="3%">1.</td>
			<td width="97%">
				Agen tidak berhak atas, dan Andalan Finance tidak berkewajiban untuk memberikan kompensasi berupa apapun juga.
			</td>
		</tr>
		<tr>
			<td width="3%">2.</td>
			<td width="97%">
				Agen tidak berhak menggunakan dan/atau memperoleh segala fasilitas yang telah dan/atau akan diperoleh Agen di dalam melaksanakan tugas dan fungsinya sebagaimana dimaksud dalam Syarat dan Ketentuan ini.
			</td>
		</tr>
		<tr>
			<td width="3%">3.</td>
			<td width="97%">
				Agen wajib mengembalikan dan menyerahkan kepada Andalan Finance semua sarana dan prasarana yang telah diperoleh dan diterima dari Andalan Finance dalam rangka pelaksanaan tugas dan fungsinya sebagai Agen.
			</td>
		</tr>
		<tr>
			<td width="3%">4.</td>
			<td width="97%">
				Agen berkewajiban untuk melunasi seluruh utangnya kepada Andalan Finance (bila ada).
			</td>
		</tr>
		<tr>
			<td width="3%">5.</td>
			<td width="97%">
				Agen dan Andalan Finance wajib untuk menyelesaikan segala kewajiban kepada masing-masing pihak yang belum selesai saat Kerja Sama berakhir.
			</td>
		</tr>
		</table>
		
		<br><br>
		<p style="text-align: center;"><b>PASAL 6<br>KETENTUAN PENUTUP</b></p>
		<table width="100%">
		<tr>
			<td width="3%">1.</td>
			<td width="97%">
				Hal-hal yang belum diatur dalam Syarat dan Ketentuan ini akan diatur kemudian dan menjadi bagian yang tidak terpisahkan dari Syarat dan Ketentuan ini.
			</td>
		</tr>
		<tr>
			<td width="3%">2.</td>
			<td width="97%">
				Kerja Sama ini dengan segala akibat dan pelaksanaannya kedua belah pihak memilih domisili hukum di Kantor Pengadilan Negeri Tangerang di Tangerang, akan tetapi pemilihan domisili hukum tersebut tidak membatasi atau tidak boleh diartikan sebagai membatasi hak para pihak untuk mengajukan tuntutan-tuntutan hukum berkenaan dengan Kerja Sama ini di pengadilan lain di Indonesia. Domisili hukum tersebut berlaku pula terhadap (para) pengganti dan/ atau (para) penerima hak dari kedua belah pihak.
			</td>
		</tr>
		</table>
		
		<br><br><br><br>
		Demikianlah Agen menyatakan dan telah menyetujui perjanjian Kerja Sama sebagai Agen Pemasaran dengan Andalan Finance serta menyetujui Syarat dan Ketentuan Andalan Finance.

		
			';
		
		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		// reset pointer to the last page
		$pdf->lastPage();

		// ---------------------------------------------------------

		//Close and output PDF document
		ob_clean();
		$filename = 'Perjanjian_Kerjasama_Agen_'.$dataRequest['ticket_number'].'.pdf';
		//$pdf->Output($filename, 'I');
		$pdf->Output(__DIR__.'/user_files/inbox_files/'.$filename, 'F');
		return $filename;
	}	
	
	function getDayName() {
		
		$dayName = date('l');
		if($dayName == 'Monday') $dayName = 'Senin';
		else if($dayName == 'Tuesday') $dayName = 'Selasa';
		else if($dayName == 'Wednesday') $dayName = 'Rabu';
		else if($dayName == 'Thursday') $dayName = 'Kamis';
		else if($dayName == 'Friday') $dayName = 'Jumat';
		else if($dayName == 'Saturday') $dayName = 'Sabtu';
		else if($dayName == 'Sunday') $dayName = 'Minggu';
		
		return $dayName;
	}
?>