<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Car Prices</li>
			  <li>Tipe Mobil</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-car"></i> Tipe Mobil</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<select name="src_car_brand" class="form-control" id="src_car_brand">
							<option value="">Semua Merk Mobil</option>
							<?php 
								$query = "select * from car_brands order by name asc";
								$result= mysqli_query($mysql_connection, $query);
								while($data = mysqli_fetch_array($result)) {
									echo '<option value="'.$data['id_car_brand'].'">'.$data['name'].'</option>';
								}
							?>
						</select>
					</div>
					<div class="col-md-3">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Search</button>
						<a href="add_car_type">
							<button type="button" class="btn btn-primary  btn-flat " ><i class="fa fa-plus"></i> Tambah Tipe Mobil</button>
						</a>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
    </div><!-- ./wrapper -->

	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Gambar Tipe Mobil</h4>
			<p id="modal1Desc"><span id="addNotification"></span>
			
			<div class="col-md-12">
				<div class="form-group">
				<span id="spanImageTable"></span>
				</div>
			</div>
			
			</p>
		</div>
		<br>
		<div class="col-md-12">
		<button data-remodal-action="cancel" class="remodal-cancel">Tutup</button>
		</div>
	</div>
	
	
	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
    <script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'keyword'		: $('input[name=keyword]').val(),
								'src_car_brand'	: $('selecr[name=src_car_brand]').val(),
								'page'			: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_car_type.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );

		function resetdata() {
            $('#spanImageTable').html("");
        }
		
		function getImages(carTypeID) {

			$body.addClass("loadingClass");

            $.ajax({
                type: "POST",
                url: 'ajax_get_car_type_image.php',
                data: 'carTypeID=' + carTypeID
            }).done(function(data) { 			
				$('#spanImageTable').html(data);							
            	$body.removeClass("loadingClass");
            });
        }

        function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

        function deleteCarType(carTypeID, carTypeName){

        	if (confirm('Anda yakin akan menghapus tipe mobil ' + carTypeName + '?')) {

         		$body.addClass("loadingClass");

                $.ajax({
                    type: "POST",
                    url: 'ajax_delete_car_type.php',
                    data: 'carTypeID=' + carTypeID
                }).done(function(data) {

                	data = $.trim(data);
					$body.removeClass("loadingClass");

                	if(data == "success") {
                    	$("#mainNotification").html(showNotification("success", "Data tipe mobil telah dihapus"));
                    	searchData(1);
                    	setTimeout(clearnotif, 5000);
                   	} else if (data == 'in_use') {
						$('#mainNotification').html(showNotification("error", "Tipe mobil "+carTypeName+" tidak dapat dihapus"));
					}
                });
            }
        }

        $(document).on('closing', '.remodal', function(e) {
            resetdata();
        });
	</script>

  </body>
</html>
