<?php

ini_set("display_errors","0"); 
error_reporting(0); 
ini_set("max_execution_time",'0');

include "inc-db.php";
include "sanitize.inc.php";
include "jwt.php";
include "afis_call.php";

if ($env != 'development') {
	$captcha=$_POST['g-recaptcha-response'];
	$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcTPJoUAAAAABrzQhyvx7wdkpZEGlL76U9cQGq_&response=".$captcha);
	$g_response = json_decode($response);
	if($g_response->success!==true) {
		//invalid captcha
		$location = "location:index?e=04";
		header($location);
		exit;
	}
}

$username 	= sanitize_sql_string($_REQUEST["username"]);
$password 	= sanitize_sql_string($_REQUEST["password"]);

if ($username <> '' and $password <> '') {

    $password = md5($password);

    $query = "select a.*, b.name as user_level
			  from users a
			  left join user_levels b on a.level=b.id
        	  where username='$username' and
        	  password='$password' and status='ACTIVE'";
    $result = mysqli_query($mysql_connection, $query);

	if (mysqli_num_rows($result) > 0) {

		//login sebagai community_member
		$data = mysqli_fetch_array($result);

        session_start();
		$_SESSION['loginType'] 		= $data['level'];
		$_SESSION['loginTypeName'] 	= $data['user_level'];
        $_SESSION['fullname'] 		= $data['fullname'];
        $_SESSION['username'] 		= $data['username'];
        $_SESSION['password'] 		= $data['password'];
		$_SESSION['userID'] 		= $data['user_id'];

		//cek data cabang apakah ada perubahan
		$afis_api_url  = $afis_api_url.'/Andalanku/BranchList';
		$afis_response = json_decode(afis_call($afis_api_url, 'GET'));
		
		$responseArray = $afis_response->Response->Data;
		
		foreach($responseArray as $key=>$value) {
			
			$queryCheck	= "select id from branches where name='".$value->Name."'";
			$resultCheck= mysqli_query($mysql_connection, $queryCheck);
			
			if(mysqli_num_rows($resultCheck) == 0) {
				
				//simpan cabang baru
				$queryInsert = "insert into branches(id, name, address, phone, fax, zipcode, city, provinsi) 
								values(
								'".$value->BranchID."',
								'".$value->Name."',
								'".$value->Address."',
								'".$value->Phone."',
								'".$value->fax."',
								'".$value->Zipcode."',
								'".$value->City."',
								'".$value->Provinsi."'
								)";
				mysqli_query($mysql_connection, $queryInsert);
			}
			else {
				
				//simpan cabang baru
				$queryInsert = "update branches set  
								id='".$value->BranchID."',
								name='".$value->Name."',
								address='".$value->Address."',
								phone='".$value->Phone."',
								fax='".$value->fax."',
								zipcode='".$value->Zipcode."',
								city='".$value->City."',
								provinsi='".$value->Provinsi."'
								where name='".$value->Name."'";
				mysqli_query($mysql_connection, $queryInsert);
			}
		}
		
		//cek product list ayopop
		/*$payload = json_encode(['partnerId' => $ayopop_api_key]);
		$ayopop_token = createJWTToken($ayopop_api_secret, $payload);
		
		$ayopop_api_url = $ayopop_api_url.'partner/products';
		$ayopop_response = json_decode(ayopop_post_json($ayopop_api_url, $ayopop_api_key, $ayopop_token, $payload));
		
		if($ayopop_response->success) {
			
			$productList = $ayopop_response->data;
			
			foreach($productList as $product) {
				
				$code 		= $product->code;
				$name 		= $product->name;
				$logo 		= $product->logo;
				$amount 	= $product->amount;
				$biller 	= $product->biller;
				$category 	= $product->category;
				$active 	= $product->active;
				$type 		= $product->type;
				$description= $product->description;
				
				$queryCheck	= "select code from ppob_product where code='$code'";
				$resultCheck= mysqli_query($mysql_connection, $queryCheck);
				if(mysqli_num_rows($resultCheck) == 0) {
				
					//simpan produk
					$queryInsert = "insert into ppob_product(code, name, logo, 
									amount, biller, category, 
									active, type, description) 
									values('$code','$name','$logo',
									'$amount','$biller','$category',
									'$active','$type', '$description')";
					mysqli_query($mysql_connection, $queryInsert);
				}
				else {
					
					//simpan produk
					$queryInsert = "update ppob_product set  
									name='$name', logo='$logo', amount='$amount',
									biller='$biller', category='$category',  active='$active',
									type='$type' 
									where code='$code' ";
					mysqli_query($mysql_connection, $queryInsert);
				}
			}
		}
		*/
		
		if($data['user_level'] == 'SUPERADMIN') {
			header("location:login_otp");
			exit;
		}	

		header("location:dashboard");
        exit;

	}
	else {

		// login gagal
		header("location:index?e=02");
    	exit;
	}


} else {

    header("location:index?e=01");
    exit;
}
?>
