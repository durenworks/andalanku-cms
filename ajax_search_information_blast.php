<?php
	include "check-admin-session.php";

	$keyword	= sanitize_sql_string($_REQUEST["keyword"]);
	$page 		= sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(a.id) as num
					   from information_blasts a 
					   left join users b on a.user_id=b.user_id 
					   where title like '%$keyword%' or content like '%$keyword%' ";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select a.*, b.fullname  
			  from information_blasts a 
			  left join users b on a.user_id=b.user_id 
			  where title like '%$keyword%' or content like '%$keyword%' 
			  order by date ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='10%'>Tanggal</th>
				  <th width='20%'>Judul Informasi</th>
				  <th>Isi Informasi</th>
				  <th width='15%'>Diinput Oleh</th>
				  <th width='15%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {

		echo '<tr>
				  <td>'.$i.'</td>
				  <td>'.date("d-m-Y H:i:s", strtotime($data['date'])).'</td>
				  <td>' . @$data[title] . '</td>
				  <td>' . @$data[content] . '</td>
				  <td>' . @$data[fullname] . '</td>
				  <td align="center">
					<a href="#modal" onclick="getedit(' . @$data[id] . ')">Edit</a> | 
					<a href="#" onclick="deleteInfo(' . @$data[id] . ',\'' . @$data[title] . '\')" title="Hapus">Delete</a>
				  </td>
				  </tr>';
		
		$queryPublish	= "select a.*, b.fullname 
						   from information_blast_history a 
						   left join users b on a.user_id=b.user_id 
						   where info_id='$data[id]' order by published_date DESC LIMIT 1";
		$resultPublish	= mysqli_query($mysql_connection, $queryPublish);
		$dataPublish	= mysqli_fetch_array($resultPublish);
		
		if($dataPublish['published_date'] <> '') $published_date = date("d-m-Y H:i:s", strtotime($dataPublish['published_date']));
		else $published_date = '';
		
		echo '<tr>
				  <td>&nbsp;</td>
				  <td colspan="5">
					<button onclick="publishInfo(' . @$data[id] . ',\'' . @$data[title] . '\')" type="button" class="btn btn-sm btn-flat btn-success">Publikasikan</button>
					&nbsp;&nbsp;&nbsp;
					Dipublikasikan terakhir : '.$published_date.' &nbsp;&nbsp;&nbsp;
					Dipublikasikan oleh : '.$dataPublish[fullname].' 
				  </td>
				  </tr>';
		
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>