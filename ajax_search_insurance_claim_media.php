<?php
	include "check-admin-session.php";

	$insuranceClaimID = sanitize_int($_REQUEST["insuranceClaimID"]);
	
	$queryMedia  = "select b.url  
					from insurance_claim_media a 
					left join media b on a.id_media=b.id_media 
					where id_claim='$insuranceClaimID'";
	$resultMedia = mysqli_query($mysql_connection, $queryMedia);

	echo "<table class='table table-hover'>";
	
	$i = 1;

	while ($dataMedia = mysqli_fetch_array($resultMedia)) {

		if($i == 1) echo '<tr>';
	
		echo '<td align="center">
				<a href="user_files/media_image/'.$dataMedia['url'].'" target="_blank">
					<img src="user_files/media_image/'.$dataMedia['url'].'" style="width:150px;">
				</a>
			  </td>';
		$i++;
		
		if($i == 3) {
			echo '</tr>';
			$i = 1;
		}
	}

	echo "</table>";
?>
