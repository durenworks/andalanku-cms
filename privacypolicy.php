<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname 		= $_SESSION['fullname'];
	$loginTypeName	= $_SESSION['loginTypeName'];	
	$legal_type 	= "Privacy Policy";
	$legal_caption	= "Privacy Policy";
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Display</li>
			  <li><?php echo $legal_caption; ?></li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-file-text-o"></i> <?php echo $legal_caption; ?></h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<div class="row">
					<div class="col-md-3">
						<input type="hidden" name="legal_type" id="legal_type" value="<?php echo $legal_type; ?>" />
						<input type="hidden" name="legal_caption" id="legal_caption" value="<?php echo $legal_caption; ?>" />
						<input type="text" name="keyword" class="form-control" placeholder="Keyword" />
					</div>
					<div class="col-md-6">
						<button type="button" class="btn btn-primary  btn-flat " onClick="searchData(1);"><i class="fa fa-search"></i> Cari</button>
						<a href="add_privacy_policy">
							<button type="button" class="btn btn-primary  btn-flat " ><i class="fa fa-plus"></i> Tambah <?php echo $legal_caption; ?></button>
						</a>
					</div>
					<div class="remodal-bg"> </div>
				</div>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
                </div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->

    <?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

    <script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

            var formData = {
								'legal_type'	: $('input[name=legal_type]').val(),
								'legal_caption'	: $('input[name=legal_caption]').val(),
								'keyword'		: $('input[name=keyword]').val(),
								'page'			: searchPage
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_legal_document.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }
		
		searchData(1);
		
		<?php if($loginTypeName == 'SUPERADMIN') { ?>
		
			function processData(idLegalDocument, status) {
				
				var legal_caption = $('input[name=legal_caption]').val();
				
				if(confirm(status + ' ' + legal_caption + ' ?')) {
					
					$body.addClass("loadingClass");

					var form_data = new FormData();
					form_data.append('id_legal_document', idLegalDocument);
					form_data.append('legal_type', $('input[name=legal_type]').val());
					form_data.append('status', status);
					
					$.ajax({
						type: "POST",
						url: 'ajax_update_status_legal_document.php',
						processData: false,
						contentType: false,
						data: form_data
					}).done(function(data) {

						data = $.trim(data); 
						$body.removeClass("loadingClass");

						if (data == 'success') {
							$("#mainNotification").html(showNotification("success", "Data Sudah Disimpan"));
							setTimeout(clearnotif, 5000);
							searchData(1);

						} else if (data == 'empty') {
							$('#mainNotification').html(showNotification("error", "Input Tidak Lengkap"));
						} else if (data == 'insufficient_access') {
							$('#mainNotification').html(showNotification("error", "Anda tidak berhak melakukan proses ini"));
						} 						
					});
				}
			}
		
		<?php } ?>
		
		function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }
	</script>

  </body>
</html>
