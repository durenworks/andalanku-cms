<?php
	include "check-admin-session.php";
	
	$page = sanitize_int($_REQUEST["page"]);
	
	$query 			= "select COUNT(id_slider) as num from sliders";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data['num']; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select * from sliders 
			  order by id_slider ASC LIMIT $start,$limit"; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='10%'>Gambar Apps</th>
				  <th width='10%'>Gambar Web</th> 
				  <th width='15%'>Promo</th>
				  <th width='15%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {
		
		$title = '';
		
		if($data['id_promo']<>'0') {
			$queryPromo  = 'select title from promos where id_promo="'.$data['id_promo'].'"'; 
			$resultPromo = mysqli_query($mysql_connection, $queryPromo);
			$dataPromo   = mysqli_fetch_array($resultPromo);
			$title		 = $dataPromo['title'];
		}

		echo '<tr>
				  <td>'.$i.'</th>
				  <td>
					<a href="user_files/slider_image/' . $data['image'] . '" target="_blank">
					<img src="user_files/slider_image/' . $data['image'] . '" width="200px" alt=" ">
					</a>
				  </td>
				  <td>
					<a href="user_files/slider_image/' . $data['image_web'] . '" target="_blank">
					<img src="user_files/slider_image/' . $data['image_web'] . '" width="200px" alt=" ">
					</a>
				  </td>
				  <td>' . $title . '</td>
				  <td align="center">
					<a href="#modal" onclick="getedit(' . $data['id_slider'] . ')">Edit</a> | 
					<a href="#" onclick="deleteImageSlider(' . $data['id_slider'] . ',\'' . $data['image'] . '\',\'' . $data['image_web'] . '\')" title="Hapus">Delete</a>
				  </td>
				  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>