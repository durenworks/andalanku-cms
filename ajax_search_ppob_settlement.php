<?php
	include "check-admin-session.php";

	$searchDate		= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$page 			= sanitize_int($_REQUEST["page"]);

	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";

	} else {
		$startDate	= date("Y-m-d")." 00:00:00";
		$endDate	= date("Y-m-d")." 23:59:59";		
	}

	$sql_date	= "and es.rq_datetime between '$startDate' and '$endDate'";
	
	if($keyword!='') {
			$sql_key = "and (c.customer_name like '%$keyword%' or 
						pt.order_id like '%$keyword%' or ep.payment_ref like '%$keyword%')";
	}

	$query 			= "select DISTINCT COUNT(*) as num
						from espay_settlement es
						left join espay_settlement_data esd on es.rq_uuid = esd.rq_uuid
						left join espay_payment ep on esd.tx_id = ep.payment_ref
						left join ppob_transaction pt on ep.payment_ref = pt.espay_payment_ref
						left join customers c on pt.id_customer = c.id_customer
						where esd.payment_id like 'AYO%' and
						$sql_key $sql_date $sql_status";
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num]; 

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;		

	$query = "select DISTINCT
			DATE_FORMAT(es.rq_datetime,'%d-%m-%Y %H:%i' ) as tanggal,
			c.customer_name as nama, 
			c.phone_number as hppel,
			pt.order_id,
			esd.settlement_amount as amount,
			ep.payment_ref as espay_payment_ref
			from espay_settlement es
			left join espay_settlement_data esd on es.rq_uuid = esd.rq_uuid
			left join espay_payment ep on esd.tx_id = ep.payment_ref
			left join ppob_transaction pt on ep.payment_ref = pt.espay_payment_reff
			left join customers c on pt.customer_id = c.id_customer 
			where esd.payment_id like 'AYO%'
			$sql_key $sql_date $sql_status LIMIT $start,$limit"; 

	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='8%'>Tanggal</th>
				  <th width='8%'>No. Transaksi</th>
				  <th width='8%'>Jml Bayar</th>	
				  <th width='10%'>Espay Ref</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {
		echo '<tr>
				  <td>'.$i.'.</th>
				  <td>' . $data[tanggal] . '</td>
				  <td>' . $data[order_id] . '</td>
				  <td>' . number_format($data[amount],0) . '</td> 
				  <td>' .$data[espay_payment_ref] . '</td>				  
			  </tr>';
		$i++;
	}

	echo "</table>";
	
	include "inc-paging.php";
?>