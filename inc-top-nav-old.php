      <?php
      	$fullname = $_SESSION['fullname'];
		$loginType= $_SESSION['loginType'];
      ?>

      <header class="main-header">
        <nav class="navbar navbar-static-top bg-blue-2">
          <div class="container">
            <div class="navbar-header">
              <a href="dashboard" class="navbar-brand">
              	<div class="text-white" style="display: inline-block;">
                  <img src="img/logo_sm.png" alt="Logo">
              	</div>
              </a>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
              </button>
            </div>

			<?php 
				if($loginType == 'SUPERADMIN') { 
			?>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
					  <ul class="nav navbar-nav">
						
						<li class="dropdown">
						  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-database"></i> Master Data<span class="caret"></span></a>
						  <ul class="dropdown-menu" role="menu">
							<li><a href="user_level"><i class="fa fa-tasks"></i> User Level</a></li>
							<li><a href="level_permission"><i class="fa fa-unlock-alt"></i> Level Permission</a></li>
							<li class="divider"></li>
							<li><a href="administrator"><i class="fa fa-user-secret"></i> Administrator</a></li>
							<li><a href="subscriber"><i class="fa fa-users"></i> Subscriber</a></li>
							<li class="divider"></li>
							<li><a href="setting"><i class="fa fa-gears"></i> Settings</a></li>
						  </ul>
						</li>
						
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-tv"></i> Display<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="news"><i class="fa fa-newspaper-o"></i> Berita</a></li>
								<li><a href="promo"><i class="fa fa-tags"></i> Promo</a></li>
								<li><a href="partner"><i class="fa fa-handshake-o"></i> Partner</a></li>
								<li><a href="faq"><i class="fa fa-question-circle-o"></i> FAQ</a></li>
								<li class="divider"></li>
								<li><a href="tos"><i class="fa fa-file-text-o"></i> Term of Service</a></li>
								<li><a href="privacypolicy"><i class="fa fa-file-text-o"></i> Privacy Policy</a></li>
							</ul>
						</li>
						
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bar-chart"></i> Rates<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="insurance_rate"><i class="fa fa-shield"></i> Insurance Rate</a></li>
								<li><a href="car_rate"><i class="fa fa-car"></i> Car Rate</a></li>
								<li><a href="rate"><i class="fa fa-bar-chart"></i> Rate</a></li>
							</ul>
						</li>
						
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-car"></i> Car Prices<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="car_brand"><i class="fa fa-car"></i> Merk Mobil</a></li>
								<li><a href="car_type"><i class="fa fa-car"></i> Tipe Mobil</a></li>
								<li><a href="car_trim"><i class="fa fa-car"></i> Model Mobil</a></li>
								<li><a href="car_exim"><i class="fa fa-file-excel-o"></i> Export / Import</a></li>
							</ul>
						</li>
						
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-id-badge"></i> Konsumen<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="customer"><i class="fa fa-id-badge"></i> &nbsp;&nbsp;Data Konsumen</a></li>
								<li><a href="profile_update"><i class="fa fa-vcard-o"></i> Pembaharuan Data</a></li>
								<li class="divider"></li>
								<li><a href="loan_application"><i class="fa fa-shopping-bag"></i> Pengajuan Kredit</a></li>
								<li><a href="insurance_claim"><i class="fa fa-shield"></i> &nbsp;Klaim Asuransi</a></li>
								<li><a href="collateral_claim"><i class="fa fa-check-square-o"></i> Pengambilan Jaminan</a></li>
								<li class="divider"></li>
								<li><a href="contract_document"><i class="fa fa-file-text-o"></i> Dokumen Kontrak</a></li>
								<li><a href="private_document"><i class="fa fa-file-text-o"></i> Dokumen Pribadi</a></li>
								<li><a href="payment_history"><i class="fa fa-file-text-o"></i> History Pembayaran</a></li>
								<li class="divider"></li>
								<li><a href="complain"><i class="fa fa-exclamation-triangle"></i> Komplain</a></li>
								<li class="divider"></li>
								<li><a href="information_blast"><i class="fa fa-bullhorn"></i> Broadcast Informasi</a></li>
							</ul>
						</li>
						
						
												
					  </ul>
					</div><!-- /.navbar-collapse -->
			<?php 
				} else if($loginType == 'ADMINISTRATOR') { 
			?>		
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
					  <ul class="nav navbar-nav">
						
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-tv"></i> Display<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="news"><i class="fa fa-newspaper-o"></i> Berita</a></li>
								<li><a href="promo"><i class="fa fa-tags"></i> Promo</a></li>
								<li><a href="faq"><i class="fa fa-question-circle-o"></i> FAQ</a></li>
							</ul>
						</li>
							
						<li><a href="subscriber"><i class="fa fa-users"></i> Subscriber</a></li>
					  </ul>
					</div><!-- /.navbar-collapse -->
			<?php 	  
				} 
			?>
					
			
            <!-- Navbar Right Menu -->
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <!-- User Account Menu -->
                  <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <!-- The user image in the navbar-->
                      	<img src='img/user2-160x160.jpg' class='img-circle' alt='User Image' style='width:16px;'/>
                      <!-- hidden-xs hides the username on small devices so only the image appears. -->
                      <span class="hidden-xs"><?php echo $fullname; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- The user image in the menu -->
                      <li class="user-header">
                        <img src='img/user2-160x160.jpg' class='img-circle' alt='User Image'/>
                        <p>
                          <?php echo $fullname; ?>
                        </p>
                      </li>

                      <!-- Menu Footer-->
                      <li class="user-footer">
						<div class="pull-left">
                          <a href="profile" class="btn btn-default btn-flat"><i class="fa fa-lock"></i> Edit Profile</a>
                        </div>
                        <div class="pull-right">
                          <a href="logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Logout</a>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div><!-- /.navbar-custom-menu -->
          </div><!-- /.container-fluid -->
        </nav>
      </header>
