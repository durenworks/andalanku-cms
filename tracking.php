<?php 
	include "check-admin-session.php";
	
	$id_customer = sanitize_sql_string($_REQUEST["id"]);
	
	$query 	= "select last_latitude,last_longitude 
			   from customers where id_customer='$id_customer' ";
	$result = mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_assoc($result);
	$lat	= $data['last_latitude'];  
	$long	= $data['last_longitude']; 
	
	if($lat=='' || $long=='' || $lat=='0.00000000' || $lang=='0.00000000') {
		
		echo "<script>
				alert('Gagal mendapatkan lokasi user');
				window.close();
			  </script>";
		exit;
	}
?>
<!DOCTYPE html>
<html>
  <head>
    <style>
      #map {
        height: 500px;  
        width: 100%;  
       }
    </style>
  </head>
  <body>
    <h3>GPS Tracking Konsumen Andalanku</h3>
    <!--The div element for the map -->
    <div id="map"></div>
    <script>
		// Initialize and add the map
		function initMap() {
			// The location of Uluru
			var uluru = {lat: <?php echo $lat; ?> , lng: <?php echo $long; ?>};
			// The map, centered at Uluru
			var map = new google.maps.Map(
			document.getElementById('map'), {zoom: 15, center: uluru});
			// The marker, positioned at Uluru
			var marker = new google.maps.Marker({position: uluru, map: map});
		}
    </script>
    <script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzQUpYqPaHgkZRwQZsqstIUrOo40X0uzo&callback=initMap">
    </script>
  </body>
</html>