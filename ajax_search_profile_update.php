<?php
	include "check-admin-session.php";

	$status 		= sanitize_sql_string($_REQUEST["src_status"]);
	$searchDate		= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$page 			= sanitize_int($_REQUEST["page"]);
	
	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";
	} else {
		$startDate	= date("Y-m")."-01 00:00:00";
		$endDate	= date("Y-m")."-31 23:59:59";
	}

	$query 	= "select COUNT(*) as num
				from profile_update_request a 
				left join customers b on a.user_id=b.id_customer 
				left join agreement_list c on c.customer_id = b.id_customer
				where 
				(
					a.id in (
						select profile_update_request_id from profile_update_request_address 
						where address_type='LEGAL' or address_type='OFFICE' 
					)
					or 
					(a.mobile_phone!='' or a.mobile_phone is not null 
					or a.area_code!='' or a.area_code is not null 
					or a.phone!='' or a.phone is not null) 
				)
				and 
				(customer_name like '%$keyword%' or ticket_number like '%$keyword%' 
				or c.agreement_number like '%$keyword%' or b.customer_name like '%$keyword%') 
				and request_date>='$startDate' and request_date<='$endDate' ";
	if($status <> '') $query = $query." and a.status='$status' ";
					  

	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_array($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;

	$query 	= "select a.*, b.customer_name 
			   from profile_update_request a 
				left join customers b on a.user_id=b.id_customer 
				left join agreement_list c on c.customer_id = b.id_customer
				where 
				(
					a.id in (
						select profile_update_request_id from profile_update_request_address 
						where address_type='LEGAL' or address_type='OFFICE' 
					)
					or 
					(a.mobile_phone!='' or a.mobile_phone is not null 
					or a.area_code!='' or a.area_code is not null 
					or a.phone!='' or a.phone is not null) 
				)
				and 
				(customer_name like '%$keyword%' or ticket_number like '%$keyword%' 
				or c.agreement_number like '%$keyword%' or b.customer_name like '%$keyword%') 
				and request_date>='$startDate' and request_date<='$endDate' ";
	if($status <> '') $query = $query." and a.status='$status' ";
	$query = $query." order by request_date ASC LIMIT $start,$limit"; 
	
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
					<th width='5%'>No</th>
					<th width='20%'>Nomor Tiket</th>
					<th width='20%'>Tanggal Permintaan</th>
					<th width='20%'>Nama Konsumen</th>
					<th width='20%'>Status</th>
					<th width='15%'>&nbsp;</th>
				</tr>	";

	$i = ($page*$limit) - ($limit-1);

	while ($data = mysqli_fetch_array($result)) {
		
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data[ticket_number] . '</td>
				  <td>' . date("d-m-Y H:i:s", strtotime($data[request_date])) . '</td>
				  <td>' . $data[customer_name] . '</td>
				  <td>' . $data[status] . '</td>
				  <td>
					<a href="#modal" onclick="viewDetail('.$data[id].')"><i class="fa fa-search-plus"></i> View Detail</a>
				  </td>
				</tr>';
		$i++;
	}

	echo "</table>";

	include "inc-paging.php";
?>
