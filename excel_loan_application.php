<?php
	include "check-admin-session.php";
	
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Data_Pengajuan_Kredit_Andalanku.xls");
	
	echo "<b>Data Pengajuan Kredit Andalanku</b><br><br>";

	$product 		= sanitize_sql_string($_REQUEST["src_product"]);
	$status 		= sanitize_sql_string($_REQUEST["src_status"]);
	$searchDate		= sanitize_sql_string($_REQUEST["searchDate"]);
	$keyword 		= sanitize_sql_string($_REQUEST["keyword"]);
	$src_pengajuan	= sanitize_sql_string($_REQUEST["src_pengajuan"]);	
	
	if($searchDate<>'') {
		$tempArray	= explode(" - ",$searchDate);
		$startDate	= $tempArray[0];
		$tempArrays = explode("/",$startDate);
		$startDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 00:00:00";
		
		$endDate	= $tempArray[1];
		$tempArrays = explode("/",$endDate);
		$endDate	= $tempArrays[2]."-".$tempArrays[0]."-".$tempArrays[1]." 23:59:59";
	} else {
		$startDate	= date("Y-m")."-01 00:00:00";
		$endDate	= date("Y-m-t")." 23:59:59";
	}
	
	echo '<table>
			<tr>
				<td>Tanggal</td><td>: '.date('d-m-Y', strtotime($startDate)).' s/d '.date('d-m-Y', strtotime($endDate)).'</td><td>&nbsp;</td>
				<td>Produk</td><td>: '.$product.'</td><td>&nbsp;</td>
				<td>Status</td><td>: '.$status.'</td><td>&nbsp;</td>
			</tr>
			<tr>
				<td>Pengajuan Oleh</td><td>: '.$src_pengajuan.'</td><td>&nbsp;</td>
				<td>Keyword</td><td>: '.$keyword.'</td><td>&nbsp;</td>
			</tr>
		  </table><br><br>';
			
	$query 	= "select a.*, b.customer_name, c.name as occupation_name, 
				
				d.name as village_name, 
				e.name as district_name, 
				f.name as regency_name, 
				g.name as province_name, 
				
				h.name as village_name_domicile, 
				i.name as district_name_domicile, 
				j.name as regency_name_domicile, 
				k.name as province_name_domicile,

				l.name as village_name_office, 
				m.name as district_name_office, 
				n.name as regency_name_office, 
				o.name as province_name_office 			
				
				from loan_applications a
				left join customers b on a.customer_id=b.id_customer 
				left join occupations c on a.loan_occupation_id=c.id 
				
				left join villages  d on a.loan_village_id	= d.id
				left join districts e on a.loan_district_id	= e.id
				left join regencies f on a.loan_city_id		= f.id
				left join provinces g on a.loan_province_id	= g.id

				left join villages  h on a.loan_village_id_domicile 	= h.id
				left join districts i on a.loan_district_id_domicile 	= i.id
				left join regencies j on a.loan_city_id_domicile 		= j.id
				left join provinces k on a.loan_province_id_domicile 	= k.id
				
				left join villages  l on a.loan_village_id_office	= l.id
				left join districts m on a.loan_district_id_office	= m.id
				left join regencies n on a.loan_city_id_office		= n.id
				left join provinces o on a.loan_province_id_office	= o.id
				
				where (ticket_number like '%$keyword%' or loan_customer_name like '%$keyword%' or loan_phone_number like '%$keyword%') 
				and date>='$startDate' and date<='$endDate' and a.is_active='1' ";
				
	if($status <> '') $query = $query." and a.status='$status' ";
	if($product <> '') $query = $query." and product='$product' ";	

	if($src_pengajuan == 'Agent') $query = $query." and loan_application_referal='Y' ";	
	else if($src_pengajuan == 'Pribadi') $query = $query." and loan_application_referal='N' ";	
	
	$query = $query." order by date DESC"; 
	
	$result = mysqli_query($mysql_connection, $query); 

	echo "<table>
			  <tr>
					<th>No</th>
					<th>Nomor Tiket</th>
					<th>Tanggal</th>
					<th>Konsumen</th>
					<th>Alamat Legal</th>
					<th>Alamat Domisili</th>
					<th>Alamat Kantor</th>
					<th>Jenis Kelamin</th>
					<th>Tempat Tanggal Lahir</th>
					<th>Nama Ibu Kandung</th>
					<th>Pekerjaan</th>
					<th>Pendapatan Per Bulan</th>
					<th>Pendapatan Tambahan</th>
					<th>Pekerjaan Sampingan</th>
					<th>Nama Referal</th>
					<th>Produk</th>
					<th>OTR</th>
					<th>Jumlah DP</th>
					<th>Plafond</th>
					<th>Tenor</th>
					<th>Asuransi</th>
					<th>Status</th>
					<th>Jaminan</th>
					<th>Tanggal Survey</th>
				</tr>	";

	$i = 1;

	while ($data = mysqli_fetch_array($result)) {
		
		if($data['loan_application_referal'] == 'Y') {
			$referal_name = $data['customer_name'];
		} else {
			$referal_name = '-';
		}
		
		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data[ticket_number] . '</td>
				  <td>' . str_replace(' ','<br>',date("d-m-Y H:i:s", strtotime($data['date']))) . '</td>
				  <td>' . $data[loan_customer_name] . '<br>Telp : ' . $data[loan_phone_number] . '
						<br>Email : ' . $data[loan_email] . '<br>No. Identitas : ' . $data[loan_id_number] . '
						<br>No. Telp Rumah : ' . $data[loan_home_phone_number] . '
				  </td>
				  <td>' . $data[loan_address] . '
						<br>RT ' . $data[loan_rt] . ' RW ' . $data[loan_rw] . '
						<br>' . $data[village_name] . ', ' . $data[district_name] . ' 
						<br>' . $data[city_name] . ', ' . $data[province_name] . ' ' . $data[loan_zip_code] . ' 
				  </td>
				  <td>' . $data[loan_address_domicile] . '
						<br>RT ' . $data[loan_rt_domicile] . ' RW ' . $data[loan_rw_domicile] . '
						<br>' . $data[village_name_domicile] . ', ' . $data[district_name_domicile] . ' 
						<br>' . $data[city_name_domicile] . ', ' . $data[province_name_domicile] . ' ' . $data[loan_zip_code_domicile] . ' 
				  </td>
				  <td>' . $data[loan_address_office] . '
						<br>RT ' . $data[loan_rt_office] . ' RW ' . $data[loan_rw_office] . '
						<br>' . $data[village_name_office] . ', ' . $data[district_name_office] . ' 
						<br>' . $data[city_name_office] . ', ' . $data[province_name_office] . ' ' . $data[loan_zip_code_office] . ' 
				  </td>
				  <td>' . $data[loan_gender] . '</td>
				  <td>' . $data[loan_place_of_birth] . ', ' . date("d-m-Y", strtotime($data['loan_date_of_birth'])) . '</td>
				  <td>' . $data[loan_mother_name] . '</td>
				  <td>' . $data[occupation_name] . '</td>
				  <td>Rp ' . number_format($data[loan_monthly_income],2,',','.') . '</td>
				  <td>Rp ' . number_format($data[loan_additional_income],2,',','.') . '</td>
				  <td>' . $data[loan_side_job] . '</td>
				  <td>' . $referal_name . '</td>
				  <td>' . $data[product] . '</td>
				  <td>Rp ' . number_format($data[otr],2,',','.') . '</td>
				  <td>' . number_format($data[dp_percent],0,',','.') . '% <br>
					  Rp ' . number_format($data[dp_amount],2,',','.') . '</td>
				  <td>Rp ' . number_format($data[plafond],2,',','.') . '</td>
				  <td>' . number_format($data[tenor],0,',','.') . ' bulan</td>
				  <td>' . $data[insurance] . '</td>
				  <td>' . $data[status] . '</td>
				  <td>' . $data[collateral_type] . '<br>' . $data[collateral] . '<br>' . $data[collateral_owner] . '<br>' . $data[collateral_paper_number] . '</td>
				  <td>' . str_replace(' ','<br>',date("d-m-Y H:i:s", strtotime($data['survey_date']))) . '</td>
				</tr>';
		$i++;
	}

	echo "</table>";
?>