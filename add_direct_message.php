<?php
	//include "check-admin-session.php";
	include "inc-db.php";
	include "sanitize.inc.php";
	include "inc-header-admin.php";

	$destID = sanitize_int($_REQUEST["id"]);
?>

<link href="plugins/summernote/summernote.css" rel="stylesheet">

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Direct Message</li>
              <li>Kirim Pesan</a></li>
            </ol>
          </section>
		  <br>
          <!-- Main content -->
          <section class="content">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Kirim Pesan</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>


				<div class="row form-group">
					<div class="col-md-12">
						<label>Subject</label>
						<input type="hidden" name="dest_id" id="dest_id" value="<?php echo $destID; ?>" />
						<input type="text" name="subject" id="subject" class="form-control" placeholder="Subject"/>
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-12">
						<label>Pesan</label>
						<textarea name="message" id="message" rows="8" class="form-control" placeholder="Pesan"></textarea>
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-12">
						<label>Lampiran</label>
						<table width="100%" class='table'>
							<tr></tr>
								<td>
									<input type='file' name="fileInput1" id="fileInput1" accept='application/pdf'/>
								</td>
							</tr>
						</table>
				   </div>
				</div>

				<div class="row form-group">
					<div class="col-md-12 text-center">
						<button type="button" style="width:100px" class="btn btn-primary btn-large" onclick="save();">Kirim</button>
						<button type="button" style="width:100px" class="btn btn-warning btn-large" onclick="cancel();">Batal</button>
					</div>
				</div>

				<br><br><br>

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>
	
	<script src="plugins/summernote/summernote.js"></script>

    <script src="js/inc-function.js"></script>

    <script type="text/javascript">

    	$body = $("body");
		
		$(document).ready(function() {
		  $('#message').summernote({
			toolbar: [
				// [groupName, [list of button]]
				['style', ['bold', 'italic', 'underline', 'clear']],
				['font', ['strikethrough', 'superscript', 'subscript']],
				['fontsize', ['fontsize']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['height', ['height']]
			],
			height: 300, 
			placeholder: 'Pesan'
		  });
		});

    	function save() {

    		$body.addClass("loadingClass");

    		var fileInput1 = $('#fileInput1').prop('files')[0];

    		var form_data = new FormData();
			form_data.append('dest_id', $('input[name=dest_id]').val());
			form_data.append('subject', $('input[name=subject]').val());
			form_data.append('message', $('textarea[name=message]').val());
			form_data.append('fileInput1', fileInput1);
			form_data.append('_ref', '<?php echo md5('news#'.date('d-m-Y')); ?>');

			$.ajax({
				type: "POST",
				url: 'ajax_add_dm.php',
				processData: false,
				contentType: false,
				data: form_data
			}).done(function(data) {

				data = $.trim(data); console.log(data);
				$body.removeClass("loadingClass");

				if (data == 'success') {
					//$("#mainNotification").html(showNotification("success", "Data Sudah Disimpan"));
					//setTimeout(clearnotif, 5000);
					window.location = "direct_message";

				} else if (data == 'empty') {
					$('#mainNotification').html(showNotification("error", "Input Tidak Lengkap"));
				} else if (data == 'incorrect_file_type') {
					$('#mainNotification').html(showNotification("error", "File attachmemnt harus berupa PDF"));
				}

				$('html, body').animate({scrollTop: '0px'}, 0);
			});
    	}

    	function cancel() {
    		window.location = 'news';
    	}

    	function clearnotif() {
			$("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
		}

    	function readURL(input, view) {

		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $(view).attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imageInput1").change(function(){
		    readURL(this, "#imageView1");
		});


    </script>

  </body>
</html>
