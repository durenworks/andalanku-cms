<?php
	include "check-admin-session.php";
	include "afis_call.php";

	$customer_id		= sanitize_int($_REQUEST["customer_id"]);
	$customer_name 		= sanitize_sql_string($_REQUEST["customer_name"]);
	$customer_type 		= sanitize_sql_string($_REQUEST["customer_type"]);
	$id_number 			= sanitize_sql_string($_REQUEST["id_number"]);
	$nip 				= sanitize_sql_string($_REQUEST["nip"]);
	$npwp 				= sanitize_sql_string($_REQUEST["npwp"]);
	$email 				= sanitize_sql_string($_REQUEST["email"]);
	$phone_number 		= sanitize_sql_string($_REQUEST["phone_number"]);
	$home_phone_number 	= sanitize_sql_string($_REQUEST["home_phone_number"]);
	$referral_code 		= sanitize_sql_string($_REQUEST["referral_code"]);
	$place_of_birth 	= sanitize_sql_string($_REQUEST["place_of_birth"]);
	$date_of_birth 		= sanitize_sql_string($_REQUEST["date_of_birth"]);
	$gender 			= sanitize_sql_string($_REQUEST["gender"]);
	$mother_name 		= sanitize_sql_string($_REQUEST["mother_name"]);
	$occupation 		= sanitize_sql_string($_REQUEST["occupation"]);
	$monthly_income 	= sanitize_sql_string($_REQUEST["monthly_income"]);
	$additional_income 	= sanitize_sql_string($_REQUEST["additional_income"]);
	$side_job 			= sanitize_sql_string($_REQUEST["side_job"]);	
	
	$address 		= sanitize_sql_string($_REQUEST["address"]);
	$rt				= sanitize_sql_string($_REQUEST["rt"]);
	$rw				= sanitize_sql_string($_REQUEST['rw']);
	$zip_code		= sanitize_sql_string($_REQUEST['zip_code']);
	$village_id		= sanitize_sql_string($_REQUEST["village_id"]);
	$district_id	= sanitize_sql_string($_REQUEST["district_id"]);
	$regency_id		= sanitize_sql_string($_REQUEST["regency_id"]);
	$province_id	= sanitize_sql_string($_REQUEST["province_id"]);
	
	$address_domicile 		= sanitize_sql_string($_REQUEST["address_domicile"]);
	$rt_domicile			= sanitize_sql_string($_REQUEST["rt_domicile"]);
	$rw_domicile			= sanitize_sql_string($_REQUEST['rw_domicile']);
	$zip_code_domicile		= sanitize_sql_string($_REQUEST['zip_code_domicile']);
	$village_id_domicile	= sanitize_sql_string($_REQUEST["village_id_domicile"]);
	$district_id_domicile	= sanitize_sql_string($_REQUEST["district_id_domicile"]);
	$regency_id_domicile	= sanitize_sql_string($_REQUEST["regency_id_domicile"]);
	$province_id_domicile	= sanitize_sql_string($_REQUEST["province_id_domicile"]);
	
	$address_office 		= sanitize_sql_string($_REQUEST["address_office"]);
	$rt_office				= sanitize_sql_string($_REQUEST["rt_office"]);
	$rw_office				= sanitize_sql_string($_REQUEST['rw_office']);
	$zip_code_office		= sanitize_sql_string($_REQUEST['zip_code_office']);
	$village_id_office		= sanitize_sql_string($_REQUEST["village_id_office"]);
	$district_id_office		= sanitize_sql_string($_REQUEST["district_id_office"]);
	$regency_id_office		= sanitize_sql_string($_REQUEST["regency_id_office"]);
	$province_id_office		= sanitize_sql_string($_REQUEST["province_id_office"]);
	
	$monthly_income		= str_replace('.','',$monthly_income);
	$additional_income 	= str_replace('.','',$additional_income);
	$date_of_birth		= date('Y-m-d', strtotime($date_of_birth));

	if ($customer_id <> 0) {
		
		$query = "	UPDATE customers set 
						customer_name 		= '$customer_name', 
						customer_type 		= '$customer_type', 
						id_number 			= '$id_number', 
						nip 				= '$nip', 
						npwp 				= '$npwp', 
						email 				= '$email', 
						phone_number 		= '$phone_number', 
						home_phone_number 	= '$home_phone_number', 
						referral_code 		= '$referral_code', 
						place_of_birth 		= '$place_of_birth', 
						date_of_birth 		= '$date_of_birth', 
						gender 				= '$gender', 
						mother_name 		= '$mother_name', 
						occupation 			= '$occupation', 
						monthly_income 		= '$monthly_income', 
						additional_income 	= '$additional_income', 
						side_job 			= '$side_job' 
					where id_customer='$customer_id' ";
		mysqli_query($mysql_connection, $query);
		
		// ====== ALAMAT LEGAL =====	
		if($address <> '') {
			$query = "	UPDATE address_customers set is_active='0' 
						where address_type='LEGAL' and user_id='$customer_id' ";
			mysqli_query($mysql_connection, $query);
			
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id, 
							district_id, village_id, zip_code, 
							address, rt, rw) 
							values('$province_id', '$regency_id', 
							'$district_id', '$village_id', '$zip_code', 
							'$address', '$rt', '$rw')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//ambil id yang terakhir
			$query = "select id from address where address='$address' order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];		
			
			$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
						VALUES('LEGAL', '$customer_id', '$id_address', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		
		
		// ====== ALAMAT DOMISILI =====		
		if($address_domicile <> '') {
			$query = "	UPDATE address_customers set is_active='0' 
						where address_type='DOMICILE' and user_id='$customer_id' ";
			mysqli_query($mysql_connection, $query);
			
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id, 
							district_id, village_id, zip_code, 
							address, rt, rw) 
							values('$province_id_domicile', '$regency_id_domicile', 
							'$district_id_domicile', '$village_id_domicile', '$zip_code_domicile', 
							'$address_domicile', '$rt_domicile', '$rw_domicile')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//ambil id yang terakhir
			$query = "select id from address where address='$address_domicile' order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];		
			
			$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
						VALUES('DOMICILE', '$customer_id', '$id_address', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		
		
		// ====== ALAMAT KANTOR =====	
		if($address <> '') {
			$query = "	UPDATE address_customers set is_active='0' 
						where address_type='OFFICE' and user_id='$customer_id' ";
			mysqli_query($mysql_connection, $query);
			
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id, 
							district_id, village_id, zip_code, 
							address, rt, rw) 
							values('$province_id_office', '$regency_id_office', 
							'$district_id_office', '$village_id_office', '$zip_code_office', 
							'$address_office', '$rt_office', '$rw_office')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//ambil id yang terakhir
			$query = "select id from address where address='$address_office' order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];		
			
			$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
						VALUES('OFFICE', '$customer_id', '$id_address', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		
        echo 'success';
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>