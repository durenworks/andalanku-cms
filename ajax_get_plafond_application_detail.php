<?php
	include "check-admin-session.php";

	$plafondApplicationID = sanitize_int($_REQUEST["plafondApplicationID"]);
	
	$query 	= "select a.*, b.customer_name, b.phone_number, c.name as occupation_name, d.name as branch_name   
			   from plafond_applications a 
			   left join customers b on a.customer_id=b.id_customer 
			   left join occupations c on b.occupation=c.id 
			   left join branches d on a.andalan_branch_id=d.id
			   where a.id='$plafondApplicationID'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
	
	$data['application_date'] 	= date("d-m-Y H:i:s", strtotime($data['application_date']));
	$data['monthly_income']		= number_format($data['monthly_income'],2,',','.');
	$data['additional_income']	= number_format($data['additional_income'],2,',','.');
	$data['plafond_amount']		= number_format($data['plafond_amount'],2,',','.');
	$data['survey_date'] 		= date("d-m-Y H:i", strtotime($data['survey_date']));
	
	echo json_encode($data);
?>
