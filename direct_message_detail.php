<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$fullname = $_SESSION['fullname'];

	$senderID = sanitize_int($_REQUEST["id"]);
	$query = "SELECT customer_name, andalan_customer_id FROM customers where id_customer = '$senderID'";
	$result = mysqli_query($mysql_connection, $query);
	$data = mysqli_fetch_assoc($result);
	$name = $data[customer_name] . ' - ' . $data[andalan_customer_id];	
?>
<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Konsumen</li>
			  <li>Direct Message</li>
			  <li>Direct Message Detail</li>
            </ol>
          </section>
		  <br>

          <!-- Main content -->
          <section class="content">

			<div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user"></i> <?php echo $name ?></h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>
				<br>
				<div class="box-body table-responsive no-padding">
					<span id="dataSpan"></span>
				</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<a class="btn btn-primary btn-flat" style="width: 150px" href="add_direct_message.php?id=<?php echo $senderID; ?>" role="button"><i class="fa fa-reply"></i> Kirim Balasan</a>
					</div>
				</div><!-- /.box-body -->

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->

        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

      <?php include "inc-footer.php"; ?>
	</div><!-- ./wrapper -->
	
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
		<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div>
			<h4 id="modal1Title">Tambah User Level</h4>
			<p id="modal1Desc"><span id="addNotification"></span>

			<div class="col-md-12">
				<div class="form-group">
					<label for="title">Subject</label>
					<input type="text" name="name" placeholder="Nama Level" class="form-control" id="name">
				</div>
			</div>

			</p>
		</div>
		<br>
		<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
		<button data-remodal-action="confirm" class="remodal-confirm">OK</button>
	</div>

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>
	
	<script>
        window.REMODAL_GLOBALS = {
            NAMESPACE: 'remodal',
            DEFAULTS: {
                hashTracking: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                closeOnEscape: true,
                closeOnOutsideClick: false,
                modifier: ''
            }
        };
    </script>
    <script src="plugins/remodal/remodal.js"></script>
	<script src="js/inc-function.js"></script>

	<script>

		$body = $("body");

		function searchData(searchPage) {

			$body.addClass("loadingClass");

			var formData = {
								'customer_id'			: "<?php echo $senderID ?>"
						   };

            $.ajax({
                type: "POST",
                url: 'ajax_search_dm_detail.php',
                data: formData
            }).done(function(data) {
                $('#dataSpan').html(data);
                $body.removeClass("loadingClass");
            });
        }

		searchData(1);
		
		var modalAdd = $('[data-remodal-id=modal]').remodal( );
		$(document).on('confirmation', '.remodal', function() {

            var formData = {
                'user_level_id'	: $('#user_level_id').val(),
				'name'			: $('input[name=name]').val()
            };

			$body.addClass("loadingClass");

			if ($("#user_level_id").val() == '0') {
				$.ajax({
					type: "POST",
					url: 'ajax_add_user_level.php',
					data: formData
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data user level telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					} else if (data == 'name_exist') {
						$('#addNotification').html(showNotification("error", "Nama level sudah digunakan"));
					} 

				});
			} else {
				$.ajax({
					type: "POST",
					url: 'ajax_edit_user_level.php',
					data: formData
				}).done(function(data) {

					data = $.trim(data);
					$body.removeClass("loadingClass");

					if (data == 'success') {
						$("#mainNotification").html(showNotification("success", "Data user level telah disimpan"));
						modalAdd.close();
						searchData(1);
						$('#dataSpan').html(data);
						resetdata();
						setTimeout(clearnotif, 5000);

					} else if (data == 'empty') {
						$('#addNotification').html(showNotification("error", "Data yang Anda masukkan tidak lengkap"));
					} else if (data == 'name_exist') {
						$('#addNotification').html(showNotification("error", "Nama level sudah digunakan"));
					} 

				});
			}
			
        });

		function resetdata() {
            $("#user_level_id").val(0);
            $("#name").val("");
            $("#modal1Title").html('Tambah User Level');
            $("#addNotification").html("");
        }

        function clearnotif() {
            $("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
        }

		$(document).on('closing', '.remodal', function(e) {
            resetdata();
        });
		
		function saveToExcel() {
			
			var keyword  = $('input[name=keyword]').val();
			var src_type = $('select[name=src_type]').val();
			var src_status = $('select[name=src_status]').val();
			window.open("excel_customer.php?keyword="+keyword+"&src_type="+src_type+"&src_status="+src_status);
		}
		
	</script>

  </body>
</html>
