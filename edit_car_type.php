<?php
	include "check-admin-session.php";
	include "inc-header-admin.php";

	$id_car_type = sanitize_int($_REQUEST["id"]);

	$query 	= "select * from car_types where id_car_type='$id_car_type'";
	$result = mysqli_query($mysql_connection, $query);
	$data 	= mysqli_fetch_array($result);
?>

<body class="skin-blue layout-top-nav">
    <div class="wrapper">

	<?php include "inc-top-nav.php"; ?>

      <!-- Full Width Column -->
      <div class="content-wrapper custom-bg">
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <ol class="breadcrumb">
              <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			  <li>Car Prices</li>
			  <li><a href="car_type">Tipe Mobil</a></li>
              <li>Tambah Tipe Mobil</a></li>
            </ol>
          </section>
		  <br>
          <!-- Main content -->
          <section class="content">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Edit Tipe Mobil</h3>
              </div>
              <div class="box-body">

				<span id="mainNotification"></span>

				<input type="hidden" name="id_car_type" id="id_car_type" value="<?php echo $id_car_type; ?>" />

				<div class="row form-group">
					<div class="col-md-4">
						<label>Merk Mobil</label>
						<select name="id_car_brand" class="form-control" id="id_car_brand">
							<?php 
								$queryBrand = "select * from car_brands order by name asc";
								$resultBrand= mysqli_query($mysql_connection, $queryBrand);
								while($dataBrand = mysqli_fetch_array($resultBrand)) {
									echo '<option value="'.$dataBrand['id_car_brand'].'" ';
									if($dataBrand['id_car_brand']==$data['id_car_brand']) echo ' selected ';
									echo '>'.$dataBrand['name'].'</option>';
								}
							?>
						</select>
					</div>
					<div class="col-md-4">
						<label>Nama</label>
						<input type="text" name="typeName" id="typeName" class="form-control" placeholder="Nama Tipe Mobil" value="<?php echo $data['name']; ?>"/>
					</div>
					<div class="col-md-4">
						<label for="is_active">Status</label>
						<select name="is_active" class="form-control" id="is_active">
							<option value="1" <?php if($data['is_active']=='1') echo 'selected'; ?> >Aktif</option>
							<option value="0" <?php if($data['is_active']=='0') echo 'selected'; ?> >Tidak Aktif</option>
						</select>
					</div>
					<div class="col-md-12">
						<hr style="border-color:#bbbbbb;">
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-md-12">
						<button type="button" class="btn btn-primary  btn-flat " onClick="addImage();"><i class="fa fa-plus"></i> Tambah Gambar</button>
					</div>
				</div>
				
				<span id="spanAnswerTable"></span>

				<div class="row form-group">
					<div class="col-md-12 text-center">
						<button type="button" id="btnSave" style="width:100px" class="btn btn-primary btn-large" onclick="save();">Simpan</button>
						<button type="button" style="width:100px" class="btn btn-warning btn-large" onclick="cancel();">Batal</button>
					</div>
				</div>

				<br><br><br>

              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->
        </div><!-- /.container -->
      </div><!-- /.content-wrapper -->

    </div><!-- ./wrapper -->

	<?php include "custom_loading.php"; ?>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
    <script src="js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

    <script src="js/inc-function.js"></script>

    <script type="text/javascript">

		var rowNumber = 0;
		var rowCount  = 0;
		
		function addImage(parIdMedia=0, parImage='', parMediaType='NEW') {
			
			rowNumber++;
			rowCount++;
			
			var spanImageRowName 		= 'spanImageRow_'+rowNumber;
			var imageViewName 			= 'imageView_'+rowNumber;
			var answerImageName 		= 'answerImage[]';
			var mediaIDName 			= 'media_id[]';
			var mediaTypeName 			= 'media_type[]';
			
			if(parImage != '') parImage = 'user_files/media_image/'+parImage;
			else parImage = '#';
			
			var input = "<span id='"+ spanImageRowName +"'>"+
						"<input type='hidden' name='"+ mediaIDName +"' id='"+ mediaIDName +"' value='"+ parIdMedia +"' />"+
						"<input type='hidden' name='"+ mediaTypeName +"' id='"+ mediaTypeName +"' value='"+ parMediaType +"' />"+
						"<div class='row form-group'> "+
						"<div class='col-md-4'>"+
						"<table width='100%' class='table'>"+
							"<tr>"+
								"<td width='150px' align='center'>"+
									"<img id='"+ imageViewName +"' src='"+parImage+"' style='width: 90px;' alt='Gambar'/>"+
								"</td>"+
								"<td>"+
									"<input type='file' name='"+ answerImageName +"' id='"+ answerImageName +"' accept='image/*' onchange='readURL(this, \"#"+ imageViewName +"\")'/>"+
								"</td>"+
							"</tr>"+
						"</table>"+
						"</div>"+
						"<div class='col-md-1'>"+
						"<button type='button' class='btn btn-danger btn-large' onclick='removeImage("+ rowNumber +", "+ parIdMedia +");'> X </button>"+
						"</div>"+
						"</div>"+
						"</span>";
						
			$('#spanAnswerTable').append(input);
		}
		
		function removeImage(rowNum, idMedia=0) {
			
			if (confirm('Hapus gambar?')) {
				
				if(idMedia != 0) {					

					$body.addClass("loadingClass");

					$.ajax({
						type: "POST",
						url: 'ajax_delete_media.php',
						data: 'idMedia=' + idMedia + '&rel=car_type_media'
					}).done(function(data) {

						data = $.trim(data);
						$body.removeClass("loadingClass");

						if(data == "success") {
							$('#spanImageRow_'+rowNum).html('');
							rowCount--;
						} 
					});
				}
				else {
					
					$('#spanImageRow_'+rowNum).html('');
					rowCount--;
				}				
			}
		}
		
		function readURL(input, view) { 
			
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $(view).attr('src', e.target.result); 
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}
	
    	$body = $("body");

    	function save() {

    		$body.addClass("loadingClass");

    		var form_data = new FormData();
			form_data.append('id_car_type', $('input[name=id_car_type]').val());
          	form_data.append('id_car_brand', $('select[name=id_car_brand]').val());
          	form_data.append('typeName', $('input[name=typeName]').val());
			form_data.append('is_active', $('select[name=is_active]').val());
			form_data.append('image_row_count', rowCount);
			form_data.append('_ref', '<?php echo md5('car_type#'.date('d-m-Y')); ?>');
			
			var n = 1			
			$('input[name^="answerImage"]').each(function() {				
				var answerImageName = 'answerImage_'+n;
				form_data.append(answerImageName, $(this).prop('files')[0]);
				n++;
			});
			
			n = 1			
			$('input[name^="media_id"]').each(function() {				
				var mediaIDName = 'media_id_'+n;
				form_data.append(mediaIDName, $(this).val());
				n++;
			});
			
			n = 1			
			$('input[name^="media_type"]').each(function() {				
				var mediaTypeName = 'media_type_'+n;
				form_data.append(mediaTypeName, $(this).val());
				n++;
			});

			$.ajax({
				type: "POST",
				url: 'ajax_edit_car_type.php',
				processData: false,
				contentType: false,
				data: form_data
			}).done(function(data) {

				data = $.trim(data); 
				$body.removeClass("loadingClass");

				if (data == 'success') {
					$("#mainNotification").html(showNotification("success", "Data Sudah Disimpan"));
					$("#btnSave").attr("disabled", true);
					setTimeout(cancel, 2000);

				} else if (data == 'empty') {
					$('#mainNotification').html(showNotification("error", "Input Tidak Lengkap"));
				} else if (data == 'car_type_exist') {
					$('#mainNotification').html(showNotification("error", "Nama tipe mobil sudah ada"));
				} else if (data == 'incorrect_file_type') {
					$('#mainNotification').html(showNotification("error", "File gambar artikel harus berupa file jpg atau png atau gif"));
				}

				$('html, body').animate({scrollTop: '0px'}, 0);
			});

    	}

    	function cancel() {
    		window.location = 'car_type';
    	}

    	function clearnotif() {
			$("#mainAlert").fadeTo(2000, 500).fadeOut(500, function(){
				$("#mainAlert").alert('close');
			});
		}
		
		<?php 
			// ambil daftar gambar 
			$query 	= "select b.*
					   from car_type_media a 
					   left join media b on a.id_media=b.id_media 
					   where id_car_type='".$id_car_type."'";
			$result = mysqli_query($mysql_connection, $query);
			
			while($dataImage = mysqli_fetch_array($result)) {
				
				echo " addImage('".$dataImage['id_media']."', '".$dataImage['url']."', 'OLD'); ";
			}
		?>

	</script>

  </body>
</html>
