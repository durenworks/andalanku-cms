<?php
	include "check-admin-session.php";

	$administrator_id				= sanitize_sql_string($_REQUEST["administrator_id"]);
	$administrator_name 			= sanitize_sql_string($_REQUEST["administrator_name"]);
	$administrator_phone_number		= sanitize_sql_string($_REQUEST["administrator_phone_number"]);
	$administrator_username			= sanitize_sql_string($_REQUEST["administrator_username"]);
	$administrator_password			= sanitize_sql_string($_REQUEST['administrator_password']);
	$administrator_passwordConfirm	= sanitize_sql_string($_REQUEST['administrator_passwordConfirm']);
	$administrator_level			= sanitize_int($_REQUEST["administrator_level"]);
	$administrator_status			= sanitize_sql_string($_REQUEST["administrator_status"]);

	if ($administrator_id <> 0 && $administrator_name <> '' &&  $username <> '' && $administrator_level <> '0' && $administrator_status <> '') {
		
		if($administrator_level == '1' && $administrator_phone_number == '') {
			echo "superadmin_phone_empty";
			exit;
		}	
		
		if(($administrator_password<>"") && ($administrator_password <> $administrator_passwordConfirm)) {
			echo "password_not_match";
			exit;
		}
		
		if($administrator_password<>"") {
			
			// Validate password strength
			$uppercase = preg_match('@[A-Z]@', $administrator_password);
			$lowercase = preg_match('@[a-z]@', $administrator_password);
			$number    = preg_match('@[0-9]@', $administrator_password);
			$specialChars = preg_match('@[^\w]@', $administrator_password);

			if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($administrator_password) < 8) {
				echo "invalid_password";
				exit;
			}
		}
		
		$queryCheck		= "SELECT user_id from users WHERE username='$administrator_username' and user_id<>'$administrator_id'";
		$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck ) > 0) {
			echo "username_exist";
			exit;
		}
	
        $query = "UPDATE users SET fullname='$administrator_name', 
				  username='$administrator_username', level='$administrator_level', 
				  status='$administrator_status', phone_number='$administrator_phone_number' ";
		if($administrator_password <> '') {
			$administrator_password = md5($administrator_password);
			$query = $query.",password='$administrator_password' ";	
		} 
		$query = $query." where user_id='$administrator_id'";
        mysqli_query($mysql_connection, $query);
		
        echo 'success';
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>