<?php

	$_ref = $_REQUEST['_ref'];
	if($_ref == md5('tutorial#'.date('d-m-Y'))) {
		$currentURL = 'tutorial';
	}

	include "check-admin-session.php";
	include "plugins/image-resize/lib/ImageResize.php";
	
	function uploadImage($file) {

		$response = "";

		$path_parts = pathinfo($file["name"]);
		$extension = strtolower($path_parts['extension']);

		if($extension<>'jpg' && $extension<>'jpeg' && $extension<>'png' && $extension<>'gif') {

			$response = "";

		} else {

			$folder = "user_files/tutorial_image";

			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;

			while(file_exists($folder . '/' . $newnamefile)) {
				$stringrand = md5(microtime());
				$random = substr($stringrand, 0, 16);
				$newnamefile = 'andalanku_' . $random . '.'.$extension;
			}

			if (@move_uploaded_file($file['tmp_name'], $folder . '/' . $newnamefile)) {

				$response = $newnamefile;

				//resize image
				$image = new \Eventviva\ImageResize($folder . '/' . $newnamefile);
				$image->resizeToBestFit(800, 600);
				$image->save($folder . '/' . $newnamefile);

			} else {
				$response = "";
			}
		}

		return $response;
	}

	$id_tutorial	= sanitize_int($_REQUEST["id_tutorial"]);
	$question 		= sanitize_sql_string($_REQUEST["question"]);
	$is_active		= sanitize_sql_string($_REQUEST["is_active"]);
	$anser_row_count= sanitize_int($_REQUEST["anser_row_count"]);

	if ($id_tutorial <> '0' && $question <> '' &&  $is_active <> '' ) {
		
		$queryCheck		= "SELECT id_tutorial from tutorials WHERE question='$question' and id_tutorial<>'$id_tutorial'";
		$resultCheck 	= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck ) > 0) {
			echo "question_exist";
			exit;
		}
	
        $query = "UPDATE tutorials SET question='$question', is_active='$is_active'
				  where id_tutorial='$id_tutorial'";
        mysqli_query($mysql_connection, $query);
		
		for($i=0; $i<$anser_row_count; $i++) {
			
			$n = $i+1;
			$answerImageName 		= 'answerImage_'.$n;
			$answerDescriptionName 	= 'answerDescription_'.$n;
			$answerStatusName 		= 'is_active_answer_'.$n;
			$answerIDName 			= 'answer_id_'.$n;
			$answerTypeName 		= 'answer_type_'.$n;
			
			$imageFileName = "";
			if (!empty($_FILES[$answerImageName])) {
				$imageFileName = uploadImage($_FILES[$answerImageName]);
			} else {
				$imageFileName = "";
			}
			
			$status				= sanitize_sql_string($_REQUEST[$answerStatusName]);
			$answerDescription	= sanitize_sql_string($_REQUEST[$answerDescriptionName]);
			$answerDescription	= str_replace("\r\n","<br />",$answerDescription );
			$id_answer			= sanitize_int($_REQUEST[$answerIDName]);
			$answerType			= sanitize_sql_string($_REQUEST[$answerTypeName]);
			
			if($answerType=='NEW') {
				$query = "	INSERT INTO tutorial_answers(id_tutorial, image, description, is_active)
							VALUES ('$id_tutorial', '$imageFileName', '$answerDescription', '$status')";
			}
			else {
				$query = "	UPDATE tutorial_answers SET description='$answerDescription', is_active='$status' ";
				if($imageFileName <> '') $query = $query." , image='$imageFileName' ";
				$query = $query." where id_answer='$id_answer' ";
			}
						
			mysqli_query($mysql_connection, $query);
		}
		
		
        echo 'success';
		exit;    
	} else {
		echo "empty";
		exit;
	}
?>