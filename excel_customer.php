<?php
	include "check-admin-session.php";
	
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Data_Konsumen_Andalanku.xls");
	
	echo "Data Konsumen Andalanku<br><br>";

	$keyword	= sanitize_sql_string($_REQUEST["keyword"]);
	$src_type	= sanitize_sql_string($_REQUEST["src_type"]);
			
	$query = "select * 
			  from customers
			  left join agreement_list al on id_customer = customer_id
			  where (customer_name like '%$keyword%' or phone_number like '%$keyword%' or email like '%$keyword%' or al.agreement_number like'%$keyword%') ";
	if($src_type<>'') $query = $query." and customer_type='$src_type' ";
	$query = $query." order by customer_name ASC "; 
	$result = mysqli_query($mysql_connection, $query);

	echo "<table class='table table-hover'>
			  <tr>
				  <th width='2%'>No</th>
				  <th width='20%'>Nama</th>
				  <th width='15%'>No Handphone</th>
				  <th width='15%'>Email</th>				  
				  <th width='15%'>No Handphone Lainnya</th>
				  <th width='15%'>Jenis Kelamin</th>	
				  <th width='15%'>Tempat dan Tanggal Lahir</th>
				  <th width='15%'>Nomor KTP</th>
				  <th width='15%'>Alamat</th>
				  <th width='15%'>Nama Ibu Kandung</th>
				  <th width='15%'>No NPWP</th>
				  <th width='15%'>Nomor Induk Karyawan</th>
				  <th width='15%'>Nomor Kontrak</th>
				</tr>	";

	$i = 1;

	while ($data = mysqli_fetch_array($result)) {
		
		$userID = $data['id_customer'];
		
		$queryAddress  	= "select b.address, b.rt, b.rw, b.zip_code, 
							f.name as village_name, e.name as district_name, 
							d.name as regency_name, c.name as province_name 							 
							from address_customers a 
							left join address b on a.address_id=b.id
							left join provinces c on b.province_id=c.id
							left join regencies d on b.regency_id=d.id
							left join districts e on b.district_id=e.id
							left join villages f on b.village_id=f.id
							where a.user_id='$userID'
							and a.address_type='DOMICILE' 
							and a.is_active='1' ";
		$resultAddress 	= mysqli_query($mysql_connection, $queryAddress);
		$dataAddress 	= mysqli_fetch_array($resultAddress);
		
		if(mysqli_num_rows($resultAddress) > 0) {
			
			$address = $dataAddress['address']."\r\n".
					   'RT '.$dataAddress['rt'].' RW '.$dataAddress['rw']."\r\n".
					   $dataAddress['village_name'].', '.$dataAddress['district_name']."\r\n".
					   $dataAddress['regency_name'].', '.$dataAddress['province_name']."\r\n".
					   $dataAddress['zip_code'];
		} 
		else $address = '';

		//----- Ambil Nomor Kontrak ------//
		$agreement_number = '';
		$branch_stack = '';
		$query_agrrement = "select agreement_number, branch from agreement_list where customer_id = '$userID'";
		//var_dump($query_agrrement); die();
		$result_agreement = mysqli_query($mysql_connection, $query_agrrement);
		//var_dump($result_agreement);die();
		//$agreement_number = $query_agrrement;
		
		while ($data_agreement = mysqli_fetch_assoc($result_agreement)) {
			// /var_dump("fbar");die();
			if ($data_agreement[branch] == '') {
				
				$api_url	= $afis_api_url.'/Agreement/ActiveAgrementNumber/'.$data[andalan_customer_id];
				$ch 		= curl_init();

				curl_setopt($ch, CURLOPT_URL, $api_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

				$result_afis = curl_exec($ch);
				$result_afis = json_decode($result_afis,true);

				$responseArray = $result_afis['Response']['Data'];

				foreach($responseArray as $key=>$value) {
					
					$agreementNo = trim($value['agreementNo']);

					if ($data_agreement[agreement_number] == $agreementNo) {
						$branch = trim($value['branchfullname']);
						
						$query = "update agreement_list set branch='$branch ' where agreement_number='$data_agreement[agreement_number]'";
						mysqli_query($mysql_connection, $query);
					}
				}
			}
			else {
				$branch = $data_agreement[branch];
			}

			if($agreement_number != '') {
				$agreement_number = $agreement_number . ', ' . $data_agreement[agreement_number];
				$branch_stack = $branch_stack . ', ' . $branch;
			}
			else {
				$agreement_number = $data_agreement[agreement_number];
				$branch_stack = $branch;
			}
		}

		echo '<tr>
				  <td>'.$i.'</th>
				  <td>' . $data['customer_name'] . '</td>
				  <td>\'' . $data['phone_number'] . '</td>
				  <td>' . $data['email'] . '</td>
				  <td>\'' . $data['phone_number'] . '</td>
				  <td>' . $data['gender'] . '</td>
				  <td>' . $data['place_of_birth'] . ', ' . $data['date_of_birth'] . '</td>
				  <td>\'' . $data['id_number'] . '</td>
				  <td>' . $address . '</td>
				  <td>' . $data['mother_name'] . '</td>
				  <td>\'' . $data['npwp'] . '</td>
				  <td>\'' . $data['nip'] . '</td>
				  <td>\'' . $agreement_number .'</td>
			  </tr>';
		$i++;
	}

	echo "</table>";
?>